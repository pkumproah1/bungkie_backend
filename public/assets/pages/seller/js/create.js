var create = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content"),
        submit = false;
    return {
        // validate
        formValidate: function() {
            $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });
            $(".form-action").validate({
                ignore: ".ql-container *",
                rules: {
                    "conf-password": {
                        equalTo: ".password"
                    }
                },
                submitHandler: function(form) {
                    if (form) {
                        _.forEach($(form).serializeArray(), function(v) {
                            if (
                                v.name == "bank" ||
                                v.name === "warehouse_province" ||
                                v.name === "warehouse_city" ||
                                v.name === "warehouse_district" ||
                                v.name === "billing_province" ||
                                v.name === "billing_city" ||
                                v.name === "billing_district" ||
                                v.name === "payment_cycle"
                            ) {
                                $("#fill-" + v.name).text(
                                    $(
                                        "#" +
                                            v.name +
                                            "_chosen .chosen-single span"
                                    ).text()
                                );
                            } else {
                                $("#fill-" + v.name).text(v.value);
                            }
                        });
                        submit = true;
                        $("#main, #verification").toggle("fade");
                        $("#text-title").text("Verification Data");
                        $("html, body").animate({ scrollTop: 0 }, 600);
                    }
                }
            });
        },
        // on submit
        handleSubmit: function() {
            $("body").loading({
                logo: true
            });
            var formArray = $(".form-action").serializeArray(),
                pathUrl = window.location.href,
                matches = pathUrl.match(/create\/(.*)/);
            if (matches) {
                formArray.push({ name: "seller_id", value: matches[1] });
            }

            $.ajax({
                type: "POST",
                url: baseurl + "/seller/inserted",
                data: formArray,
                dataType: "json"
            }).done(function(response) {
                $("body").loading("stop");
                if (response.status === false) {
                    Swal.fire("Warning!", response.message, "warning");
                    return;
                }

                Swal.fire("Success!", "Inserted", "success").then(result => {
                    if (result) {
                        window.reload();
                    }
                });
            });
        },
        // Check Dupkicate Username
        checkDuplicate: function() {
            $(document).on("blur", "input[name^=username]", function(e) {
                var val = $(this)
                    .val()
                    .trim();
                if (val !== "") {
                    $("body").loading({
                        logo: "true"
                    });
                    $.ajax({
                        type: "POST",
                        url: baseurl + "/seller/check_username",
                        data: {
                            username: val,
                            _token: $('meta[name="csrf-token"]').attr("content")
                        },
                        dataType: "json"
                    }).done(function(response) {
                        $("body").loading("stop");
                        if (response.status === false) {
                            $("input[name^=username]").val("");
                            $("input[name^=username]").focus();
                            Swal.fire(
                                "Warning!",
                                "Username Duplicate",
                                "warning"
                            );
                            return;
                        }
                    });
                }
            });
        },
        // location
        location: function(el) {
            $(".chosen").chosen({
                search_contains: true,
                no_results_text: "Oops, nothing found!"
            });
            var provinceEl = $("#" + el + "-province");
            var amphurEl = $("#" + el + "-city");
            var districtEl = $("#" + el + "-district");
            var idpostEl = $("#" + el + "-zipcode");
            provinceEl.chosen().change(function() {
                if (provinceEl.val() === "") {
                    amphurEl.html('<option value="">Select</option>');
                    amphurEl.trigger("chosen:updated");
                    districtEl.html('<option value="">Select</option>');
                    districtEl.trigger("chosen:updated");
                    idpostEl.val("");
                } else {
                    idpostEl.val("");
                    var options = "";
                    for (
                        var i = 0;
                        i < province[provinceEl.val()].child.length;
                        i++
                    ) {
                        options +=
                            '<option value="' +
                            province[provinceEl.val()].child[i].amphur_id +
                            '">' +
                            province[provinceEl.val()].child[i].amphur_name +
                            "</option>";
                    }
                    if (options != "") {
                        amphurEl.html(
                            '<option value="">Select</option> ' + options
                        );
                        amphurEl.trigger("chosen:updated");
                    }
                    districtEl.html('<option value="">Select</option>');
                    districtEl.trigger("chosen:updated");
                }
            });

            amphurEl.chosen().change(function() {
                idpostEl.val("");
                var options = "";
                for (var i = 0; i < amphur[amphurEl.val()].child.length; i++) {
                    options +=
                        '<option value="' +
                        amphur[amphurEl.val()].child[i].district_id +
                        '">' +
                        amphur[amphurEl.val()].child[i].district_name +
                        "</option>";
                }
                if (options != "") {
                    districtEl.html(
                        '<option value="">Select</option>' + options
                    );
                    districtEl.trigger("chosen:updated");
                }
            });

            districtEl.chosen().change(function() {
                if (zip_code[districtEl.val()].zip_code != "") {
                    idpostEl.val(zip_code[districtEl.val()].zip_code);
                }
            });
        },
        // autoload
        init: function() {
            $(document).on("click", "button", function(e) {
                if ($(this).data("type") === "back") {
                    $("#main, #verification").toggle("fade");
                    $("#text-title").text("Seller Registering Account");
                    $("html, body").animate({ scrollTop: 0 }, 600);
                } else if ($(this).data("type") === "save") {
                    create.handleSubmit();
                }
            });

            this.location("warehouse");
            this.location("billing");

            this.formValidate();
            this.checkDuplicate();
            // Autoload
            $(".chosen-select").chosen({
                search_contains: true,
                no_results_text: "Oops, nothing found!"
            });

            $(".user").inputmask({ regex: "[0-9a-zA-Z-_ ]*" });
            $(".numeric").inputmask({ regex: "[0-9]*" });
        }
    };
})();

create.init();
