var product = function() {
    var baseurl = $('meta[name^=baseUrl]').attr('content'),
        table;
    return {
        deleted: function() {
            $(document).on('click', '.deleted', function() {
                var id = $(this).data('id');
                $.confirm({
                    title: 'Confirm!',
                    content: 'Remove',
                    buttons: {
                        confirm: function () {
                            $.ajax({
                                type: "POST",
                                url: baseurl + '/product/deleted',
                                data: { 'product_id': id, '_token':  $('meta[name="csrf-token"]').attr('content') },
                                dataType: 'json'
                            }).done(function(response) {
                                 Swal.fire(
                                    'Success!',
                                    'remove',
                                    'success'
                                ).then((result) => {
                                    if (result) {
                                        window.location.reload();
                                    }
                                })
                            });
                        },
                        cancel: function () {
                            
                        }
                    }
                });
            });
        },
        init: function() {
            table = $('#main_tb').dataTable();
            this.deleted();
            
        }
    }
}();

product.init();