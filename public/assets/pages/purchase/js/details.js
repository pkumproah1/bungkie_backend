var details = function() {
    var baseurl = $('meta[name^=baseUrl]').attr('content')
    return {
        modalShow: function() {
            $(document).on('click', '.modal-active', function() {
                var purchase = $(this).data('json');
                $('#updatedStatus').modal('show')
                $('#modalStatus').html("");
                $('#updatedStatus').on('shown.bs.modal', function () {
                    $("#updatedStatus").loading();
                    $('#updateStatusModal').text(purchase.product_name);
                    $.ajax({
                        type: 'POST',
                        url: baseurl + '/purchase/get_status_by_id',
                        data: {
                            'purchase_id': purchase.purchase_id,
                            '_token':  $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json'
                    }).done(function(response) {
                        $("#updatedStatus").loading('stop');
                        if (response.status === true) {
                            var options = '';
                            _.forEach(response.data.status, function(value) {
                                var disable = (value.disabled === true) ? 'disabled' : '';
                                options+= '<option value="' + value.id + '"'+ disable +'>'+ value.name +'</option>'
                            });

                            $('input[name^=purchase_id]').val(response.data.purchase_id);
                            $('input[name^=purchase_number]').val(response.data.purchase_number);
                            
                            $('#modalStatus').html(options).trigger("chosen:updated");
                            $("#modalStatus").chosen({
                                no_results_text: "Oops, nothing found!"
                            }); 
                        } 
                    });
                });

                

            });
        },
        updateStatus: function() {
            console.log('up');
            $(document).on('click', '.updateStatusSubmit', function(){
                var formArray = $('.formUpdateStatus').serializeArray();
                $("#updatedStatus").loading();
                $.ajax({
                    type: 'POST',
                    url: baseurl + '/purchase/update_status',
                    data: formArray,
                    dataType: 'json'
                }).done(function(response) {
                    details.trackingLogs('reset');
                });
            });

            
        },
        trackingLogs: function(option) {
            if (option == 'reset') {
                $('table tbody').html("");
                $("#updatedStatus").loading('stop');
                $("#updatedStatus").modal('hide');
                $('.modal-backdrop').remove();
            }
            $('.ontable').each(function(key, el) {
                var id = $(el).data('id');
                $.ajax({
                    type: 'POST',
                    url: baseurl + '/purchase/get_tracking',
                    data: {
                        'purchase_id': id,
                        '_token':  $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json'
                }).done(function(response) {
                    var html = '';
                    if (response.status === true && response.data) {
                        var key = 1,
                            notRemove = [];
                            // console.log(response.data);
                        _.forEach(response.data, function(value) {
                            html+= '<tr><td>'+ key +'</td>';
                            html+= '<td>'+ value.status_name +'</td>';
                            html+= '<td>'+ value.status_desc +'</td>';
                            html+= '<td>'+ value.tracking_number +'</td>';
                            html+= '<td>'+ value.created_at +'</td>';
                            
                            if (value.purchase_status !== 1) {
                                html+= '<td id="action"><a href="#" class="text-danger" data-id="'+ value.id +'">Remove</a></td></tr>';
                            } else {
                                html+= '<td></td>';
                            }
                            if (value.purchase_status === 8) {
                                notRemove.push({
                                    id: value.purchase_id
                                });
                            }

                            if (value.notice) {
                                html+= '<tr><td colspan="6"><span class="text-danger">Notice: </span>'+ value.notice +'</td></td></tr>';
                            }

                            key++;
                        });
                        $('table[data-id^=' + id + '] tbody').append(html);

                        if (notRemove.length > 0) {
                            var el = $($('table[data-id^=' + notRemove[0].id + '] tbody'));
                            el.each( function(k, el) {
                                $(el).find('td#action').html("")
                                console.log(k,el);
                            })
                            
                        }  
                        
                        var lastStatus = $($($('table[data-id^=' + id + '] tbody tr')[0]).find('td')[1]).text();
                        $('table[data-id^=' + id + ']').closest('#product-detail').find('.lastStatus').text(lastStatus);
                    }
                });
            });
        },
        init: function() {
            console.log('detail ready');
            this.modalShow();
            this.trackingLogs();
            this.updateStatus();
            
        }
    }
}();
details.init();