moment.locale('th');
moment().format('MMMM Do YYYY, h:mm:ss a');
var Purchase = function() {
    return {
        daterange: function () {
		    $('.daterange-show').daterangepicker({
		    	'opens': 'left',
		        startDate: moment().subtract(29, 'days'),
		        endDate: moment(),
		        ranges: {
		           'Today': [moment(), moment()],
		           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		           'This Month': [moment().startOf('month'), moment().endOf('month')],
		           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		        }
		    }, function cb(start, end) {
		    	// $.extend(params, {
		    	// 	datetime_start: start.format('YYYY-MM-DD 00:00:00'),
				// 	datetime_end: end.format('YYYY-MM-DD 23:59:59')
		    	// });
		        // vm.table('#post-list', params);
		    });
		},
        init: function() {


            $('.daterange').val('');
            $('.daterange').attr("placeholder","Start - End");

            $("#table").dataTable();

        }

    }
}();

Purchase.init();