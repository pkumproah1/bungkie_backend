var pathUrl = window.location.href,
    matches = pathUrl.match(/create\/(.*)/);
var create = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content"),
        maxOption = 0,
        dataOption = [],
        editorFull,
        categorys = [],
        categoryItems = [1],
        category = {
            level1: null,
            level2: null,
            level3: null
        },
        images = [];
    return {
        // Option
        attributes: {
            html: function(params) {
                var html = '<div class="items" data-id="'+ params.id +'" id="item-'+ params.id +'">';
                html += '<div class="header"><span class="name">'+ params.name +"</span>";
                html += '<div class="tools"><ul>';
                html += '<li><a href="#" class="remove-option">Remove</a></li></ul></div></div>';
                html += '<div class="body"><div class="tag-option">';
                html += '<label for="options">Options</label>';
                html += '<input id="option_'+ params.id +'" type="text" class="form-control options"></div></div>';
                return html;
            },
            process: function() {
                if (dataOption.length < 2) {
                    var name = $("input[name^=optionName]");
                    console.log(maxOption);
                    $("#options").append(
                        create.attributes.html({
                            id: maxOption,
                            name: name.val()
                        })
                    );
                    // push data
                    dataOption.push({
                        id: maxOption,
                        name: name.val(),
                        option: []
                    });

                    // reset input
                    name.val("");
                    $("#option_" + maxOption).tagsinput("input");
                    // Event add items
                    $("#option_" + maxOption).on("itemAdded", function(event) {
                        var index = _.findIndex(dataOption, {"id": $(this).closest(".items").data("id")});
                        dataOption[index]["option"].push(event.item);
                        $("input[name^=attributes]").val(JSON.stringify(dataOption));
                    });

                    // Event remove items
                    $("#option_" + maxOption).on("itemRemoved", function(event) {
                        var oldData = _.find(dataOption, { id: $(this).closest(".items").data("id")}),
                            index = oldData.option.indexOf(event.item);

                        oldData.option.splice(index, 1);
                        $("input[name^=attributes]").val(JSON.stringify(dataOption));
                    });

                    $(".bootstrap-tagsinput input").focus();
                    maxOption++;
                }
            },
            onEnter: function() {
                $("input[name^=optionName]").keypress(function(event) {
                    if (event.keyCode == 13) {
                        event.preventDefault(); // Stop the default behaviour
                        create.attributes.process();
                    }
                });
            },
            oldData: function() {
                var attribute = $("input[name^=attributes]").val();
                if (attribute) {
                    var obj = JSON.parse(attribute);
                    maxOption = obj.length;
                    dataOption = obj;
                    var intNumber = ++maxOption;
                    $.each(obj, function(key, value) {
                        // Event add item
                        $("#option_" + intNumber).on("itemAdded", function(event) {
                            var index = _.findIndex(dataOption, { "id": $(this).closest(".items").data("id") });
                            dataOption[index]["option"].push(event.item);
                            $("input[name^=attributes]").val(JSON.stringify(dataOption));
                        });

                        // Event remove items
                        $("#option_" + intNumber).on("itemRemoved", function(event) {
                            var oldData = _.find(dataOption, { id: $(this).closest(".items").data("id") }),
                                index = oldData.option.indexOf(event.item);
                            oldData.option.splice(index, 1);
                            $("input[name^=attributes]").val(JSON.stringify(dataOption));
                        });

                        $("#option_" + intNumber).tagsinput("input");
                        intNumber++;
                    });
                }
            },
            main: function() {
                $(document).on("click", "#add-option", function() {
                    create.attributes.process();
                });

                create.attributes.oldData();
                create.attributes.remove();
                create.attributes.onEnter();
            },
            remove: function() {
                $(document).on("click", ".remove-option", function(e) {
                    e.preventDefault();
                    var id = $(this)
                        .closest(".items")
                        .data("id");
                    var index = _.findIndex(dataOption, ["id", id]);
                    dataOption.splice(index, 1);
                    $("input[name^=attributes]").val(
                        JSON.stringify(dataOption)
                    );
                    $("#item-" + id).remove();
                });
            }
        },
        // Editor
        froalaEditor: function() {
            //  for full
            editorFull = new FroalaEditor(".full-editor", {
                videoUpload: false,
                imageUploadURL: baseurl + "/upload/froala_upload",
                imageUploadParams: {
                    type: "image",
                    tag: "post",
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                imageUploadMethod: "POST",
                imageDefaultWidth: 0,
                imageAllowedTypes: ["jpeg", "jpg", "png", "gif"],
                imageMaxSize: 5 * 1024 * 1024,
                imageDefaultAlign: "left",
                heightMin: 400,
                events: {
                    blur: function() {
                        $(".form-action").validate().element(".full-editor");
                    }
                },
                toolbarButtons: [
                    "fullscreen","bold","italic","underline","strikeThrough","subscript","superscript","|","fontFamily","fontSize","color","inlineStyle","paragraphStyle","|","paragraphFormat","align","formatOL","formatUL","outdent","indent","quote","-",
                    "insertLink","insertImage","insertVideo","insertTable","|","emoticons","specialCharacters","insertHR","selectAll","clearFormatting","|","undo","redo"
                ],
                key: "OXC1lD3I3B15B7D9C6kzltG-7wtD-17E4cnoC-7J2A4D4B3C6D2F2F4G1G1=="
            });
            // for mini
            new FroalaEditor(".short-editor", {
                heightMin: 150,
                events: {
                    blur: function() {
                        $(".form-action")
                            .validate()
                            .element(".short-editor");
                    }
                },
                toolbarButtons: [
                    "fullscreen","bold","italic","underline","strikeThrough","subscript","superscript","|","fontFamily","fontSize","color",
                    "inlineStyle","paragraphStyle","|","paragraphFormat","align","formatOL","formatUL","outdent","indent","|","undo","redo"
                ],
                key: "OXC1lD3I3B15B7D9C6kzltG-7wtD-17E4cnoC-7J2A4D4B3C6D2F2F4G1G1=="
            });
        },
        // validate
        formValidate: function() {
            $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });
            $(".form-action").validate({
                ignore: ".ql-container *",
                errorClass: "is-invalid error",
                validClass: "is-valid",
                onkeyup: function(element) {
                    if ($(element).hasClass("fr-element")) {
                        $(element)
                            .closest(".col-sm-9")
                            .find("label")
                            .remove();
                    }
                },
                onblur: function(element) {
                    if ($(element).hasClass("fr-element")) {
                        $(element)
                            .closest(".col-sm-9")
                            .find("label")
                            .remove();
                    }
                },
                invalidHandler: function(form, validator) {
                    setTimeout(function() {
                        var errors = validator.numberOfInvalids();
                        if (errors) {
                            if ($(validator.errorList[0].element).hasClass("full-editor")) {
                                $("html,body").animate({
                                    scrollTop: $(".full-fr-element").offset().top - 110
                                }, "slow");
                            } else if ($(validator.errorList[0].element).hasClass("chosen-select")) {
                                $(".chosen-select").trigger("chosen:activate");
                            } else {
                                $(validator.errorList[0].element).focus();
                            }
                        }
                    }, 100);

                    setTimeout(function() {
                        $(".nav-tabs span.err-tabs").remove();
                        var validatePane = $(
                            ".language-pane .tab-pane:has(input.error, textarea.error)"
                        ).each(function(k, elm) {
                            var id = $(this).attr("id");
                            $(".nav-tabs")
                                .find('a[href^="#' + id + '"]')
                                .append(
                                    '<span class="text-danger err-tabs"> *</span>'
                                );
                        });
                    });
                },
                submitHandler: function(form) {
                    
                    $("button").prop("disabled", true);
                    $("body").loading({
                        logo: true
                    });
                    $(".nav-tabs span.err-tabs").remove();
                    var formArray = $(form).serializeArray();
                    if (matches) {
                        formArray.push({
                            name: "product_id",
                            value: matches[1]
                        });
                    }

                    if (categoryItems) {
                        formArray.push({
                            name: "categorys",
                            value: categoryItems
                        });
                        
                    }
                    $.ajax({
                        type: "POST",
                        url: baseurl + "/product/inserted",
                        data: formArray,
                        dataType: "json"
                    }).done(function(response) {
                        $('body').loading('stop');
                        if (response.status === false) {
                            Swal.fire("Warning!", response.message, "warning");
                            return;
                        }
                        var txt = (matches) ? 'Updated' : 'Inserted'
                        Swal.fire("Success!", txt, "success").then(
                            result => {
                                if (result) {
                                    window.location.href =
                                        baseurl + "/product/list";
                                }
                            }
                        );
                    });
                }
            });
        },
        chosen: function() {
            $(".chosen").chosen({
                search_contains: true,
                no_results_text: "Oops, nothing found!"
            });

            var category = $("#cat").data("select");
            if (category) {
                $(".chosen")
                    .val(category)
                    .trigger("chosen:updated");
            }
        },
        category: {  
            autoLoad: function () {
                $("#Category").loading();
                var oldData = $('#main').data('categorylist');
                $.ajax({
                    type: "GET",
                    url: baseurl + "/api/category",
                    dataType: "json",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ruaIEjWw1HGB8AADnOPRwiWznFjszCIalm8YLpBh');
                    },
                }).done(function(response) {
                    $('#Category').loading('stop');
                    if (response.status === false) {
                        Swal.fire("Warning!", response.message, "warning");
                        return;
                    }
                    // has data
                    if (response['data']) {
                        categorys.push(response['data']);
                        if (oldData !== null) {
                            category.level1 = (oldData[0] !== undefined) ? oldData[0] : null;
                            category.level2 = (oldData[1] !== undefined) ? oldData[1] : null;
                            category.level3 = (oldData[2] !== undefined) ? oldData[2] : null;
                            create.category.render(category.level1, category.level2, category.level3);
                        } else {
                            category.level1 = 1;
                            create.category.render(1);
                        }
                        create.category.selected();
                    }
                    
                    categoryItems = [];
                    _.map(category, function(item, index){
                        if (item !== null) {
                            categoryItems.push(item);
                        }
                    });
                });
            },
            selected: function(level, id) {
                $(document).on('click', '[data-category]', function(e) {
                    e.preventDefault(); 
                    var data = $(this).data();
                    $('[data-category='+ data.category +']').removeClass("active"); 
                    if (data.category == 1) {
                        $(this).addClass('active');
                        create.category.render(data.id, null, null);
                        category.level1 = data.id;
                        category.level2 = null;
                        category.level3 = null;
                    } else if(data.category == 2) {
                        create.category.render(category.level1, data.id, null);
                        category.level2 = data.id;
                        category.level3 = null;
                        $(this).addClass('active');
                    } else if(data.category == 3) {
                        create.category.render(category.level1, category.level2, data.id);
                        category.level3 = data.id;
                        $(this).addClass('active');
                    }
                    categoryItems = [];
                    if (category) {
                        _.map(category, function(item, index){
                            if (item !== null) {
                                categoryItems.push(item);
                            }
                        });
                    }
                })
            },
            render: function(main, subCategory, lastCategory) {
                var html = '';
                var sub = '';
                var last = '';

                _.map(categorys[0], function(item, index) {
                    var selected = (main === item.id) ? 'active' : '';
                    html+= '<li><img src="'+ item.image +'"><a href="#" class="'+ selected +'" data-category="1" data-id="'+ item.id +'">';
                    html+= item.name +'</a></li>';
                });

                if (main) {
                    var level2 = _.filter(categorys[0], { id: main })[0];
                    _.map(level2.parent, function(item, index) {
                        var selected = (subCategory === item.id) ? 'active' : '';
                        sub+= '<li><img src="'+ item.image +'"><a href="#" class="'+ selected +'" data-category="2" data-id="'+ item.id +'">';
                        sub+= item.name +'</a></li>';
                    });
                }

                if (subCategory) {
                    var level3 = _.filter(level2.parent, { id: subCategory })[0];
                    _.map(level3.parent, function(item, index) {
                        var selected = (lastCategory === item.id) ? 'active' : '';
                        last+= '<li><img src="'+ item.image +'"><a href="#" class="'+ selected +'" data-category="3" data-id="'+ item.id +'">';
                        last+= item.name +'</a></li>';
                    });
                }

                $('#main').html(html);
                $('#lv2').html(sub);
                $('#lv3').html(last);
                
                
                return html;
            }
        },
        discountType: function() {
            $(document).on('click', '[data-discount]', function(e) {
                e.preventDefault();
                $($(this).closest('.input-group-append').find('span')).text($(this).text());
                $('input[name="discount_type['+ $(this).data('lang') +']"]').val($(this).data('discount'));
            })
        },
        image: {
            sortable: function () {
                $(".sortable").sortable({
                    connectWith: ".both",
                    placeholder: "box-highlight",
                    start: function(event, ui) {
                        ui.item.toggleClass("in-highlight");
                    },
                    stop: function(event, ui) {
                        ui.item.toggleClass("in-highlight");
                    },
                    update: function(event, ui) {
                        create.image.update_data();
                    }
                });
            },
            update_data: function() {
                images = [];
                _.map($('.sortable li'), function(item, index) {
                    var itemHighlight = $(item).find('.highlight'),
                        rank = (index + 1);
                    $(itemHighlight).removeClass('active');
                    if (index === 0) {
                        $(itemHighlight).addClass('active');
                    }
                    $(item).attr('data-id', rank);
                    $($(item).find('.rank a')).attr('data-delete', rank);
                    images.push({
                        rank: rank,
                        name: $(item).data('image')
                    });
                });
                $('input[name=images]').val(JSON.stringify(images));
            },
            remove: function() {
                $(document).on('click', '[data-delete]', function(e) {
                    e.preventDefault();
                    var id = $(this).data('delete');
                    $.confirm({
                        title: 'Confirm!',
                        content: 'Remove',
                        buttons: {
                            confirm: function () {
                                if (images) {
                                    var index = _.findIndex(images, { rank: id });
                                    if (index != -1) {
                                        images.splice(index, 1);
                                        $('li[data-id='+ id +']').remove();
                                        create.image.update_data();
                                    }
                                }
                            },
                            cancel: function () {
                                
                            }
                        }
                    });
                });
            },
            uploadImage: function() {
                plugin.fileUpload({
                    el: '.product_upload',
                    success:function(response) {
                        if (response.status === true) {
                            var rank = ($('.sortable li').length + 1),
                                html = `<li data-id="${ rank }" data-image="${ response.name }">
                                <div class="image">
                                    <img src="${ response.url }">
                                </div>
                                <div class="highlight">รูปหลัก</div>
                                <div class="rank">
                                    <a href="#" data-delete="${ rank }">
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                </div>
                            </li>`;

                            $(".sortable").append(html);
                            create.image.update_data();
                        }
                    }
                });
            },
            init: function() {
                create.image.update_data();
                create.image.sortable();
                create.image.remove();
                create.image.uploadImage();
            }
        },
        
        // autoload
        init: function() {
            // Autoload
            this.attributes.main();
            this.froalaEditor();
            this.formValidate();
            this.chosen();
            this.category.autoLoad();
            this.discountType();
            this.image.init();
            $(".tags").tagsinput("input");
            
            $(".chosen-select")
                .chosen()
                .change(function() {
                    $(".form-action")
                        .validate()
                        .element(".chosen-select");
                });
            $(".numeric").inputmask({ regex: "[0-9]*" });
            $(".price").inputmask({ regex: "[0-9.]*" });
        }
    };
})();

create.init();
