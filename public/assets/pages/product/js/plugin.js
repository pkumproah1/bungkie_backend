var plugin = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content"),
        images = [];
    return {
        fileUpload: function(req) {
            $(req.el).fileupload({
                url: baseurl + "/product/upload",
                dataType: "json",
                formData: {
                    _token: $('meta[name="csrf-token"]').attr("content")
                },
                done: function(e, data) {
                    $("body").loading("stop");
                    req.success(data.result);
                },
                fail: function(e, data) {
                    $("body").loading("stop");
                    Swal.fire({
                        icon: "error",
                        title: "Oops...",
                        text: data.jqXHR.responseJSON.message
                    });
                },
                progressall: function(e, data) {
                    $("body").loading({
                        logo: true
                    });
                }
            });
        }
    }
})();