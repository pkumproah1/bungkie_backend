var product = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content"),
        table;
    return {
        deleted: function() {
            $(document).on("click", ".deleted", function(e) {
                e.preventDefault();
                var id = $(this).data("id");
                $.confirm({
                    title: "Confirm!",
                    content: "Remove",
                    buttons: {
                        confirm: function() {
                            $.ajax({
                                type: "POST",
                                url: baseurl + "/product/deleted",
                                data: {
                                    product_id: id,
                                    _token: $('meta[name="csrf-token"]').attr("content")
                                },
                                dataType: "json"
                            }).done(function(response) {
                                Swal.fire("Success!", "remove", "success").then(result => {
                                    if (result) {
                                        if ($('#product-tab a.active').attr('id') === "product-wait-approve-tab") {
                                            table.destroy();
                                            product.table('#tabled-wait-approve', 'wait-approve');
                                        } else {
                                            table.destroy();
                                            product.table('#tabled-approve', 'approve');
                                        }
                                    }
                                });
                            });
                        },
                        cancel: function() {}
                    }
                });
            });
        },
        tabs: function() {
            $(document).on('click', '#product-tab a', function(e) {
                if ($(this).attr('id') === "product-wait-approve-tab") {
                    table.destroy();
                    product.table('#tabled-wait-approve', 'wait-approve');
                } else {
                    table.destroy();
                    product.table('#tabled-approve', 'approve');
                }
            })
        },
        table: function(el, type) {
            table = $(el).DataTable( {
                "processing": true,
                "serverSide": true,
                "width": "100%",
                "ajax": {
                    "url": baseurl + "/product/paginate",
                    "data": { 
                        _token: $('meta[name="csrf-token"]').attr("content"),
                        type: type
                    },
                    "type": "POST"
                }
            });
        }, 
        init: function() {
            this.table('#tabled-approve', 'approve');
            this.tabs();
            this.deleted();
        }
    };
})();

product.init();
