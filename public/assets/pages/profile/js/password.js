var password = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content");
    return {
        formValidate: function() {
            $(".password-form").validate({
                rules: {
                    cpassword: {
                        equalTo: ".password"
                    }
                },
                errorClass: "is-invalid error",
                validClass: "is-valid",
                submitHandler: function(form) {
                    $("body").loading();
                    $("button").prop("disabled", true);
                    var formArray = $(form).serializeArray();
                    formArray.push({
                        name: "type",
                        value: "password"
                    });

                    $.ajax({
                        type: "POST",
                        url: baseurl + "/profile/update_profile",
                        data: formArray,
                        dataType: "json"
                    }).done(function(response) {
                        $("body").loading("stop");
                        $("button").prop("disabled", false);
                        if (response.status === false) {
                            Swal.fire("Warning!", response.message, "warning");
                            $("input[name=old_password], input[name=password], input[name=cpassword]").val("").removeClass("is-valid");
                            return;
                        }
                        Swal.fire("", "Save Setting", "success");
                        $("input[name=old_password],input[name=password], input[name=cpassword]").val("").removeClass("is-valid");
                    });
                }
            });
        },
        init: function() {
            this.formValidate();
        }
    };
})();

password.init();
