var bank = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content");
    return {
        formValidate: function() {
            $(".bank-form").validate({
                errorClass: "is-invalid error",
                validClass: "is-valid",
                submitHandler: function(form) {
                    $("body").loading();
                    $("button").prop("disabled", true);
                    var formArray = $(form).serializeArray();
                    formArray.push({
                        name: "type",
                        value: "bank"
                    });

                    $.ajax({
                        type: "POST",
                        url: baseurl + "/profile/update_profile",
                        data: formArray,
                        dataType: "json"
                    }).done(function(response) {
                        $("body").loading("stop");
                        $("button").prop("disabled", false);
                        if (response.status === false) {
                            Swal.fire("Warning!", response.message, "warning");
                            return;
                        }
                        Swal.fire("", "Save Setting", "success");
                    });
                }
            });
        },
        init: function() {
            this.formValidate();
            $(document).on("click", "#bank-tab", function() {
                console.log("asss");
                setTimeout(function() {
                    $("#banklist").chosen({
                        search_contains: true,
                        no_results_text: "Oops, nothing found!"
                    });
                }, 500);

                var category = $("#banklist").data("select");
                if (category) {
                    $("#banklist")
                        .val(category)
                        .trigger("chosen:updated");
                }
            });
        }
    };
})();

bank.init();
