var inventory = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content"),
        table;
    return {
        modal: {
            dropdown: function() {
                $(document).on("click", ".modal-action", function(e) {
                    e.preventDefault();
                    var val = $(this).data("value");
                    $("#addon").text(val);
                    $("input[name=action]").val(val);
                });
            },
            onload: function() {
                inventory.modal.dropdown();
            }
        },
        init: function() {
            $("#table").dataTable({
                searching: false
            });
            this.modal.onload();
            (pathUrl = window.location.href),
                (matches = pathUrl.match(/profile\#(.*)/));
            if (matches) {
                if (matches[1]) {
                    $('a[href="#' + matches[1] + '"]').trigger("click");
                }
            }
        }
    };
})();

inventory.init();
