var profile = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content");
    return {
        formValidate: function() {
            $(".profile-form").validate({
                errorClass: "is-invalid error",
                validClass: "is-valid",
                submitHandler: function(form) {
                    $("body").loading();
                    $("button").prop("disabled", true);
                    var formArray = $(form).serializeArray();
                    formArray.push({
                        name: "type",
                        value: "profile"
                    });

                    $.ajax({
                        type: "POST",
                        url: baseurl + "/profile/update_profile",
                        data: formArray,
                        dataType: "json"
                    }).done(function(response) {
                        $("body").loading("stop");
                        $("button").prop("disabled", false);
                        if (response.status === false) {
                            Swal.fire("Warning!", response.message, "warning");
                            return;
                        }
                        Swal.fire("", "Save Setting", "success");
                    });
                }
            });
        },
        init: function() {
            this.formValidate();

            // window.addEventListener('DOMContentLoaded', function () {
            //     var image = document.querySelector('#showAvatar');
            //     var minAspectRatio = 0.5;
            //     var maxAspectRatio = 1.5;
            //     var cropper = new Cropper(image, {

            //         ready: function () {
            //         var cropper = this.cropper;
            //         var containerData = cropper.getContainerData();
            //         var cropBoxData = cropper.getCropBoxData();
            //         var aspectRatio = cropBoxData.width / cropBoxData.height;
            //         var newCropBoxWidth;

            //         if (aspectRatio < minAspectRatio || aspectRatio > maxAspectRatio) {
            //             newCropBoxWidth = cropBoxData.height * ((minAspectRatio + maxAspectRatio) / 2);

            //             cropper.setCropBoxData({
            //             left: (containerData.width - newCropBoxWidth) / 2,
            //             width: newCropBoxWidth
            //             });
            //         }
            //         },

            //         cropmove: function () {
            //         var cropper = this.cropper;
            //         var cropBoxData = cropper.getCropBoxData();
            //         var aspectRatio = cropBoxData.width / cropBoxData.height;

            //         if (aspectRatio < minAspectRatio) {
            //             cropper.setCropBoxData({
            //             width: cropBoxData.height * minAspectRatio
            //             });
            //         } else if (aspectRatio > maxAspectRatio) {
            //             cropper.setCropBoxData({
            //             width: cropBoxData.height * maxAspectRatio
            //             });
            //         }
            //         },
            //     });
            // });
        }
    };
})();
profile.init();
