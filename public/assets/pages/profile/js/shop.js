var shop = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content"),
        currentImage = 0,
        submit = true;
    return {
        upload: {
            fileUpload: function(req) {
                $(req.el).fileupload({
                    url: baseurl + "/upload/profile/seller",
                    dataType: "json",
                    formData: req.req,
                    maxNumberOfFiles: req.nunber,
                    done: function(e, data) {
                        $("body").loading("stop");
                        req.success(data.result);
                    },
                    fail: function(e, data) {
                        $("body").loading("stop");
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: data.jqXHR.responseJSON.message
                        });
                    },
                    progressall: function(e, data) {
                        $("body").loading();
                    }
                });
            },
            avatar: function() {
                shop.upload.fileUpload({
                    el: "#avatar",
                    req: {
                        type: "avatar",
                        _token: $('meta[name="csrf-token"]').attr("content")
                    },
                    nunber: 1,
                    success: function(response) {
                        console.log(response);
                        if (response.status === true) {
                            $("#showAvatar").attr("src", response.name);
                            $(".avatar-image").attr(
                                "style",
                                "background-image: url(" + response.name + ")"
                            );
                        }
                    }
                });
            },
            slider: function() {
                if (currentImage < 5) {
                    shop.upload.fileUpload({
                        el: "#slider",
                        req: {
                            type: "slider",
                            _token: $('meta[name="csrf-token"]').attr("content")
                        },
                        nunber: 5,
                        success: function(response) {
                            if (response.status === true) {
                                var html = "";
                                html += '<li><div class="img">';
                                html += '<span class="number">Rank:</span>';
                                html +=
                                    '<div class="tool"><a href="#" class="text-danger" data-id="1">';
                                html += "Delete</a></div>";
                                html +=
                                    '<img src="' +
                                    response.name +
                                    '"/></div></li>';

                                $("#appendSlider").append(html);
                            }
                        }
                    });
                }
            }
        },
        url: function() {
            $(document).on("keypress, keyup", ".url-only", function(e) {
                var txt = $(this).val();
                $("#appendUrl").text(txt);
            });

            $(document).on("blur", ".url-only", function(e) {
                $(".form-group.url ").loading();
                $.ajax({
                    type: "POST",
                    url: baseurl + "/profile/has_url",
                    data: {
                        url: $(this).val(),
                        _token: $('meta[name="csrf-token"]').attr("content")
                    },
                    dataType: "json"
                }).done(function(response) {
                    $(".form-group.url ").loading("stop");
                    $(".form-group.url input").removeClass(
                        "is-invalid is-valid"
                    );
                    if (response.status === false) {
                        submit = false;
                        $(".form-group.url input").addClass("is-invalid");
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: "URL is duplicate"
                        });
                    } else {
                        submit = true;
                        $(".form-group.url input").addClass("is-valid");
                    }
                });
            });
        },
        sliders: function() {
            var sliders = $(".sortable").data("json");
            currentImage = sliders.length;
            console.log(sliders.length);
            var html = "",
                num = 1;
            _.forEach(sliders, function(value) {
                html += '<li><div class="img">';
                html += '<span class="number">Rank: ' + num + "</span>";
                html +='<div class="tool"><a href="#" class="text-danger delete-slider" data-id="' + value.id + '">';
                html += "Delete</a></div>";
                html += '<img src="' + value.name + '"/></div></li>';
                num++;
            });

            $("#appendSlider").append(html);
        },
        sortTable: function() {
            $(".sortable").sortable({
                connectWith: ".both",
                placeholder: "box-highlight",
                start: function(event, ui) {
                    ui.item.toggleClass("in-highlight");
                },
                stop: function(event, ui) {
                    ui.item.toggleClass("in-highlight");
                },
                update: function() {
                    // var ids = [];
                    // $('.box-banner').map(function(k,elm) {
                    // 	ids.push($(elm).find('button').data('id'));
                    // });
                    // // Axios
                    // pm.httpRequest('banner/service/do_update', 'POST', {
                    // 	type: 'rank',
                    // 	value: JSON.stringify(ids)
                    // });
                }
            });
        },
        formValidate: function() {
            $(".shop-form").validate({
                errorClass: "is-invalid error",
                validClass: "is-valid",
                submitHandler: function(form) {
                    if (submit == false) {
                        $("input[name=url]").focus();
                        return;
                    }
                    $("body").loading();
                    $("button").prop("disabled", true);
                    var formArray = $(form).serializeArray();
                    formArray.push({
                        name: "type",
                        value: "shop"
                    });

                    $.ajax({
                        type: "POST",
                        url: baseurl + "/seller/update_profile",
                        data: formArray,
                        dataType: "json"
                    }).done(function(response) {
                        $("body").loading("stop");
                        $("button").prop("disabled", false);
                        if (response.status === false) {
                            Swal.fire("Warning!", response.message, "warning");
                            return;
                        }
                        Swal.fire("", "Save Setting", "success");
                    });
                }
            });
        },
        init: function() {
            this.sliders();
            $(".tags").tagsinput({
                maxTags: 5
            });
            this.formValidate();
            this.sortTable();
            this.url();
            this.upload.avatar();
            this.upload.slider();
        }
    };
})();

shop.init();
