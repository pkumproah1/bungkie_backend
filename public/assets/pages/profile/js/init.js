var bank = (function() {
    var baseurl = $("meta[name^=baseUrl]").attr("content");
    return {
        init: function() {
            $(".url-only").inputmask({ regex: "[a-zA-Z]*" });
            $(".numeric").inputmask({ regex: "[0-9]*" });
        }
    };
})();

bank.init();
