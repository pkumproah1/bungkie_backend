var baseurl = $("meta[name^=baseUrl]").attr("content"),
    dataOrder = null,
    dataCancel = null,
    collapse = 0;
var Detail = function() {
    return {
        submit: function() {
            $(document).on('click', "[data-type]", function(e) {
                e.preventDefault();
                var data = $(this).data('params');
                if ($(this).data('type') === "cancel") {
                    $.confirm({
                        title: "ยกเลิกออเดอร์",
                        content: '' +
                        '<form action="" class="formName">' +
                        '<div class="form-group">' +
                        '<label class="text-danger">กรุณาเลือกเหตุผลในการยกเลิก</label>' +
                        '<select class="condition-cancel form-control">' +
                        '<option value="1" selected>ไม่มีสินค้า</option>' +
                        '<option value="2">ร้านไม่พร้อมส่ง</option>' +
                        '<option value="3">แฟลชเอ็กเพรสขอยกเลิก</option>' +
                        '</select>' +
                        '</div>' +
                        '</form>',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            confirm: {
                                text: 'ยืนยัน',
                                btnClass: 'btn-red',
                                action: function() {
                                    var typeCancel = this.$content.find('.condition-cancel').val();
                                    Detail.cancelOrder(data, typeCancel);
                                }
                            },
                            cancel: {
                                text: 'ยกเลิก'
                            }
                        }
                    });
                } else if ($(this).data('type') === "order") {
                    $.confirm({
                        title: "ปริ้นใบปะหน้า",
                        content: "ถ้าหากปริ้นใบปะหน้าแล้ว เจ้าหน้าที่จะเข้าไปรับของในวันเวลาที่กำหนด",
                        type: 'green',
                        typeAnimated: true,
                        buttons: {
                            confirm: {
                                text: 'ยืนยัน',
                                btnClass: 'btn-green',
                                action: function() {
                                    Detail.submitOrder(data);
                                }
                            },
                            cancel: {
                                text: 'ยกเลิก'
                            }
                        }
                    });
                }
            });
        },
        cancelOrder: function(params, type) {
            $('body').loading({
                logo: true
            });
            cancelOrder = params;
            if (cancelOrder !== null) {
                $.ajax({
                    url: baseurl + '/order/cancel_order',
                    method: 'POST',
                    data: {
                        _token: $('meta[name=csrf-token]').attr('content'),
                        reference: cancelOrder.reference,
                        shop_id: cancelOrder.shopId,
                        type_cancel: type
                    },
                    dataType: 'json'
                }).then(function(response) {
                    if (response.status === true) {
                        location.reload();
                    }
                });
            }
        },
        submitOrder: function(params) {
            $('body').loading({
                logo: true
            });
            dataOrder = params;
            if (dataOrder !== null) {
                $.ajax({
                    url: baseurl + '/order/create_order',
                    method: 'POST',
                    data: {
                        _token: $('meta[name=csrf-token]').attr('content'),
                        reference: dataOrder.reference,
                        shop_id: dataOrder.shopId
                    },
                    dataType: 'json'
                }).then(function(response) {
                    if (response.status === true) {
                        location.reload();
                    }
                });
            }
        },
        btnRefund: function() {
            $(document).on('click', '[data-type="refund"]', function(e) { 
                $("#modal-refund .modal-title").text($(this).text());
                $('#modal-refund').modal('show');
                dataCancel = $(this).data('params');
            });
        },
        init: function() {
            this.submit();
            this.btnRefund();
            $('#order').on('shown.bs.modal', function (e) {
                if (dataOrder !== null) {
                    $.ajax({
                        url: baseurl + '/order/flash/order',
                        method: 'POST',
                        data: {
                            _token: $('meta[name=csrf-token]').attr('content'),
                            reference: dataOrder.reference,
                            shop_id: dataOrder.shopId
                        },
                        dataType: 'json'
                    }).then(function(response) {
                        if (response.status === true) {
                            location.reload();
                        } else {

                        }
                    });
                }
            });

            $('#modal-refund').on('shown.bs.modal', function (e) {
                if (dataCancel !== null) {
                    $(".form-refund").validate({
                        errorClass: "is-invalid error",
                        validClass: "is-valid",
                        submitHandler: function(form) {
                            $('body').loading({
                                logo: true
                            });
                            var additional = $("#modal-refund textarea[name=additional]").val();
                            $.ajax({
                                url: baseurl + '/order/cancel_log_purchase',
                                method: 'POST',
                                data: {
                                    _token: $('meta[name=csrf-token]').attr('content'),
                                    reference: dataCancel.reference,
                                    shop_id: dataCancel.shopId,
                                    status: dataCancel.status,
                                    cancel_id: dataCancel.cancel_id,
                                    additional: additional ? additional : ''
                                },
                                dataType: 'json'
                            }).then(function(response) {
                                if (response.status === true) {
                                    location.reload();
                                } else {

                                }
                            });
                        }
                    });
                }
            });
            // reset data
            $('#order').on('hidden.bs.modal', function (e) {
                dataOrder = null;
            });
            $('#modal-refund').on('hidden.bs.modal', function (e) {
                dataCancel = null;
            });
            $(document).on('click', '.show-log', function(e) {
                e.preventDefault();
                $("#showLogs").toggle('show');
            });
        }
    }
}();

Detail.init();