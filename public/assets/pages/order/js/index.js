moment.locale('th');
moment().format('MMMM Do YYYY, h:mm:ss a');
var baseurl = $("meta[name^=baseUrl]").attr("content"),
    table,
    searchFilter = [],
    searchType = 1,
    data = {
        _token: $('meta[name="csrf-token"]').attr("content"),
        type: '1'
    };
var Order = function() {
    return {
        daterange: function () {
		    $('.daterange-show').daterangepicker({
                maxDate: moment(),
                "locale": {
                    "applyLabel": "ตกลง",
                    "cancelLabel": "ล้างข้อมูล",
                }
		    }, function cb(start, end) {
                $('input[name=start]').val(start.format('YYYY-MM-DD'));
                $('input[name=end]').val(end.format('YYYY-MM-DD'));

                searchFilter.start = start.format('YYYY-MM-DD 00:00:00');
                searchFilter.end = end.format('YYYY-MM-DD 23:59:59');
		    });
        },
        table: function() {
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                "order": [[ 5, "desc" ]],
                "ajax": {
                    "url": baseurl + "/order/paginate",
                    "data": data,
                    "type": "POST"
                }
            });
        },
        filters: {
            dropdown: function() {
                $(document).on('click', '#order-filter a', function(e) {
                    e.preventDefault();
                    var input = $('input[name=search-text]');
                    $('.input-group-prepend .dropdown-toggle').text($(this).text());
                    searchType = $(this).data('type');
                    input.val("");
                });
            },
            submit: function() {
                $(document).on('click', "[data-button]", function() {
                    if ($(this).data('button') === "submit") {
                        var input = $('input[name=search-text]'),
                            search = false;

                        if (input.val() !== "") {
                            $.extend(data, {
                                searchType: searchType,
                                searchValue: input.val()
                            });
                            search = true;
                        }

                        if (searchFilter.length !== []) {
                            $.extend(data, {
                                datetime_start: searchFilter.start,
                                datetime_end: searchFilter.end
                            });
                            search = true;
                        }

                        if (search === true) {
                            table.destroy();
                            Order.table();
                        }
                    } else if ($(this).data('button') === "reset") {
                        var input = $('input[name=search-text]').val("");
                        searchType = 1;
                        $('.input-group-prepend .dropdown-toggle').text("หมายเลขคำสั่งซื้อ");
                        $('input[name=start]').val("");
                        $('input[name=end]').val("");
                        searchFilter = [];
                        data = {
                            _token: $('meta[name="csrf-token"]').attr("content"),
                            type: 1
                        };
                        $('#ready-tab').trigger('click');
                        table.destroy();
                        Order.table();
                    }
                })
            },
            init: function() {
                Order.filters.dropdown();
                Order.filters.submit();
            }
        },
        changeTabs: function() {
            $(document).on('click', "#product-tab a", function(e) {
                table.destroy();
                $.extend(data, {
                    type : $(this).data('type')
                });
                Order.table();
            });
        },
        init: function() {
            this.daterange();
            this.changeTabs();
            this.filters.init();

            this.table();
            

            
        }

    }
}();

Order.init();