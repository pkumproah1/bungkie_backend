moment.locale('th');
moment().format('MMMM Do YYYY, h:mm:ss a');
var baseurl = $("meta[name^=baseUrl]").attr("content"),
    table,
    searchFilter = [],
    searchType = 1,
    data = {
        _token: $('meta[name="csrf-token"]').attr("content"),
        payment_status: '000'
    };
var Report = function() {
    return {
        daterange: function () {
		    $('.daterange-show').daterangepicker({
                maxDate: moment(),
                "locale": {
                    "applyLabel": "ตกลง",
                    "cancelLabel": "ล้างข้อมูล",
                }
		    }, function cb(start, end) {
                $('input[name=start]').val(start.format('YYYY-MM-DD'));
                $('input[name=end]').val(end.format('YYYY-MM-DD'));

                searchFilter.start = start.format('YYYY-MM-DD 00:00:00');
                searchFilter.end = end.format('YYYY-MM-DD 23:59:59');
		    });
        },
        table: function() {
            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "searching": false,
                "ajax": {
                    "url": baseurl + "/report/purchase_paginate",
                    "data": data,
                    "type": "POST"
                }
            });
        },
        filters: {
            dropdown: function() {
                $(document).on('click', '#order-filter a', function(e) {
                    e.preventDefault();
                    var input = $('input[name=search-text]');
                    $('.input-group-prepend .dropdown-toggle').text($(this).text());
                    searchType = $(this).data('type');
                    input.val("");
                });
                
                $(document).on('click', '.payment-status .dropdown-menu a', function(e) {
                    e.preventDefault();
                    $("#filter-by").text($(this).text());
                    data = {
                        _token: $('meta[name="csrf-token"]').attr("content"),
                        payment_status: $(this).data('value')
                    };
                    table.destroy();
                    Report.table();
                });
            },
            submit: function() {
                $(document).on('click', "[data-button]", function() {
                    if ($(this).data('button') === "submit") {
                        var input = $('input[name=search-text]'),
                            search = false;

                        if (input.val() !== "") {
                            $.extend(data, {
                                searchType: searchType,
                                searchValue: input.val()
                            });
                            search = true;
                        }

                        if (searchFilter.length !== []) {
                            $.extend(data, {
                                datetime_start: searchFilter.start,
                                datetime_end: searchFilter.end
                            });
                            search = true;
                        }

                        if (search === true) {
                            table.destroy();
                            Report.table();
                        }
                    } else if ($(this).data('button') === "reset") {
                        var input = $('input[name=search-text]').val("");
                        searchType = 1;
                        $('.input-group-prepend .dropdown-toggle').text("หมายเลขคำสั่งซื้อ");
                        $('input[name=start]').val("");
                        $('input[name=end]').val("");
                        searchFilter = [];
                        data = {
                            _token: $('meta[name="csrf-token"]').attr("content")
                        };
                        table.destroy();
                        Report.table();
                    } else if ($(this).data('button') === "export") {
                        Report.export();
                    }
                })
            },
            init: function() {
                Report.filters.dropdown();
                Report.filters.submit();
            }
        },
        export: function() {
            $.ajax({
                url: baseurl + '/report/purchase_export',
                method: 'POST',
                data: data,
                dataType: 'json'
            }).then(function(response) {
                var linkSource = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,'+ response.data.download;
                var downloadLink = document.createElement("a");
                var fileName = response.data.filename;
                downloadLink.href = linkSource;
                downloadLink.download = fileName;
                downloadLink.click();
            });
        },
        init: function() {
            this.table();
            this.daterange();
            this.filters.init();
        }
    }
}();

Report.init();