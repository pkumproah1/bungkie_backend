<?php

namespace App\Http\Middleware;

use Closure;

class IsToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->id === 1) {
            return \Response::json([
                'status' => false,
                'message' => 'Unauthenticated (Middle)'
            ], 500);
        }
        return $next($request);
    }
}
