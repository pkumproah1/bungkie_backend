<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\Mailer;

//! Model
use App\Models\Tb_order as OrderModel;
use App\Models\Tb_order_shipping as OrderShippingModel;
//! Library
use App\Libraries\OrderLibrary;
class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return $this->view('order.index', $this->data);
    }

    /**
     * detail
     * @param  mixed $reference_id
     * @return void
     */
    public function detail(string $reference_id = null, int $shop_id = null)
    {
        if (empty($reference_id) && empty($shop_id)) return redirect('/order');
        $orders = OrderLibrary::detail($reference_id, $shop_id);
        if ($orders['status'] === false) return redirect('/order');
        $this->data['orders'] = $orders['data'];
        return $this->view('order.detail', $this->data);
    }

    /**
     * paginate
     *
     * @param  mixed $request
     * @return void
     */
    public function paginate(Request $request)
    {
        return OrderLibrary::paginate($request->all());
    }

    public function cancel_log_purchase(Request $request)
    {
        $request->validate([
            'reference' => 'required',
            'shop_id' => 'required|numeric',
            'status' => 'required|numeric',
            'cancel_id' => 'required|numeric'
        ]);
        
        DB::table('tb_order_item_cancel')->where('id', $request->cancel_id)->update([
            'status' => $request->status,
            'updated_by' => Auth()->user()->id,
            'updated_at' => now()
        ]);

        DB::table('tb_order_item_cancel_detail')->insert([
            'reference_id' => $request->reference,
            'shop_id' => $request->shop_id,
            'status' => $request->status,
            'additional' => isset($request->additional) && !empty($request->additional) ? $request->additional : null,
            'created_at' => now(),
            'created_by' => Auth()->user()->id
        ]);

        return response()->json(['status' => true], 200);
    }
    
    /**
     * shipping_order
     *
     * @param  mixed $request
     * @return void
     */
    public function shipping_order(Request $request)
    {
        $request->validate([
            'reference' => 'required',
            'shop_id' => 'required|numeric'
        ]);
        
        $shipping = OrderLibrary::shipping_create($request->reference, $request->shop_id);
        if ($shipping['status'] === false) return response()->json($shipping, 200);
        return response()->json([
            'status' => true,
            'data' => array(
                'pno' => $shipping['data']['data']['pno'],
                'download' => array(
                    'big' => $shipping['data']['download']['big']['url'],
                    'small' => $shipping['data']['download']['small']['url'],
                )
            )
        ], 200);
    }
    
    /**
     * cancel_order
     *
     * @param  mixed $request
     * @return void
     */
    public function cancel_order(Request $request)
    {
        $request->validate([
            'reference' => 'required',
            'shop_id' => 'required|numeric',
            'type_cancel' => 'required|numeric'
        ]);
        
        if (!in_array($request->type_cancel, [1,2,3])) return response()->json([
            'status' => false,
            'message' => "Type not supported"
        ], 400);

        $shipping = OrderLibrary::cancel_order($request->reference, $request->shop_id, $request->type_cancel);
        return response()->json($shipping, 200);
    }

    /**
     * shipping_tracking
     *
     * @param  mixed $pno
     * @return void
     */
    public function shipping_tracking(string $pno = null)
    {
        $routes = OrderLibrary::shipping_routes($pno);
        return response()->json($routes);
    }
    
    /**
     * shipping_notifications
     *
     * @return void
     */
    public function shipping_notifications(Request $request)
    {
        $date = ($request->date) ? $request->date : date('Y-m-d');
        $notifications = OrderLibrary::flash_notifications($date);
        return response()->json($notifications);
    }
}
