<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//! Model
use App\Models\Tb_order as OrderModel;
use App\Models\Tb_order_shipping as OrderShippingModel;
//! Library
use App\Libraries\OrderLibrary;
class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return $this->view('dashboard.index', $this->data);
    }
}
