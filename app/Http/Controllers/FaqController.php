<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faq;
use App\Models\FaqCategory;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $faqs = Faq::get();//paginate(20);
        return view('dashboard.faq.show', compact('faqs'));
    }

    public function show($id)
    {
        $faq = Faq::find($id);
        $faqCat = FaqCategory::where('id', $faq->cat_id)->first();
        return view('dashboard.faq.show', compact('faq', 'faqCat'))->with('fagCat', $faqCat);
    }

    public function create()
    {
        $faqCategories = FaqCategory::get();
        return view('dashboard.faq.create', compact('faqCategories'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'topic'    => 'required|min:1|max:100',
            'desc' => 'required|min:1',
        ]);
        $faq = new Faq();
        $faq->topic = $request->input('topic');
        $faq->desc = $request->input('desc');
        $faq->tag = $request->input('tag');
        $faq->cat_id = $request->input('faq_cat');

        $faq->save();

        $request->session()->flash('message', ' FAQ Created Successfully.');
        return redirect()->route('faq.index');
    }

    public function edit($id)
    {
        $faq = Faq::findOrFail($id);
        $faqCategories = FaqCategory::get();

        return view('dashboard.faq.edit', compact('faq', 'faqCategories'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'topic'    => 'required|min:1|max:100',
            'desc' => 'required|min:1'
        ]);

        $faq = Faq::find($request->id);
        $faq->topic = $request->input('topic');
        $faq->desc = $request->input('desc');
        $faq->tag = $request->input('tag');
        $faq->cat_id = $request->input('faq_cat');

        $faq->save();

        $request->session()->flash('message', 'FAQ edited successfully.');

        return redirect()->route('faq.index');
    }

    public function destroy(Request $request, $id)
    {
        $faq = Faq::find($id);
        if($faq)
        {
            $faq->delete();
        }

        $request->session()->flash('message', 'Successfully deleted');
        return redirect()->route('faq.index');
    }

}

?>
