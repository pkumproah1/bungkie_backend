<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
//! Model
use App\Models\Tb_order as OrderModel;
use App\Models\Tb_order_shipping as OrderShippingModel;
//! Library
use App\Libraries\OrderLibrary;
use App\Libraries\ReportLibrary;
class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * index
     *
     * @return void
     */
    public function purchase()
    {
        return $this->view('report.index', $this->data);
    }
    
    /**
     * purchase_export
     *
     * @return void
     */
    public function purchase_export(Request $request)
    {
        // $mockup = array('xx' => 'xs');
        // $reports = ReportLibrary::report_purchase($mockup);
        $reports = ReportLibrary::report_purchase($request->all());
        // dd($reports);
        return response()->json($reports,200);
    }
    
    /**
     * purchase_paginate
     *
     * @param  mixed $request
     * @return void
     */
    public function purchase_paginate(Request $request)
    {
        return ReportLibrary::paginate_purchase($request->all());
    }
    
}
