<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Image;
use Response;
use Illuminate\Support\Facades\Storage;
class FroalaController extends Controller
{
    public function upload(Request $request)
    {
        // $request->file('file')->store('products');

        $resize = Image::make($request->file('file'))->resize(768, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');

        $filename = time();
        $save = Storage::put('products/upload/'.  $filename .'.jpg', $resize->__toString());

        if ($save) {
            header('Content-Type: application/json');
            echo json_encode([
                'link' => asset('/storage/app/products/upload/'.  $filename .'.jpg')
            ]);
            exit;
        }
    }
}
