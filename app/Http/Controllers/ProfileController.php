<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\ProfileLibrary;
use App\Libraries\SellerLibrary;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Response;

use App\Models\Tb_notification as Notification;
use App\Mail\Mailer;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
       
        $me = ProfileLibrary::me() ?? '';
        // dd($me);
        $sellerLib = new SellerLibrary();
        $bankings = $sellerLib->get_banks();
        $bank_lists = ($bankings['status'] === true) ? $bankings['data'] : null;

        $this->data['me'] = $me['data'];
        $this->data['bank_lists'] = $bank_lists;

        return $this->view('seller.profile.index', $this->data);
    }
    
    /**
     * has_url
     *
     * @param  mixed $request
     * @return void
     */
    public function has_url(Request $request)
    {
        $request->validate([
            'url' => 'required'
        ]);

        $url = DB::table("tb_user_sellers")
            ->where('user_id', '!=', Auth()->user()->id)
            ->where("url", $request->url)->first();

        if ($url) return Response::json([
            'status' => false
        ]);
        
        return Response::json([
            'status' => true
        ]);
    }
    
    /**
     * update_profile
     *
     * @param  mixed $request
     * @return void
     */
    public function update_profile(Request $request)
    {
        if ($request->type === "shop") {
            // Tab Shop Setting
            DB::table("tb_user_sellers")
                ->where("user_id", Auth()->user()->id)
                ->update([
                    "url" => $request->url ?? '',
                    "shop_description" => $request->shop_description ?? '',
                    "shop_keyword" => $request->keyword ?? ''
                ]);
        } else if ($request->type === "bank") {
            // Tab Bank Setting
            DB::table("tb_user_sellers")
                ->where("user_id", Auth()->user()->id)
                ->update([
                    "bank_id" => $request->bank ?? '',
                    "account_number" => $request->account_number ?? '',
                    "account_name" => $request->account_name ?? ''
                ]);
        } else if ($request->type === "profile") {
            // Tab Profile Setting
            DB::table("tb_user_sellers")
                ->where("user_id", Auth()->user()->id)
                ->update([
                    "company_name" => $request->company_name ?? '',
                    "tax_id" => $request->tax_id ?? '',
                    "brand_name" => $request->brand_name ?? '',
                    "shop_name" => $request->shop_name ?? '',
                    "person_name" => $request->person ?? '',
                    "basic_telephone" => $request->basic_telephone ?? '',
                    "email" => $request->email ?? '',
                    "warehouse_address" => $request->shipping_address ?? '',
                    "warehouse_name" => $request->warehouse_name ?? '',
                    "warehouse_telephone" => $request->warehouse_tel ?? '',
                    "billing_address" => $request->billing_address ?? '',
                ]);
        } else if ($request->type === "password") {
            $user = Auth()->user();
            if (Hash::check($request->old_password, $user->password) !== true) {
                return Response::json([
                    'status' => false,
                    'message' => "Old password is invalid."
                ]); 
            } else {

                // Username: info@bungkie.com Password: Vud13366
                // Host : smtp.office365.com Port : 587
                DB::table('users')
                    ->where('id', Auth()->user()->id)
                    ->update([
                        'password' => Hash::make($request->password),
                    ]);


                $me = DB::table("tb_user_sellers")
                    ->where("user_id", Auth()->user()->id)->first();
                $details = [
                    'to' => $me->email,
                    'from' => 'info@bungkie.com',
                    'subject' => 'Change password',
                    'title' => 'Bungkie | Change password',
                    'name' => $me->shop_name,
                    'fname' => 'sdsds',
                    'layout' => 'contact'
                ];

                \Mail::to($me->email)->send(new Mailer($details));
            }
        }

        return Response::json([
            'status' => true
        ]);        
    }
}
