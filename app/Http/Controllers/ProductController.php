<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\Category;
use App\Models\Product_images;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mail;
use File;
use App\Models\Form;
use Image;
use Response;
use App\Models\Tb_product;
use App\Libraries\ProductLibrary;
use App\Libraries\ProductBatchLibrary;

// new 1 Feb 21
use App\Models\Tb_user_sellers as sellerModel;
class ProductController extends Controller
{
    private $productLib;
    private $me;
    public function __construct()
    {
        $this->middleware('auth');
        $this->productLib = new ProductLibrary();
    }
        
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        return $this->view('product.index');
    }

    public function saveimage()
    {
        
        // ProductBatchLibrary::download_template(['type' => 'stock']);
        ProductBatchLibrary::download_template(['type' => 'price']);
        exit;
        $url = "https://img.kingpowerclick.com/cdn-cgi/image/format=auto/kingpower-com/image/upload/w_320,h_320,f_auto/v1602127999/prod/4496285-L1.jpg";
        $img    = 'miki.png';
        $filename   = basename($url);

        $resize_m = Image::make($url)->resize(360, 360, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');

        // resize = 1024
        $resize_b = Image::make($url)->resize(1024, 1024, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg');

        $filename = time();
        $save = Storage::disk('public_uploads')->put('/m/' . $filename .'.jpg', $resize_m->__toString());
        $save = Storage::disk('public_uploads')->put('/b/' . $filename .'.jpg', $resize_b->__toString());

        // Image::make($url)->save(public_path('test/' . time()));
        // $result = file_put_contents($img, $file);
        // dd($result);
        
    }
    
    /**
     * paginate
     *
     * @param  mixed $request
     * @return void
     */
    public function paginate(Request $request)
    {
        return Tb_product::find_paginate($request->all());
    }
    
    /**
     * create
     *
     * @param  mixed $id
     * @return void
     */
    public function create($id = null)
    {  
        $product = null;
        if (!empty($id)) {
            $product = $this->productLib->find_product($id);
            if (empty($product)) return redirect('/product/list');
        }
        // dd($product);
        $sellers = sellerModel::get_pagainate();
        $this->data['product'] = $product;
        $this->data['sellers'] = $sellers;
        return $this->view('product.create');
    }

    /**
     * inserted
     *
     * @param  mixed $request
     * @return void
     */
    public function inserted(Request $request)
    {
        $input = $request->all();
        if (isset($input['product_id']) && !empty($input['product_id'])) {
            return self::updated($input);
        } else {
            return self::saved($input);
        }
    }

    /**
     * removed
     *
     * @param  mixed $request
     * @return void
     */
    public function removed(Request $request)
    {
        $input = $request->all();
        $request->validate([
            'product_id' => 'required'
        ]);

        $product = DB::table('tb_product')->where('id', $input['product_id'])->delete();
        $language = DB::table('tb_product_language')
            ->where('product_id', $input['product_id'])
            ->delete();
        $product_price_remove = DB::table('tb_product_price')
            ->where('product_id', $input['product_id'])
            ->delete();
        if ($language) {
            $options = DB::table('tb_product_option')
                ->where('product_id', $input['product_id'])
                ->get();

            if ($options) {
                foreach($options as $option) {
                    DB::table('tb_product_option_item')
                        ->where('option_id', $option->id)
                        ->delete();
                }
                $product_option_remove = DB::table('tb_product_option')
                    ->where('product_id', $input['product_id'])
                    ->delete();
            }
        }
    

        return Response::json([
            'status' => true
        ]);
    }
     
    /**
     * saved
     *
     * @param  mixed $input
     * @return void
     */
    private function saved($input = array())
    {
        // ? gen sku
        $sku = $this->productLib->generate_sku();
        $product = array(
            'sku' => $sku,
            'brand' => $input['brand'],
            's_weight' => $input['s_weight'],
            's_width' => $input['s_width'],
            's_length' => $input['s_length'],
            's_height' => $input['s_height'],
            'in_stock' => $input['in_stock'],
            'status' => $input['status'],
            'product_type' => $input['product_type'],
            'tags' => isset($input['tags']) && !empty($input['tags']) ? $input['tags'] : null,
            'viewer' => 0,
            'created_at' => now(),
            'created_by' => Auth()->user()->id
        );
        // ! is admin approve
        if (ProductLibrary::hasRole('admin')) {
            $product['approval'] = $input['approval'];
            $product['recommend'] = $input['recommend'];
            $product['approved_by'] = Auth()->user()->id;
            $product['owner'] = $input['seller'];
        } elseif (ProductLibrary::hasRole('seller')) {
            $product['owner'] = Auth()->user()->id;
        }
        // * get productId
        $product_id = DB::table('tb_product')->insertGetId($product);
        // ! ถ้า insert ไม่ได้
        if (!$product_id) return Response::json([
            'status' => false,
            'message' => 'error insert general data'
        ]);
        
        // ? Insert relate category
        if ($input['categorys']) {
            $categorys = explode(',', $input['categorys']); 
            if ($categorys) {
                $cats = array();
                foreach($categorys as $cat) {
                    $cats[] = array(
                        'product_id' => $product_id,
                        'category_id' => $cat
                    );
                }
                // * inserted
                DB::table('tb_product_category_relation')->insert($cats);
            }
        }
        
        // ? Insert Images
        if ($input['images']) {
            $images = json_decode($input['images'], TRUE);
            if ($images) {
                $imgs = array();
                foreach($images as $img) {
                    Storage::disk('public_uploads')->move('products/temp/m/' . $img['name'], 'products/'. $product_id .'/m/' . $img['name']);
                    Storage::disk('public_uploads')->move('products/temp/b/' . $img['name'], 'products/'. $product_id .'/b/' . $img['name']);
                    $imgs[] = array(
                        'product_id' => $product_id,
                        'path' => $img['name'],
                        'rank' => $img['rank'],
                        'is_new' => 1
                    );
                } 
            }
            DB::table('tb_product_images')->insert($imgs);
        }

        $products = array();
        $price = array();
        // Language { id: 1 == "thai", id: 2 == "english" }
        foreach(array(['id'=> 1], ['id' => 2]) as $lang) {
            $products[] = array(
                'product_id' => $product_id,
                'language_id' => $lang['id'],
                'name' => isset($input['prod_name'][$lang['id']]) ? $input['prod_name'][$lang['id']] : '',
                'short_description' => isset($input['short_desc'][$lang['id']]) ? $input['short_desc'][$lang['id']] : '',
                'description' => isset($input['full_desc'][$lang['id']]) ? $input['full_desc'][$lang['id']] : '',
                'special_condition' => isset($input['special_condition'][$lang['id']]) ? $input['special_condition'][$lang['id']] : '',
            );

            $prices[] = array(
                'product_id' => $product_id,
                'type_price' => $lang['id'],
                'operation' => isset($input['discount_type'][$lang['id']]) ? strtolower($input['discount_type'][$lang['id']]) : 'baht',
                'base_price' => isset($input['base_price'][$lang['id']]) ? $input['base_price'][$lang['id']] : '',
                'front_price' => isset($input['front_price'][$lang['id']]) ? $input['front_price'][$lang['id']] : '',
                'sale_price' => isset($input['sale_price'][$lang['id']]) ? $input['sale_price'][$lang['id']] : ''
            );
        }

        // Insert Product_language
        $product_language = DB::table('tb_product_language')->insert($products);
        if (!$product_language) return Response::json([
            'status' => false,
            'message' => 'error insert product data'
        ]);

        // Insert Prices
        $prices = DB::table('tb_product_price')->insert($prices);
        if (!$prices) {
            return Response::json([
                'status' => false,
                'message' => 'error insert prices'
            ]);
        }

        // Section option
        if ((isset($input['attributes'])) && !empty($input['attributes'])) {
            $option = json_decode($input['attributes'], true);
            $options = array_filter($option, function($callback) {
                return !empty($callback['option']);
            });

            if (!empty($options)) {
                foreach ($options as $option) {
                    $option_id = DB::table('tb_product_option')->insertGetId([
                        'product_id' => $product_id,
                        'name' => $option['name'],
                        'status' => 1,
                        'created_at' => now()
                    ]);
                    
                    if (!$option_id) return Response::json([
                        'status' => false,
                        'message' => 'error insert prices'
                    ]);

                    foreach($option['option'] as $item) {
                        $item = DB::table('tb_product_option_item')->insertGetId([
                            'option_id' => $option_id,
                            'name' => $item
                        ]);

                        // Check insert
                        if (!$item) return Response::json([
                            'status' => false,
                            'message' => 'error insert item'
                        ]);
                    }                    
                }
            }
        }

        // response success
        return Response::json([
            'status' => true
        ]);
    }
            
    /**
     * updated
     *
     * @param  mixed $input
     * @return void
     */
    private function updated($input = array())
    {
        $product = DB::table('tb_product')->where('tb_product.id', $input['product_id'])->first();        
        if (empty($product)) return Response::json([
            'status' => false,
            'message' => 'empty product'
        ]);
        
        $product_data = array(
            'brand' => $input['brand'],
            's_weight' => $input['s_weight'],
            's_width' => $input['s_width'],
            's_length' => $input['s_length'],
            's_height' => $input['s_height'],
            'in_stock' => $input['in_stock'],
            'status' => $input['status'],
            'product_type' => $input['product_type'],
            'tags' => isset($input['tags']) && !empty($input['tags']) ? $input['tags'] : null,
            'updated_at' => now(),
            'updated_by' => Auth()->user()->id
        );

        // ! is admin approve
        if (ProductLibrary::hasRole('admin')) {
            $product_data['approval'] = $input['approval'];
            $product_data['recommend'] = $input['recommend'];
            $product_data['approved_by'] = Auth()->user()->id;
            $product_data['owner'] = $input['seller'];
        }

        // update product
        $product = DB::table('tb_product')
            ->where('id', $input['product_id'])
            ->update($product_data);
        
        if (!$product) return Response::json([
            'status' => false,
            'message' => 'update Product!'
        ]); 
        
        // ! remove and new insert product language
        DB::table('tb_product_language')->where('product_id', $input['product_id'])->delete();
        DB::table('tb_product_price')->where('product_id', $input['product_id'])->delete();
        
        $products = array();
        $price = array();
        // ! Language { id: 1 == "thai", id: 2 == "english" }
        foreach(array(['id'=> 1], ['id' => 2]) as $lang) {
            $products[] = array(
                'product_id' => $input['product_id'],
                'language_id' => $lang['id'],
                'name' => isset($input['prod_name'][$lang['id']]) ? $input['prod_name'][$lang['id']] : '',
                'short_description' => isset($input['short_desc'][$lang['id']]) ? $input['short_desc'][$lang['id']] : '',
                'description' => isset($input['full_desc'][$lang['id']]) ? $input['full_desc'][$lang['id']] : '',
                'special_condition' => isset($input['special_condition'][$lang['id']]) ? $input['special_condition'][$lang['id']] : '',
            );

            $prices[] = array(
                'product_id' => $input['product_id'],
                'type_price' => $lang['id'],
                'operation' => isset($input['discount_type'][$lang['id']]) ? strtolower($input['discount_type'][$lang['id']]) : 'baht',
                'base_price' => isset($input['base_price'][$lang['id']]) ? $input['base_price'][$lang['id']] : '',
                'front_price' => isset($input['front_price'][$lang['id']]) ? $input['front_price'][$lang['id']] : '',
                'sale_price' => isset($input['sale_price'][$lang['id']]) ? $input['sale_price'][$lang['id']] : ''
            );
        }

        // * Insert Product_language
        DB::table('tb_product_language')->insert($products);
        // * Insert Prices
        DB::table('tb_product_price')->insert($prices);
            
        // ? Insert relate category
        if ($input['categorys']) {
            // ! delete
            DB::table('tb_product_category_relation')
                ->where('product_id', $input['product_id'])->delete();
            // ? to array
            $categorys = explode(',', $input['categorys']); 
            if ($categorys) {
                $cats = array();
                foreach($categorys as $cat) {
                    $cats[] = array(
                        'product_id' => $input['product_id'],
                        'category_id' => $cat
                    );
                }
                // * inserted
                DB::table('tb_product_category_relation')->insert($cats);
            }
        }
        
        // ? Insert Images
        if ($input['images']) {
            // ? old images
            $old_images = DB::table('tb_product_images')
                ->where('product_id', $input['product_id'])->get()->toArray();
            // ! images from client
            $images = json_decode($input['images'], TRUE);
            $sub_item = array_column($images, 'name');
            if (!empty($old_images)) {
                // ? merge images
                foreach($old_images as $key => $img) {
                    if (in_array($img->path, $sub_item)) {
                        $key = array_search($img->path, $sub_item);
                        // * update rank
                        DB::table('tb_product_images')
                            ->where('id', $img->id)
                            ->update(['rank' => $images[$key]['rank']]);
                        unset($images[$key]);
                    } else {
                        // ! delete items in database
                        DB::table('tb_product_images')
                            ->where('id', $img->id)->delete();
                    }
                    
                }
                // ? new images
                if (!empty($images)) {
                    $imgs = array();
                    foreach($images as $img) {
                        Storage::disk('public_uploads')->move('products/temp/m/' . $img['name'], 'products/'. $input['product_id'] .'/m/' . $img['name']);
                        Storage::disk('public_uploads')->move('products/temp/b/' . $img['name'], 'products/'. $input['product_id'] .'/b/' . $img['name']);
                        $imgs[] = array(
                            'product_id' => $input['product_id'],
                            'path' => $img['name'],
                            'rank' => $img['rank'],
                            'is_new' => 1
                        );
                    }
                    DB::table('tb_product_images')->insert($imgs);
                }
                
            }
        }

        // ! remove and new insert product option [color, size]
        $options = DB::table('tb_product_option')->where('product_id', $input['product_id'])->get();
        if ($options) {
            foreach($options as $option) {
                DB::table('tb_product_option_item')->where('option_id', $option->id)->delete();
            }

            DB::table('tb_product_option')->where('product_id', $input['product_id'])->delete();
            // Section option
            if ((isset($input['attributes'])) && !empty($input['attributes'])) {
                $option = json_decode($input['attributes'], true);
                $options = array_filter($option, function($callback) {
                    return !empty($callback['option']);
                });

                if (!empty($options)) {
                    foreach ($options as $option) {
                        $option_id = DB::table('tb_product_option')->insertGetId([
                            'product_id' => $input['product_id'],
                            'name' => $option['name'],
                            'status' => 1,
                            'created_at' => now()
                        ]);
                        
                        if (!$option_id) return Response::json([
                            'status' => false,
                            'message' => 'error insert prices'
                        ]);

                        foreach($option['option'] as $item) {
                            $item = DB::table('tb_product_option_item')->insertGetId([
                                'option_id' => $option_id,
                                'name' => $item
                            ]);

                            // Check insert
                            if (!$item) return Response::json([
                                'status' => false,
                                'message' => 'error insert item'
                            ]);
                        }                    
                    }
                }
            }
            
        } 
    
        // response success
        return Response::json([
            'status' => true
        ]);
    }
}
