<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class APIController extends JsonResource
{
    
    public function index(Request $request){
        if($request->has('id')){
            $thisFolder = Folder::where('id', '=', $request->input('id'))->first();
            if($thisFolder->folder_id == null){
                $result = view('dashboard.media.index', array(
                    'medias' => $thisFolder->getMedia(),
                    'mediaFolders' =>  Folder::where('folder_id', '=', $thisFolder->id)->get(),
                    'thisFolder' => $thisFolder->id,
                    'parentFolder' => 'disable'
                ));
            }else{
                $result = view('dashboard.media.index', array(
                    'medias' => $thisFolder->getMedia(),
                    'mediaFolders' =>  Folder::where('folder_id', '=', $request->input('id'))->get(),
                    'thisFolder' => $request->input('id'),
                    'parentFolder' => $thisFolder['folder_id']
                ));
            }
        }else{
            $rootFolder = Folder::whereNull('folder_id')->first();
            $result = view('dashboard.media.index', array(
                'medias' => $rootFolder->getMedia(),
                'mediaFolders' =>  Folder::where('folder_id', '=', $rootFolder->id)->get(),
                'thisFolder' => $rootFolder->id,
                'parentFolder' => 'disable'
            ));
        }
        return $result;
    }
}
