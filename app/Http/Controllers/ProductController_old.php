<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\Category;
use App\Models\Product_images;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mail;
use File;
use App\Models\Form;
use Image;
use Response;
use App\Models\Tb_product;
use App\Libraries\ProductLibrary;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $status = array('', 'Inactive', 'Active', 'Deleted');
    private $productLib;
    private $me;
    public function __construct()
    {
        $this->middleware('auth');
        $this->productLib = new ProductLibrary();
    }
    

    // public function migatedata()
    // {
    //     $products = DB::table('products')
    //         ->get()->toArray();
    //     // echo "<pre>";
    //     // print_r($products);
    //     // exit;

    //     $error = array();
    //     foreach ($products as $prod) {
    //         $sku = $this->productLib->generate_sku($prod->sku);
    //         $product_id = DB::table('tb_product')->insertGetId([
    //             'category_id' => ($prod->cat_id) ? $prod->cat_id : 1,
    //             'sku' => $prod->sku,
    //             'brand' => $prod->brand,
    //             'dimension' => $prod->dimension,
    //             'dimension_unit' => $prod->dimension_unit,
    //             'weight' => $prod->weight,
    //             'weight_unit' => $prod->weight_unit,
    //             'in_stock' => $prod->in_stock,
    //             'status' => $prod->status,
    //             'hilight_flag' => $prod->hilight_flag,
    //             'created_at' => $prod->created_at,
    //             'updated_at' => $prod->deleted_at,
    //             'shipment_price' => $prod->shipment_price,
    //             'viewer' => $prod->viewer ? $prod->viewer : 0,
    //         ]);
    //         // thai language
    //         DB::table('tb_product_language')->insert([
    //             'product_id' => $product_id,
    //             'language_id' => 1,
    //             'name' => $prod->prod_name_th,
    //             'short_description' => $prod->short_desc_th,
    //             'description' => $prod->full_desc_th,
    //             'shipment_description' => null,
    //             'special_condition' => null
    //         ]);
            
    //         // english language
    //         DB::table('tb_product_language')->insert([
    //             'product_id' => $product_id,
    //             'language_id' => 2,
    //             'name' => $prod->prod_name_en,
    //             'short_description' => $prod->short_desc_en,
    //             'description' => $prod->full_desc_en,
    //             'shipment_description' => null,
    //             'special_condition' => null
    //         ]);

    //         // thb
    //         DB::table('tb_product_price')->insert([
    //             'product_id' => $product_id,
    //             'type_price' => 1,
    //             'base_price' => $prod->base_price_th,
    //             'front_price' => $prod->front_price_th,
    //             'sale_price' => $prod->sale_price_th
    //         ]);
            
    //         // usd
    //         DB::table('tb_product_price')->insert([
    //             'product_id' => $product_id,
    //             'type_price' => 2,
    //             'base_price' => $prod->base_price_en,
    //             'front_price' => $prod->front_price_en,
    //             'sale_price' => $prod->sale_price_en
    //         ]);


    //     }
    // }

    // public function update_migatedata()
    // {
    //     $products = DB::table('tb_product')
    //         ->where('sku', '-')
    //         ->get()->toArray();
    //     // echo "<pre>";
    //     // print_r($products);
    //     // exit;

    //     $error = array();
    //     foreach ($products as $prod) {
           
    //         $sku = $this->productLib->generate_sku($prod->category_id);
    //         $product_id = DB::table('tb_product')
    //         ->where('id', $prod->id)
    //         ->update([
    //             'sku' => $sku,
    //         ]);
    //     }
    // }
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        $is_seller = $this->productLib->hasRole('seller');
        $products = DB::table('tb_product')
            ->join('category', 'category.id', '=', 'tb_product.category_id')
            ->join('tb_product_language', 'tb_product_language.product_id', '=', 'tb_product.id')
            ->where('tb_product_language.language_id', 2)
            ->whereNull('tb_product.deleted_at')
            ->select('tb_product.id', 'tb_product_language.name', 'tb_product.brand', 'tb_product.sku', 'category.cat_name_en', 'tb_product.status', 'tb_product.approval', 'tb_product.in_stock')
            ->orderBy('tb_product.created_at', 'DESC');
        if ($is_seller) {
            $products->where('owner', Auth()->user()->id);
        }
        $products = $products->get();
        return view('dashboard.product.index', compact('products'));
    }
    
    /**
     * create
     *
     * @param  mixed $id
     * @return void
     */
    public function create($id = null)
    {  
        $product = null;
        if (!empty($id)) {
            $product = $this->productLib->find_product($id);
            if (empty($product)) return redirect('/product/list');
        }

        $category = DB::table('category')
            // ->where('category.main_cat', 1)
            ->get();


        $languages = array(
            array(
                'id' => 1,
                'name' => 'thai'
            ),
            array(
                'id' => 2,
                'name' => 'english'
            )
        );

        return view('dashboard.product.create', compact('category' , 'languages', 'product'));
    }

    public function image($id)
    {
        //print_r($request);
        $pi = DB::table('product_images')->where('prod_id', '=', $id)->get();
        //print_r($pi);
        return view('dashboard.product.image')
        ->with("product_images", $pi)
        ->with("id", $id);
    }

    public function store_image(Request $request)
    {
        $pi = new Product_images();
        $pi->prod_id = $request->input('prod_id');
        $pi->path = $request->file('prod_img')->store('products');
        
        $pi->banner_flag = $request->input('banner_flag');
        if($request->input('banner_flag') != '1'){
            $resize = Image::make($request->file('prod_img'))->resize(1000, null, function ($constraint) {
                $constraint->aspectRatio();
              })->encode('jpg');
              $save = Storage::put($pi->path, $resize->__toString());
        }else{
            // $resize = Image::make($request->file('prod_img'))->resize(1600, null, function ($constraint) {
            //     $constraint->aspectRatio();
            //   })->encode('jpg');
            //$pi->path = $request->file('prod_img');
            //$save = Storage::put($pi->path, $pi->path);
        }
        
        $pi->main_flag = $request->input('main_flag');
        
        $pi->save();
        $request->session()->flash('message', 'Successfully upload a new image');
        //exit;
        //return redirect()->route("product.image/$request->input('prod_id')");
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $validatedData = $request->validate([
        //     'name'    => 'required|min:1|max:64',
        //     'subject' => 'required|min:1|max:128',
        //     'content' => 'required|min:1',
        // ]);

        $template = new Products();
        $template->prod_name_en = $request->input('prod_name_en');
        $template->short_desc_en = $request->input('short_desc_en');
        $template->full_desc_en = $request->input('full_desc_en');
        $template->shipment_detail_en = $request->input('shipment_detail_en');
        $template->prod_name_th = $request->input('prod_name_th');
        $template->short_desc_th = $request->input('short_desc_th');
        $template->full_desc_th = $request->input('full_desc_th');
        $template->shipment_detail_th = $request->input('shipment_detail_th');
        $template->sku = $request->input('sku');        
        $template->shipment_price = $request->input('shipment_price');
        
        $template->base_price_th = $request->input('base_price_th');
        $template->front_price_th = $request->input('front_price_th');
        $template->sale_price_th = $request->input('sale_price_th');

        $template->base_price_en = $request->input('base_price_en');
        $template->front_price_en = $request->input('front_price_en');
        $template->sale_price_en = $request->input('sale_price_en');

        $template->status = $request->input('status');
        $template->hilight_flag = $request->input('hilight_flag');
        $template->cat_id = $request->input('cat_id');
        $template->sub_cat_id = "0";
        
        $template->in_stock = $request->input('in_stock');        
        $template->brand = $request->input('brand');
        $template->dimension = $request->input('dimension');
        $template->dimension_unit = $request->input('dimension_unit');
        $template->weight = $request->input('weight');
        $template->weight_unit = $request->input('weight_unit');        
        $template->special_condition_en = $request->input('special_condition_en');
        $template->special_condition_th = $request->input('special_condition_th');
        
        $template->save();


        $request->session()->flash('message', 'Successfully created a new Product');
        return redirect()->route('product.list');
    }

    /**
     * inserted
     *
     * @param  mixed $request
     * @return void
     */
    public function inserted(Request $request)
    {
        $input = $request->all();
        if (isset($input['product_id']) && !empty($input['product_id'])) {
            return self::updated($input);
        } else {
            return self::saved($input);
        }
    }

    /**
     * removed
     *
     * @param  mixed $request
     * @return void
     */
    public function removed(Request $request)
    {
        $input = $request->all();
        $request->validate([
            'product_id' => 'required'
        ]);
        
        $purchase = DB::table('purchase')->where('prod_id', $input['product_id'])->get()->toArray();
        if (!empty($purchase)) {
            // Flag delete
            $product = DB::table('tb_product')
                ->where('id', $input['product_id'])
                ->update(['deleted_at' => now()]);
            if (!$product) return Response::json([
                'status' => false,
                'message' => 'flag delete!'
            ]);
        } else {
            // real delete
            $product = DB::table('tb_product')->where('id', $input['product_id'])->delete();
            // if ($product) {
                $language = DB::table('tb_product_language')
                    ->where('product_id', $input['product_id'])
                    ->delete();
                $product_price_remove = DB::table('tb_product_price')
                    ->where('product_id', $input['product_id'])
                    ->delete();
                if ($language) {

                    $options = DB::table('tb_product_option')
                        ->where('product_id', $input['product_id'])
                        ->get();

                    if ($options) {
                        foreach($options as $option) {
                            DB::table('tb_product_option_item')
                                ->where('option_id', $option->id)
                                ->delete();
                        }
                        $product_option_remove = DB::table('tb_product_option')
                            ->where('product_id', $input['product_id'])
                            ->delete();
                    }
                }
            // }

            // if (!$product) return Response::json([
            //     'status' => false,
            //     'message' => 'delete!'
            // ]);
        }

        return Response::json([
            'status' => true
        ]);
    }
     
    /**
     * saved
     *
     * @param  mixed $input
     * @return void
     */
    private function saved($input = array())
    {
        $sku = $this->productLib->generate_sku($input['cat_id']);
        // Insert General Data

        $is_seller = $this->productLib->hasRole('seller');
        $product = array(
            'category_id' => $input['cat_id'],
            'sku' => $sku,
            'owner' => '',
            'brand' => $input['brand'],
            'dimension' => $input['dimension'],
            'dimension_unit' => $input['dimension_unit'],
            'weight' => $input['weight'],
            'weight_unit' => $input['weight_unit'],
            'in_stock' => $input['in_stock'],
            'status' => $input['status'],
            'hilight_flag' => $input['hilight_flag'],
            'created_at' => now(),
            'tags' => isset($input['tags']) && !empty($input['tags']) ? $input['tags'] : null,
            'shipment_price' => $input['shipment_price'],
            'viewer' => 0,
        );
        if ($is_seller) {
            $product['owner'] = Auth()->user()->id;
        }
        $product_id = DB::table('tb_product')->insertGetId($product);
        
        if (!$product_id) return Response::json([
            'status' => false,
            'message' => 'error insert general data'
        ]);

        $products = array();
        $price = array();
        // Language { id: 1 == "thai", id: 2 == "english" }
        foreach(array(['id'=> 1], ['id' => 2]) as $lang) {
            $products[] = array(
                'product_id' => $product_id,
                'language_id' => $lang['id'],
                'name' => isset($input['prod_name'][$lang['id']]) ? $input['prod_name'][$lang['id']] : '',
                'short_description' => isset($input['short_desc'][$lang['id']]) ? $input['short_desc'][$lang['id']] : '',
                'description' => isset($input['full_desc'][$lang['id']]) ? $input['full_desc'][$lang['id']] : '',
                'shipment_description' => isset($input['shipment_detail'][$lang['id']]) ? $input['shipment_detail'][$lang['id']] : '',
                'special_condition' => isset($input['special_condition'][$lang['id']]) ? $input['special_condition'][$lang['id']] : '',
            );

            $prices[] = array(
                'product_id' => $product_id,
                'type_price' => $lang['id'],
                'operation' => 'baht',
                'base_price' => isset($input['base_price'][$lang['id']]) ? $input['base_price'][$lang['id']] : '',
                'front_price' => isset($input['front_price'][$lang['id']]) ? $input['front_price'][$lang['id']] : '',
                'sale_price' => isset($input['sale_price'][$lang['id']]) ? $input['sale_price'][$lang['id']] : ''
            );
        }

        // Insert Product_language
        $product_language = DB::table('tb_product_language')->insert($products);
        if (!$product_language) return Response::json([
            'status' => false,
            'message' => 'error insert product data'
        ]);

        // Insert Prices
        $prices = DB::table('tb_product_price')->insert($prices);
        if (!$prices) {
            return Response::json([
                'status' => false,
                'message' => 'error insert prices'
            ]);
        }

        // Section option
        if ((isset($input['attributes'])) && !empty($input['attributes'])) {
            $option = json_decode($input['attributes'], true);
            $options = array_filter($option, function($callback) {
                return !empty($callback['option']);
            });

            if (!empty($options)) {
                foreach ($options as $option) {
                    $option_id = DB::table('tb_product_option')->insertGetId([
                        'product_id' => $product_id,
                        'name' => $option['name'],
                        'status' => 1,
                        'created_at' => now()
                    ]);
                    
                    if (!$option_id) return Response::json([
                        'status' => false,
                        'message' => 'error insert prices'
                    ]);

                    foreach($option['option'] as $item) {
                        $item = DB::table('tb_product_option_item')->insertGetId([
                            'option_id' => $option_id,
                            'name' => $item
                        ]);

                        // Check insert
                        if (!$item) return Response::json([
                            'status' => false,
                            'message' => 'error insert item'
                        ]);
                    }                    
                }
            }

            // print_r($options);
        }

        // response success
        return Response::json([
            'status' => true
        ]);
    }
            
    /**
     * updated
     *
     * @param  mixed $input
     * @return void
     */
    private function updated($input = array())
    {
        $product = DB::table('tb_product')->where('tb_product.id', $input['product_id'])->first();
        if (empty($product)) return Response::json([
            'status' => false,
            'message' => 'empty product'
        ]);

        // update product
        $product = DB::table('tb_product')
            ->where('id', $input['product_id'])
            ->update([
                'category_id' => $input['cat_id'],
                'brand' => $input['brand'],
                'dimension' => $input['dimension'],
                'dimension_unit' => $input['dimension_unit'],
                'weight' => $input['weight'],
                'weight_unit' => $input['weight_unit'],
                'in_stock' => $input['in_stock'],
                'status' => $input['status'],
                'hilight_flag' => $input['hilight_flag'],
                'approval' => isset($input['approval']) && !empty($input['approval']) ? $input['approval'] : null,
                'approved_by' => null,
                'updated_at' => now(),
                'tags' => isset($input['tags']) && !empty($input['tags']) ? $input['tags'] : null,
                'shipment_price' => $input['shipment_price']
            ]);
        
        if (!$product)  return Response::json([
            'status' => false,
            'message' => 'update Product!'
        ]); 
        
        // remove and new insert product language
        $product_language_remove = DB::table('tb_product_language')->where('product_id', $input['product_id'])->delete();
        $product_price_remove = DB::table('tb_product_price')->where('product_id', $input['product_id'])->delete();
        if ($product_language_remove) {
            // update language
            $products = array();
            $price = array();
            // Language { id: 1 == "thai", id: 2 == "english" }
            foreach(array(['id'=> 1], ['id' => 2]) as $lang) {
                $products[] = array(
                    'product_id' => $input['product_id'],
                    'language_id' => $lang['id'],
                    'name' => isset($input['prod_name'][$lang['id']]) ? $input['prod_name'][$lang['id']] : '',
                    'short_description' => isset($input['short_desc'][$lang['id']]) ? $input['short_desc'][$lang['id']] : '',
                    'description' => isset($input['full_desc'][$lang['id']]) ? $input['full_desc'][$lang['id']] : '',
                    'shipment_description' => isset($input['shipment_detail'][$lang['id']]) ? $input['shipment_detail'][$lang['id']] : '',
                    'special_condition' => isset($input['special_condition'][$lang['id']]) ? $input['special_condition'][$lang['id']] : '',
                );

                $prices[] = array(
                    'product_id' => $input['product_id'],
                    'type_price' => $lang['id'],
                    'operation' => 'baht',
                    'base_price' => isset($input['base_price'][$lang['id']]) ? $input['base_price'][$lang['id']] : '',
                    'front_price' => isset($input['front_price'][$lang['id']]) ? $input['front_price'][$lang['id']] : '',
                    'sale_price' => isset($input['sale_price'][$lang['id']]) ? $input['sale_price'][$lang['id']] : ''
                );
            }

            // Insert Product_language
            $product_language = DB::table('tb_product_language')->insert($products);
            if (!$product_language) return Response::json([
                'status' => false,
                'message' => 'error insert product data'
            ]);

            // Insert Prices
            $prices = DB::table('tb_product_price')->insert($prices);
            if (!$prices) {
                return Response::json([
                    'status' => false,
                    'message' => 'error insert prices'
                ]);
            }
        }

        // remove and new insert product option [color, size]
        $options = DB::table('tb_product_option')->where('product_id', $input['product_id'])->get();
        if ($options) {
            foreach($options as $option) {
                DB::table('tb_product_option_item')->where('option_id', $option->id)->delete();
            }

            $product_option_remove = DB::table('tb_product_option')->where('product_id', $input['product_id'])->delete();
            if ($product_option_remove) {
                // Section option
                if ((isset($input['attributes'])) && !empty($input['attributes'])) {
                    $option = json_decode($input['attributes'], true);
                    $options = array_filter($option, function($callback) {
                        return !empty($callback['option']);
                    });

                    if (!empty($options)) {
                        foreach ($options as $option) {
                            $option_id = DB::table('tb_product_option')->insertGetId([
                                'product_id' => $input['product_id'],
                                'name' => $option['name'],
                                'status' => 1,
                                'created_at' => now()
                            ]);
                            
                            if (!$option_id) return Response::json([
                                'status' => false,
                                'message' => 'error insert prices'
                            ]);

                            foreach($option['option'] as $item) {
                                $item = DB::table('tb_product_option_item')->insertGetId([
                                    'option_id' => $option_id,
                                    'name' => $item
                                ]);

                                // Check insert
                                if (!$item) return Response::json([
                                    'status' => false,
                                    'message' => 'error insert item'
                                ]);
                            }                    
                        }
                    }

                    // print_r($options);
                }
            }
        } 
    
        // response success
        return Response::json([
            'status' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $template = Products::find($id);
        $cat = DB::table('category')->get();
        $pi = DB::table('product_images')->where('prod_id', '=', $id)->get();
        return view('dashboard.product.show', [ 'template' => $template ])->with('category', $cat)->with("product_images", $pi);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pr = Products::find($id);
        $cat = DB::table('category')->get();
        return view('dashboard.product.edit', [ 'pr' => $pr ])->with('category', $cat)->with('status', $this->status);
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');
        $template = Products::find($id);
        if($template){
            //$template->delete();
            $template->status = 'N';
            $template->save();
        }
        $request->session()->flash('message', 'Successfully deleted Product');
        return redirect()->route('product.list');
    }


    public function delete(Request $request)
    {
        $id = $request->input('id');
        $template = Products::find($id);
        if($template){
            //$template->delete();
            $template->status = 'N';
            $template->save();
        }
        #echo $id; exit;
        $request->session()->flash('message', 'Successfully deleted Product');
        return redirect()->route('product.list');
        // $emailTemplates = Products::orderBy('prod_name', 'ASC')->paginate( 20 );
        // return view('dashboard.product.index', ['emailTemplates' => $emailTemplates]);
    }

    public function delete_image(Request $request)
    {
        $id = $request->input('pid');
        $path = $request->input('path');
        $template = Product_images::find($id);
        if($template){
            File::delete($path);
            #Storage::delete($path);
            $template->delete();
            //$template->save();
        }
        #echo $path; exit;
        #echo $id; exit;
        $request->session()->flash('message', 'Successfully deleted Product image');
        //return redirect()->route('product.list');
        return back();
        // $emailTemplates = Products::orderBy('prod_name', 'ASC')->paginate( 20 );
        // return view('dashboard.product.index', ['emailTemplates' => $emailTemplates]);
    }

}
