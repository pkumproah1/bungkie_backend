<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Models\Tb_user_sellers;
use Response;
use Image;


class TempController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * upload_shop
     *
     * @param  mixed $request
     * @return void
     */
    public function upload_shop(Request $request)
    {

        $seller = Tb_user_sellers::where('user_id', Auth()->user()->id)->first();
        if ($seller) {
            $seller_folder = str_replace(" ", "-", $seller['shop_name']);
        }
        if ($request->hasFile('file')) {
            //  Let's do everything here
            if ($request->file('file')->isValid()) {
                $path = '';
                $size = array();
                if ($request->type === "avatar") {
                    $path = "shop/". $seller_folder ."/avatar/";
                    $validated = $request->validate([
                        'file' => 'mimes:jpg,jpeg,png',
                    ]);
                } elseif ($request->type === "slider") {
                    $path = "shop/". $seller_folder ."/slider/";
                    $size['w'] = 1280;
                    $size['h'] = 300;
                    $validated = $request->validate([
                        'file' => 'mimes:jpg,jpeg,png',
                    ]);
                }

                $resize = Image::make($request->file('file'))->encode('jpg');
                $filename = time();
                $save = Storage::disk('public_uploads')->put($path . $filename .'.jpg', $resize->__toString());

                if ($request->type === "slider") {

                    DB::table('tb_user_seller_sliders')->insert([
                        'seller_id' => $seller['id'],
                        'name' => $filename .'.jpg',
                        'rank' => 0,
                        'created_at' => now()
                    ]);
                    
                } else if ($request->type === "avatar") {
                    $product = DB::table('users')
                        ->where('id', Auth()->user()->id)
                        ->update(['avatar' => $filename .'.jpg']);
                }
                return Response::json([
                    'status' => true,
                    'name' => asset('public/statics/images/' . $path .  $filename . '.jpg')
                ]); 
            }
        }
    }

    public function upload_product(Request $request)
    {
        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {

                $path = "products/temp/";
                $validated = $request->validate([
                    'file' => 'mimes:jpg,jpeg,png',
                ]);
            
                // resize = 360
                $resize_m = Image::make($request->file('file'))->resize(360, 360, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg');

                // resize = 1024
                $resize_b = Image::make($request->file('file'))->resize(1024, 1024, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg');

                $filename = time();
                $save = Storage::disk('public_uploads')->put($path . '/m/' . $filename .'.jpg', $resize_m->__toString());
                $save = Storage::disk('public_uploads')->put($path . '/b/' . $filename .'.jpg', $resize_b->__toString());
                return Response::json([
                    'status' => true,
                    'name' => $filename . '.jpg',
                    'url' => asset('public/statics/images/products/temp/m/' . $filename . '.jpg')
                ], 200); 
            }
        }
    }
}
