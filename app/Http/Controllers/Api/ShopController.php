<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function __construct()
    {
       
    }
    
    /**
     * index
     *
     * @param  mixed $request
     * @return json
     */
    public function index(Request $request)
    {
        // ! Data validation
        if (!isset($request->url) && empty($request->url)) return response()->json([
            'status' => false,
            'message' => "Missing data or invalid."
        ], 400);

        // ? feild filter
        $feilds = array('shop_id', 'avatar', 'company_name', 'tax_id', 'slider',
            'brand_name', 'url', 'shop_name', 'shop_description', 'shop_keyword', 'person_name',
            'basic_telephone', 'warehouse_address', 'warehouse_name', 'warehouse_telephone', 'billing_address');
        
        // * Query
        $user = DB::table('users')
            ->join('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'users.id')
            ->where('tb_user_sellers.url', $request->url)
            ->where('tb_user_sellers.status', 1)
            ->whereNull('users.deleted_at')
            ->select(
                'users.id as shop_id', 'tb_user_sellers.id as id', 'users.avatar', 'company_name',
                'tax_id', 'brand_name', 'url', 'shop_name', 'shop_description', 'shop_keyword',
                'person_name', 'basic_telephone', 'warehouse_address', 'warehouse_name',
                'warehouse_telephone', 'billing_address'
            )->first();

        // ! shop not found
        if (!$user) return response()->json([
            'status' => false,
            'message' => "Shop Not Found"
        ], 400);

        // ? Filter data
        $response = array();
        foreach($feilds as $field) {
            if ($field === "avatar") {
                $response['avatar'] = url('/') . $user->avatar;
            } else if ($field === "slider") {
                $sliders = DB::table("tb_user_seller_sliders")
                    ->select('name', 'rank')
                    ->where('seller_id', $user->id)
                    ->orderBy('rank', 'asc')
                    ->get()->toArray();
                $images = array();
                if ($sliders) {
                    foreach($sliders as $slider) {
                        $images[$slider->rank] = url('/') . $slider->name;
                    }
                }
                
                $response['sliders'] = $images;
            } else {
                $response[$field] = isset($user->{$field}) ? $user->{$field} : 'null';
            }
        }

        // * Success 
        return response()->json([
            'status' => 200,
            'data' => $response
        ], 200);
    }
}
