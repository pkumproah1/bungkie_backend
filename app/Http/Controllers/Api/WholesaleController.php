<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Libraries\Api\WholesaleLibrary;
class WholesaleController extends Controller
{
    private $headers;
    private $language;
    private $currency;
    public function __construct()
    {
        //* get all headers
        $this->headers = \Request::header();
        // ? defalut language [1 = th, 2 = en]
        $this->language = 1;
        $this->currency = 1;
        // ! dont have header -> lang
        if (isset($this->headers['language']) && !empty($this->headers['language'])) {
            if ($this->headers['language'][0] === 'th') {
                // ? lang === ภาษาไทย
                $this->lang = 1;
            } else if ($this->headers['language'][0] === 'en') {
                // ? lang === english 
                $this->lang = 2;
            }
        }

        // ! dont have header -> currency
        if (isset($this->headers['currency']) && !empty($this->headers['currency'])) {
            if ($this->headers['currency'][0] === 'thb') {
                // ? currency === thb
                $this->currency = 1;
            } else if ($this->headers['currency'][0] === 'usd') {
                // ? currency === usd 
                $this->currency = 2;
            }
        }
    }
    
    /**
     * save_wholesale
     *
     * @param  mixed $request
     * @return void
     */
    public function save_wholesale(Request $request)
    {
        // ! validate data
        $request->validate([
            'product_id' => 'required|numeric',
            'qty' => 'required|numeric',
            'unit' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'company_name' => 'required',
            'phone' => 'required',
            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
            'address' => 'required',
            'province' => 'required|numeric',
            'city' => 'required|numeric',
            'district' => 'required|numeric',
            'zipcode' => 'required|numeric'
        ]);

        //! input xss
        $input = preg_replace('/<[^>]*>/', '', $request->all());
        
        // * process 
        $wholesale = WholesaleLibrary::inserted($input);
        if($wholesale['status'] === false) return response()->json($wholesale, 400);
        
        // * success
        return response()->json([
            'status' => true
        ], 200);
    }
}
