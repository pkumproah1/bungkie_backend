<?php
namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Libraries\LineLibrary;
class CallbackController extends Controller
{
        
    /**
     * flash_courier
     *
     * @param  mixed $request
     * @return void
     */
    public function flash_courier(Request $request)
    {
        if (!empty($request->all())) {
            DB::table('tb_logs')->insert([
                'class' => 'CALLBACKCONTROLLER',
                'method' => 'FLASH_COURIER',
                'name' => 'เรียกรถ',
                'json_data' => json_encode($request->all(), JSON_UNESCAPED_UNICODE),
                'created_at' => now()
            ]);

            $notify = new LineLibrary([
                'pAnkb0tyekvDiyTFc3Aiu0CaymN11HEb8DM0pNy4SAd',// All
            ]);
            $success_text = "\r\nFLASH COURIER\r\n";
            $success_text.= json_encode($request->all(), JSON_UNESCAPED_UNICODE);
            $notify->send($success_text);
            return response()->json([
                'errorCode' => "000",
                'errorDetail' => "success",
                'state' => true
            ], 200);
        }
    }
    
    /**
     * flash_status
     *
     * @param  mixed $request
     * @return void
     */
    public function flash_status(Request $request)
    {
        if (!empty($request->all())) {
            DB::table('tb_logs')->insert([
                'class' => 'CALLBACKCONTROLLER',
                'method' => 'FLASH_STATUS',
                'name' => 'อัพเดทติดตาม',
                'json_data' => json_encode($request->all(), JSON_UNESCAPED_UNICODE),
                'created_at' => now()
            ]);

            $notify = new LineLibrary([
                'pAnkb0tyekvDiyTFc3Aiu0CaymN11HEb8DM0pNy4SAd',// All
            ]);
            $success_text = "\r\nFLASH STATUS\r\n";
            $success_text.= json_encode($request->all(), JSON_UNESCAPED_UNICODE);
            $notify->send($success_text);
            return response()->json([
                'errorCode' => "000",
                'errorDetail' => "success",
                'state' => true
            ], 200);
        }
    }

}