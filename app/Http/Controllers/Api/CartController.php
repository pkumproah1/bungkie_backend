<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Libraries\Api\CartLibrary;
use App\Libraries\Api\ProductLibrary as Product;

use App\Models\Tb_product as ProductModel;
class CartController extends Controller
{
    private $headers;
    private $language;
    private $currency;
    public function __construct()
    {
        //* get all headers
        $this->headers = \Request::header();
        // ? defalut language [1 = th, 2 = en]
        $this->language = 1;
        $this->currency = 1;
        // ! dont have header -> lang
        if (isset($this->headers['lang']) && !empty($this->headers['lang'])) {
            if (in_array(strtolower($this->headers['lang'][0]), ['th', 'en'])) {
                if (strtolower($this->headers['lang'][0]) === 'th') {
                    // ? lang === ภาษาไทย
                    $this->language = 1;
                } else if (strtolower($this->headers['lang'][0]) === 'en') {
                    // ? lang === english 
                    $this->language = 2;
                }
            }
        }

        // ! currency
        if (isset($this->headers['currency']) && !empty($this->headers['currency'])) {
            if (in_array(strtolower($this->headers['currency'][0]), ['thb', 'usd'])) {
                if (strtolower($this->headers['currency'][0]) === 'thb') {
                    // ? lang === ภาษาไทย
                    $this->currency = 1;
                } else if (strtolower($this->headers['currency'][0]) === 'usd') {
                    // ? lang === english 
                    $this->currency = 2;
                }
            }
        }
    }
    
    /**
     * index
     *
     * @param  mixed $request
     * @return void
     */
    public function index(Request $request)
    {
        $request->validate([
            'type' => 'required|numeric'
        ]);
        
        if (!in_array($request->type, [1,2,3])) return response()->json([
            'status' => false,
            'message' => "Type not supported"
        ], 400);

        $inventory = DB::table('tb_inventory')
            ->where('tb_inventory.user_id', $request->user()->id)
            ->first();

        if (empty($inventory)) return response()->json([
            'status' => false,
            'message' => 'Invalid Inventory'
        ], 200);

        // * render display
        return self::render($inventory->id, $request->type);
    }

    /**
     * remove
     *
     * @param  mixed $request
     * @return void
     */
    public function remove(Request $request)
    {
        $request->validate([
            'productId' => 'required|numeric|min:1|max:11',
            'type' => 'required|numeric'
        ]);
        
        if (!in_array($request->type, [1,2,3])) return response()->json([
            'status' => false,
            'message' => "Type not supported"
        ], 400);

        if ($request->type === "1") {
            $request->validate([
                'qty' => 'required|numeric|min:1|max:20',
            ]);
        }

        // * get old inventory
        $inventory = DB::table('tb_inventory')
            ->where('tb_inventory.user_id', $request->user()->id)
            ->first();

        $inventory_id = null;
        if (empty($inventory)) {
            // ? new user inventory
            $inventory_id = DB::table('tb_inventory')->insertGetId([
                'user_id' => $request->user()->id,
                'created_at' => now()
            ]);
        } else {
            // ? update timestamp for add cart
            $inventory_id = $inventory->id;
            DB::table('tb_inventory')
                ->where('id', $inventory->id)    
                ->update(['updated_at' => now()]);
        }

        // ? find old product not payment
        $item = DB::table('tb_inventory_item')
            ->select('id')
            ->where('inventory_id', $inventory_id)
            ->where('type_id', $request->type)
            ->where('product_id', $request->productId)
            ->where('status', 0)->first();

        if (empty($item)) {
            $inserted = DB::table('tb_inventory_item')->insert([
                'inventory_id' => $inventory_id,
                'type_id' => $request->type,
                'qty' => $request->qty,
                'product_id' => $request->productId,
                'status' => 0,
                'created_at' => now()
            ]);
        } else {
            $updated = DB::table('tb_inventory_item')->update([
                'qty' => DB::raw('qty + ' . $request->qty),
                'updated_at' => now()
            ]);
        }


        $items = DB::table('tb_inventory_item')
            ->select('product_id', 'qty as quantity')
            ->where('inventory_id', $inventory_id)
            ->where('type_id', $request->type)
            ->where('status', 0)->get()->toArray();
        return CartLibrary::calculate($items, $this->language, $this->currency);
    }
    
    /**
     * add
     *
     * @param  mixed $request
     * @return void
     */
    public function add(Request $request)
    {
        $request->validate([
            'productId' => 'required|numeric|min:1',
            'type' => 'required|numeric'
        ]);
    
        if (!in_array($request->type, [1,2,3])) return response()->json([
            'status' => false,
            'message' => "Type not supported"
        ], 400);

        if ($request->type === "1") {
            $request->validate([
                'qty' => 'required|numeric|min:1|max:20',
                'action' => 'required'
            ]);
        }

        if (!in_array($request->action, ["increment", "decrement"])) return response()->json([
            'status' => false,
            'message' => "Action not supported"
        ], 400);

        // * get old inventory
        $inventory = DB::table('tb_inventory')
            ->where('tb_inventory.user_id', $request->user()->id)
            ->first();

        $inventory_id = null;
        if (empty($inventory)) {
            // ? new user inventory
            $inventory_id = DB::table('tb_inventory')->insertGetId([
                'user_id' => $request->user()->id,
                'created_at' => now()
            ]);
        } else {
            // ? update timestamp for add cart
            $inventory_id = $inventory->id;
            DB::table('tb_inventory')
                ->where('id', $inventory->id)    
                ->update(['updated_at' => now()]);
        }

        // ? find old product not payment
        $item = DB::table('tb_inventory_item')
            ->select('id','qty', 'product_id')
            ->where('inventory_id', $inventory_id)
            ->where('type_id', $request->type)
            ->where('product_id', $request->productId)
            ->where('status', 0)->first();

        if ($request->action === "increment") {
            // ? เช็คสินค้าคงเหลือ
            $is_instock = CartLibrary::product_is_in_stock($request->productId, 
                empty($item) ? $request->qty : ($item->qty + $request->qty));
            // ! สินค้าไม่พอ
            if ($is_instock !== true) return response()->json([
                'status' => false,
                'message' => "Product Out of stock"
            ], 400);

            // add product to cart
            if (!empty($item)) {
                // ! increment update
                $updated = DB::table('tb_inventory_item')
                    ->where('inventory_id', $inventory_id)
                    ->where('product_id', $request->productId)
                    ->where('status', 0)
                    ->where('type_id', 1)
                    ->update([
                        'qty' => DB::raw('qty + ' . $request->qty),
                        'updated_at' => now()
                    ]);
            }
        } else {
            // ! การลดจำนวนสินค้าในตะกร้า
            if (!empty($item)) {
                $qty = ($item->qty - $request->qty);
                if ($qty === 0) {
                    // ! remove item in cart
                    DB::table('tb_inventory_item')
                        ->where('inventory_id', $inventory_id)
                        ->where('product_id', $request->productId)
                        ->where('status', 0)
                        ->where('type_id', 1)
                        ->delete();
                } else {
                    // ! decrement update
                    $updated = DB::table('tb_inventory_item')
                        ->where('inventory_id', $inventory_id)
                        ->where('product_id', $request->productId)
                        ->where('status', 0)
                        ->where('type_id', 1)
                        ->update([
                            'qty' => DB::raw('qty - ' . $request->qty),
                            'updated_at' => now()
                        ]);
                }
            } else {
                // * render display
                return self::render($inventory_id, $request->type);
            }
        }

        // * add new product to cart
        if (empty($item)) {
            $inserted = DB::table('tb_inventory_item')->insert([
                'inventory_id' => $inventory_id,
                'type_id' => $request->type,
                'qty' => $request->qty,
                'product_id' => $request->productId,
                'remark' => isset($request->option) && !empty($request->option) ? $request->option : '',
                'status' => 0,
                'created_at' => now()
            ]);
        }

        // * render display
        return self::render($inventory_id, $request->type);
    }
    
    /**
     * render
     *
     * @param  int $id
     * @param  int $type
     * @return void
     */
    private function render($id = null, $type = null)
    {
        if (empty($id) && empty($type)) return response()->json([
            'status' => false
        ], 200);

        // * fetch all inventory by user
        $items = DB::table('tb_inventory_item')
            ->select('product_id', 'qty as quantity')
            ->where('inventory_id', $id)
            ->where('type_id', $type)
            ->where('status', 0)->get()->toArray();
        // * process calculate
        return CartLibrary::calculate($items, $this->language, $this->currency);
    }

    /**
     * calculate_product
     *
     * @param  mixed $request
     * @return void
     */
    public function calculate_product(Request $request)
    {
        $request->validate([
            'products' => 'required|array'
        ]);

        $addressId = ($request->addressId) ? $request->addressId : 0;
        $calc = CartLibrary::calculate_product_and_address([
            'products' => $request->products,
            'currency' => $this->currency,
            'language' => $this->language,
            'addressId' => $addressId
        ], $request->user()->id);
        if (FALSE === $calc['status']) return response()->json($calc, 400);
        return response()->json([
            'status' => true,
            'data' => $calc['data']
        ], 200);
    }

    
    
}
