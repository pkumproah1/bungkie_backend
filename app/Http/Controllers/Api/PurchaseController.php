<?php
namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Libraries\Api\CartLibrary;
use App\Libraries\Api\PaymentLibrary;
use App\User;
use App\Models\Tb_order as OrderModel;
use App\Models\Tb_order_item as OrderItemModel;
use App\Libraries\LineLibrary;
class PurchaseController extends Controller
{
    private $headers;
    private $lang;
    private $currency;
    public function __construct()
    {
        //* get all headers
        $this->headers = \Request::header();
        // ? defalut language [1 = th, 2 = en]
        $this->lang = 1;
        $this->currency = 1;
        // ! dont have header -> lang
        if (isset($this->headers['lang']) && !empty($this->headers['lang'])) {
            if (in_array(strtolower($this->headers['lang'][0]), ['th', 'en'])) {
                if (strtolower($this->headers['lang'][0]) === 'th') {
                    // ? lang === ภาษาไทย
                    $this->lang = 1;
                } else if (strtolower($this->headers['lang'][0]) === 'en') {
                    // ? lang === english 
                    $this->lang = 2;
                }
            }
        }

        // ! currency
        if (isset($this->headers['currency']) && !empty($this->headers['currency'])) {
            if (in_array(strtolower($this->headers['currency'][0]), ['thb', 'usd'])) {
                if (strtolower($this->headers['currency'][0]) === 'thb') {
                    // ? lang === ภาษาไทย
                    $this->currency = 1;
                } else if (strtolower($this->headers['currency'][0]) === 'usd') {
                    // ? lang === english 
                    $this->currency = 2;
                }
            }
        }
    }
    
    /**
     * find_order
     *
     * @param  mixed $request
     * @return void
     */
    public function find_order(Request $request)
    {
        // ! validate
        $request->validate([
            'referenceId' => 'required'
        ]);
        // * find order by reference Id
        $order = OrderModel::find_order_purchase_by_ref($request->referenceId);
        // ! ไม่มี order
        if (!$order) {
            return response()->json([
                'status' => false,
                'message' => 'Missing Order'
            ], 400);
        }

        $products = OrderItemModel::find_product_by_order_id($order->id);
        $items = array();
        $fields = array('product_id', 'name', 'path', 'shop_name', 'front_price');
        if ($products) {
            foreach ($products as $item) {
                $row = array();
                foreach($fields as $field) {
                    if ($field === "path") {
                        // ? ดึงรูปทั้งหมดมาและเพิ่ม prefix baseurl 
                        $img = ($item->is_new == 1)
                            ? url('/') . '/public/statics/images/' . 'products/'. $item->product_id . '/m/' . $item->path
                            : url('/') . '/public/statics/images/' . $item->path;
                        $row['images'] = $img;
                    } elseif($field === "name") {
                        // ? จัดการชื่อสินค้าและสร้าง URL
                        $row['name'] = isset($item->name) ? $item->name : 'NO PRODUCT';
                        $row['url'] = mb_convert_encoding(strtolower(clean($item->name)), 'UTF-8', 'UTF-8');
                    } elseif($field === "shop_name") {
                        $row['shop'] = array(
                            'shop_name' => $item->shop_name,
                            'url' => mb_convert_encoding(strtolower(clean($item->url)), 'UTF-8', 'UTF-8')
                        );
                    } elseif ($field === "front_price") {
                        $row['price'] = array(
                            'discount' => ($item->qty > 1) 
                                ? number_format(($item->discount * $item->qty), 2) 
                                : number_format($item->discount, 2),
                            'price' => number_format($item->sum_price, 2),
                            'shipping' => $item->shipping_price,
                            'shipping_provider' => "Flash Express"
                        );
                    } else {
                        // ? Dont Filter
                        $row[$field] = isset($item->{$field}) ? $item->{$field} : 'null';
                    }
                }
                //* new data in result
                $items[] = $row;
            }
        }
        
        $data = array(
            'payment_code' => $order->payment_status,
            'reference_id' => $order->reference_id,
            'amount' => number_format($order->amount, 2),
            'currency' => ($order->currency == 764) ? '฿' : '$',
            'transaction_ref' => $order->transaction_ref,
            'approval_code' => $order->approval_code,
            'transaction_datetime' => $order->transaction_datetime,
            'payment_channel' => _2c2p_channel($order->payment_channel, $this->lang),
            'payment_status' => _2c2p_status($order->payment_status, $this->lang),
            'channel_response_desc' => $order->channel_response_desc,
            'created' => $order->created_at,
            'products' => $items,
            'customer' => array(
                'name' => $order->display_name,
                'email' => $order->email,
                'phone' => $order->tel,
                'address' => $order->address. ' ' . $order->province_name_th . ' ' . $order->amphur_name_th . ' ' . $order->district_name_th . ' ' . $order->zipcode 
            )
        );

        return response()->json([
            'status' => true,
            'data' => $data
        ], 200);

    }
	
	/**
     * find_order_by_user_id
     *
     * @param  mixed $request
     * @return void
     */
    public function find_order_by_user_id(Request $request)
    {
		
		if (!$request->user()) return response()->json([
            'status' => false,
            'message' => "Invalid User"
        ], 400);
        // ? get profile by authen
        $user = $request->user();

        // * find order by reference Id
        $orders = OrderModel::find_order_purchase_by_user_id($user->id);
        // return array('x' => $order);
        // ! ไม่มี order
        if (!$orders) {
            return response()->json([
                'status' => false,
                'message' => 'No Order'
            ], 400);
        }
		
		$data = array();
		foreach($orders as $order){
			$products = OrderItemModel::find_product_by_order_id($order->id);
			$items = array();
			$fields = array('product_id', 'name', 'path', 'shop_name', 'front_price');
			if ($products) {
				foreach ($products as $item) {
					$row = array();
					foreach($fields as $field) {
						if ($field === "path") {
							// ? ดึงรูปทั้งหมดมาและเพิ่ม prefix baseurl 
							$img = ($item->is_new == 1)
								? url('/') . '/public/statics/images/' . 'products/'. $item->product_id . '/m/' . $item->path
								: url('/') . '/public/statics/images/' . $item->path;
							$row['images'] = $img;
						} elseif($field === "name") {
							// ? จัดการชื่อสินค้าและสร้าง URL
							$row['name'] = isset($item->name) ? $item->name : 'NO PRODUCT';
							$row['url'] = mb_convert_encoding(strtolower(clean($item->name)), 'UTF-8', 'UTF-8');
						} elseif($field === "shop_name") {
							$row['shop'] = array(
								'shop_name' => $item->shop_name,
								'url' => mb_convert_encoding(strtolower(clean($item->url)), 'UTF-8', 'UTF-8')
							);
						} elseif ($field === "front_price") {
							$row['price'] = array(
								'discount' => ($item->qty > 1) 
									? number_format(($item->discount * $item->qty), 2) 
									: number_format($item->discount, 2),
								'price' => number_format($item->sum_price, 2),
								'qty' => $item->qty,
								'front_price' => $item->front_price,
								'shipping' => $item->shipping_price,
								'shipping_provider' => "Flash Express"
							);
						} else {
							// ? Dont Filter
							$row[$field] = isset($item->{$field}) ? $item->{$field} : 'null';
						}
					}
					//* new data in result
					$items[] = $row;
				}
			}
			
			$data[] = array(
				'order_id' => $order->id,
				'payment_code' => $order->payment_status,
				'reference_id' => $order->reference_id,
				'amount' => number_format($order->amount, 2),
				'currency' => ($order->currency == 764) ? '฿' : '$',
				'transaction_ref' => $order->transaction_ref,
				'approval_code' => $order->approval_code,
				'transaction_datetime' => $order->transaction_datetime,
				'payment_channel' => _2c2p_channel($order->payment_channel, $this->lang),
				'payment_status' => _2c2p_status($order->payment_status, $this->lang),
				'channel_response_desc' => $order->channel_response_desc,
				'created' => $order->created_at,
				'products' => $items,
				'customer' => array(
					'name' => $order->display_name,
					'email' => $order->email,
					'phone' => $order->tel,
					'address' => $order->address. ' ' . $order->province_name_th . ' ' . $order->amphur_name_th . ' ' . $order->district_name_th . ' ' . $order->zipcode 
				)
			);
		}
        
		if(count($data) > 0){
			return response()->json([
				'status' => true,
				'data' => $data
			], 200);
		}else{
			return response()->json([
                'status' => false,
                'message' => 'No Order'
            ], 400);
		}

    }

    /**
     * index
     *
     * @param  mixed $request
     * @return void
     */
    public function index(Request $request)
    {
        // ! validate
        $request->validate([
            'products' => 'required|array',
            'addressId' => 'required|numeric',
            'paymentMethod' => 'required',
        ]);
        
        if (empty($request->user()->email)) return response()->json([
            'status' => false,
            'message' => "Missing email address."
        ], 200);
        if (empty($request->user()->tel)) return response()->json([
            'status' => false,
            'message' => "Missing Phone number."
        ], 200);
        // * get old inventory
        $inventory = DB::table('tb_inventory')
            ->where('tb_inventory.user_id', $request->user()->id)
            ->first();
        // * find items
        $items = DB::table('tb_inventory_item')
            ->select('id as item_id','product_id', 'qty as quantity', 'remark')
            ->where('inventory_id', $inventory->id)
            ->whereIn('product_id', $request->products)
            ->where('type_id', 1)
            ->where('status', 0)->get()->toArray();
        // ! calc items price
        $render = CartLibrary::calculate($items, $this->lang, $this->currency);
        if ($render['status'] === false) return response()->json([
            'status' => false,
            'message' => "not calculate"
        ], 500);
        
        // ? find product in stock
        $products = $render['data']['mini']['InStock'];
        // ! has products
        if ($products) {
            // * new reference (order id)
            $reference = "BK" . date('Ymdhi') . rand(000,999); 
            // * insert and get order id
            $order_id = DB::table('tb_order')->insertGetId([
                'user_id' => $request->user()->id,
                'reference_id' => $reference,
                'status' => 0,
                'payment_status' => 0,
                'amount' => floatval(preg_replace('/[^\d.]/', '', $render['data']['totalPrice'])),
                'currency' => ($this->currency === 1) ? 764 : 840,
                'created_at' => now()
            ]);
 
            $batch = array();
            // ? loop items
            foreach($products as $product) {
                $discount = floatval(preg_replace('/[^\d.]/', '', $product['price']['discount']));
                $batch[] = array(
                    'order_id' => $order_id,
                    'product_id' => $product['productId'],
                    'shop_id' => $product['shopId'],
                    'inventory_item_id' => $product['inventoryItemId'],
                    'front_price' => floatval(preg_replace('/[^\d.]/', '', $product['price']['after'])),
                    'sum_price' => floatval(preg_replace('/[^\d.]/', '', $product['price']['total'])),
                    'discount' => ($discount / $product['price']['quantity']),
                    'qty' => $product['price']['quantity'],
                    'status' => 0,
                    'created_at' => now()
                );
            }
            // * Insert order items batch
            DB::table('tb_order_item')->insert($batch);
            // ? calc 
            $calc = CartLibrary::calculate_product_and_address([
                'products' => $request->products,
                'currency' => $this->currency,
                'language' => $this->lang,
                'addressId' => $request->addressId
            ], $request->user()->id, $order_id);
            if (FALSE === $calc['status']) return response()->json($calc, 400);    
            
            // * update amout for shipping
            DB::table('tb_order')->where(['id' => $order_id])
                ->update(['amount' => floatval(preg_replace('/[^\d.]/', '', $calc['data']['total']))]);
            // * create Payment with 2C2P
            $response = PaymentLibrary::_2c2p([
                'currency' => ($this->currency === 1) ? 'thb' : 'usd',
                'amount' => str_pad(filter_var($calc['data']['total'], FILTER_SANITIZE_NUMBER_INT), 12, '0', STR_PAD_LEFT),
                'order_id' => $reference,
                'product_name' => $reference
            ]);

            // * loop update
            foreach($products as $product) {
                // ! remove stock by order
                $updateStock = DB::table('tb_product')
                    ->where('id', $product['productId'])
                    ->update([
                        'in_stock' => DB::raw('in_stock - ' . $product['price']['quantity'])
                    ]);
                // ! update item in inventory 
                $updated = DB::table('tb_inventory_item')
                    ->where('inventory_id', $inventory->id)
                    ->where('product_id', $product['productId'])
                    ->where('status', 0)
                    ->where('type_id', 1)
                    ->update([
                        'status' => 1,
                        'updated_at' => now()
                    ]);
            }
            // * set response
            return response()->json($response, 200);
        }
        
        // ! set response error
        return response()->json([ 
            'status' => false,
        ], 400);
    }
          
    /**
     * ! cronjob service cancel order
     *
     * @param  mixed $request
     * @return void
     */
    public function cancel_order(Request $request)
    {
        return OrderModel::cancal_order();
    }

    /**
     * callback2c2p
     *
     * @param  mixed $request
     * @return void
     */
    public function callback2c2p(Request $request)
    {        
        // * Insert Logs 
        if (!empty($request)) {
            if (!empty($request->hash_value)) {
                $secret = config('constants.2c2p.secret_key');
                // ! เช็ค Response ด้วยการเข้ารหัส
                $hash = $request->version . $request->request_timestamp . $request->merchant_id . $request->order_id . 
                $request->invoice_no . $request->currency . $request->amount . $request->transaction_ref . $request->approval_code . 
                $request->eci . $request->transaction_datetime . $request->payment_channel . $request->payment_status . 
                $request->channel_response_code . $request->channel_response_desc . $request->masked_pan . 
                $request->stored_card_unique_id . $request->backend_invoice . $request->paid_channel . $request->paid_agent . 
                $request->recurring_unique_id . $request->user_defined_1 . $request->user_defined_2 . $request->user_defined_3 . 
                $request->user_defined_4 . $request->user_defined_5 . $request->browser_info . $request->ippPeriod . 
                $request->ippInterestType . $request->ippInterestRate . $request->ippMerchantAbsorbRate . $request->payment_scheme .
                $request->process_by . $request->sub_merchant_list;
                $hash_hmac = hash_hmac('sha256', $hash, $secret, false); 

                // ! เช็ค hash ต้องถูกต้อง
                if (strtoupper($hash_hmac) == $request->hash_value) {
                    PaymentLibrary::process($request);
                } else {
                    // ! logs 
                    DB::table('tb_logs')->insert([
                        'class' => strtoupper('PurchaseController'),
                        'method' => strtoupper('callback2c2p'),
                        'name' => 'Hash ไม่ถูก',
                        'json_data' => json_encode($request->all()),
                        'created_at' => now(),
                    ]);  
                    // !notify
                    $notify = new LineLibrary();
                    $notify->send("Hash ไม่ถูก", [
                        'type' => false,
                        'class' => "PurchaseController",
                        'method' => "callback2c2p"
                    ]);
                }
            }
        }
    }
}
