<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Libraries\Api\ProductLibrary;

class FlashController extends Controller
{
    public function __construct()
    {
        // $this->middleware('istoken');
    }

    public function calculate_shipping(Request $request)
    {
        // ? validate
        $request->validate([
            'products' => 'required|array',
            'addressId' => 'required|numeric'
        ]);

        return ProductLibrary::calculate_shipping([
            'addressId' => $request->addressId,
            'products' => $request->products
        ], $request->user()->id);
    }
}
