<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Tb_contact_us as ContactModel;
class ContactController extends Controller
{
    private $headers;
    private $lang;
    public function __construct()
    {
        //* get all headers
        $this->headers = \Request::header();
        // ? defalut language [1 = th, 2 = en]
        $this->lang = 2;
        // ! dont have header -> lang
        if (isset($this->headers['lang']) && !empty($this->headers['lang'])) {
            if ($this->headers['lang'][0] === 'th') {
                // ? lang === ภาษาไทย
                $this->lang = 1;
            } else if ($this->headers['lang'][0] === 'en') {
                // ? lang === english 
                $this->lang = 2;
            }
        }
    }
    /**
     * index
     *
     * @param  mixed $request
     * @return json
     */
    public function savedata(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'subject' => 'required',
            'display_name' => 'required',
            'message' => 'required'
        ]);

        $id = ContactModel::InsertGetId([
            'email' => $request->email,
            'subject' => $request->subject,
            'display_name' => $request->display_name,
            'message' => $request->message,
            'status' => 0,
            'created_at' => now()
        ]);
        
        if (empty($id)) return response()->json([
            'status' => false,
            'message' => 'Please try again later.'
        ], 400);

        return response()->json([
            'status' => true,
            'message' => 'success'
        ], 200);
    }
}
