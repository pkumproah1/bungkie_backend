<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    private $headers;
    private $lang;
    public function __construct()
    {
        //* get all headers
        $this->headers = \Request::header();
        // ? defalut language [1 = th, 2 = en]
        $this->lang = 2;
        // ! dont have header -> lang
        if (isset($this->headers['lang']) && !empty($this->headers['lang'])) {
            if ($this->headers['lang'][0] === 'th') {
                // ? lang === ภาษาไทย
                $this->lang = 1;
            } else if ($this->headers['lang'][0] === 'en') {
                // ? lang === english 
                $this->lang = 2;
            }
        }
    }

    
    /**
     * index
     *
     * @param  mixed $request
     * @return json
     */
    public function index(Request $request)
    {

        // $main_cate = $this->lists_main(78);
        // $sub = $main_cate->id;

        // return array(
        //     'main_name' => $main_cate->cat_name_en,
        //     'sub_category' => $this->lists($sub)
        // );

        return response()->json([
            'status' => true,
            'data' => $this->lists()
        ], 200);
    }
    
    private function lists_main($id)
    {
        //* query category data
        $categorys = DB::table("category")
            // ->where('sub_cat_id', $parent_id)
            ->where('id', $id)
            ->where('cat_status', 1)
            ->select('id', 'cat_name_en', 'cat_name_th', 'cat_main_image', 'sub_cat_id')
            ->first();
        
        // * response data
        return $categorys;
    }

    private function lists2($parent_id = 0)
    {
        //* query category data
        $categorys = DB::table("category")
            ->where('sub_cat_id', $parent_id)
            ->where('cat_status', 1)
            ->select('id', 'cat_name_en', 'cat_name_th', 'cat_main_image', 'sub_cat_id')
            ->get()->toArray();
        $response = array();
        // ! check category
        if ($categorys) {
            // ? for loop data
            foreach ($categorys as $key => $category) {
                // ! check sub cat id === parent id
                if ($category->sub_cat_id === $parent_id) {
                    // * set data response
                    $response[] = array(
                        'id' => $category->id,
                        'name' => ($this->lang === 1) ? $category->cat_name_th : $category->cat_name_en,
                        'url' => strtolower(clean($category->cat_name_en)),
                        'image' => 'http://bungkie.com/admin/storage/app/' . $category->cat_main_image,
                        'parent' => $this->lists($category->id) // ? this callback
                    );
                }
            }
        }
        // * response data
        return $response;
    }

    /**
     * lists
     *
     * @param  int $parent_id
     * @return response_data
     */
    private function lists($parent_id = 0)
    {
        //* query category data
        $categorys = DB::table("category")
            ->where('sub_cat_id', $parent_id)
            ->where('cat_status', 1)
            ->select('id', 'cat_name_en', 'cat_name_th', 'cat_main_image', 'sub_cat_id')
            ->get()->toArray();
        $response = array();
        // ! check category
        if ($categorys) {
            // ? for loop data
            foreach ($categorys as $key => $category) {
                // ! check sub cat id === parent id
                if ($category->sub_cat_id === $parent_id) {
                    // * set data response
                    $response[] = array(
                        'id' => $category->id,
                        'name' => ($this->lang === 1) ? $category->cat_name_th : $category->cat_name_en,
                        'url' => strtolower(clean($category->cat_name_en)),
                        'image' => 'http://bungkie.com/admin/storage/app/' . $category->cat_main_image,
                        'parent' => $this->lists($category->id) // ? this callback
                    );
                }
            }
        }
        // * response data
        return $response;
    }
}
