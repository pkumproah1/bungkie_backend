<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Libraries\Api\CartLibrary;

class PaymentController extends Controller
{
    private $headers;
    private $lang;
    public function __construct()
    {
        //* get all headers
        $this->headers = \Request::header();
        // ? defalut language [1 = th, 2 = en]
        $this->lang = 1;
        // ! dont have header -> lang
        if (isset($this->headers['lang']) && !empty($this->headers['lang'])) {
            if ($this->headers['lang'][0] === 'th') {
                // ? lang === ภาษาไทย
                $this->lang = 1;
            } else if ($this->headers['lang'][0] === 'en') {
                // ? lang === english 
                $this->lang = 2;
            }
        }
    }
}
