<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $headers;
    private $language;
    public function __construct()
    {
        //* get all headers
        $this->headers = \Request::header();
        // ? defalut language [1 = th, 2 = en]
        $this->language = 1;
        // ! dont have header -> lang
        if (isset($this->headers['lang']) && !empty($this->headers['lang'])) {
            if ($this->headers['lang'][0] === 'th') {
                // ? lang === ภาษาไทย
                $this->language = 1;
            } else if ($this->headers['lang'][0] === 'en') {
                // ? lang === english 
                $this->language = 2;
            }
        }
    }
    
    /**
     * index
     *
     * @param  mixed $request
     * @return json
     */
    public function index(Request $request)
    {
        return response()->json([
            'status' => true,
            'data' => $this->lists()
        ], 200);
    }
    
    /**
     * lists
     *
     * @param  int $parent_id
     * @return response_data
     */
    private function lists($parent_id = 0)
    {
        //* query category data
        $categorys = DB::table("tb_product_category")
            ->join('tb_product_category_language', 'tb_product_category_language.category_id', '=', 'tb_product_category.id')
            ->where(['language_id' => $this->language, 'parent_id' => $parent_id, 'status' => 1])
            ->select('tb_product_category.id', 'name', 'image', 'parent_id', 'flash_track', 'created_at')
            ->get()->toArray();
        $response = array();
        // ! check category
        if ($categorys) {
            // ? for loop data
            foreach ($categorys as $key => $category) {
                // ! check sub cat id === parent id
                if ($category->parent_id === $parent_id) {
                    // * set data response
                    $image = empty($category->image) 
                        ? url('/') . '/public/assets/img/default.png' 
                        : url('/') . '/public/statics/images/' . $category->image;
                    $response[] = array(
                        'id' => $category->id,
                        'name' => $category->name,
                        'url' => mb_convert_encoding(strtolower(clean($category->name)), 'UTF-8', 'UTF-8'),
                        'image' => $image,
                        'parent' => $this->lists($category->id) // ? this callback
                    );
                }
            }
        }
        // * response data
        return $response;
    }
}
