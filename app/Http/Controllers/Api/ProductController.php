<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Tb_product_option;
use App\Models\Tb_product;
use App\Libraries\Api\ProductLibrary;

class ProductController extends Controller
{
    // ? default object
    private $default = array(
        'limit' => 60,
        'offset' => 0
    );
    private $image_path;
    private $headers;
    private $lang;
    private $currency;
    public function __construct()
    {
        //* get all headers
        $this->headers = \Request::header();
        $this->image_path = url('/') . '/public/statics/images/';
        // ? defalut language [1 = th, 2 = en]
        $this->lang = 1;
        $this->currency = 1;
        // ! dont have header -> lang
        if (isset($this->headers['lang']) && !empty($this->headers['lang'])) {
            if ($this->headers['lang'][0] === 'th') {
                // ? lang === ภาษาไทย
                $this->lang = 1;
            } else if ($this->headers['lang'][0] === 'en') {
                // ? lang === english 
                $this->lang = 2;
            }
        }

        if (isset($this->headers['currency']) && !empty($this->headers['currency'])) {
            if ($this->headers['currency'][0] === 'thb') {
                // ? currency === thb
                $this->currency = 1;
            } else if ($this->headers['currency'][0] === 'usd') {
                // ? currency === usd 
                $this->currency = 2;
            }
        }
    }

    /**
     * listhome
     *
     * @param  mixed $request
     * @return void
     */
    public function listhome(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'typeId' => 'required'
        ]);
        if (!in_array($request->type,['category'])) {
            return response()->json([
                'status' => false,
                'message' => 'Section not supported'
            ], 400);
        }
        
        return response()->json([
            'status' => true,
            'data' => ProductLibrary::homepage($request, $this->lang, $this->currency)
        ],200);
    }
 
    /**
     * more_list
     *
     * @param  mixed $request
     * @return void
     */
    public function more_list(Request $request)
    {   
        $request->validate([
            'section' => 'required'
        ]);

        if (!in_array($request->section,['new-arrival', 'hot-deals', 'recommend'])) {
            return response()->json([
                'status' => false,
                'message' => 'Section not supported'
            ], 400);
        }
        
        $list = ProductLibrary::_paginate([
            'type' => $request->section,
            'limit' => $request->limit,
            'offset' => $request->offset
        ], $this->lang, $this->currency);

        if (empty($list['data'])) return response()->json([
            'status' => false,
            'message' => 'Missing data or invalid.'
        ], 400);

        return response()->json([
            'status' => true,
            'data' => $list['data']
        ],200);
    }

    /**
     *
     * @param  mixed $request
     * @param  string $type [result, count]
     * @return Products
     */
    private function process_paginate($request, $type = "result")
    {
        //? select fields
        $selected = array(
            'tb_product.id as product_id', 'in_stock', 'name', 'product_type',
            'base_price','front_price','sale_price', 'operation','path', 'is_new'
        );
        //* query product
        $products = DB::table("tb_product")
            ->where('tb_product_language.language_id', $this->lang)
            ->where('tb_product_price.type_price', $this->currency)
            ->where('tb_product.status', 1)
            ->where('tb_product.approval', 1)
            ->whereNull('tb_product.deleted_at')
            ->where('tb_product_images.rank', 1);
            
        // TODO การค้นหาแบบแบ่ง type ยังต้องรอทำ type เพิ่ม
        if ((isset($request->type) && !empty($request->type)) && (isset($request->typeId) && !empty($request->typeId))) {
            switch ($request->type) {
                // * type by category id
                case 'category':
                    if (is_numeric($request->typeId)) {
                        $products = $products
                            ->join('tb_product_category_relation', 'tb_product_category_relation.product_id', '=', 'tb_product.id')
                            ->where('tb_product_category_relation.category_id', $request->typeId);
                    } else {

                        $ids = array_reduce(explode(',', $request->typeId), function($callback, $item) {
                            $callback[] = $item;
                            return $callback;
                        }, []);

                        $products = $products
                            ->join('tb_product_category_relation', 'tb_product_category_relation.product_id', '=', 'tb_product.id')
                            ->whereIn('tb_product_category_relation.category_id', $ids);
                    }
                    break;
                // * type by seller id
                case 'seller':
                    if (is_numeric($request->typeId)) 
                        $products = $products->where('tb_product.owner', $request->typeId);
                    break;
                case 'wishlist':
                    if (is_numeric($request->typeId)) 
                        $products = $products->where('tb_product.owner', $request->typeId);
                    break;
                default:
                    break;
            }
        }
        //? ถ้ามีการส่งข้อมูลค้นหา
        if ((isset($request->search) && !empty($request->search))) {
            //* GROUP where
            $products = $products->where(function($query) use($request) {
                // ? group start (ถ้าจะเพิ่ม field ในการค้นหาให้เพิ่มข้างในนี้)
                $query->where('tb_product_language.name', 'like', "%{$request->search}%")
                    ->orWhere('tb_product_language.short_description', 'like', "%{$request->search}%")
                    ->orWhere('tb_product.tags', 'like', "%{$request->search}%")
                    ->orWhere('tb_product_language.description', 'like', "%{$request->search}%")
                    ->orWhere('tb_product_language.special_condition', 'like', "%{$request->search}%");
                // ? group end
            });
        }
        // ? ถ้า type == result (ข้อมูล product)
        if ($type === "result") {
            // ! เช็คว่ามีการส่ง params limit && offset
            // ? ถ้าไม่มีการส่งค่า limit มา ระบบจะใช้ค่า default แทน
            $products = $products->limit($this->default['limit'])->offset($this->default['offset']);
            if ((isset($request->limit) && !empty($request->limit)) || (isset($request->offset) && !empty($request->offset))) {
                // ! limit && offset จะต้องเป็นตัวเลขเท่านั้นถึงจะถูกคำนวน
                if (is_numeric($request->limit) && is_numeric($request->offset)) {
                    // ? limit มากว่าค่า default จะใช้ค่า default แทน
                    if ($request->limit >= $this->default['limit']) {
                        // ? ถ้าส่งค่า limit มาเกินกว่าค่า default จะให้ดึงค่า default มาใช้งาน
                        $products = $products->limit($this->default['limit']);
                    } else {
                        //* ใช้ค่าตาม Params
                        $products = $products->limit($request->limit)->offset($request->offset);
                    }
                }
            }
            // ? set result = toArray();
            $result = "toArray";
        // ? ถ้า type == count (จะนับข้อมูลสินค้าทั้งหมดที่ query ได้โดยจะไม่ถูก limit เอาไว้ใช้สำหรับทำ pagination)
        } else {
            //! remove old selected
            unset($selected);
            // ? add new selected by COUNT
            $selected = array('tb_product.id');
            // ? set result = count();
            $result = "count";
        }
        //* query next step
        $products = $products
            ->select($selected)
            ->join("tb_product_language", "tb_product_language.product_id", "=", "tb_product.id")
            ->join("tb_product_price", "tb_product_price.product_id", "=", "tb_product.id")
            ->join("tb_product_images", "tb_product_images.product_id", "=", "tb_product.id")
            ->orderBy('tb_product.created_at', 'desc')
            ->get()->{$result}();
        // * return products
        return $products;
    }

    /**
     * paginate
     *
     * @param  mixed $request
     * @return void
     */
    public function paginate(Request $request)
    {
        // ? get all product by codition request
        $products = $this->process_paginate($request, "result");
        // ? get count all product
        $total = $this->process_paginate($request, "count");
        $response = array();
        // * has products
        if ($products) {
            //* ดึง Rating ทั้งหมดมา Filter ตาม Product
            $ratings = $this->get_rating();
            //? Fixed field for filter
            $fields = array('product_id', 'in_stock', 'name', 'base_price', 'path', 'score','product_type');
            //* แปลงข้อมูลให้พร้อมใช้งานในรูปแบบของ API
            foreach ($products as $key => $prod) {
                $row = array();
                //* Filter field
                foreach ($fields as $field) {
                    if ($field === "path") {
                        // ? ดึงรูปทั้งหมดมาและเพิ่ม prefix baseurl 
                        $img = ($prod->is_new == 1)
                            ? $this->image_path . 'products/'. $prod->product_id . '/m/' . $prod->path
                            : $this->image_path . $prod->path;
                        $row['images'] = $img;
                    } elseif ($field === "product_type") {
                        $type = array(1 => 'BOTH', 2 => 'B2C', 3 => 'B2B');
                        $row['product_type'] = isset($type[$prod->product_type]) ? $type[$prod->product_type] : null;
                    } elseif($field === "name") {
                        // ? จัดการชื่อสินค้าและสร้าง URL
                        $row['name'] = isset($prod->name) ? $prod->name : 'NO PRODUCT';
                        $row['url'] = mb_convert_encoding(strtolower(clean($prod->name)), 'UTF-8', 'UTF-8');
                    } elseif($field === "score") {  
                        //* คำนวณ Rating
                        //? Find ratings by product_id
                        $rating = array_values(array_filter($ratings, function($rat) use($prod) {
                            return ($rat->product_id == $prod->product_id);
                        }));
                        // ? check rating
                        if ($rating) {
                            //* has rating
                            $score;
                            $number;
                            //* loop ratings
                            foreach ($rating as $r) {
                                $score = $r->score;
                                $number = $r->number;
                            }
                            $star = ($score / $number);
                            //* response rating
                            $row['rating'] = array(
                                'star' => number_format($star),
                                'score' => $score,
                                'count' => $number
                            );
                        } else {
                            // * dont has rating response "NULL"
                            $row['rating'] = null; 
                        }
                    } elseif($field === "base_price") {
                        //* นำนวณเงิน คำนวณโปรโมชั่น
                        $price = array();
                        $prefix = ($this->currency == 1) ? 'THB' : 'USD';
                        if ($prod->operation == "baht") {
                            // ? Discount by "baht"
                            $price['price'] = number_format(($prod->front_price - $prod->sale_price),2);
                            $price['base'] = number_format($prod->front_price, 2);
                            $price['discount'] = number_format($prod->sale_price, 2);
                            $price['display_percent'] = @number_format(($prod->sale_price / $prod->front_price) * 100);
                        } else if ($prod->operation == "percent") {
                            // ? Discount by "percent"
                            $price['price'] = number_format(((100 - $prod->sale_price) / 100 * $prod->front_price),2);
                            $price['base'] = number_format($prod->front_price,2);
                            $price['discount'] = number_format($prod->sale_price,2). '%';
                            $price['display_percent'] = @number_format(($prod->sale_price / $prod->front_price) * 100);
                        } else {
                            // ? No Discount
                            $price['price'] = number_format($prod->front_price,2);
                            $price['base'] = number_format($prod->front_price,2);
                            $price['discount'] = null;
                            $price['display_percent'] = null;
                        }
                        //? Response New Price
                        $row['currency'] = $price;
                    } else {
                        // ? Dont Filter
                        $row[$field] = isset($prod->{$field}) ? $prod->{$field} : 'null';
                    }
                }
                //* new data in result
                $response[] = $row;
            }
        }
        // ! Missing Product
        if (empty($response)) return response()->json([
            'status' => false,
            'message' => "Missing or invalid."
        ], 400);

        //* Response Data "TRUE"
        return array(
            'status' => true,
            'data' => $response,
            'total' => $total,
            'params' => $request->all()
        );
    }

    /**
     * findby
     *
     * @param  mixed $request
     * @return void
     */
    public function findby(Request $request)
    {
        // tb_product_view
        DB::table("tb_product_view")->insert([
            'ipaddress' => $request->getClientIp(),
            'user_agent' => $request->server('HTTP_USER_AGENT'),
            'product_id' => $request->product_id,
            'created_at' => now()
        ]);
        // ! check validate "product_id"
        if (isset($request->product_id) && !empty($request->product_id) 
        && !is_numeric($request->product_id)) return response()->json([
            'status' => false,
            'message' => "Missing Product ID"
        ], 400);

        //? Fixed field for filter
        $fields = array(
            'product_id', 'brand', 'sku',  'in_stock', 'tags', 
            'name', 'short_description', 'description', 'special_condition',
            'base_price', 'image','score', 'seller','option','product_type','category_name'
        );
        
        // ? Select Field by Database
        $selected = array(
            'tb_product.id as product_id', 'owner', 'brand', 'sku', 'in_stock', 'tags',
            'name', 'short_description', 'description', 'special_condition','product_type',
            'base_price', 'front_price', 'sale_price','tb_product_price.operation as op', 'users.avatar', 'tb_user_sellers.shop_name', 'tb_user_sellers.url as shop_link', 'tb_user_sellers.shop_keyword'
        );

        //* Query Product by Filter
        $product = DB::table("tb_product")
            ->where('tb_product_language.language_id', $this->lang)
            ->where('tb_product_price.type_price', $this->currency)
            ->where('tb_product.status', 1)
            ->where('tb_product.approval', 1)
            ->whereNull('tb_product.deleted_at')
            ->where('tb_product.id', $request->product_id)
            ->select($selected)
            ->join("tb_product_language", "tb_product_language.product_id", "=", "tb_product.id")
            ->join("tb_product_price", "tb_product_price.product_id", "=", "tb_product.id")
            ->leftJoin("users", "users.id", "=", "tb_product.owner")
            ->leftJoin("tb_user_sellers", "tb_user_sellers.user_id", "=", "users.id")
            ->first();
    
        $response = array();
        //! is missing data or invalid or error
        if (empty($product)) return response()
            ->json(['status' => false, 'message' => 'Missing or invalid.'], 400);

        //* ดึง Rating ทั้งหมดมา Filter ตาม Product
        $ratings = $this->get_rating();
        //* แปลงข้อมูลให้พร้อมใช้งานในรูปแบบของ API
        foreach ($fields as $field) {
            if ($field === "image") {
                // ? ดึงรูปทั้งหมดมาและเพิ่ม prefix baseurl 
                $images = DB::table("tb_product_images")
                    ->where('product_id', $product->product_id)
                    ->orderBy('rank', 'asc')
                    ->get()->toArray();
                $image = array();
                foreach ($images as $key => $img) {
                    $img = ($img->is_new === 1)
                        ? $this->image_path . 'products/'. $product->product_id . '/b/' . $img->path
                        : $this->image_path . $img->path;
                    $response['images'][$key] = $img;
                }
            } elseif ($field === "product_type") {
                $type = array(1 => 'BOTH', 2 => 'B2C', 3 => 'B2B');
                $response['product_type'] = isset($type[$product->product_type]) ? $type[$product->product_type] : null;
            } elseif($field === "category_name") {
                // ? หมวดหมู่
                $category = DB::table("tb_product_category_relation")
                    ->join('tb_product_category_language', 'tb_product_category_language.category_id', '=', 'tb_product_category_relation.category_id')
                    ->where('tb_product_category_relation.product_id', $product->product_id)
                    ->where('tb_product_category_language.language_id', $this->lang)
                    ->orderBy('tb_product_category_relation.id', 'desc')->first();
                $response['category'] = array(
                    'url' => 'categories/' . mb_convert_encoding(strtolower(clean($category->name)), 'UTF-8', 'UTF-8'),
                    'name' => isset($category->name) && !empty($category->name) ? $category->name : '',
                    'category_id' => $category->category_id
                );
            } elseif($field === "option") {
                // ? Option เช่น Color 
                $options = new Tb_product_option();
                $options = $options->get_options($product->product_id);
                $response['option'] = $options ? $options['data'] : null;
            } elseif($field === "seller") {
                // ? ดึงข้อมูลของผู้ขายมาแสดง
                $response['seller'] = array(
                    'logo' => url('/') .'/'. $product->avatar,
                    'name' => $product->shop_name,
                    'link' => $product->shop_link,
                    'shop_keyword' => $product->shop_keyword
                );
            } elseif($field === "score") {  
                //* คำนวณ Rating
                //? Find ratings by product_id
                $rating = array_values(array_filter($ratings, function($rat) use($product) {
                    return ($rat->product_id == $product->product_id);
                }));
                // ? check rating
                if ($rating) {
                    //* has rating
                    $score;
                    $number;
                    //* loop ratings
                    foreach ($rating as $r) {
                        $score = $r->score;
                        $number = $r->number;
                    }
                    $star = ($score / $number);
                    //* response rating
                    $response['rating'] = array(
                        'star' => number_format($star),
                        'score' => $score,
                        'count' => $number,
                        'percent' => number_format((($score / $number) / 5) * 100)
                    );
                } else {
                    // * dont has rating response "NULL"
                    $response['rating'] = null; 
                }
            } elseif($field === "base_price") {
                //* นำนวณเงิน คำนวณโปรโมชั่น
                $price = array();
                $prefix = ($this->currency == 1) ? 'THB' : 'USD';
                if ($product->sale_price > 0) {
                    if ($product->op == "baht") {
                        // ? Discount by "baht"
                        $price['price'] = number_format(($product->front_price - $product->sale_price), 2);
                        $price['base'] = number_format($product->front_price, 2);
                        $price['discount'] = number_format($product->sale_price, 2);
                        $price['display_percent'] = number_format(($product->sale_price / $product->front_price) * 100);
                    } else if ($product->op == "percent") {
                        // ? Discount by "Percent"
                        $price['price'] = number_format(((100 - $product->sale_price) / 100 * $product->front_price), 2);
                        $price['base'] = number_format($product->front_price, 2);
                        $price['discount'] = number_format($product->sale_price, 2);
                        $price['display_percent'] = number_format(($product->sale_price / $product->front_price) * 100);
                    } 
                } else {
                    $price['price'] = number_format($product->front_price, 2);
                    $price['base'] = number_format($product->front_price, 2);
                    $price['discount'] = null;
                    $price['display_percent'] = null;
                }
                //? Response New Price
                $response['currency'] = $price;
            } else {
                // ? Dont Filter
                $response[$field] = $product->{$field};
            }
        }
        //* Response Data "TRUE"
        return response()->json([
            'status' => true, 
            'data' => $response
        ], 200);
    }

    /**
     * get_rating
     *
     * @return void
     */
    private function get_rating()
    {
        // * Query Rating
        $ratings = DB::table("tb_product_rating")
            ->select('product_id', DB::raw('SUM(tb_product_rating.score) as score'), DB::raw('COUNT(tb_product_rating.product_id) as number'))
            ->groupBy('tb_product_rating.product_id')
            ->get()->toArray();
        //* return ratings
        return $ratings;
    }

    /**
     * findby
     *
     * @param  mixed $request
     * @return void
     */
    public function tracking(Request $request)
    {
        $request->validate([
            'reference_id' => 'required'
        ]);

        $tracking = ProductLibrary::tracking($request->reference_id);
        if ($tracking['status'] == false) return response()->json($tracking, 400);

        return response()->json($tracking, 200);

    }
}
