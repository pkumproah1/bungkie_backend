<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Personal_access_tokens;
use Illuminate\Support\Str;
use App\Libraries\Api\MemberLibrary;
use Carbon\Carbon;

class MemberController extends Controller
{
    /**
     * register_newsletter
     *
     * @param  mixed $request
     * @return void
     */
    public function register_newsletter(Request $request)
    {
        // ! validate
        $request->validate([
            'email' => 'required|email',
        ]);
        
        //* process
        $result = MemberLibrary::newsletter_inserted($request->all());
        if ($result['status'] === false) return response()->json($result, 400);
        return response()->json($result, 200);
    }

    /**
     * index
     *
     * @param  mixed $request
     * @return json
     */
    public function signup(Request $request)
    {
        // ! if not has request email field
        if (!isset($request->email) && empty($request->email)) return response()->json([
            'status' => false,
            'message' => 'Invalid field email.'    
        ], 400);

        // ! check duplicate by email 
        $check_duplicate = User::where('email', $request->email)->first();
        if ($check_duplicate) return response()->json([
            'status' => false,
            'message' => 'email is duplicate'
        ], 400);

        // ? insert new user
        $user = DB::table('users')->insertGetId([
            'fname' => isset($request->firstName) && !empty($request->firstName) ? $request->firstName : null,
            'lname' => isset($request->lastName) && !empty($request->lastName) ? $request->lastName : null,
            'display_name' => ((isset($request->firstName) && !empty($request->firstName)) 
                || (isset($request->lastName) && !empty($request->lastName))) ? $request->firstName. " " . $request->lastName : null,
            'email' => isset($request->email) && !empty($request->email) ? $request->email : null,
            'tel' => isset($request->phone) && !empty($request->phone) ? $request->phone : null,
            'password' => isset($request->password) && !empty($request->password) ? Hash::make($request->password) : null,
            'menuroles' => 'user',
            'status' => 'Y',
            'PV_id' => 0,
            'point' => 0,
            'wallet' => 0,
            'created_at' => now()
        ]);
        
        // ! if not insert
        if (empty($user)) return response()->json([
            'status' => false,
            'message' => 'Bad Request'
        ], 500);

        // * insert user type
        DB::table('users_type')->insert([
            'user_id' => $user,
            'type' => "local",
            'social_id' => null
        ]);
        
        // ? find user
        $user = User::find($user);
        // ? add role
        $user->assignRole('user');

        // ! register fill
        if (!$user) return response()->json([
            'status' => false,
            'message' => 'Register Fail'
        ], 500);

        // ? Job queue for email
        DB::table('tb_cronjob_email')->insert([
            'service' => 'register',
            'data' => json_encode($user),
            'error_logs' => null,
            'status' => 0,
            'created_at' => now()
        ]);
        
        // * Response
        return self::genarate_token($request, $user->id);
    }
        
    /**
     * signin
     *
     * @param  mixed $request
     * @return void
     */
    public function signin(Request $request)
    {
        // ! if not has request email field
        if (!isset($request->type) && empty($request->type)) return response()->json([
            'status' => false,
            'message' => 'Invalid field type.'    
        ], 400);

        // ! check duplicate by email 
        $user = DB::table('users')
            ->select('users.id')
            ->leftJoin('users_type', 'users_type.user_id', '=', 'users.id')
            ->where('users.status', 'Y')
            ->where('users_type.type', $request->type);
            
        // ? if type === "local"
        if ($request->type === "local") {
            $user = $user->where('email', $request->email)->first();
            // ! if not user
            if (empty($user)) return response()->json([
                'status' => false,
                'message' => "Username not found"
            ], 400);

            // ? find user
            $user = User::find($user->id);
            // ! if not user
            if (!$user) return response()->json([
                'status' => false,
                'message' => "Username not found"
            ], 400);

            // ! hash password match
            if (!Hash::check($request->password, $user->password)) {
                return response()->json([
                    'status' => false,
                    'message' => "Password is incorrect"
                ], 400);
            }

            if (isset($request->remember) && !empty($request->remember)) {
                // * save remember
                $remember = ($request->remember === "on") ? 1 : 0;
                DB::table('users_type')
                    ->where('user_id', $user->id)
                    ->update(['remember' => $remember]);
            }

            // * Response
            return self::genarate_token($request, $user->id);
            

        } else { // ? if type === "social"
            // ! type not = facebook or google
            if (($request->type !== "facebook") && ($request->type !== "google")) return response()->json([
                'status' => false,
                'message' => "Type not suported"
            ], 400);

            // ! Required ID
            if (!isset($request->id) && empty($request->id)) return response()->json([
                'status' => false,
                'message' => 'Invalid field ID.'    
            ], 400);

            // ! Required numeric ID
            if (!is_numeric($request->id)) return response()->json([
                'status' => false,
                'message' => 'ID not supported'    
            ], 400);

            // ! Required NAME
            if (!isset($request->name) && empty($request->name)) return response()->json([
                'status' => false,
                'message' => 'Invalid field NAME.'    
            ], 400);

        
            $user = $user->where('users_type.social_id', $request->id)->first();
            $display_name = explode(" ", $request->name);
            
            if (empty($user)) {
                // * Insert new data
                $user = DB::table('users')->insertGetId([
                    'fname' => $display_name[0],
                    'lname' => $display_name[1],
                    'display_name' => $request->name,
                    'email' => isset($request->email) && !empty($request->email) ? $request->email : null,
                    'avatar' => isset($request->avatar) && !empty($request->avatar) ? $request->avatar : null,
                    'password' => 'null',
                    'menuroles' => 'user',
                    'status' => 'Y',
                    'PV_id' => 0,
                    'point' => 0,
                    'wallet' => 0,
                    'tel' => isset($request->phone) && !empty($request->phone) ? $request->phone : null,
                    'created_at' => now()
                ]);

                // ! if not insert
                if (empty($user)) return response()->json([
                    'status' => false,
                    'message' => 'Bad Request'
                ], 500);

                // * insert user type
                DB::table('users_type')->insert([
                    'user_id' => $user,
                    'type' => $request->type,
                    'social_id' => $request->id,
                    'remember' => 0
                ]);
                
                // ? find user
                $user = User::find($user);
                // ? add role
                $user->assignRole('user');

                // ! register fill
                if (!$user) return response()->json([
                    'status' => false,
                    'message' => 'Register Fail'
                ], 500);

                // ? Job queue for email
                if (isset($request->email) && !empty($request->email)) {
                    DB::table('tb_cronjob_email')->insert([
                        'service' => 'register',
                        'data' => json_encode($user),
                        'error_logs' => null,
                        'status' => 0,
                        'created_at' => now()
                    ]);
                }

                // * Response
                return self::genarate_token($request, $user->id);
            } else {
                // ? old account
                $user = User::find($user->id);
                // dd($user->id);
                if (empty($user->tel) || empty($user->email)) {
                    DB::table('users')->where('id', $user->id)->update([
                        'tel' => $request->phone,
                        'email' => $request->email
                    ]);
                }
                // * Response
                return self::genarate_token($request, $user->id);
            }
        }
    }
    
    
    /**
     * profile
     *
     * @param  mixed $request
     * @return void
     */
    public function profile(Request $request) 
    {
        if (!$request->user()) return response()->json([
            'status' => false,
            'message' => "Invalid User"
        ], 400);
        // ? get profile by authen
        $user = $request->user();
        // ? find account type
        $type = DB::table('users_type')
            ->select('type')
            ->where('user_id', $user->id)->first();
        // * response
        return response()->json([
            'status' => true,
            'data' => array(
                'profile' => array(
                    'display_name' => $user->display_name,
                    'first_name' => $user->fname,
                    'last_name' => $user->lname,
                    'email' => $user->email,
                    'phone' => $user->tel,
                    'birthday' => $user->birthday,
                    'avatar' => ($user->avatar) ? $user->avatar : null
                ),
                'type' => $type->type
            )
        ], 200);
    }
    
    /**
     * address
     *
     * @param  mixed $request
     * @return void
     */
    public function address(Request $request)
    {
        if (!$request->user()) return response()->json([
            'status' => false,
            'message' => "Invalid User"
        ], 400);
        // ? get profile by authen
        $user = $request->user();

        // ? find account type
        $address = DB::table('user_address')
            ->select('user_address.id as address_id', 'is_default', 'name', 'phone', 'address', 'zipcode', 'province_name_th', 'amphur_name_th', 'district_name_th')
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'user_address.province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'user_address.city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'user_address.district')
            ->where('user_address.user_id', $user->id)->get()->toArray();
        if (empty($address)) {
            return response()->json([
                'status' => false,
                'message' => 'empty address'
            ], 200);
        }
        $response = array();
        foreach ($address as $key => $addr) {
            $response[] = array(
                'address_id' => $addr->address_id,
                'is_default' => ($addr->is_default === 1) ? 'true' : 'false',
                'display_name' => $addr->name,
                'phone' => $addr->phone,
                'address' => $addr->address,
                'province' => trim($addr->province_name_th),
                'city' => trim($addr->amphur_name_th),
                'district' => trim($addr->district_name_th),
                'zipcode' => $addr->zipcode
            );
        }
        
        // * response
        return response()->json([
            'status' => true,
            'data' => $response
        ], 200);

    }
        
    /**
     * save_address
     *
     * @param  mixed $request
     * @return void
     */
    public function save_address(Request $request)
    {
        if (!$request->user()) return response()->json([
            'status' => false,
            'message' => "Invalid User"
        ], 400);

        $request->validate([
            'default' => 'required|numeric',
            'name' => 'required',
            'phone' => 'required|numeric',
            'address' => 'required',
            'province' => 'required|numeric',
            'city' => 'required|numeric',
            'district' => 'required|numeric',
            'zipcode' => 'required|numeric',
        ]);
        //! validate phone number
        $phone = strlen($request->phone);
        if (!in_array($phone, [9,10])) return response()->json([
            'status' => false,
            'message' => "Phone format not supported."
        ], 400);
        
        
        // ? get profile by authen
        $user = $request->user();
        $id = DB::table('user_address')->insertGetId([
            'is_default' => $request->default,
            'user_id' => $user->id,
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'province' => $request->province,
            'city' => $request->city,
            'district' => $request->district,
            'zipcode' => $request->zipcode,
            'created_at' => now()
        ]);

        if ($request->default == 1) {
            DB::table('user_address')
                ->where('user_id', $user->id)
                ->where('id', '!=', $id)
                ->update(['is_default' => 0]);
        }

        // * response
        return response()->json([
            'status' => true,
            'data' => array(
                'addressId' => $id
            )
        ], 200);
    }
    
    /**
     * edit_address
     *
     * @param  mixed $request
     * @return void
     */
    public function edit_address(Request $request)
    {
        if (!$request->user()) return response()->json([
            'status' => false,
            'message' => "Invalid User"
        ], 400);

        $request->validate([
            'address_id' => 'required|numeric',
            'default' => 'required|numeric',
            'name' => 'required',
            'phone' => 'required|numeric',
            'address' => 'required',
            'province' => 'required|numeric',
            'city' => 'required|numeric',
            'district' => 'required|numeric',
            'zipcode' => 'required|numeric',
        ]);
        //! validate phone number
        $phone = strlen($request->phone);
        if (!in_array($phone, [9,10])) return response()->json([
            'status' => false,
            'message' => "Phone format not supported."
        ], 400);
        // ? get profile by authen
        $user = $request->user();
        $updated = DB::table('user_address')->where([
            'id' => $request->address_id,
            'user_id' => $user->id
        ])->update([
            'is_default' => $request->default,
            'user_id' => $user->id,
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address,
            'province' => $request->province,
            'city' => $request->city,
            'district' => $request->district,
            'zipcode' => $request->zipcode,
            'updated_at' => now()
        ]);

        if ($request->default == 1) {
            DB::table('user_address')
                ->where('user_id', $user->id)
                ->where('id', '!=', $request->address_id)
                ->update(['is_default' => 0]);
        }

        // * response
        return response()->json([
            'status' => true,
            'data' => array(
                'addressId' => $request->address_id
            )
        ], 200);

    }
    
    /**
     * remove_address
     *
     * @param  mixed $request
     * @return void
     */
    public function remove_address(Request $request)
    {
        if (!$request->user()) return response()->json([
            'status' => false,
            'message' => "Invalid User"
        ], 400);

        $request->validate([
            'address_id' => 'required|numeric'
        ]);
        
        // ? get profile by authen
        $user = $request->user();
        $address = DB::table('user_address')->where([
            'id' => $request->address_id,
            'user_id' => $user->id
        ])->first();
        
        $next = DB::table('user_address')
            ->where('id', '<', $request->address_id)
            ->where('user_id', $user->id)
            ->max('id');

        if ($address->is_default == '1') {
            DB::table('user_address')
                ->where('user_id', $user->id)
                ->where('id', '!=', $next)
                ->update(['is_default' => 0]);
        }

        DB::table('user_address')->where([
            'id' => $request->address_id,
            'user_id' => $user->id
        ])->delete();

        // * response
        return response()->json([
            'status' => true,
            'data' => array(
                'addressId' => $next
            )
        ], 200);

    }

    /**
     * recovery
     *
     * @param  mixed $request
     * @return void
     */
    public function recovery(Request $request)
    {
        // ! validate data
        $request->validate([
            'email' => 'required|min:6|regex:/(.+)@(.+)\.(.+)/i'
        ]);
        
        // * find user by email
        $user = User::where('email', $request->email)->first();
        // ! is empty
        if (empty($user)) {
            return response()->json([
                'status' => false,
                'message' => "Invalid User"
            ], 400);
        }
        // * find old data
        $old_data = DB::table('users_password_reset')
            ->where('sended', 0)
            ->where('email', $request->email)->first();
        $token = null;
        if ($old_data) {
            $token = $old_data->token;
        } else {
            // ? random string 
            $token = Str::random(64);
            // * Insert data
            DB::table('users_password_reset')->insert([
                'email' => $request->email,
                'sended' => 0,
                'token' => $token,
                'status' => 0,
                'created_at' => now()
            ]);
        }

        // * response
        return response()->json([
            'status' => true,
            'data' => array(
                'email' => $request->email,
                'token' => $token,
                'createdAt' => time()
            )
        ], 200);
    }
    
    /**
     * recovery_password
     *
     * @param  mixed $request
     * @return void
     */
    public function recovery_password(Request $request)
    {
        // ! validate data
        $request->validate([
            'token' => 'required|min:64|max:64',
            'password' => 'required|min:6|max:32'
        ]);

        // * find user by token
        $token = DB::table('users_password_reset')
            ->select('id', 'email', 'token')
            // ->where(DB::raw('created_at >= NOW() + INTERVAL 1 DAY'))
            ->where('status', 0)
            ->where('sended', 1)
            ->where('token', $request->token)->first();

        // ! token
        if (empty($token)) return response()->json([
            'status' => false,
            'message' => 'Invalid Token'
        ], 400);

        // ! user
        $user = User::where('email', $token->email)->first();
        if (empty($user)) return response()->json([
            'status' => false,
            'message' => 'Invalid User'
        ], 400);

        // * update password
        DB::table('users')->where('id', $user->id)
            ->update(['password' => Hash::make($request->password)]);
        // * update status 
        DB::table('users_password_reset')->where('id', $token->id)
            ->update(['status' => 1]);
        
        // * response
        return response()->json([
            'status' => true,
            'message' => 'success'
        ], 200);
    }
    
    /**
     * update_password
     *
     * @param  mixed $request
     * @return void
     */
    public function update_password(Request $request)
    {
        // ! validate data
        $request->validate([
            'passwordOld' => 'required|min:6|max:32',
            'password' => 'required|min:6|max:32'
        ]);

        // ! user
        $user = DB::table('users')->select('email','password')
            ->where('status', 'Y')->where('email', $request->user()->email)->first();
        if (empty($user)) return response()->json([
            'status' => false,
            'message' => 'Invalid User'
        ], 400);

        // ! password old incorrect
        if (!Hash::check($request->passwordOld, $user->password)) {
            return response()->json([
                'status' => false,
                'message' => "Password Old is incorrect"
            ], 400);
        }

        // * update new password
        DB::table('users')->where('id', $request->user()->id)
            ->update(['password' => Hash::make($request->password)]);

        // * response
        return response()->json([
            'status' => true,
            'message' => 'success'
        ], 200);
    }
    
    /**
     * update_profile
     *
     * @param  mixed $request
     * @return void
     */
    public function update_profile(Request $request)
    {
        // ! validate data
        $request->validate([
            'fname' => 'required|min:6|max:32',
            'lname' => 'required|min:6|max:32',
            'displayName' => 'required|min:6|max:32',
            'phone' => 'required|numeric',
            'birthday' => 'required|date'
        ]);

        // ! user
        $user = DB::table('users')->select('id')
            ->where('status', 'Y')->where('email', $request->user()->email)->first();
        if (empty($user)) return response()->json([
            'status' => false,
            'message' => 'Invalid User'
        ], 400);

        // * update new password
        DB::table('users')->where('id', $request->user()->id)
            ->update([
                'fname' => isset($request->fname) && !empty($request->fname) ? $request->fname : null,
                'lname' => isset($request->lname) && !empty($request->lname) ? $request->lname : null,
                'display_name' => isset($request->displayName) && !empty($request->displayName) ? $request->displayName : null,
                'tel' => isset($request->phone) && !empty($request->phone) ? $request->phone : null,
                'birthday' => isset($request->birthday) && !empty($request->birthday) ? $request->birthday : null,
                'updated_at' => now()
            ]);

        // * response
        return response()->json([
            'status' => true,
            'message' => 'success'
        ], 200);
    }

    /**
     * genarate_token
     *
     * @param  mixed $request
     * @param  mixed $user
     * @return void
     */
    private function genarate_token($request, $id)
    {
        // ? User agent
        $user = User::find($id);
        $user_agent = ($request->header('User-Agent')) ? $request->header('User-Agent') : null;
        if (empty($user_agent)) return response()->json([
            'status' => false,
            'message' => 'Not supported agent'
        ], 500);
        if (isset($user->tokens) && !empty($user->tokens)) {
            // ? recheck device duplicate
            foreach($user->tokens as $key => $token) {
                if ($token->name === $user_agent) {
                    Personal_access_tokens::find($token->id)->delete();
                }
            }
        }
        
        // ? gen token
        $user_token = $user->createToken($user_agent);
        // ! if not user token
        if (!$user_token) return response()->json([
            'status' => false,
            'message' => "Invalid Token"
        ], 500);

        // * response
        return response()->json([
            'status' => true,
            'data' => array(
                'profile' => array(
                    'display_name' => $user->display_name,
                    'first_name' => $user->fname,
                    'last_name' => $user->lname,
                    'email' => $user->email,
                    'phone' => $user->tel,
                    'birthday' => $user->birthday,
                    'avatar' => ($user->avatar) ? $user->avatar : null
                ),
                'token' => explode('|', $user_token->plainTextToken)[1],
                'type' => isset($request->type) && !empty($request->type) ?  $request->type : 'local'
            )
        ], 200);
    }
}
