<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    private $headers;
    private $language;
    public function __construct()
    {
        //* get all headers
        $this->headers = \Request::header();
        // ? defalut language [1 = th, 2 = en]
        $this->language = 1;
        // ! dont have header -> lang
        if (isset($this->headers['lang']) && !empty($this->headers['lang'])) {
            if ($this->headers['lang'][0] === 'th') {
                // ? lang === ภาษาไทย
                $this->language = 1;
            } else if ($this->headers['lang'][0] === 'en') {
                // ? lang === english 
                $this->language = 2;
            }
        }
    }
        
    /**
     * get_province
     *
     * @return void
     */
    public function get_province()
    {
        $province = DB::table('tb_location_province')
            ->select('province_code', ($this->language === 1) ? 'province_name_th as name' : 'province_name_en as name')
            ->get()->toArray();
    
        return response()->json([
            'status' => true,
            'data' => $province
        ], 200);
    }
    
}
