<?php
// Seller Controller 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Libraries\SellerLibrary;

class SellerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');

    }
    
    /**
     * @return view
     */
    public function create()
    {   
        $sellerLib = new SellerLibrary();
        $bankings = $sellerLib->get_banks();
        $province = $sellerLib->get_province('th');
        $bank_lists = ($bankings['status'] === true) ? $bankings['data'] : null;
        return view('dashboard.seller.create', compact('bank_lists', 'province'));
    }

    
    /**
     * inserted
     *
     * @param  mixed $request
     * @return void
     */
    public function inserted(Request $request)
    {
        return self::saved($request->all());
    }
    
    /**
     * saved
     *
     * @param  mixed $input
     * @return void
     */
    private function saved($input = array())
    {
        $user = User::create([
            'fname' => isset($input['company_name']) && !empty($input['company_name']) ? $input['company_name'] : null,
            'lname' => $input['brand_name'],
            'display_name' => $input['shop_name'],
            'email' => $input['username'],
            'password' => Hash::make($input['password']),
            'menuroles' => 'seller',
            'status' => isset($input['approve']) && !empty($input['approve']) ? $input['approve'] : 'N',
            'PV_id' => 0,
            'point' => 0,
            'wallet' => 0,
            'tel' => $input["basic_tel"],
            'created_at' => now()
        ]);
        
        
        $seller = DB::table('tb_user_sellers')->insertGetId([
            'user_id' => $user->id,
            'tax_id' => $input["tax_number"],
            'company_name' => $input["company_name"],
            'brand_name' => $input['brand_name'],
            'person_name' => $input["person_name"],
            'shop_name' => $input["shop_name"],
            'basic_telephone' => $input["basic_tel"],
            'email' => $input['email'],
            'warehouse_name' => $input["warehouse_name"],
            'warehouse_telephone' => $input["warehouse_tel"],
            'warehouse_address' => $input["warehouse_address"],
            'warehouse_province' => $input["warehouse_province"],
            'warehouse_city' => $input["warehouse_city"],
            'warehouse_district' => $input["warehouse_district"],
            'warehouse_zipcode' => $input["warehouse_zipcode"],
            'billing_address' => $input["billing_address"],
            'billing_province' => $input["billing_province"],
            'billing_city' => $input["billing_city"],
            'billing_district' => $input["billing_district"],
            'billing_zipcode' => $input["billing_zipcode"],
            'bank_id' => $input['bank'],
            'account_number' => $input['bank_number'],
            'account_name' => $input["bank_accout"],
            'payment_cycle' => $input["payment_cycle"],
            'status' => ($input['status'] == 'N') ? 0 : 1,
            'free_shipping' => ($input["shipping"] == 'N') ? 0 : 1
        ]);

        $user->assignRole('seller');
        $user->save();
        
        // * update menu roles
        DB::table('users')->where('id', $user->id)
            ->update(['menuroles' => 'seller']);

        if (!$seller) return Response::json([
            'status' => false,
            'message' => 'error insert general data'
        ]);

        // response success
        return Response::json([
            'status' => true
        ]);
    }
    
    /**
     * check_username
     *
     * @param  mixed $request
     * @return void
     */
    public function check_username(Request $request)
    {
        $input = $request->all();
        $request->validate([
            'username' => 'required'
        ]);

        $sellerLib = new SellerLibrary();
        $seller = $sellerLib->check_username_duplicate($input['username'], 'seller');
        if ($seller['status'] !== true) {
            return Response::json([
                'status' => false
            ]);
        }
        return Response::json([
            'status' => true
        ]);
    }
}
