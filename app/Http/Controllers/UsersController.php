<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public $status = array(
        'Y' => 'Active', 'N' => 'Inactive');

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $you = auth()->user();
        //$users = User::paginate(20);
        $users = User::Where('menuroles', 'like', '%admin')->Orderby('fname', 'ASC')->paginate(20);
        return view('dashboard.user.index', compact('users', 'you'))->with('status', $this->status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('dashboard.user.show', compact( 'user' ))->with('status', $this->status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('dashboard.user.edit', compact('user'));
    }

    public function create()
    {        
        return view('dashboard.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $template = new Users();
        // $template->cat_name_en = $request->input('cat_name_en');
        // $template->cat_name_th = $request->input('cat_name_th');
        // $template->cat_desc_en = $request->input('cat_desc_en');
        // $template->cat_desc_th = $request->input('cat_desc_th');
        // $template->sub_cat_id = $request->input('sub_cat_id');
        // $template->main_cat = $request->input('main_cat');
        // $template->cat_status = $request->input('cat_status');
        // $template->cat_type = "1";
        // $template->cat_level = "1";
        // $template->icon_path = "";
        // $template->cat_main_image = $request->file('cat_main_image')->store('category');
        $template->save();
        $request->session()->flash('message', 'Successfully created Category');
        return redirect()->route('user.list');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'       => 'required|min:1|max:256',
            'email'      => 'required|email|max:256'
        ]);
        $user = User::find($id);
        $user->name       = $request->input('name');
        $user->email      = $request->input('email');
        $user->save();
        $request->session()->flash('message', 'Successfully updated user');
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user){
            $user->delete();
        }
        return redirect()->route('user.index');
    }

    public function delete(Request $request)
    {
        $id = $request->input('id');
        $act = $request->input('act');
        $template = User::find($id);
        if($template){
            //$template->delete();
            $template->status = 'N';
            $template->updated_at = date('Y-m-d H:i:s');
            $template->deleted_at = date('Y-m-d H:i:s');
            $template->save();
        }
        $request->session()->flash('message', 'Successful');
        return redirect()->route('user.list');
    }
}
