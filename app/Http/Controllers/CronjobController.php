<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use App\User;

use App\Libraries\OrderLibrary;
class CronjobController extends Controller
{
    /**
     * recovery
     *
     * @return void
     */
    public function recovery()
    {
        // * get all
        $recoverys = DB::table("users_password_reset")
            ->select('id', 'email', 'token', 'created_at')
            ->limit(5)
            ->orderBy('id', 'asc')
            ->where('sended', 0)->get()->toArray();
        // ! not null
        if ($recoverys) {
            // ! loop
            foreach($recoverys as $recovery) {
                // * find user
                $user = User::where('email', $recovery->email)->first();
                // ? email setup and send
                \Mail::to($recovery->email)->send(new Mailer([
                    'to' => $recovery->email,
                    'from' => 'info@bungkie.com',
                    'subject' => 'Bungkie: Password changed',
                    'title' => 'Bungkie: Password changed',
                    'name' => $user->display_name,
                    'token' => $recovery->token,
                    'layout' => 'reset-password'
                ]));

                // ! error
                if (\Mail::failures()) {
                    // ? update false
                    DB::table("users_password_reset")
                        ->where('id', $recovery->id)
                        ->update(['sended' => 2]);
                } else {
                    // ? update success
                    DB::table("users_password_reset")
                        ->where('id', $recovery->id)
                        ->update(['sended' => 1]);
                }
            }
        }

        self::emails();
    }
    
    /**
     * emails
     *
     * @return void
     */
    public function emails()
    {
        // * get all email
        $email = DB::table("tb_cronjob_email")
            ->limit(5)
            ->orderBy('id', 'asc')
            ->where('status', 0)->get()->toArray();
        //! email not empty 
        if ($email) {
            // * loop send email
            foreach($email as $e) {
                if ($e->email) {
                    \Mail::to($e->email)->send(new Mailer([
                        'to' => $e->email,
                        'from' => 'info@bungkie.com',
                        'subject' => $e->title,
                        'title' => $e->title,
                        'data' => json_decode($e->data),
                        'layout' => $e->service
                    ]));

                    // ! error
                    if (\Mail::failures()) {
                        // ? update false
                        DB::table("tb_cronjob_email")
                            ->where('id', $e->id)
                            ->update(['status' => 2, 'updated_at' => now()]);
                    } else {
                        // ? update success
                        DB::table("tb_cronjob_email")
                            ->where('id', $e->id)
                            ->update(['status' => 1,'updated_at' => now()]);
                    }
                } else {
                    DB::table("tb_cronjob_email")
                        ->where('id', $e->id)
                        ->update(['status' => 2,'updated_at' => now()]);
                }
            }
        }
    }
    
    /**
     * flash_notify
     *
     * @param  mixed $request
     * @return void
     */
    public function flash_notify(Request $request)
    {
        OrderLibrary::shipping_notify();
    }
}


