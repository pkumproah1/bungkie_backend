<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FaqCategory;

class FaqCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function index()
    {
        $faq_cats = FaqCategory::paginate(20);
        return view('dashboard.faqCategory.show', compact('faq_cats'))->with('faq', $faq_cats);
    }

    public function list($id)
    {
        $faq = FaqCategory::find($id);
        return view('dashboard.faq.show', compact('faq'));
    }

    public function create()
    {
        return view('dashboard.faq.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'topic'    => 'required|min:1|max:100',
            'desc' => 'required|min:1',
        ]);
        $faq = new FaqCategory();
        $faq->topic = $request->input('topic');
        $faq->desc = $request->input('desc');
        $faq->tag = $request->input('tag');
        $faq->cat_id = $request->input('cat_id');

        $faq->save();

        $request->session()->flash('message', ' FAQ Created Successfully.');
        return redirect()->route('faq.index');
    }

    public function edit($id)
    {
        $faq = Faq::findOrFail($id);

        return view('dashboard.faq.edit', compact('faq'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'topic'    => 'required|min:1|max:100',
            'desc' => 'required|min:1'
        ]);

        $faq = FaqCategory::find($request->id);
        $faq->topic = $request->input('topic');
        $faq->desc = $request->input('desc');
        $faq->tag = $request->input('tag');

        $faq->save();

        $request->session()->flash('message', 'FAQ edited successfully.');

        return redirect()->route('faq.index');
    }

    public function delete($id, Request $request)
    {
        $faq = FaqCategory::find($id);
        if($faq)
        {
            $faq->delete();
        }

        $request->session()->flash('message', 'Successfully deleted');
        return redirect()->route('faq.list');
    }


}

?>
