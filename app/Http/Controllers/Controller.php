<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Libraries\NotifyLibrary;
use Illuminate\Routing\Controller as BaseController;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $data = array();
    private $template = "website.";
   
    public function view($view = null)
    {
        if (empty($view)) return abort(404);
        self::get_all_notify();
        return view($this->template . $view, $this->data);
    }

    public function get_all_notify()
    {
        $notify = NotifyLibrary::notifys();
        $this->data['notify'] = $notify;
    }
}
