<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Purchases;
use Illuminate\Support\Facades\DB;
use Response;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = Purchases::join('users', 'users.id', '=', 'purchase.user_id')
            ->select(DB::raw('SUM(purchase.sale_price) as price, SUM(purchase.quantity) as quantity, purchase.session_id, purchase.currency, purchase.created_at, users.display_name'))
            ->groupBy('session_id')  
            ->get();
        //dd($purchases);

        $show = true;
        // dd($purchases);
        return view('dashboard.purchase.index', compact('purchases', 'show'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.purchase.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name'    => 'required|min:1|max:64',
            'subject' => 'required|min:1|max:128',
            'content' => 'required|min:1',
        ]);
        $template = new Purchases();
        $template->name = $request->input('name');
        $template->subject = $request->input('subject');
        $template->content = $request->input('content');
        $template->save();
        $request->session()->flash('message', 'Successfully created Email Template');
        return redirect()->route('purchase.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($session_id = null)
    {
        if (empty($session_id)) return redirect('/purchase');

        $purchases =  DB::table('purchase')
            ->where('purchase.session_id', $session_id)
            ->where('tb_product_language.language_id', 1)
            ->join('tb_product', 'tb_product.id', '=', 'purchase.prod_id')
            ->join('tb_product_language', 'tb_product_language.product_id', '=', 'tb_product.id')
            ->leftJoin('product_images', 'product_images.prod_id', '=', 'purchase.prod_id')
            ->select('purchase.id', 'purchase.session_id', 'tb_product_language.name', 'purchase.sale_price', 'purchase.quantity', 'purchase.currency', 'purchase.created_at', 'product_images.path')
            ->get();

            //dd($purchases);

        return view('dashboard.purchase.show', compact('purchases'));
    }
    
    /**
     * fetchTracking
     * route [ purchase/get_tracking ]
     * @param  mixed $request
     * @return void
     */
    public function fetchTracking(Request $request)
    {
        $input = $request->all();
        $validatedData = $request->validate([
            'purchase_id'    => 'required'
        ]);

        $response = DB::table('purchase_tracking')
            ->join('purchase_status', 'purchase_status.id', '=', 'purchase_tracking.purchase_status')
            ->where('purchase_id', $input['purchase_id'])
            ->orderBy('purchase_tracking.id', 'desc')
            ->select('purchase_tracking.purchase_id', 'purchase_tracking.id', 'purchase_tracking.purchase_status', 'purchase_tracking.tracking_number', 'purchase_status.status_name', 'purchase_status.status_desc', 'purchase_tracking.notice', 'purchase_tracking.created_at')
            ->get();

        // has error
        if (!$response) return Response::json([
            'status' => false
        ]); 
        
        // response success
        return Response::json([
            'status' => true,
            'data' => $response
        ]);
    }
    
    /**
     * fetchAllStatusById
     * route [ purchase/get_status_by_id ]
     * @param  mixed $request
     * @return void
     */
    public function fetchAllStatusById(Request $request)
    {
        $input = $request->all();
        $validatedData = $request->validate([
            'purchase_id'    => 'required'
        ]);

        // All Status
        $status = DB::table('purchase_status')
            ->select('purchase_status.id', 'purchase_status.status_name', 'purchase_status.status_desc')
            ->get();
        
        // get status by purchase id on click
        $trackings = DB::table('purchase_tracking')
            ->select('purchase_tracking.purchase_status','purchase_tracking.tracking_number')
            ->where('purchase_tracking.purchase_id', $input['purchase_id'])
            ->get()->toArray();
        // process
        $selected = array();
        if ($status) {
            foreach($status as $st) {
                $selected[] = array(
                    'id' => $st->id,
                    'name' => $st->status_name,
                    'disabled' => !empty(array_filter($trackings, function($value) use($st) {
                        return ($value->purchase_status == $st->id) ? true : false;
                    })) ? true : false
                );
            }
            
        }
        
        // has error
        if (!$selected) return Response::json([
            'status' => false
        ]); 
        
        // response success
        return Response::json([
            'status' => true,
            'data' => array(
                'status' => $selected,
                'purchase_number' => $trackings[0]->tracking_number,
                'purchase_id' => $input['purchase_id'],
            )
        ]);
    }
    
    /**
     * saveUpdateStatus
     *
     * @param  mixed $request
     * @return void
     */
    public function saveUpdateStatus(Request $request)
    {
        $input = $request->all();
        $validatedData = $request->validate([
            'purchase_id'    => 'required'
        ]);

        if (!isset($input['status']) && empty($input['status'])) return Response::json([
            'status' => false,
            'message' => 'No Status'
        ]);

        // Check Purchase
        $purchase = DB::table('purchase')->where('id', $input['purchase_id'])
            ->get();
        if (!$purchase) return Response::json([
            'status' => false,
            'message' => 'Missing Data or invalid!'
        ]);

        $inserted = DB::table('purchase_tracking')->insert([
            'purchase_id' => $input['purchase_id'],
            'purchase_status' => $input['status'],
            'tracking_number' => $input['purchase_number'],
            'notice' => isset($input['notice']) && !empty($input['notice']) ? $input['notice'] : '',
            'created_at' => now()
        ]);

        if (!$inserted) return Response::json([
            'status' => false,
            'message' => 'Error Insert Data'
        ]);

        return Response::json([
            'status' => true
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $template = Purchases::find($id);
        return view('dashboard.purchase.edit', [ 'template' => $template ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'    => 'required|min:1|max:64',
            'subject' => 'required|min:1|max:128',
            'content' => 'required|min:1',
        ]);
        $template = Purchases::find($id);
        $template->name = $request->input('name');
        $template->subject = $request->input('subject');
        $template->content = $request->input('content');
        $template->save();
        $request->session()->flash('message', 'Successfully updated Email Template');
        return redirect()->route('purchase.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $template = Purchases::find($id);
        if($template){
            $template->delete();
        }
        $request->session()->flash('message', 'Successfully deleted Email Template');
        return redirect()->route('purchase.index');
    }

    public function prepareSend($id){
        $template = Purchases::find($id);
        return view('dashboard.purchase.send', [ 'template' => $template ]);
    }

    public function send($id, Request $request){
        $template = Purchases::find($id);
        Mail::send([], [], function ($message) use ($request, $template)
        {
            $message->to($request->input('email'));
            $message->subject($template->subject);
            $message->setBody($template->content,'text/html');
        });
        $request->session()->flash('message', 'Successfully sended Email');
        return redirect()->route('purchase.index');
    }
}
