<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Tb_order_purchase extends Model
{
    public $table = 'tb_order_purchase';    

    /**
     * find_by_order_id
     *
     * @param  mixed $order_id
     * @return void
     */
    public static function find_by_order_id(int $order_id = null, string $result = 'get')
    {
        if (empty($order_id)) return false;
        $query = DB::table('tb_order_purchase')
            ->select(
                'tb_order_purchase.transaction_ref', 'tb_order_purchase.approval_code',
                'tb_order_purchase.transaction_datetime', 'tb_order_purchase.payment_status', 'tb_order_purchase.channel_response_desc',
                'tb_order_purchase.payment_channel', 'tb_order_purchase_logs.logs'
            )
            ->join('tb_order_purchase_logs', 'tb_order_purchase_logs.id', '=', 'tb_order_purchase.purchase_log_id')
            ->where([
                'order_id' => $order_id
            ])
            ->orderBy('tb_order_purchase_logs.created_at')
            ->{$result}();
        return $query;
    }

    public static function find_by_order_id2(int $order_id = null, int $shop_id = null)
    {
        if (empty($order_id)) return false;
        $query = DB::table('tb_order_purchase')
            ->select(
                'tb_order_purchase.transaction_ref', 'tb_order_purchase.approval_code',
                'tb_order_purchase.transaction_datetime', 'tb_order_purchase.payment_status', 'tb_order_purchase.channel_response_desc',
                'tb_order_purchase.payment_channel', 'tb_order_purchase_logs.logs', 'tb_order_item.shipping_is_paid',
                DB::raw('SUM(tb_order_item.sum_price) as total_price'), DB::raw('SUM(tb_order_item.shipping_price) as total_shipping')
            )
            ->join('tb_order_purchase_logs', 'tb_order_purchase_logs.id', '=', 'tb_order_purchase.purchase_log_id')
            ->join('tb_order_item', 'tb_order_item.order_id', '=', 'tb_order_purchase.order_id')
            ->where([
                'tb_order_item.order_id' => $order_id,
                'tb_order_item.shop_id' => $shop_id
            ])
            ->orderBy('tb_order_purchase_logs.created_at')
            ->groupBy('tb_order_item.shop_id')
            ->first();
        return $query;
    }
    
}

