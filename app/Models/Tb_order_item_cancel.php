<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Tb_order_item_cancel extends Model
{
    public $table = 'tb_order_item_cancel';    
    /**
     * find_by_order_id
     *
     * @param  mixed $order_id
     * @return void
     */
    public static function find_by_order_id(int $order_id = null)
    {
        if (empty($order_id)) return false;
        $query = DB::table('tb_order_item_cancel')->select(
            'tb_order_item_cancel.id as cancel_id', 'tb_order_item_cancel.status', 'refund', 'detail', 'order_id',
            'type_cancel', 'tb_order_item_cancel.created_at', 'tb_order_item_cancel.updated_at',
            'u_create.display_name as c_name', 'u_update.display_name as u_name'
        )
        ->join('users as u_create', 'u_create.id', '=', 'tb_order_item_cancel.created_by')
        ->leftJoin('users as u_update', 'u_update.id', '=', 'tb_order_item_cancel.updated_by')
        ->where('order_id', $order_id)
        ->orderBy('tb_order_item_cancel.created_at', 'desc')
        ->get();
        return $query;
    }
}

