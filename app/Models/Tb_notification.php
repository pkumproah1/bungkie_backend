<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Libraries\UserLibrary;
class Tb_notification extends Model
{
    public $table = 'tb_notification';
    /**
     * notifys
     *
     * @return void
     */
    public static function notifys()
    {
        $pagenate = DB::table('tb_notification')
            ->select('id', 'type', 'action', 'message', 'created_at' );
        //! ถ้่าเป็น seller
        if (UserLibrary::hasRole('seller') === true) {
            $pagenate = $pagenate->where('owner', Auth()->user()->id);
        }
        return $pagenate
            ->limit(10)
            ->orderBy('action', 'asc')
            ->get()->toArray();
    }
}

