<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Tb_order_item extends Model
{
    public $table = 'tb_order_item';

    public static function find_product_by_order_id(int $order_id = null, int $lang = 1)
    {
        if(empty($order_id)) return false;
        
        $product = DB::table('tb_order_item')
            ->select(
                'tb_order_item.product_id', 'name', 'path', 'is_new', 'shop_name', 'url', 'user_id',
                'front_price', 'discount', 'sum_price', 'qty', 'shipping_price', 'shipping_provider'
            )
            ->join('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'tb_order_item.shop_id')
            ->join('tb_product_language', 'tb_product_language.product_id', '=', 'tb_order_item.product_id')
            ->join('tb_product_images', 'tb_product_images.product_id', '=', 'tb_order_item.product_id')
            ->where(['rank' => 1, 'language_id' => $lang, 'order_id' => $order_id])
            ->get();
        
        return $product;
    }

}
