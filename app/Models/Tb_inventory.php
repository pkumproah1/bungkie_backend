<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Tb_inventory extends Model
{
    public $table = 'tb_inventory';
    public static function find_products(int $userId = 0, array $products = [], int $type = 1, int $currency = 1)
    {
        if (empty($userId) && empty($products)) return array(
            'status' => false,
            'message' => 'Missing data or invalid.'
        );

        // * find inventory by user id
        $inventory = DB::table('tb_inventory')
            ->where('tb_inventory.user_id', $userId)
            ->first();

        // ! missing inventory
        if (empty($inventory)) return array(
            'status' => false,
            'message' => 'Missing Inventory'
        );

        // * get all product in inventory
        $items = DB::table('tb_inventory_item')
            ->select(
                'owner', 'tb_inventory_item.product_id', 'qty', 's_weight', 'email',
                'warehouse_address', 'province_name_th', 'amphur_name_th', 'district_name_th',
                'warehouse_zipcode', 'free_shipping', 'front_price', 'sale_price', 'operation'
            )
            // Product
            ->join('tb_product', 'tb_product.id', '=', 'tb_inventory_item.product_id')
            ->join('tb_product_price', 'tb_product_price.product_id', '=', 'tb_inventory_item.product_id')

            // Seller
            ->join('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'tb_product.owner')
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'tb_user_sellers.warehouse_province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'tb_user_sellers.warehouse_city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'tb_user_sellers.warehouse_district')
            
            ->whereIn('tb_inventory_item.product_id', $products)
            ->where('inventory_id', $inventory->id)
            ->where('tb_product_price.type_price', $currency)
            ->where('tb_inventory_item.type_id', $type)
            ->where('tb_inventory_item.status', 0)
            ->groupBy('tb_inventory_item.product_id')
            ->get()->toArray();
            
        // ! missing products
        if (empty($items)) return array(
            'status' => false,
            'message' => 'Missing Product'
        );

        // * response
        return array(
            'status' => true,
            'data' => $items
        );
    }
}
