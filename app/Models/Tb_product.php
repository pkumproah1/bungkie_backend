<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Libraries\UserLibrary;
class Tb_product extends Model
{   
    const LIMIT = 60;
    const OFFSET = 0;
    public $table = 'tb_product'; 

    
    /**
     * find_product_by_ids_calc
     * @param  mixed $currency
     * @param  mixed $language
     * @param  mixed $ids
     * @return void
     */
    public static function find_product_by_ids_calc(int $currency = 1, int $language = 1, array $ids = [])
    {
        if (empty($currency) && empty($language) && empty($ids)) return array(
            'status' => false,
            'message' => 'Missing parameter'
        );

        $products = DB::table('tb_product')
            ->select('tb_product.id as product_id', 'path', 'tb_product_language.name', 'in_stock', 
                'tb_product.status', 'operation', 'front_price', 'sale_price', 'owner', 'shop_name')
            ->join('tb_product_price', 'tb_product_price.product_id', '=', 'tb_product.id')
            ->join('tb_product_language', 'tb_product_language.product_id', '=', 'tb_product_price.product_id')
            ->leftJoin('tb_product_images', 'tb_product_images.product_id', '=', 'tb_product_price.product_id')
            ->leftJoin('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'tb_product.owner')
            ->where('tb_product_price.type_price', $currency)
            ->where('tb_product_language.language_id', $language)
            ->where('tb_product_images.main_flag', 1)
            ->whereIn('tb_product_price.product_id', $ids)
            ->groupBy('tb_product.id')
            ->get()->toArray();

        if (empty($products)) return array(
            'status' => false,
            'message' => 'Missing data or invalid.'
        );

        return array(
            'status' => true,
            'data' => $products
        );
    }
    
    /**
     * count for datatabled
     * @param  mixed $params
     * @param  mixed $type
     * @return void
     */
    private static function query_paginate(array $params = [], string $type = null)
    {
        if (empty($params)) return array(
            'status' => false,
            'message' => 'Required Params!'
        );

        $orderby = array('tb_product_language.name','tb_product_price.front_price','tb_product.in_stock','tb_product.status', 'tb_product.created_at');    
        $pagenate = DB::table('tb_product')->select(
            'tb_product.id as product_id', 'tb_product.owner', 'tb_product_language.name', 'sku',
            'front_price','sale_price','operation','in_stock','tb_product.status',
            'tb_product.created_at','path', 'rank', 'is_new'
        )
        ->join("tb_product_price", "tb_product_price.product_id", "=", "tb_product.id")
        ->join("tb_product_images", "tb_product_images.product_id", "=", "tb_product.id")
        ->join("tb_product_language", "tb_product_language.product_id", "=", "tb_product.id")
        ->whereNull('tb_product.deleted_at')
        ->where('tb_product_price.type_price', 1)
        ->where('tb_product_language.language_id', 1)
        ->where('tb_product_images.rank', 1)
        ->groupBy('tb_product.id')
        ->orderBy($orderby[$params['order']['0']['column']], $params['order']['0']['dir']);

        //! ถ้่าเป็น seller
        if (UserLibrary::hasRole('seller') === true) {
            $pagenate = $pagenate->where('owner', Auth()->user()->id);
        }

        //? ถ้ามีการส่งข้อมูลค้นหา
        if ((isset($params['search']['value']) && !empty($params['search']['value']))) {
            //* GROUP where
            $pagenate = $pagenate->where(function($query) use($params) {
                // ? group start (ถ้าจะเพิ่ม field ในการค้นหาให้เพิ่มข้างในนี้)
                $query->where('tb_product_language.name', 'like', "%{$params['search']['value']}%")
                    ->orWhere('tb_product.tags', 'like', "%{$params['search']['value']}%");
                // ? group end
            });
        }

        //? Type
        if (isset($params['type']) && !empty($params['type'])) {
            $approve = ($params['type'] == "approve") ? 1 : 0;
            $pagenate = $pagenate->where('tb_product.approval', $approve);
        }

        //? ค้นหาจากวันที่
        if (isset($params['datetime_start'])) {
            $pagenate = $pagenate
                ->where('created_at', '>=', $params['datetime_start'])
                ->where('created_at', '<=', $params['datetime_end']);
        }    

        //? limit and length
        if ($type === null) {
            if($params['start'] > 0) {
                $pagenate = $pagenate
                    ->offset($params['start'])
                    ->limit($params['length']);
            } else {
                $pagenate = $pagenate->limit($params['length']);
            }
        }
        if ($type === null) {
            return $pagenate->get();
        } else {
            return $pagenate->get()->count();
        }
    }
    
    /**
     * datatabled
     * @param  mixed $params
     * @return void
     */
    public static function find_paginate(array $params = [])
    {
        $pagenate = self::query_paginate($params);
        $response = array();
        $response['data'] = [];
        if ($pagenate) {
            $fields = array('name', 'price', 'stock', 'status', 'created', 'action');
            foreach($pagenate as $key => $item) {
                $row = array();
                foreach($fields as $field) {
                    if ($field == "name") {
                        $image = ($item->is_new == 1)
                            ? url('/') . '/public/statics/images/products/'. $item->product_id . '/m/' . $item->path
                            : url('/') . '/public/statics/images/' . $item->path;
                        $html = '<div class="product"><div class="thumbnail">';
                        $html.= '<a href="'. url('/product/create/') . '/' . $item->product_id .'"><img src="'. $image .'">';
                        $html.= '</a></div><div class="detail">';
                        $html.= '<a href="'. url('/product/create/') . '/' . $item->product_id .'"><span class="product-name">'. $item->name .'</span>';
                        $html.= '<span class="sku">'. $item->sku .'</span></a>';
                        $html.= '<div class="category"><ul>';
                        $html.= '<li>Main Category</li><li>Sub Category</li><li>Last Category</li>';
                        $html.= '</ul></div></div></div>';
                        $row[0] = $html;

                    } else if($field == "price") {
                        $row[1] = '<span>'. number_format($item->front_price,2) .'</span>';
                    } else if($field == "stock") {
                        $row[2] = '<span>'. $item->in_stock .'</span>';
                    } else if($field == "status") {
                        $txt = ($item->status == 1)
                            ? '<span class="text-success">เปิดใช้งาน</span>'
                            : '<span class="text-warning">ปิดใช้งาน</span>';
                        $row[3] = $txt;
                    } else if($field == "created") {
                        $row[4] = '<span>'. $item->created_at .'</span>';
                    } else if($field == "action") {
                        $action = '<div class="action">';
                        $action.= '<a href="'. url('/product/create/') . '/' . $item->product_id .'" class="text-info">ดู/แก้ไข</a> <br />';
                        $action.= '<a href="#" class="text-danger deleted" data-id="'. $item->product_id .'">ลบ</a></div>';
                        $row[5] = $action;
                    }
                }
                $response['data'][] = $row;
            }
        }

        $response["draw"] = intval($params['draw']);
        $response["recordsTotal"] = $pagenate = self::query_paginate($params, 'total');
        $response["recordsFiltered"] = $pagenate = self::query_paginate($params, 'total');
        return $response;
    }
     
    /**
     * homepage_paginate for api
     * @param  mixed $request
     * @param  mixed $type
     * @param  mixed $language
     * @param  mixed $currency
     * @return void
     */
    public static function homepage_paginate(
        array $request = [],
        string $type = "result",
        int $language = 1,
        int $currency = 1
    ) {

        // dd($requ)
        //? select fields
        $selected = array(
            'tb_product.id as product_id', 'in_stock', 'tb_product_language.name as product_name', 'recommend',
            'base_price','front_price','sale_price', 'operation','path', 'is_new', 'product_type'
        );
        //* query product
        $products = DB::table("tb_product")
            ->where([
                'tb_product.status' => 1,
                'tb_product.approval' => 1,
                'tb_product_images.rank' => 1,
                'tb_product_language.language_id' => $language,
                'tb_product_price.type_price' => $currency
            ])
            ->whereNull('tb_product.deleted_at');
        
        // TODO การค้นหาแบบแบ่ง type ยังต้องรอทำ type เพิ่ม
        if ((isset($request['type']) && !empty($request['type']))) {
            switch ($request['type']) {
                // * type by category id
                case 'category':
                    $selected[] = 'tb_product_category_language.name as category_name';
                    $selected[] = 'tb_product_category.id as category_id';
                    $products = $products->join('tb_product_category_relation', 'tb_product_category_relation.product_id', '=', 'tb_product.id')
                        ->join('tb_product_category', 'tb_product_category.id', '=', 'tb_product_category_relation.category_id')
                        ->join('tb_product_category_language', 'tb_product_category_language.category_id', '=', 'tb_product_category.id')
                        ->where([
                            'tb_product_category.status' => 1,
                            'tb_product_category_language.language_id' => $language
                        ]);
                    
                    if (is_numeric($request['type_id'])) {
                        $products = $products->where('tb_product_category_relation.category_id', $request['type_id']);
                    } else {

                        $ids = array_reduce(explode(',', $request['type_id']), function($callback, $item) {
                            $callback[] = $item;
                            return $callback;
                        }, []);

                        $products = $products->whereIn('tb_product_category_relation.category_id', $ids);
                    }
                    break;
                // * type by seller id
                case 'seller':
                    if (is_numeric($request['type_id'])) {
                        $products = $products->where('tb_product.owner', $request['type_id']);
                    }
                    break;
                case 'recommend':
                        $products = $products->where('tb_product.recommend', 1);
                    break;
                case 'hot-deals':
                    $products = $products->where('tb_product_price.sale_price', '>', 0);
                    break;
                case 'wishlist':
                    if (is_numeric($request['type_id'])) 
                        $products = $products->where('tb_product.owner', $request['type_id']);
                    break;
                default:
                    break;
            }
        }

        if ((isset($request->search) && !empty($request->search))) {
            //* GROUP where
            $products = $products->where(function($query) use($request) {
                // ? group start (ถ้าจะเพิ่ม field ในการค้นหาให้เพิ่มข้างในนี้)
                $query->where('tb_product_language.name', 'like', "%{$request->search}%")
                    ->orWhere('tb_product_language.short_description', 'like', "%{$request->search}%")
                    ->orWhere('tb_product.tags', 'like', "%{$request->search}%")
                    ->orWhere('tb_product_language.description', 'like', "%{$request->search}%")
                    ->orWhere('tb_product_language.special_condition', 'like', "%{$request->search}%");
                // ? group end
            });
        }

        // ? ถ้า type == result (ข้อมูล product)
        if ($type === "result") {
            // ! เช็คว่ามีการส่ง params limit && offset
            // ? ถ้าไม่มีการส่งค่า limit มา ระบบจะใช้ค่า default แทน
            $products = $products->limit(self::LIMIT)->offset(self::OFFSET);
            if ((isset($request['limit']) && !empty($request['limit'])) || (isset($request['offset']) && !empty($request['offset']))) {
                // ! limit && offset จะต้องเป็นตัวเลขเท่านั้นถึงจะถูกคำนวน
                if (is_numeric($request['limit']) && is_numeric($request['offset'])) {
                    // ? limit มากว่าค่า default จะใช้ค่า default แทน
                    if ($request['limit'] >= self::LIMIT) {
                        // ? ถ้าส่งค่า limit มาเกินกว่าค่า default จะให้ดึงค่า default มาใช้งาน
                        $products = $products->limit(self::LIMIT);
                    } else {
                        //* ใช้ค่าตาม Params
                        $products = $products->limit($request['limit'])->offset($request['offset']);
                    }
                }
            }
            // ? set result = toArray();
            $result = "toArray";
        // ? ถ้า type == count (จะนับข้อมูลสินค้าทั้งหมดที่ query ได้โดยจะไม่ถูก limit เอาไว้ใช้สำหรับทำ pagination)
        } else {
            //! remove old selected
            unset($selected);
            // ? add new selected by COUNT
            $selected = array('tb_product.id');
            // ? set result = count();
            $result = "count";
        }
        //* query next step
        $products = $products
            ->select($selected)
            ->join("tb_product_language", "tb_product_language.product_id", "=", "tb_product.id")
            ->join("tb_product_price", "tb_product_price.product_id", "=", "tb_product.id")
            ->join("tb_product_images", "tb_product_images.product_id", "=", "tb_product.id")
            ->orderBy('tb_product.created_at', 'desc')
            ->get()->{$result}();
        // * return products
        return $products;
    }
    
    /**
     * get_rating
     *
     * @return void
     */
    public static function get_rating()
    {
        // * Query Rating
        $ratings = DB::table("tb_product_rating")
            ->select('product_id', DB::raw('SUM(tb_product_rating.score) as score'), DB::raw('COUNT(tb_product_rating.product_id) as number'))
            ->groupBy('tb_product_rating.product_id')
            ->get()->toArray();
        //* return ratings
        return $ratings;
    }
    
    /**
     * get_product_for_template_stock
     *
     * @return void
     */
    public static function get_product_for_template_stock()
    {
        $pagenate = DB::table('tb_product')->select(
            'tb_product.owner', 'tb_product.sku'
        )->where([
            'tb_product.approval' => 1,
            'tb_product.status' => 1
        ])->whereNull('tb_product.deleted_at')
        ->orderBy('created_at', 'desc');

        //! ถ้่าเป็น seller
        if (UserLibrary::hasRole('seller') === true) {
            $pagenate = $pagenate->where('owner', Auth()->user()->id);
        }

        return $pagenate->get();

    }
    
    /**
     * get_product_for_template
     *
     * @return void
     */
    public static function get_product_for_template_price()
    {
        $pagenate = DB::table('tb_product')->select(
            'tb_product.owner', 'tb_product.sku',
            'type_price', 'base_price', 'front_price', 'sale_price', 'operation'
        )->where([
            'tb_product.approval' => 1,
            'tb_product.status' => 1
        ])->whereNull('tb_product.deleted_at')
        ->join('tb_product_price', 'tb_product_price.product_id', '=', 'tb_product.id')
        ->orderBy('created_at', 'desc');

        //! ถ้่าเป็น seller
        if (UserLibrary::hasRole('seller') === true) {
            $pagenate = $pagenate->where('owner', Auth()->user()->id);
        }

        return $pagenate->get();

    }
    
}