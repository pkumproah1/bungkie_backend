<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Tb_user_sellers extends Model
{
    public $table = 'tb_user_sellers';

    public static function get_pagainate()
    {
        $sellers = DB::table('tb_user_sellers')
            ->select('user_id', 'shop_name')
            ->where('status', 1)
            ->get()->toArray();
        
        if ($sellers) return $sellers;
    }
    
    /**
     * find_seller_by_id
     *
     * @param  mixed $shop_id
     * @return void
     */
    public static function find_seller_by_id(int $shop_id = null)
    {
        if (empty($shop_id)) return false;
        $seller = DB::table('tb_user_sellers')
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'tb_user_sellers.warehouse_province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'tb_user_sellers.warehouse_city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'tb_user_sellers.warehouse_district')
            ->where('id', $shop_id)
            ->first();
        return $seller;
    }
}