<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Tb_user_address extends Model
{
    public $table = 'user_address';
    public static function find_address_by_user_and_id(int $userId = 0, int $addressId = 0)
    {
        if (empty($userId)) return array(
            'status' => false,
            'message' => 'Missing data or invalid.'
        );

        $address = DB::table('user_address')
            ->select('user_address.id as address_id', 'is_default', 'name', 'phone', 'address', 'zipcode', 'province_name_th', 'amphur_name_th', 'district_name_th')
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'user_address.province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'user_address.city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'user_address.district')
            ->where('user_address.user_id', $userId);

        $address = ($addressId !== 0) 
            ? $address->where('user_address.id', $addressId)->first()
            : $address->get()->toArray();
        
        if (empty($address)) {
            return array(
                'status' => false,
                'message' => 'Missing Data'
            );
        }

        return array(
            'status' => true,
            'data' => $address
        );   
    }
    
    /**
     * find_address_by_id
     *
     * @param  mixed $address_id
     * @return void
     */
    public static function find_address_by_id(int $address_id = 0)
    {
        if (empty($address_id)) return false;
        $address = DB::table('user_address')
            ->select(
                'user_address.id as address_id', 'is_default', 'name', 'phone',
                'address', 'zipcode', 'province_name_th', 'amphur_name_th', 'district_name_th',
                'display_name', 'tel'
            )
            ->join('users', 'users.id', '=', 'user_address.user_id')
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'user_address.province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'user_address.city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'user_address.district')
            ->where('user_address.id', $address_id)
            ->first();
        return $address;
    

    }
}
