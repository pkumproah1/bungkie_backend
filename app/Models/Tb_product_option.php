<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Tb_product_option extends Model
{
    public $table = 'tb_product_option';

    public function get_options($id = null)
    {
        if (empty($id)) return array(
            'status' => false
        );
        
        // query data
        $options = DB::table('tb_product_option')
            ->where([
                'tb_product_option.product_id' => $id,
                'tb_product_option.status' => 1
            ])
            ->select('id', 'product_id', 'name', 'status', 'created_at')
            ->get();
        // verify data
        if (empty($options)) return array(
            'status' => false
        );

        $response = array();
        $k = 1;
        foreach ($options as $option) {
            $response[] = array(
                'name' => $option->name,
                'id' => $k,
                'option' => self::get_items($option->id)
            );
            $k++;
        }

        if ($response) {
            return array(
                'data' => $response,
                'json' => json_encode($response)
            );
        } else {
            return;
        }
    }

    // get items
    private function get_items($option_id = null)
    {
        if (empty($option_id)) return null;
        $items = DB::table('tb_product_option_item')
            ->where('option_id', $option_id)
            ->select('name')->get()->toArray();
        $response = array();
        foreach($items as $item) {
            $response[] = $item->name;
        }
        
        return $response;
    }

}
