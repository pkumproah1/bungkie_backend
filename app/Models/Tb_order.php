<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Libraries\UserLibrary;
class Tb_order extends Model
{
    public $table = 'tb_order';
        
    /**
     * find_all_order
     *
     * @param  mixed $status
     * @param  mixed $user_id
     * @return void
     */
    public static function cancal_order()
    {
        $orders = DB::table('tb_order')
            ->select('tb_order.id as order_id', 'tb_order_item.product_id', 'qty')
            ->where('tb_order.status', 0)
            ->join('tb_order_item', 'tb_order_item.order_id', '=', 'tb_order.id')
            ->orderBy('tb_order.created_at', 'asc')
            ->get()->toArray();

        $result = array();
        if (!empty($orders)) {
            foreach($orders as $order) {
                // ? Roback In Stock
                $updated = DB::table('tb_product')->where(['id' => $order->product_id])
                    ->update(['in_stock' => DB::raw('IF (in_stock = "0", "1", in_stock + '. $order->qty .')')]);
                // ! ถ้า updated สำเร็จ
                if ($updated) {
                    // * update order
                    DB::table('tb_order')->where('id', $order->order_id)
                        ->update(['status' => 99, 'updated_at' => now()]);
                    // * update tb_order_item
                    DB::table('tb_order_item')->where('order_id', $order->order_id)
                        ->update(['status' => 99, 'updated_at' => now()]);
                    // ? success log
                    $result['success'][$order->order_id][] = array(
                        'product_id' => $order->product_id,
                        'qty' => $order->qty
                    );
                } else {
                    // ! error logs
                    $result['error'][$order->order_id][] = array(
                        'product_id' => $order->product_id,
                        'qty' => $order->qty
                    );
                }
            }
        }
        
        if (!empty($result)) {
            // ! save logs
            DB::table('tb_logs')->insert([
                'class' => strtoupper('Tb_order'),
                'method' => strtoupper('cancal_order'),
                'name' => 'ยกเลิกออเดอร์ออโต้',
                'json_data' => json_encode($result),
                'created_at' => now(),
            ]);
            return true;
        }

        return false;
    }

    /**
     * find_order_purchase_by_ref
     *
     * @param  mixed $reference_id
     * @return void
     */
    public static function find_order_purchase_by_ref(string $reference_id = null)
    {
        if (empty($reference_id)) return false;
        $order = DB::table('tb_order')
            ->select(
                'user_address.address', 'user_address.phone', 'province_name_th', 'amphur_name_th', 'district_name_th', 'user_address.zipcode',
                'tb_order.id','users.display_name', 'users.email', 'users.tel', 'tb_order.reference_id', 'tb_order.status',
                'tb_order.amount', 'tb_order.currency', 'transaction_ref', 'transaction_ref', 'approval_code',
                'transaction_datetime', 'payment_channel', 'tb_order.payment_status', 'channel_response_desc', 'tb_order.created_at'
            )
            ->join('tb_order_purchase', 'tb_order_purchase.order_id', '=', 'tb_order.id')
            ->join('users', 'users.id', '=', 'tb_order.user_id')
            
            // address
            ->join('user_address', 'user_address.user_id', '=', 'users.id')
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'user_address.province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'user_address.city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'user_address.district')
            ->where('tb_order.status', '!=', 0)
            ->where('tb_order.reference_id', $reference_id)
            ->first();
        return $order;
    }
	
	static public function find_order_purchase_by_user_id(string $user_id = null)
	{
		if(empty($user_id)) return false;
		$order = DB::table('tb_order')
            ->select(
                'user_address.address', 'user_address.phone', 'province_name_th', 'amphur_name_th', 'district_name_th', 'user_address.zipcode', 'tb_order.id', 'users.display_name', 'users.email', 'users.tel', 'tb_order.reference_id', 'tb_order.status', 'tb_order.amount', 'tb_order.currency', 'transaction_ref', 'transaction_ref', 'approval_code', 'transaction_datetime', 'payment_channel', 'channel_response_desc', 'tb_order.created_at', 'users.id as user_id', 'tb_order.payment_status'
            )
            ->join('tb_order_purchase', 'tb_order_purchase.order_id', '=', 'tb_order.id')
            ->join('users', 'users.id', '=', 'tb_order.user_id')
            
            // address
            ->join('user_address', 'user_address.user_id', '=', 'users.id')
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'user_address.province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'user_address.city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'user_address.district')
            ->where('tb_order.status', '!=', 0)
            ->where('tb_order.user_id', $user_id)
			->orderBy('transaction_datetime', 'desc')
            ->get();			
        return $order;
	}
    
    /**
     * query_paginate
     *
     * @param  mixed $params
     * @param  mixed $type
     * @return void
     */
    public static function paginate(array $params = [], string $type = null)
    {
        if (empty($params)) return array(
            'status' => false,
            'message' => 'Required Params!'
        );
        $orderby = array('reference_id', 'amount', 'display_name', 'tb_user_sellers.shop_name', 'tb_order_item.status', 'tb_order.created_at', '');    
        $pagenate = DB::table('tb_order')
            ->select(
                'tb_order.id', 'display_name', 'reference_id', 'amount', 'tb_order.payment_status',
                'currency', 'tb_order.status as order_status', 'tb_order.created_at', 'shipping_is_paid',
                DB::raw('sum(tb_order_item.sum_price) as price_total'),  DB::raw('sum(tb_order_item.shipping_price) as shopping_total'),
                'tb_user_sellers.shop_name', 'tb_order_item.status', 'tb_order_item.shipping_is_paid', 'tb_order_item.shipping_provider',
                'tb_order_item.shop_id'
            )
            ->leftJoin('tb_order_item', 'tb_order_item.order_id', '=', 'tb_order.id')
            ->leftJoin('users', 'users.id', '=', 'tb_order.user_id')
            ->leftJoin('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'tb_order_item.shop_id')
            ->groupBy('tb_order_item.shop_id', 'tb_order_item.order_id')
            ->orderBy($orderby[$params['order']['0']['column']], $params['order']['0']['dir']);

        //! ถ้่าเป็น seller
        if (UserLibrary::hasRole('seller') === true) {
            $pagenate = $pagenate->where('tb_order_item.shop_id', Auth()->user()->id);
        }

        //? ถ้ามีการส่งข้อมูลค้นหา
        if ((isset($params['searchType']) && !empty($params['searchType']))) {
            if ($params['searchType'] == 1) {
                //* GROUP where
                $pagenate = $pagenate->where(function($query) use($params) {
                    // ? group start (ถ้าจะเพิ่ม field ในการค้นหาให้เพิ่มข้างในนี้)
                    $query->where('tb_order.reference_id', 'like', "%{$params['searchValue']}%");
                    // ? group end
                });
            } else if($params['searchType'] == 2) {
                //* GROUP where
                $pagenate = $pagenate->where(function($query) use($params) {
                    // ? group start (ถ้าจะเพิ่ม field ในการค้นหาให้เพิ่มข้างในนี้)
                    $query->where('users.display_name', 'like', "%{$params['searchValue']}%")
                        ->orWhere('users.fname', 'like', "%{$params['searchValue']}%")
                        ->orWhere('users.lname', 'like', "%{$params['searchValue']}%");
                    // ? group end
                });
            } else if ($params['searchType'] == 3) {
                //* GROUP where
                $pagenate = $pagenate->where(function($query) use($params) {
                    // ? group start (ถ้าจะเพิ่ม field ในการค้นหาให้เพิ่มข้างในนี้)
                    $query->where('users.tel', 'like', "%{$params['searchValue']}%");
                    // ? group end
                });
            }
        }

        //? Type
        if (isset($params['type'])) {
            if ($params['type'] == '0') {
                $pagenate = $pagenate->where('tb_order_item.status', '1')
                    ->where('tb_order.payment_status', '001');
            } elseif ($params['type'] == '1') {
                $pagenate = $pagenate->where('tb_order_item.status', '1')
                    ->where('tb_order.payment_status', '000');
            } elseif ($params['type'] == '2') {
                $pagenate = $pagenate->where('tb_order_item.status', '2')
                    ->where('tb_order.payment_status', '000');
            } elseif ($params['type'] == '3') {
                $pagenate = $pagenate->where('tb_order_item.status', '3')
                    ->where('tb_order.payment_status', '000');
            } elseif ($params['type'] == '4') {
                $pagenate = $pagenate->where('tb_order_item.status', '4');
            } elseif ($params['type'] == '5') {
                $pagenate = $pagenate->where('tb_order_item.status', '5');
            }
        }

        //? ค้นหาจากวันที่
        if (isset($params['datetime_start'])) {
            $pagenate = $pagenate
                ->where('tb_order.created_at', '>=', $params['datetime_start'])
                ->where('tb_order.created_at', '<=', $params['datetime_end']);
        }    

        //? limit and length
        if ($type === null) {
            if($params['start'] > 0) {
                $pagenate = $pagenate
                    ->offset($params['start'])
                    ->limit($params['length']);
            } else {
                $pagenate = $pagenate->limit($params['length']);
            }
        }

        // $pagenate = $pagenate->get();
        // dd(DB::getQueryLog());

        if ($type === null) {
            return $pagenate = $pagenate->get();
        } else {
            return $pagenate = $pagenate->get()->count();
        }
    }
    
    /**
     * detail
     *
     * @param  mixed $reference_id
     * @param  mixed $language_id
     * @return void
     */
    public static function detail(string $reference_id = null, int $shop_id = null, int $language_id = 1)
    {
        if (empty($reference_id)) return false;
        $order_detail = DB::table('tb_order')
            ->select(
                'tb_user_sellers.shop_name', 'tb_user_sellers.warehouse_name', 'tb_user_sellers.warehouse_telephone',
                'tb_user_sellers.email', 'tb_user_sellers.company_name', 'tb_user_sellers.tax_id',
                'user_address.name as customer_name', 'address', 'user_address.phone as customer_phone', 'province_name_th', 'amphur_name_th', 'district_name_th',
                'user_address.zipcode', 'tb_product_language.name', 'tb_order.id', 'tb_order_item.shop_id',
                'is_new', 'path', 'tb_order_item.product_id', 'tb_order_item.id as item_id',
                'tb_inventory_item.remark', 'tb_order_shipping.pno', 'tb_order_shipping.id as shipping_id',
                'tb_order_item.shipping_provider', 'tb_order_item.sum_price', 'tb_order_item.qty', 'tb_order_item.discount',
                'tb_order_item.shipping_price', 'tb_order_item.shipping_provider', 'tb_order_item.shipping_is_paid',
                'tb_order_item.status', 'tb_order_item.created_at', 'tb_order_item.shop_id', 'tb_order_item.order_id',
                'users.email as customer_email'
            )
            ->join('tb_order_item', 'tb_order_item.order_id', '=', 'tb_order.id')
            ->leftJoin('tb_inventory_item', 'tb_inventory_item.id', '=', 'tb_order_item.inventory_item_id')
            ->leftJoin('tb_order_shipping', function($q) {
                $q->on('tb_order_shipping.shop_id', '=', 'tb_order_item.shop_id')
                  ->on('tb_order_shipping.reference_id', '=', 'tb_order.reference_id');
            })
            ->join('tb_product_language', 'tb_product_language.product_id', '=', 'tb_order_item.product_id')
            ->join('tb_product_images', 'tb_product_images.product_id', '=', 'tb_order_item.product_id')
            ->join('user_address', 'user_address.id', '=', 'tb_order_item.customer_address_id')
            ->join('users', 'users.id', '=', 'user_address.user_id')
            ->join('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'tb_order_item.shop_id')
            // address
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'user_address.province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'user_address.city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'user_address.district')
            ->where(['tb_product_images.rank' => 1, 'tb_product_language.language_id' => $language_id])
            ->where('tb_order.reference_id', $reference_id);
            
        //! ถ้่าเป็น seller
        if (UserLibrary::hasRole('seller') === true) {
            $order_detail = $order_detail->where('tb_order_item.shop_id', Auth()->user()->id);
        } else {
            $order_detail = $order_detail->where('tb_order_item.shop_id', $shop_id);
        }
        $order_detail = $order_detail->get()->toArray();
        return $order_detail;
    }
    
    /**
     * find_order
     *
     * @param  mixed $reference_id
     * @param  mixed $shop_id
     * @return void
     */
    public static function find_order(string $reference_id = null, int $shop_id = null)
    {
        if (empty($reference_id) && empty($shop_id)) return false;
        $order = DB::table('tb_order')
            ->select(
                'shop_name', 'warehouse_name', 'warehouse_address', 'province_name_th', 'amphur_name_th',
                'district_name_th', 'warehouse_zipcode', 'warehouse_telephone', 's_weight',
                'tb_order.user_id', 'tb_order.id as order_id', 'tb_order.currency', 'tb_order.amount',
                DB::raw('SUM(s_weight) as weight'), 'customer_address_id', 'tb_order_item.shop_id',
                'tb_order.status'
            )
            ->join('tb_order_item', 'tb_order_item.order_id', '=', 'tb_order.id')
            ->join('tb_product', 'tb_product.id', '=', 'tb_order_item.product_id')

            // seller
            ->join('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'tb_order_item.shop_id')
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'tb_user_sellers.warehouse_province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'tb_user_sellers.warehouse_city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'tb_user_sellers.warehouse_district')

            ->where(['tb_order.reference_id' => $reference_id, 'tb_order_item.shop_id' => $shop_id])
            ->groupBy('tb_order_item.order_id')
            ->orderBy(DB::raw('tb_order_item.customer_address_id IS NULL'))
            ->first();
        return $order;
    }
    
    /**
     * report_purchase
     *
     * @param  mixed $payment_status
     * @return void
     */
    public static function report_purchase(array $params = [], $type = null)
    {
        $reports = DB::table('tb_order')->select(
            'tb_order.created_at as date_create', 'tb_order_purchase.created_at as received_date', 
            'tb_order_purchase.transaction_datetime as transaction_date', 'tb_user_sellers.shop_name as store',
            'tb_order_item.shop_id as store_id', 'tb_order.reference_id as order_number',
            'tb_order_purchase.transaction_ref as transaction_number', 'tb_order_item.qty as quantity',
            'tb_product.sku', 'tb_product_language.name as item_name', 'tb_order.payment_status as paid',
            'tb_order_item.sum_price as amount', 'tb_order_purchase.payment_channel as channal_payment',
            'tb_order_item.shipping_price', 'tb_order_item.shipping_is_paid as is_paid',
            'tb_order_purchase.currency', 'tb_product_category_language.name as category_name',
            'tb_product_category_relation.id as cate_relate_id', 'tb_product_category.commission_rate',
            'tb_order_item.id as item_id'
        )
        ->join('tb_order_item', 'tb_order_item.order_id', '=', 'tb_order.id')
        ->join('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'tb_order_item.shop_id')
        ->join('tb_product', 'tb_product.id', '=', 'tb_order_item.product_id')
        ->join('tb_product_language', 'tb_product_language.product_id', '=', 'tb_order_item.product_id')
        ->join('tb_order_purchase', 'tb_order_purchase.order_id', '=', 'tb_order.id')
        ->join('tb_product_category_relation', 'tb_product_category_relation.product_id', '=', 'tb_order_item.product_id')
        ->join('tb_product_category', 'tb_product_category.id', '=', 'tb_product_category_relation.category_id')
        ->join('tb_product_category_language', 'tb_product_category_language.category_id', '=', 'tb_product_category_relation.category_id')
        ->where([
            'tb_product_language.language_id' => 2,
            'tb_product_category_language.language_id' => 2
        ]);
        
        //! ถ้่าเป็น seller
        if (UserLibrary::hasRole('seller') === true) {
            $reports = $reports->where('tb_order_item.shop_id', Auth()->user()->id);
        }

        //! payment status != 0
        if (isset($params['payment_status']) && !empty($params['payment_status'])) {
            $reports = $reports->where('tb_order.payment_status', $params['payment_status']);
        }

        //! where date
        if (isset($params['datetime_start'])) {
            $reports = $reports
                ->where('tb_order.created_at', '>=', $params['datetime_start'])
                ->where('tb_order.created_at', '<=', $params['datetime_end']);
        }

        //! Search
        if ((isset($params['searchType']) && !empty($params['searchType']))) {
            if ($params['searchType'] == 1) {
                //* GROUP where
                $reports = $reports->where(function($query) use($params) {
                    // ? group start (ถ้าจะเพิ่ม field ในการค้นหาให้เพิ่มข้างในนี้)
                    $query->where('tb_order.reference_id', 'like', "%{$params['searchValue']}%");
                    // ? group end
                });
            }
        }

        //? ORDER BY
        $reports = $reports->orderBy('tb_order_item.shipping_price', 'desc')
            ->orderBy('tb_product_category_relation.id', 'asc')
            ->orderBy('tb_order_item.shop_id', 'asc');
        if ($type === null) {
            if($params['start'] > 0) {
                $reports = $reports
                    ->offset($params['start'])
                    ->limit($params['length']);
            } else {
                $reports = $reports->limit($params['length']);
            }
            return $reports = $reports->get()->toArray();
        } elseif ($type === "export") {
            return $reports = $reports->get()->toArray();
        } else {
            return $reports = $reports->get()->count();
        }
        
        return $reports;
    }
}

