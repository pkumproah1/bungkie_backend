<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Libraries\UserLibrary;
use Carbon\Carbon;
class Tb_order_shipping extends Model
{
    public $table = 'tb_order_shipping'; 

    /**
     * find_order_for_notify_flash
     *
     * @return void
     */
    public static function find_order_for_notify_flash()
    {
        $query = DB::table('tb_order_shipping')
            ->select(
                'tb_order_shipping.id', 'shop_id', 'tb_order_shipping.reference_id', 
                DB::raw('count(tb_order_shipping.reference_id) as estimate'),
                'tb_user_sellers.shop_name', 'warehouse_telephone', 'province_name_th',
                'amphur_name_th', 'district_name_th', 'warehouse_zipcode', 'warehouse_address',
                'tb_order.id as order_id'
            )
            ->where('tb_order_shipping.status', 0)
            ->whereNotNull('tb_order_shipping.pno')
            // ->whereDate('tb_order_shipping.created_at', Carbon::today())
            ->join('tb_order', 'tb_order.reference_id', '=', 'tb_order_shipping.reference_id')
            ->join('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'tb_order_shipping.shop_id')
            ->join('tb_location_province', 'tb_location_province.province_code', '=', 'tb_user_sellers.warehouse_province')
            ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'tb_user_sellers.warehouse_city')
            ->join('tb_location_district', 'tb_location_district.district_code', '=', 'tb_user_sellers.warehouse_district')
            ->groupBy('tb_order_shipping.shop_id')
            ->get()->toArray();
        return $query;
    }
    
    /**
     * find_order_by_pno
     *
     * @param  mixed $pno
     * @return void
     */
    public static function find_order_by_pno(string $pno = null)
    {
        if (empty($pno)) return false;
        $query = DB::table('tb_order_shipping')
            ->select(
                'tb_order.id as order_id', 'tb_order.status as order_status', 'tb_order_shipping.id as shipping_id',
                'notify_pickup_id', 'staff_id'
            )
            ->where('tb_order_shipping.pno', $pno)
            ->join('tb_order', 'tb_order.reference_id', '=', 'tb_order_shipping.reference_id')
            ->first();
        return $query;
    }
        
    /**
     * find_order_by_order_id
     *
     * @param  mixed $order_id
     * @return void
     */
    public static function find_order_by_order_id(string $order_id = null)
    {
        if (empty($order_id)) return false;
        $query = DB::table('tb_order_shipping')
            ->select(
                'tb_order_shipping.id as shipping_id',
                'provider_id', 'pno', 'status', 'notify_pickup_id', 'staff_id'
            )
            ->where('tb_order_shipping.reference_id', $order_id)
            ->first();
        return $query;
    }
    
    /**
     * find_order_by_shop_id
     *
     * @param  mixed $order_id
     * @return void
     */
    public static function find_order_by_shop_id(string $shop_id = null)
    {
        if (empty($shop_id)) return false;
        $query = DB::table('tb_order_shipping')
            ->select(
                'reference_id','notify_pickup_id', 'staff_id',
                'tb_order_shipping.id as shipping_id',
                'provider_id', 'pno', 'status'
            )
            ->where('tb_order_shipping.shop_id', $shop_id)
            ->get();
        return $query;
    }
    
    /**
     * find_by_shipping_id
     *
     * @param  mixed $id
     * @return void
     */
    public static function find_by_shipping_id(int $id = null)
    {
        if (empty($id)) return false;
        $query = DB::table('tb_order_shipping')
            ->select('shipping_id', 'status', 'message')
            ->where('shipping_id', $id)
            ->get()->toArray();
        return $query;
    }

    

    



}

