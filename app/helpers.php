<?php



if(! function_exists('clean')) {
    function clean($string) {
        $string = str_replace(array(' ', '/'), '-', $string);
        $string = preg_replace('/[^A-Za-z0-9ก-๙เ\-]/', '', $string);
        return preg_replace('/-+/', '-', $string);
    }
}

if(! function_exists('group_shop')) {
    function group_shop($products) {

        $items = array_reduce($products, function($accumulator, $item) {
            $key = $item["shopId"];
            if (!array_key_exists($key, $accumulator)) {
                $accumulator[$key] = array();
            }
            array_push($accumulator[$key], $item);      
            return $accumulator;
        }, []);

        $result = array();
        foreach ($items as $key => $item) {
            $result[$key]['shopName'] = $item[0]['shopName'];
            $result[$key]['shopId'] = $item[0]['shopId'];
            $result[$key]['product'] = array_reduce($item, function($accumulator, $v) {
                unset($v['shopName'], $v['shopId']);
                array_push($accumulator, $v);
                return $accumulator;
            }, []);
        }
        return $result;
    }
}

if (!function_exists('_2c2p_status')) {
    function _2c2p_status(string $code = null, int $lang = 1) {
        if (empty($code)) return;
        $message = array(
            '000' => ($lang === 1) ? "การชำระเงินสำเร็จ" : "Payment Successful",
            '001' => ($lang === 1) ? "รอการชำระเงิน" : "Payment Pending",
            '002' => ($lang === 1) ? "การชำระเงินถูกปฏิเสธ" : "Payment Rejected",
            '003' => ($lang === 1) ? "การชำระเงินถูกยกเลิกโดยผู้ใช้" : "Payment was canceled by user",
            '999' => ($lang === 1) ? "การชำระเงินล้มเหลว" : "Payment Failed"
        );

        $err = ($lang === 1) ? "การชำระเงินล้มเหลว(1)" : "Payment Failed(1)";
        return isset($message[$code]) 
            ? $message[$code] : $err;
    }
}

if (!function_exists('_2c2p_channel')) {
    function _2c2p_channel(string $code = null, int $lang = 1) {
        if (empty($code)) return;
        $message = array(
            '001' => ($lang === 1) ? "บัตรเครดิตและบัตรเดบิต" : "Credit and debit cards",
            '002' => ($lang === 1) ? "ช่องทางการชำระเงินสด" : "Cash payment channel",
            '003' => ($lang === 1) ? "การหักบัญชีธนาคาร" : "Direct debit",
            '004' => ($lang === 1) ? "อื่น ๆ" : "Others",
            '005' => ($lang === 1) ? "ธุรกรรม IPP" : "IPP transaction"
        );

        $err = ($lang === 1) ? "อื่น ๆ(1)" : "Others(1)";
        return isset($message[$code]) 
            ? $message[$code] : $err;
    }
}

if (!function_exists('_order_status')) {
    function _order_status(string $code = null, int $lang = 1) {
        if (empty($code)) return;
        $message = array(
            '001' => ($lang === 1) 
                ? "<span class='text-warning'>รอการชำระเงิน</span>" 
                : "<span class='text-warning'>Waiting for payment</a>",
            '000' => ($lang === 1) 
                ? "<span class='text-info'>รอการจัดส่ง</span>" 
                : "<span class='text-info'>Ready to ship</span>",
            '1' => ($lang === 1) 
                ? "<span class='text-info'>รอการจัดส่ง</span>" 
                : "<span class='text-info'>Ready to ship</span>",
            '2' => ($lang === 1) 
                ? "<span class='text-primary'>กำลังจัดส่ง</span>" 
                : "<span class='text-primary'>Shipping</span>",
            '3' => ($lang === 1) 
                ? "<span class='text-success'>จัดส่งสำเร็จแล้ว</span>" 
                : "<span class='text-success'>Completed</span>",
            '4' => ($lang === 1) 
                ? "<span class='text-danger'>การชำระเงินล้มเหลว</span>" 
                : "<span class='text-danger'>Payment failed</span>",
            '5' => ($lang === 1)
                ? "<span class='text-danger'>คำสั่งซื้อถูกยกเลิก</span>" 
                : "<span class='text-danger'>Order Canceled</span>"
        );
        $err = ($lang === 1) 
            ? "<span class='text-danger'>มีเกิดข้อผิดพลาด</span>"
            : "<span class='text-danger'>Something went wrong.</span>";
        return isset($message[$code]) 
            ? $message[$code] : $err;
    }
}

if (!function_exists('_order_cancel_status')) {
    function _order_cancel_status(string $code = null, int $lang = 1) {
        if (empty($code)) return;
        $message = array(
            '1' => ($lang === 1) 
                ? "ไม่มีสินค้า" 
                : "Waiting for payment",
            '2' => ($lang === 1) 
                ? "ร้านค้าไม่รับออเดอร์" 
                : "Ready to ship",
            '3' => ($lang === 1) 
                ? "แฟลชเอ็กเพรสยกเลิก" 
                : "Ready to ship",
        );
        $err = ($lang === 1) 
            ? "มีเกิดข้อผิดพลาด"
            : "Something went wrong.";
        return isset($message[$code]) 
            ? $message[$code] : $err;
    }
}