<?php
namespace App\Libraries;
use App\Models\Tb_product;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Libraries\ProductLibrary;
class ProfileLibrary {
    
        
    /**
     * me
     *
     * @return void
     */
    static public function me()
    {
        if (ProductLibrary::hasRole('seller') === false) return false;
        $id = Auth()->user()->id;
        $response = array();
        $fields = array(
            'tb_user_sellers.id',
            'tb_user_sellers.company_name',
            'tb_user_sellers.tax_id',
            'tb_user_sellers.brand_name',
            'tb_user_sellers.url',
            'tb_user_sellers.shop_name',
            'tb_user_sellers.shop_description',
            'tb_user_sellers.person_name',
            'tb_user_sellers.shop_keyword',
            'tb_user_sellers.basic_telephone',
            'tb_user_sellers.email',
            'tb_user_sellers.warehouse_address',
            'tb_user_sellers.warehouse_name',
            'tb_user_sellers.warehouse_telephone',
            'tb_user_sellers.billing_address',
            'tb_user_sellers.account_number',
            'tb_user_sellers.bank_id',
            'tb_user_sellers.account_name',
            'tb_user_sellers.status',
            'users.avatar',
            'tb_banking.name as bank_name'
        );
        $user = DB::table("users")
            ->where('users.id', $id)
            ->select($fields)
            ->join('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'users.id')
            ->join('tb_banking', 'tb_banking.id', '=', 'tb_user_sellers.bank_id')
            ->first();
        if (!$user) return false;

        $response['seller'] = $user;
        $sliders = DB::table("tb_user_seller_sliders")
            ->where('seller_id', $user->id)
            ->select('id','name','rank')
            ->orderBy('rank', 'asc')
            ->orderBy('created_at', 'desc')
            ->limit(5)
            ->get()->toArray();
        
        if ($sliders) {
            $fields = array('id','name','rank');
            $result = array();
            foreach($sliders as $key => $val) {
                $row = array();
                foreach($fields as $field) {
                    if ($field == 'name') {
                        $row['name'] = url('/') . $val->name;
                    } else {
                        $row[$field] = $val->{$field};
                    }
                }
                $result[] = $row;
            }
            $response['sliders'] = $result;
        }

        return array(
            'status' => true,
            'data' => $response
        );
    }

}