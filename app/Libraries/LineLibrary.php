<?php

namespace App\Libraries;
use GuzzleHttp\Client;

class LineLibrary
{
    const API_URL = 'https://notify-api.line.me/api/notify';
    private $token = array();
    private $http = null;
    public function __construct($token = array())
    {
        //  qs4TaMKfEk70DdIh4ZhzPp7id5nZgvulDkyUewUktqh - group bd
        //  pAnkb0tyekvDiyTFc3Aiu0CaymN11HEb8DM0pNy4SAd - group dev
        $this->token = $token;
        $this->http = new Client();
    }
        
    /**
     * send
     *
     * @param  mixed $text
     * @return void
     */
    public function send(string $message = null, array $params = [])
    {
        if (empty($message)) {
            return false;
        }

        if (isset($params['type']) && $params['type'] == false) {
            $text = "\n\r==== ERROR ====\n\r";
            $text.= "MODE: " . config('constants.line_mode') . "\r\n";
            $text.= isset($params['class']) && !empty($params['class']) ? "Class: ". $params['class'] ."\r\n" : '';
            $text.= isset($params['method']) && !empty($params['method']) ? "Method: ". $params['method'] ."\r\n" : '';
            $text.= "Date: ". date('Y-m-d H:i:s') . "\r\n";
            $text.= "Message: " . $message;
        } else if(isset($params['type']) && $params['type'] == true) {
            $text = "\n\r==== SUCCESS ====\n\r";
            $text.= "MODE: " . config('constants.line_mode') . "\r\n";
            $text.= isset($params['class']) && !empty($params['class']) ? "Class: ". $params['class'] ."\r\n" : '';
            $text.= isset($params['method']) && !empty($params['method']) ? "Method: ". $params['method'] ."\r\n" : '';
            $text.= "Date: ". date('Y-m-d H:i:s') . "\r\n";
            $text.= "Message: " . $message;
        } else {
            $text= "MODE: " . config('constants.line_mode') . "\r\n";
            $text.= "Message: " . $message;
        }

        foreach($this->token as $token) {
            $request_params = [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                ],
            ];
            
            $request_params['multipart'] = [
                [
                    'name' => 'message',
                    'contents' => $text
                ]
            ];
            $response = $this->http->request('POST', LineLibrary::API_URL, $request_params);
            if ($response->getStatusCode() != 200) {
                return false;
            }

            $body = (string) $response->getBody();
            $json = json_decode($body, true);
            if (empty($json['status']) || empty($json['message'])) {
                return false;
            }   
        }
        return true;
    }
}