<?php
namespace App\Libraries;
use Illuminate\Support\Facades\DB;
class UserLibrary
{
    /**
     * hasRole
     *
     * @param  mixed $role
     * @return void
     */
    static public function hasRole($role = 'seller')
    {
        $userRoles = Auth()->user()->getRoleNames();
        $status = false;
        foreach($userRoles as $userRole){
            if($userRole == $role){
                $status = true;
                break;
            }
        }
        return $status;
    }
}