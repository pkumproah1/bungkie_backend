<?php
namespace App\Libraries;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;
use App\Models\Tb_product as ProductModel;
class ProductBatchLibrary
{
    public static function download_template(array $params = [])
    {
        if (empty($params)) return array(
            'status' => false,
            'message' => 'Missing data or invalid.'
        );

        if (isset($params['type']) && $params['type'] == "stock") {
            $products = ProductModel::get_product_for_template_stock();
            return self::export_stock($products);
        } elseif (isset($params['type']) && $params['type'] == "price") {
            $products = ProductModel::get_product_for_template_price();
            return self::export_price($products);
        }
    }
    
    /**
     * export_stock
     *
     * @param  mixed $data
     * @return void
     */
    private static function export_stock(object $data = null)
    {
        if (empty($data)) return array(
            'status' => false,
            'message' => 'Product is null.'
        );
        $sheet = new Spreadsheet();

        //* Batch_Stock
        $sheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Seller SKU ID')
            ->setCellValue('B1', 'SKU ID')
            ->setCellValue('C1', 'Quantity');
        $sheet->createSheet();
        //* Batch_Preorder_Stock
        $sheet->setActiveSheetIndex(1)
            ->setCellValue('A1', 'Seller SKU ID')
            ->setCellValue('B1', 'SKU ID')
            ->setCellValue('C1', 'Quantity')
            ->setCellValue('D1', 'Period');
        
        $n = 2;
        if ($data) {
            foreach ($data as $v) {
                //* Batch_Stock
                $sheet->setActiveSheetIndex(0)
                    ->setCellValue('A' . $n, $v->owner)
                    ->setCellValue('B' . $n, $v->sku)
                    ->setCellValue('C' . $n, 0);
                    //* Batch_Preorder_Stock
                $sheet->setActiveSheetIndex(1)
                    ->setCellValue('A' . $n, $v->owner)
                    ->setCellValue('B' . $n, $v->sku)
                    ->setCellValue('C' . $n, 0)
                    ->setCellValue('D' . $n, 0);
                $n++;
            }
        }
        $sheet->getActiveSheet(0)->setTitle('Batch_Stock');
        $sheet->getActiveSheet(1)->setTitle('Batch_Preorder_Stock');
        $fileName = time() . '.xlsx';
        $writer = new Xlsx($sheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');
    }
        
    /**
     * export_price
     *
     * @param  mixed $data
     * @return void
     */
    private static function export_price(object $data = null)
    {
        if (empty($data)) return array(
            'status' => false,
            'message' => 'Product is null.'
        );

        $sheet = new Spreadsheet();
        //* Batch_Stock
        $sheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Seller SKU ID')
            ->setCellValue('B1', 'SKU ID')
            ->setCellValue('C1', 'Full Price (THB)')
            ->setCellValue('D1', 'Full Price (USD)')
            ->setCellValue('E1', 'Discount Price(THB)')
            ->setCellValue('F1', 'Discount Price(USD)')
            ->setCellValue('G1', 'Discount Type(THB)')
            ->setCellValue('H1', 'Discount Type(USD)');
        $n = 2;
        $items = array_reduce($data->toArray(), function($accumulator, $item) {
            $key = $item->sku;
            if (!array_key_exists($key, $accumulator)) {
                $accumulator[$key] = array();
            }
            $accumulator[$key]['owner'] = $item->owner;
            $accumulator[$key]['sku'] = $item->sku;
            if ($item->type_price == 1) {
                $accumulator[$key]['price_thb'] = $item->front_price;
                $accumulator[$key]['discount_thb'] = $item->sale_price;
                $accumulator[$key]['discount_type_thb'] = $item->operation;
            } else {
                $accumulator[$key]['price_usd'] = $item->front_price;
                $accumulator[$key]['discount_usd'] = $item->sale_price;
                $accumulator[$key]['discount_type_usd'] = $item->operation;
            }
            return $accumulator;
        }, []);

        if (!empty($items)) {
            foreach ($items as $v) {
                $sheet->setActiveSheetIndex(0)
                    ->setCellValue('A' . $n, $v['owner'])
                    ->setCellValue('B' . $n, $v['sku'])
                    ->setCellValue('C' . $n, $v['price_thb'])
                    ->setCellValue('E' . $n, $v['price_usd'])
                    ->setCellValue('G' . $n, $v['discount_thb'])
                    ->setCellValue('D' . $n, $v['discount_usd'])
                    ->setCellValue('F' . $n, $v['discount_type_thb'])
                    ->setCellValue('H' . $n, $v['discount_type_usd']);
                $n++;
            }
        }   
        $sheet->getActiveSheet(0)->setTitle('Batch_Price');
        $fileName = time() . '.xlsx';
        $writer = new Xlsx($sheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');
    }
}