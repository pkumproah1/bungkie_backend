<?php
namespace App\Libraries;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Tb_notification as Notification;
class NotifyLibrary
{    
    /**
     * notifys
     *
     * @param  mixed $params
     * @return void
     */
    public static function notifys(array $params = [])
    {
        $notifys = Notification::notifys();
        if (!empty($notifys)) {
            $result = array();
            $number = 0;
            foreach ($notifys as $notify) {
                if ($notify->action == 0) {
                    $number = ++$number;
                }
                $result[$notify->type]['data'][] = array(
                    'id' =>  $notify->id,
                    'message' => json_decode($notify->message, TRUE),
                    'read' => $notify->action == 1 ? 'true' : 'false'
                );
                $result[$notify->type]['count'] = $number;
            }
            return array('status' => true, 'data' => $result);
        }
        return array('status' => false);
    }
}