<?php
namespace App\Libraries;
use App\Models\Tb_banking;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
class SellerLibrary {    
    /**
     * 
     * get All banking
     * @return void
     */
    public function get_banks()
    {
        $bankings = Tb_banking::where('enabled', 1)
            ->select('id', 'value', 'name')
            ->orderBy('name', 'asc')
            ->get();
        if (!$bankings) return array(
            'status' => false
        );

        return array(
            'status' => true,
            'data' => $bankings
        );
    }

    public function get_province($language = "th")
    {
        $province = DB::table('tb_location_province')
            ->select('province_code', ($language === "th") ? 'province_name_th as name' : 'province_name_en as name')
            ->get()->toArray();
        return $province;
    }
    
    /**
     * Find by ID
     * @param  mixed $id
     * @return void
     */
    public function find_bank_by_id($id = null)
    {
        if (empty($id)) return array(
            'status' => false,
            'message' => 'Missing data or invalid ID'
        );

        $bankings = Tb_banking::where('enabled', 1)
            ->where('id', $id)
            ->select('id', 'value', 'name')
            ->first();
            
        if (!$bankings) return array(
            'status' => false
        );

        return array(
            'status' => true,
            'data' => $bankings->toArray()
        );
    }
    
    /**
     * Check email duplicate in Users
     * @param  mixed $username
     * @param  mixed $roles
     * @return void
     */
    public function check_username_duplicate($username = null, $roles = 'seller')
    {
        if (empty($username)) return array(
            'status' => false,
            'message' => 'Missing data or invalid USERNAME'
        );

        $user = Users::where('menuroles', $roles)
            ->where('email', $username)
            ->select('id')
            ->first();
        
        if ($user) {
            return array(
                'status' => false,
                'message' => 'Duplicate Username'
            );
        }
        
        return array(
            'status' => true
        );
    }

}