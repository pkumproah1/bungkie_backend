<?php
namespace App\Libraries;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;

//* models
use App\Models\Tb_order as OrderModel;
class ReportLibrary
{    
    /**
     * paginate_purchase
     *
     * @param  mixed $params
     * @return void
     */
    public static function paginate_purchase(array $params = [])
    {
        $reports = OrderModel::report_purchase($params, null);
        // dd($reports);
        //* merge data
        $response = array();
        $response['data'] = [];
        if ($reports) {
            $items = self::reduce($reports);
            $fields = array(
                'date_create', 'received_date', 'order_number', 'transaction_number', 'quantity',
                'description', 'type', 'payment_status', 'amount', 'shpping_price',
                'shipping_is_paid', 'balance', 'action'
            );
            foreach($items as $key => $item) {
                $row = array();
                foreach($fields as $field) {
                    if ($field === "action") {
                        $row[] = '<a href="#" class="text-info">ดูรายละเอียด</a>';
                    } else {
                        $row[] = isset($item[$field]) ? $item[$field] : null;
                    }
                }
                $response['data'][] = $row;
            }
        }

        $response["draw"] = intval($params['draw']);
        $response["recordsTotal"] = $reports = OrderModel::report_purchase($params, 1);
        $response["recordsFiltered"] = $reports = OrderModel::report_purchase($params, 1);
        return $response;
    }

    /**
     * report_purchase
     *
     * @param  mixed $params
     * @return void
     */
    public static function report_purchase(array $params = [])
    {
        if (empty($params)) return array('status' => false);
        $reports = OrderModel::report_purchase($params, 'export');
        // dd($reports);
        if (empty($reports)) return array('status' => false);
        //* merge data
        $items = self::reduce($reports);
        // dd($items);
        if (empty($items)) return array('status' => false);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'Date Created')
            ->setCellValue('B1', 'Date Received')
            ->setCellValue('C1', 'Transaction Date')
            ->setCellValue('D1', 'Store')
            ->setCellValue('E1', 'Store ID')
            ->setCellValue('F1', 'Order Number')
            ->setCellValue('G1', 'Transaction Number')
            ->setCellValue('H1', 'Quantity')
            ->setCellValue('I1', 'Sub Category (Level 1)')
            ->setCellValue('J1', 'Sub Category (Level 2)')
            ->setCellValue('K1', 'Sub Category (Level 3)')
            ->setCellValue('L1', 'SKU')
            ->setCellValue('M1', 'Item Name')
            ->setCellValue('N1', 'Description')
            ->setCellValue('O1', 'Type')
            ->setCellValue('P1', 'Payment Status')
            ->setCellValue('Q1', 'Payment Reference')
            ->setCellValue('R1', 'Amount')
            ->setCellValue('S1', 'Channel Payment')
            ->setCellValue('T1', 'Shipping Price')
            ->setCellValue('U1', 'Shipping Is Paid')
            ->setCellValue('V1', 'Balance')
            ->setCellValue('W1', 'Total GP Deduction')
            ->setCellValue('X1', 'Delivery Status')
            ->setCellValue('Y1', 'Currency');

            $index = 2;
            foreach ($items as $item) {
                //* Add data
                $sheet = $spreadsheet->getActiveSheet()
                    ->setCellValue('A' . $index, $item['date_create'])
                    ->setCellValue('B' . $index, $item['received_date'])
                    ->setCellValue('C' . $index, $item['transaction_date'])
                    ->setCellValue('D' . $index, $item['store'])
                    ->setCellValue('E' . $index, $item['store_id'])
                    ->setCellValue('F' . $index, $item['order_number'])
                    ->setCellValue('G' . $index, $item['transaction_number'])
                    ->setCellValue('H' . $index, $item['quantity'])
                    ->setCellValue('I' . $index, isset($item['category_name'][0]) && !empty($item['category_name'][0]['name']) ? $item['category_name'][0]['name'] : '-')
                    ->setCellValue('J' . $index, isset($item['category_name'][1]) && !empty($item['category_name'][1]['name']) ? $item['category_name'][1]['name'] : '-')
                    ->setCellValue('K' . $index, isset($item['category_name'][2]) && !empty($item['category_name'][2]['name']) ? $item['category_name'][2]['name'] : '-')
                    ->setCellValue('L' . $index, $item['sku'])
                    ->setCellValue('M' . $index, $item['item_name'])
                    ->setCellValue('N' . $index, $item['description'])
                    ->setCellValue('O' . $index, $item['type'])
                    ->setCellValue('P' . $index, $item['payment_status'])
                    ->setCellValue('Q' . $index, $item['payment_reference'])
                    ->setCellValue('R' . $index, $item['amount'])
                    ->setCellValue('S' . $index, $item['payment_channel'])
                    ->setCellValue('T' . $index, $item['shpping_price'])
                    ->setCellValue('U' . $index, $item['shipping_is_paid'])
                    ->setCellValue('V' . $index, $item['balance'])
                    ->setCellValue('W' . $index, $item['gp'])
                    ->setCellValue('X' . $index, $item['delivery'])
                    ->setCellValue('Y' . $index, $item['currency']);
                ++$index;
            }
            
            $writer = new Xlsx($spreadsheet);
            ob_start();
            $writer->save('php://output');
            $content = ob_get_contents();
            ob_end_clean();

            $path = "report/" . date('Y-m-d') . "/";
            $filename = "purchase_".date('His').".xlsx";
            \Storage::disk('public_download')->put($path . $filename, $content); 

        return array('status' => true, 'data' => array(
            'filename' => $filename,
            'path' => $path,
            'download' => base64_encode($content)
        ));
        
    }
    
    /**
     * reduce
     *
     * @param  mixed $data
     * @return void
     */
    public static function reduce($data)
    {
        if (empty($data)) return false;
        $items = array_reduce($data, function($accumulator, $item) {
            $key = $item->item_id;
            if (!array_key_exists($key, $accumulator)) {
                $cate[] = array('name' => $item->category_name, 'rate' => $item->commission_rate);
                $accumulator[$key]['order_amount'] = array(
                    'date_create' => date("m/d/y H:i", strtotime($item->date_create)),
                    'received_date' => date("m/d/y H:i", strtotime($item->received_date)),
                    'transaction_date' => date("m/d/y H:i", strtotime($item->transaction_date)),
                    'store' => $item->store,
                    'store_id' => $item->store_id,
                    'order_number' => $item->order_number,
                    'transaction_number' => $item->transaction_number,
                    'quantity' => $item->quantity,
                    'category_name' => $cate,
                    'sku' => $item->sku,
                    'item_name' => $item->item_name,
                    'description' => "Order amount for " . $item->item_name,
                    'type' => "Order amount",
                    'payment_status' => _2c2p_status($item->paid, 2),
                    'payment_reference' => "Payment_Confirmation_Automatic_Invoice_". $item->transaction_number ."_" . date("m/d/Y", strtotime($item->transaction_date)),
                    'amount' => number_format($item->amount, 2),
                    'payment_channel' => _2c2p_channel($item->channal_payment),
                    'shpping_price' => number_format($item->shipping_price, 2),
                    'shipping_is_paid' => ($item->is_paid == 1) ? "Yes" : "No",
                    'balance' => number_format(($item->amount + $item->shipping_price),2),
                    'gp' => '-',
                    'delivery' => 'Delivered',
                    'currency' => ($item->currency != "764") ? "USD" : "THB"
                );
            }  else {
                $accumulator[$key]['order_amount']['category_name'][] = array('name' => $item->category_name, 'rate' => $item->commission_rate);
            }

            //! คำนวน commission
            $accumulator[$key]['commission'] = $accumulator[$key]['order_amount'];
            $commission_rate = end($accumulator[$key]['order_amount']['category_name'])['rate'];
            $commission =  (($item->amount + $item->shipping_price) * $commission_rate) / 100;
            $accumulator[$key]['commission']['amount'] = $commission;
            $accumulator[$key]['commission']['shpping_price'] = 0;
            $accumulator[$key]['commission']['balance'] = $commission;
            $accumulator[$key]['commission']['description'] = "Commission (excl. tax)";
            $accumulator[$key]['commission']['type'] = "Commission";
            $accumulator[$key]['commission']['payment_status'] = "To Pay";

            //! คำนวน commission tax
            $accumulator[$key]['commission_tax'] = $accumulator[$key]['order_amount'];
            $commission_tax = ($commission * 7) / 100;
            $accumulator[$key]['commission_tax']['amount'] = $commission_tax;
            $accumulator[$key]['commission_tax']['shpping_price'] = 0;
            $accumulator[$key]['commission_tax']['balance'] = $commission_tax;
            $accumulator[$key]['commission_tax']['gp'] = ($commission_tax + $commission);
            $accumulator[$key]['commission_tax']['description'] = "Commission tax (Tax 7.00%)";
            $accumulator[$key]['commission_tax']['type'] = "Commission tax";
            $accumulator[$key]['commission_tax']['payment_status'] = "To Pay";
            return $accumulator;
        }, []);

        if (empty($items)) return false;
        $result = array();
        $column = array('order_amount', 'commission', 'commission_tax');
        foreach($items as $key => $item) {
            foreach($column as $c) {
                $result[] = $item[$c];
            }
        }
        return $result;
    }   
}