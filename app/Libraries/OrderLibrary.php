<?php
namespace App\Libraries;
use Illuminate\Support\Facades\DB;
use App\Models\Tb_order as OrderModel;
use App\Models\Tb_order_purchase as OrderPurchaseModel;
use App\Models\Tb_user_address as UserAddressModel;
use App\Models\Tb_order_shipping as OrderShipping;
use App\Models\Tb_order_shipping_tracking as OrderShippingTracking;
use App\Models\Tb_order_item_cancel as OrderItemCancel;
use App\Models\Tb_user_sellers as UserSellerModel;
use App\Libraries\Api\FlashexpressLibrary;
use Carbon\Carbon;
use App\Libraries\LineLibrary;
class OrderLibrary
{
    /**
     * paginate
     *
     * @param  mixed $params
     * @return void
     */
    public static function paginate(array $params = [])
    {
        if (empty($params)) return false;
        $pagenate = OrderModel::paginate($params);
        $response = array();
        $response['data'] = [];
        if ($pagenate) {
            $fields = array('reference', 'price', 'display', 'shop_name', 'status', 'created', 'action');
            foreach($pagenate as $key => $item) {
                $row = array();
                foreach($fields as $field) {
                    if ($field == "reference") {
                        $html = '<a href="'. url('/') .'/order/view/' . $item->reference_id . '/' . $item->shop_id .'" class="reference text-info">' . $item->reference_id . '<a>';
                        $row[] = $html;
                    } else if($field == "price") {
                        $total = $item->price_total;
                        if ($item->shipping_is_paid == '1') {
                            $total = ($item->price_total + $item->shopping_total);
                        }
                        $row[] = '<span>'. number_format($total, 2) .'</span>';
                    } else if($field == "display") {
                        $row[] = '<span>'. $item->display_name. '</span>';
                    } else if($field == "status") {
                        $order_status = ($item->status == '1') ? _order_status($item->payment_status) : _order_status($item->status);
                        $row[] = $order_status;
                    } else if($field == "created") {
                        $row[] = '<span>'. $item->created_at .'</span>';
                    } else if($field == "action") {
                       $row[] = self::action_paginate($item->status, $item->reference_id, $item->shop_id);
                    } else {
                        $row[] = isset($item->{$field}) ? $item->{$field} : '';
                    }
                }
                $response['data'][] = $row;
            }
        }       
        $response["draw"] = intval($params['draw']);
        $response["recordsTotal"] = $pagenate = OrderModel::paginate($params, 'total');
        $response["recordsFiltered"] = $pagenate = OrderModel::paginate($params, 'total');
        return $response;
    }
    
    /**
     * action_paginate
     *
     * @param  mixed $status
     * @param  mixed $reference
     * @return void
     */
    private static function action_paginate(int $status = null, string $reference = null, int $shop_id)
    {
        $html = '<div class="action">';
        switch ($status) {
            case '0':
            case '1':
            case '2':
                $html.= '<a href="'. url('/') .'/order/view/' . $reference . '/' . $shop_id .'" class="text-info">จัดการ</a>';
                break;
            case '3':
                $html.= '<a href="'. url('/') .'/order/view/' . $reference . '/' . $shop_id .'" class="text-success">ดูรายละเอียด</a>';
                break;
            case '4':
            case '5':
                $html.= '<a href="'. url('/') .'/order/view/' . $reference . '/' . $shop_id .'" class="text-danger">ดูรายละเอียด</a>';
                break;
        }
        $html.= '</div>';
        return $html;
    }
    
    /**
     * detail
     *
     * @param  mixed $reference
     * @return void
     */
    public static function detail(string $reference = null, int $shop_id = null)
    {
        // ! check validate
        if(empty($reference) && empty($shop_id)) return array('status' => false);
        // * get oders 
        $orders = OrderModel::detail($reference, $shop_id);
        // ? ถ้ามี order
        if ($orders) {
            $cancels = OrderItemCancel::find_by_order_id($orders[0]->order_id);
            // dd($cancels);
            //! update tracking
            if(isset($orders[0]->pno) && !empty($orders[0]->pno)) {
                self::shipping_routes($orders[0]->pno);
            }
            //? group data
            $items = array_reduce($orders, function($accumulator, $item) use($reference) {
                $key = $item->shop_id;
                $shipping = 0;
                if (!array_key_exists($key, $accumulator)) {
                    $accumulator[$key] = array();
                }
                $accumulator[$key]['customer'] = array(
                    'name' => $item->customer_name,
                    'address' => $item->address,
                    'address2' => $item->province_name_th . ' ' . $item->amphur_name_th . ' ' . $item->district_name_th . ' ' . $item->zipcode,
                    'phone' => $item->customer_phone,
                    'email' => $item->customer_email
                );
                $accumulator[$key]['shop'] = array(
                    'shop_name' => $item->shop_name,
                    'name' => $item->warehouse_name,
                    'phone' => $item->warehouse_telephone,
                    'email' => $item->email,
                    'company' => $item->company_name,
                    'tax' => $item->tax_id
                );
                //! list item products
                $path = ($item->is_new === 1)
                    ? url('/') . '/public/statics/images/products/' . $item->product_id . '/m/'
                    : url('/') . '/public/statics/images/';
                $img = empty($item->path) ? 'default.jpg' : $item->path;
                $accumulator[$key]['products'][] = array(
                    'id' => $item->product_id,
                    'status' => $item->status,
                    'name' => $item->name,
                    'image' => $path . $item->path,
                    'attr' => !empty($item->remark) ? json_decode($item->remark, TRUE) : null,
                    'price' => $item->sum_price,
                    'qty' => $item->qty
                );
                
                $shipping += $item->shipping_price;
                $accumulator[$key]['order'] = array(
                    'order' => $reference,
                    'status' => $item->status,
                    'date' => Carbon::parse($item->created_at)->locale('th')->isoFormat("LLL"),
                    'shipping' => ($item->shipping_is_paid == 1) ? $shipping : 0,
                );
                //! shpping
                $accumulator[$key]['shipping']['pno'] = $item->pno;
                $accumulator[$key]['shipping']['price'] = $shipping;
                $accumulator[$key]['shipping']['is_paid'] = $item->shipping_is_paid;
                if ($item->pno != null) {
                    $pdfs = DB::table('tb_order_shipping_print')->where('shipping_id', $item->shipping_id)
                        ->get()->toArray();
                    if ($pdfs) {
                        foreach($pdfs as $pdf) {
                            $accumulator[$key]['shipping']['download'][$pdf->id] = array(
                                'size' => $pdf->size,
                                'file' => url('/') . '/public/download/flash/print/' . $pdf->name
                            );
                        }
                    }
                }
                return $accumulator;
            }, []);
            // * success
            $item = array_values($items)[0];
            $subtotal = array_reduce($item['products'], function($carry, $item) {
                $carry += $item['price'];
                return $carry;
            });
            $cancal_data = array();
            if ($cancels) {
                foreach($cancels as $cancel) {
                    $cancal_data[] = array(
                        'cancel_id' => $cancel->cancel_id,
                        'refund' => ($cancel->refund == 1) ? 'ต้องคืนเงินลูกค้า' : 'ไม่ต้องคืนเงินลูกค้า',
                        'status' => $cancel->status,
                        'purchase' => json_decode($cancel->detail),
                        'type' => _order_cancel_status($cancel->type_cancel),
                        'created_at' => Carbon::parse($cancel->created_at)->locale('th')->isoFormat("LLL"),
                        'created_by' => $cancel->c_name,
                        'updated_at' => Carbon::parse($cancel->updated_at)->locale('th')->isoFormat("LLL"),
                        'updated_by' => $cancel->u_name
                    );
                }
            }
            return array('status' => true, 'data' => array(
                'items' => $item,
                'cancels' => $cancal_data,
                'sumary' => array(
                    'item_count' => count(array_values($items)[0]['products']),
                    'subtotal' => $subtotal,
                    'shipping' => $item['order']['shipping'],
                    'total' => ($subtotal + $item['order']['shipping'])
                )
            ));
        }
        // ! error
        return array('status' => false);
    }
    
    /**
     * shipping_create
     *
     * @param  mixed $reference
     * @param  mixed $shop_id
     * @return void
     */
    public static function shipping_create(string $reference = null, int $shop_id = null)
    {
        if (empty($reference) && empty($shop_id)) return array(
            'status' => false,
            'message' => "Missing Data or invalid."
        );

        $seller = OrderModel::find_order($reference, $shop_id);
        if ($seller) {
            $customer = UserAddressModel::find_address_by_id($seller->customer_address_id);
            if ($customer) {
                $result = self::create_shipping_order($reference, $seller, $customer);
                if (isset($result['code']) && $result['code'] === 1) {

                    // ? อัพเดทเป็น กำลังเตรียมจัดส่ง
                    $shipping_id = DB::table('tb_order_shipping')->insertGetId([
                        'reference_id' => $reference,
                        'provider_id' => 1,
                        'user_id' => $seller->user_id,
                        'shop_id' => $shop_id,
                        'pno' => $result['data']['pno'],
                        'status' => 0,
                        'created_at' => now(),
                        'created_by' => Auth()->user()->id
                    ]);

                    $notify = new LineLibrary([
                        'pAnkb0tyekvDiyTFc3Aiu0CaymN11HEb8DM0pNy4SAd', // All
                    ]);
                    
                    $success_text = "\r\nคุณ: ". Auth()->user()->display_name . " ได้ทำการสร้างออเดอร์เรียบร้อยแล้ว\r\n";
                    $success_text.= "=======================================\r\n";
                    $success_text.= "เลขที่ออเดอร์: " . $reference  . "\r\n";
                    $success_text.= "รายละเอียดเพิ่มเติม: " . url('order/view/' . $reference . '/' . $shop_id)  . "\r\n";
                    $success_text.= "=======================================\r\n";
                    $notify->send($success_text);

                    if ($result['download']) {
                        $prints = array();
                        foreach ($result['download'] as $key => $dl) {
                            $prints[] = array(
                                'shipping_id' => $shipping_id,
                                'size' => ($key === "big") ? 'b' : 'm',
                                'name' => $dl['name']
                            );
                        }
                        DB::table('tb_order_shipping_print')->insert($prints);
                    }

                    OrderModel::where('reference_id', $reference)->update(['status' => 1]);
                    return array('status' => true, 'data' => $result);
                } else {
                    DB::table('tb_order_shipping')->insert([
                        'reference_id' => $reference,
                        'provider_id' => 1,
                        'user_id' => $seller->user_id,
                        'shop_id' => $shop_id,
                        'pno' => null,
                        'status' => 999,
                        'created_at' => now()
                    ]);
                    return array('status' => false);
                }
            }
        }
        return array('status' => false);
    }
        
    /**
     * cancel_order
     *
     * @param  mixed $reference
     * @param  mixed $shop_id
     * @param  mixed $type_cancel
     * @return void
     */
    public static function cancel_order(string $reference = null, int $shop_id = null, int $type_cancel = null)
    {
        if (empty($reference) && empty($shop_id) && empty($type_cancel)) return array(
            'status' => false,
            'message' => "Missing Data or invalid."
        );
        
        $order = OrderModel::find_order($reference, $shop_id);
        if (empty($order)) return array(
            'status' => false,
            'message' => 'Order not supported'
        );

        //! ไม่สามารถยกเลิกสินค้าที่ถูกส่งถึงมือลูกค้าแล้ว
        if ($order->status == 3) return array(
            'status' => false,
            'message' => 'ไม่สามารถยกเลิกสินค้าที่ถูกส่งถึงมือลูกค้าแล้ว'
        );

        $response = array();
        //! check payment
        $purchase = OrderPurchaseModel::find_by_order_id2($order->order_id, $shop_id);
        if (!empty($purchase)) {
            $logs = json_decode($purchase->logs, TRUE);
            $amount = $purchase->total_price;
            if ($purchase->shipping_is_paid == 1) {
                $amount = ($purchase->total_price + $purchase->total_shipping);
            }
            $response['purchase'] = array(
                'paid_channel' => !empty($logs['request']['paid_channel']) ? $logs['request']['paid_channel'] : "2C2P",
                'paid_agent' => !empty($logs['request']['paid_agent']) ? $logs['request']['paid_agent'] : "2C2P",
                'transaction_ref' => $purchase->transaction_ref,
                'approval_code' => $purchase->approval_code,
                'payment_status' => _2c2p_status($purchase->payment_status, 1),
                'payment_channel' => _2c2p_channel($purchase->payment_channel, 1),
                'product_price' => number_format($purchase->total_price, 2),
                'shipping_price' => ($purchase->shipping_is_paid == 1) ? number_format($purchase->total_shipping, 2) : 0,
                'total' => number_format($amount, 2)
            );
        }
        //! check shipping
        $shippings = OrderShipping::find_order_by_shop_id($order->shop_id);
        if (!empty($shippings)) {
            $number = count($shippings);
            foreach($shippings as $key => $shipping) {
                if ($shipping->reference_id == $reference) {
                    if ($shipping->provider_id === 1) {
                        $flashLibrary = new FlashexpressLibrary();
                        $result = $flashLibrary->request("open/v1/orders/". $shipping->pno ."/routes", []);
                        if (isset($result['code']) && $result['code'] === 1) {
                            $cancel_order = $flashLibrary->request("open/v1/orders/". $shipping->pno ."/cancel", []);
                            $cancel_courier = $flashLibrary->request("/open/v1/notify/". $shipping->notify_pickup_id ."/cancel", []);
                            $response['courier'] = $cancel_courier;
                            $response['order'] = $cancel_order;                            
                        }
                        // DELETE Shipping and tracking
                        DB::table('tb_order_shipping')->where('pno', $shipping->pno)->delete();
                        DB::table('tb_order_shipping_tracking')->where('shipping_id', $shipping->shipping_id)->delete();
                    }
                }
            }
        }
        if ($type_cancel == 2 || $type_cancel == 3) {
            DB::table('tb_order_item')->where('order_id', $order->order_id)->update([
                'status' => 1,
                'updated_at' => now()
            ]);
        } else {
            //! update status items for cancel
            DB::table('tb_order_item')->where('order_id', $order->order_id)->update([
                'status' => 5,
                'updated_at' => now()
            ]);
        }
        //! Insert cancel order
        DB::table('tb_order_item_cancel')->insert([
            'order_id' => $order->order_id,
            'type_cancel' => $type_cancel,
            'refund' => isset($purchase->payment_status) && ($purchase->payment_status == 000) ? 1 : 0,
            'status' => 0,
            'detail' => json_encode($response['purchase']),
            'created_by' => Auth()->user()->id,
            'created_at' => now()
        ]);

        $list_cancel_order = array(
            '1' => "ไม่มีสินค้า",
            '2' => "ร้านไม่พร้อมส่ง",
            '3' => "แฟลชเอ็กเพรสขอยกเลิก"
        );

        $notify = new LineLibrary([
            'pAnkb0tyekvDiyTFc3Aiu0CaymN11HEb8DM0pNy4SAd',// All
        ]);

        $user = Auth()->user()->find($order->user_id);
        $success_text = "\r\nคุณ: ". Auth()->user()->display_name . " ได้ทำการยกเลิกออเดอร์เรียบร้อยแล้ว\r\n";
        $success_text.= "=======================================\r\n";
        $success_text.= "เลขที่ออเดอร์: " . $reference  . "\r\n";
        $success_text.= "ร้านค้า: " . $order->shop_name  . "\r\n";
        $success_text.= "=======================================\r\n";
        $success_text.= "ชื่อลูกค้า: " . $user->display_name  . "\r\n";
        $success_text.= "อีเมลลูกค้า: " . $user->email  . "\r\n";
        $success_text.= "หมายเลขโทรศัพท์ลูกค้า: " . $user->tel  . "\r\n";
        $success_text.= "=======================================\r\n";
        $success_text.= "หมายเหตุที่ยกเลิกออเดอร์: " . $list_cancel_order[$type_cancel]  . "\r\n";
        $success_text.= "รายละเอียดเพิ่มเติม: " . url('order/cancel/' . $reference . '/' . $order->shop_id)  . "\r\n";
        $success_text.= "=======================================\r\n";
        $notify->send($success_text);
        return array(
            'status' => true
        );
    }
    
    /**
     * create_shipping_order
     *
     * @param  mixed $reference
     * @param  mixed $seller
     * @param  mixed $customer
     * @return void
     */
    private static function create_shipping_order(string $reference = null, $seller, $customer)
    {
        $flashLibrary = new FlashexpressLibrary();
        // * create order
        $result = $flashLibrary->request("open/v3/orders", [
            'outTradeNo' => $reference,
            'srcName' => $seller->shop_name,
            'srcPhone' => $seller->warehouse_telephone,
            'srcProvinceName' => $seller->province_name_th,
            'srcCityName' => str_replace("เขต", "", $seller->amphur_name_th),
            'srcDistrictName' => $seller->district_name_th,
            'srcPostalCode' => strval($seller->warehouse_zipcode),
            'srcDetailAddress' => $seller->warehouse_address .' ' . $seller->province_name_th . ' '. $seller->amphur_name_th . ' ' . $seller->district_name_th . $seller->warehouse_zipcode,
            'dstName' => $customer->display_name,
            'dstPhone' => ($customer->phone) ? $customer->phone : '',
            'dstProvinceName' => $customer->province_name_th,
            'dstCityName' => str_replace("เขต", "", $customer->amphur_name_th),
            'dstDistrictName' => $customer->district_name_th,
            'dstPostalCode' => strval($customer->zipcode),
            'dstDetailAddress' => $customer->address . ' ' . $customer->province_name_th . ' '. $customer->amphur_name_th . ' ' . $customer->district_name_th . $customer->zipcode,
            'articleCategory' => 1,
            'expressCategory' => 1,
            'weight' => ($seller->weight >= 1000) ? $seller->weight : 1000,
            'insured' => 0,
            'insureDeclareValue' => 0,
            'insureDeclareValue' => 0,
            'freightInsureEnabled' => 0,
            'codEnabled' => 0,
            'codAmount' => 0
        ]);

        if ($result['code'] === 1) {
            $big = $flashLibrary->generate_pdf("open/v1/orders/". $result['data']['pno'] ."/pre_print");
            $small = $flashLibrary->generate_pdf("open/v1/orders/". $result['data']['pno'] ."/small/pre_print");
            if ($big['status'] === true && $small['status'] === true) {
                $result['download']['big'] = $big;
                $result['download']['small'] = $small;
            } else {
                return false;
            }
            
        } else {
            return false;
        }
        
        return $result;
    }
    
    /**
     * shipping_notify
     *
     * @param  mixed $shop_id
     * @return void
     */
    public static function shipping_notify()
    {
        $order_shipping = OrderShipping::find_order_for_notify_flash();
        $line_token[] = "pAnkb0tyekvDiyTFc3Aiu0CaymN11HEb8DM0pNy4SAd";
        $notify = new LineLibrary($line_token);
        if (!empty($order_shipping)) {
            $flashLibrary = new FlashexpressLibrary();
            foreach($order_shipping as $key => $order) {
                $result = $flashLibrary->request("open/v1/notify", [
                    'srcName' => "บริษัทเซ็นทรัลสยาม",
                    // 'srcName' => $order->shop_name,
                    'srcPhone' => $order->warehouse_telephone,
                    'srcProvinceName' => $order->province_name_th,
                    'srcCityName' => str_replace("เขต", "", $order->amphur_name_th),
                    'srcDistrictName' => $order->district_name_th,
                    'srcPostalCode' => strval($order->warehouse_zipcode),
                    'srcDetailAddress' => $order->warehouse_address .' ' . $order->province_name_th . ' '. $order->amphur_name_th . ' ' . $order->district_name_th . $order->warehouse_zipcode,
                    'estimateParcelNumber' => $order->estimate,
                    'remark' => "รบกวนแจ้งร้านค้าด้วยว่ามารับของจาก บริษัทเซ็นทรัลสยาม"
                ]);

                if ($result['code'] == "1") {
                    DB::table('tb_order_shipping_tracking')->insert([
                        'shipping_id' => $order->id,
                        'status' => 0, // รอการจัดส่ง
                        'message' => 'รับออเดอร์แล้ว อยู่ระหว่างจัดส่ง',
                        'remark' => json_encode($result['data']),
                        'created_at' => now()
                    ]);

                    // * update notify
                    DB::table('tb_order_shipping')
                        ->where(['reference_id' => $order->reference_id, "shop_id" => $order->shop_id])
                        ->update([
                            'status' => 1,
                            'updated_at' => now(),
                            'notify_pickup_id' => $result['data']['ticketPickupId'] ,
                            'staff_id' => $result['data']['staffInfoId']
                        ]);
                    //* update items for กำลังจัดส่ง
                    DB::table('tb_order_item')->where('order_id', $order->order_id)->update([
                        'status' => 2,
                        'updated_at' => now()
                    ]);
                    $notify->send("\r\nFlashExpress จะเดินทางไปรับสินค้าที่ร้าน: " . $order->shop_name . "\r\nORDER: " . $order->reference_id . "\r\nเวลา: " . $result['data']['timeoutAtText'] . "\r\nโดย: " . $result['data']['staffInfoName'] . ' ' . $result['data']['staffInfoPhone'] . "\r\n\r\nPickupId: " . $result['data']['ticketPickupId'] . "\r\nStaffId: " . $result['data']['staffInfoId']);
                
                } else {
                    $dev_token[] = "pAnkb0tyekvDiyTFc3Aiu0CaymN11HEb8DM0pNy4SAd";
                    $n = new LineLibrary($dev_token);
                    $n->send("ไม่สามารถเข้าไปรับของร้าน: " . $order->shop_name,[
                        'type' => false,
                        'class' => 'OrderLibrary',
                        'method' => 'shipping_notify'
                    ]);
                    
                    // ! flag error 
                    DB::table('tb_order_shipping')->where(['reference_id' => $order->reference_id, "shop_id" => $order->shop_id])->update([
                        'status' => 0,
                        'updated_at' => now()
                    ]);
                }
            }
        }
    }
    
    /**
     * shipping_routes
     *
     * @param  mixed $pno
     * @return void
     */
    public static function shipping_routes(string $pno = null)
    {
        $order = OrderShipping::find_order_by_pno($pno);
        if (!empty($order)) {
            $flashLibrary = new FlashexpressLibrary();
            $result = $flashLibrary->request("open/v1/orders/". $pno ."/routes", []);
            if ($result['code'] == "1") {
                if (!empty($result['data']['routes'])) {
                    if ($result['data']['state'] == "5" && $order->order_status != "3") {
                        // * อัพเดทเป็นจัดส่งแล้ว
                        DB::table('tb_order')->where('id', $order->order_id)
                            ->update(['status' => 3]);
                    } else {
                        // * อัพเดทเป็นกำลังจัดส่ง
                        DB::table('tb_order')->where('id', $order->order_id)
                            ->update(['status' => 2]);
                    }
                    $olds = OrderShippingTracking::where('shipping_id', $order->shipping_id)
                        ->get()->toArray();
                    if (!empty($result['data']['routes'])) {
                        // ? check old
                        $sub_item = array_column($result['data']['routes'], 'message');
                        foreach($olds as $old) {
                            if (in_array($old['message'], $sub_item)) {
                                $key = array_search($old['message'], $sub_item);
                                unset($result['data']['routes'][$key]);
                            }
                        }
                    }

                    if (!empty($result['data']['routes'])) {
                        foreach($result['data']['routes'] as $route) {
                            DB::table('tb_order_shipping_tracking')->insert([
                                'shipping_id' => $order->shipping_id,
                                'status' => $route['state'],
                                'message' => $route['message'],
                                'created_at' => date('Y-m-d H:i:s', $route['routedAt']),
                                'remark' => json_encode($route)
                            ]);
                        }
                    }
                }
            } else {
                return array('status' => false);
            }
            $olds = OrderShippingTracking::where('shipping_id', $order->shipping_id)
                ->orderBy('created_at', 'desc')
                ->get()->toArray();
        
            if (!empty($olds)) {
                $result = array();
                foreach($olds as $old) {
                    $result[] = array(
                        'created_at' => Carbon::parse($old['created_at'])->locale('th')->isoFormat("LLL"),
                        'message' => $old['message']
                    );
                }
                return array('status' => true, 'data' => $result);
            } else {
                return array('status' => false);
            }
        }
        return array('status' => false);
    }

    /**
     * flash_notifications
     *
     * @return void
     */
    public static function flash_notifications(string $date = null)
    {
        $flashLibrary = new FlashexpressLibrary();
        // * create order
        $result = $flashLibrary->request("open/v1/notifications", ['date' => $date]);
        return $result;
    }

    /**
     * flash_notifications
     *
     * @return void
     */
    public static function flash_cancel(int $id = null)
    {
        if (!empty($id)) {
            $flashLibrary = new FlashexpressLibrary();
            // * create order
            $result = $flashLibrary->request("open/v1/notifications", ['date' => date('Y-m-d')]);
            return $result;
        }
    }
}