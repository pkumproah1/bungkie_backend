<?php
namespace App\Libraries\Api;
use Illuminate\Support\Facades\DB;
use App\Mail\Mailer;
use App\Models\Tb_order as OrderModel;
use App\Models\Tb_order_item as OrderItemModel;
use App\Models\Tb_notification as Notification;
use Carbon\Carbon;
use App\Libraries\LineLibrary;
class PaymentLibrary
{
    /**
     * _2c2p
     *
    * @param  array $params
     * amount
     * product_name
     * order_id
     * currency
     * @return void
     */
    static public function _2c2p($params = array())
    {
        if (empty($params)) return array(
            'status' => false
        );
        
        $payment_option = "123,CC,BANK";
        $merchant_id = config('constants.2c2p.merchant_id');
        $secret_key = config('constants.2c2p.secret_key');
        $currency = (strtolower($params['currency']) === "usd") ? '840' : '764';
        $amount = $params['amount'];
        // $payment_expiry = date('Y-m-d H:i:s', strtotime('4 minute'));
        $version = '8.5';
        $payment_url = config('constants.2c2p.payment_url');
        $result_url = config('constants.2c2p.result_url') . $params['order_id'];
        $hash = $version.$merchant_id.$params['product_name'].$params['order_id'].$currency.$amount.$result_url.$payment_option;
        $hash_value = hash_hmac('sha256', $hash, $secret_key, false);
        $response = array(
            'status' => true,
            'status_code' => 200,
            'action' => $payment_url,
            'data' => array(
                'version' => $version,
                'merchant_id' => $merchant_id,
                'currency' => $currency,
                'result_url_1' => $result_url,
                'payment_option' => $payment_option,
                // 'payment_expiry' => $payment_expiry,
                'hash_value' => $hash_value,
                'payment_description' => $params['product_name'],
                'order_id' => $params['order_id'],
                'amount' => $amount
            )
        );

        return array(
            'status' => true,
            'data' => $response
        );
    }
     
    /**
     * process
     *
     * @param  mixed $request
     * @return void
     */
    public static function process(object $request = null)
    {
        if (empty($request)) { 
            // !notify
            $notify->send("ไม่มี request", [
                'type' => false,
                'class' => "PaymentLibrary",
                'method' => "process"
            ]); 
            return false;
        }
        $log_id = DB::table('tb_order_purchase_logs')->insertGetId([
            'logs' => json_encode([
                'request' => $request->all()
            ]),
            'status' => 0,
            'created_at'=> now()
        ]);
        $order = OrderModel::select(
            'users.email', 'amount', 'tb_order.id', 'users.display_name',
            'user_address.id as address_id', 'name as shipping_name', 'users.id as user_id',
            'phone', 'address', 'zipcode', 'province_name_th', 'amphur_name_th', 'district_name_th',
            'tb_order.payment_status', 'tb_order_item.shop_id'
        )
        ->join('tb_order_item', 'tb_order_item.order_id', '=', 'tb_order.id')
        ->join('users', 'tb_order.user_id', '=', 'users.id')
        ->join('user_address', 'user_address.id', '=', 'tb_order_item.customer_address_id')
        ->join('tb_location_province', 'tb_location_province.province_code', '=', 'user_address.province')
        ->join('tb_location_amphur', 'tb_location_amphur.amphur_code', '=', 'user_address.city')
        ->join('tb_location_district', 'tb_location_district.district_code', '=', 'user_address.district')
        ->where(['tb_order.reference_id' => $request->order_id])
        ->first();
        
        if (empty($order)) return;
        
        if ($order->payment_status !== "000") {
            // ! ไม่มี order
            if (empty($order)) {
                // ! save logs
                DB::table('tb_logs')->insert([
                    'class' => strtoupper('PaymentLibrary'),
                    'method' => strtoupper('process'),
                    'name' => 'ออเดอร์ไม่ตรง',
                    'json_data' => json_encode($request->all()),
                    'created_at' => now(),
                ]);   
                // !notify
                $notify->send("ออเดอร์ไม่ตรง", [
                    'type' => false,
                    'class' => "PaymentLibrary",
                    'method' => "process"
                ]); 
                return false;
            }
            $amount = str_pad(filter_var(number_format($order->amount,2), FILTER_SANITIZE_NUMBER_INT), 12, '0', STR_PAD_LEFT);
            //! เงินไม่ตรง
            if ($amount !== $request->amount) {
                // ! save logs
                DB::table('tb_logs')->insert([
                    'class' => strtoupper('PaymentLibrary'),
                    'method' => strtoupper('process'),
                    'name' => 'เงินไม่ตรง',
                    'json_data' => json_encode($request->all()),
                    'created_at' => now(),
                ]);
                // !notify
                $notify->send("เงินไม่ตรง", [
                    'type' => false,
                    'class' => "PaymentLibrary",
                    'method' => "process"
                ]);
                return false;
            }

            if (($request->payment_status == "000") || ($request->payment_status == "001")) {
                // ! update order
                OrderModel::where(['id' => $order->id])
                    ->update(['status' => 1, 'payment_status' => $request->payment_status]);
                // ! purchase update order item fall
                $status =  ($request->payment_status == "000") ? 1 : 2;
                OrderItemModel::where('order_id', $order->id)
                    ->update(['status' => $status]);
                    
                //! notify
                if ($request->payment_status == "000") {

                    $notify = new LineLibrary([
                        'pAnkb0tyekvDiyTFc3Aiu0CaymN11HEb8DM0pNy4SAd',
                        'qs4TaMKfEk70DdIh4ZhzPp7id5nZgvulDkyUewUktqh' // bd
                    ]);
                    $provider = !empty($request->paid_agent) ? $request->paid_agent : '2c2p';
                    $currency = $request->currency == '764' ? 'THB' : 'USD';
                    $success_text = "\r\n==== มีออเดอร์ใหม่ ====\r\n";
                    $success_text.= "ชื่อลูกค้า: " . $order->display_name . "\r\n";
                    $success_text.= "เบอร์โทรศัพท์: " . $order->phone . "\r\n";
                    $success_text.= "=======================================\r\n";
                    $success_text.= "สถานะการจ่ายเงิน: ". _2c2p_status($request->payment_status) . "\r\n";
                    $success_text.= "ช่องทาง: ". _2c2p_channel($request->payment_channel) . "\r\n";
                    $success_text.= "จำนวน: ". number_format($order->amount, 2) . $currency ."\r\n";
                    $success_text.= "=======================================\r\n";
                    $success_text.= "Transaction_ref: ". $request->transaction_ref . "\r\n";
                    $success_text.= "Approval code: ". $request->approval_code . "\r\n";
                    $success_text.= "Provider: ". $provider . "\r\n";
                    $success_text.= "=======================================\r\n";
                    $success_text.= "รายละเอียดเพิ่มเติม: " . url('order/view/' . $request->order_id . '/' . $order->shop_id)  . "\r\n";
                    $success_text.= "=======================================\r\n";
                    $notify->send($success_text);
                    self::_notify($request->order_id, $request->payment_status);
                }
                
            } else {
                // ! roback in stock
                $order_items = OrderItemModel::where(['order_id' => $order->id])
                    ->get()->toArray();
                // ! ถ้ามีข้อมูล
                if (!empty($order_items)) {
                    // ! loop เพิ่มคืนสต๊อกสินค้า
                    foreach($order_items as $key => $item) {
                        // * คืนสต๊อก
                        DB::table('tb_product')->where(['id' => $item['product_id']])
                            ->update(['in_stock' => DB::raw('IF (in_stock = "0", "1", in_stock + '. $item['qty'] .')')]);
                    }
                }
                // ! update order
                OrderModel::where(['id' => $order->id])
                    ->update(['status' => 4, 'payment_status' => $request->payment_status]);
                // ! purchase update order item fall
                OrderItemModel::where('order_id', $order->id)
                    ->update(['status' => 4]);

                $error_text = "order: ". $request->order_id . "\r\n";
                $error_text.= "status: ". _2c2p_status($request->payment_status) . "\r\n";
                $notify->send($error_text, [
                    'type' => false,
                    'class' => "PaymentLibrary",
                    'method' => "process"
                ]);
            }

            // * purchase logs
            $old_order_purchase = DB::table('tb_order_purchase')->where('order_id', $order->id)->first();
            if (empty($old_order_purchase)) {
                // * Save purchase logs
                DB::table('tb_order_purchase')->insert([
                    'order_id' => $order->id,
                    'purchase_log_id' => $log_id,
                    'currency' => isset($request->currency) && !empty($request->currency) ? $request->currency : null,
                    'amount' => isset($request->amount) && !empty($request->amount) ? $request->amount : null,
                    'transaction_ref' => isset($request->transaction_ref) && !empty($request->transaction_ref) ? $request->transaction_ref : null,
                    'approval_code' => isset($request->approval_code) && !empty($request->approval_code) ? $request->approval_code : null,
                    'transaction_datetime' => isset($request->transaction_datetime) && !empty($request->transaction_datetime) ? $request->transaction_datetime : null,
                    'payment_channel' => isset($request->payment_channel) && !empty($request->payment_channel) ? $request->payment_channel : null,
                    'payment_status' => isset($request->payment_status) && !empty($request->payment_status) ? $request->payment_status : null,
                    'channel_response_desc' => isset($request->channel_response_desc) && !empty($request->channel_response_desc) ? $request->channel_response_desc : null,
                    'paid_channel' => isset($request->paid_channel) && !empty($request->paid_channel) ? $request->paid_channel : $request->payment_channel,
                    'paid_agent' => isset($request->paid_agent) && !empty($request->paid_agent) ? $request->paid_agent : '2C2P',
                    'created_at' => now()
                ]);
            } else {
                // * update purchase logs
                DB::table('tb_order_purchase')->where('order_id', $order->id)->update([
                    'currency' => isset($request->currency) && !empty($request->currency) ? $request->currency : null,
                    'amount' => isset($request->amount) && !empty($request->amount) ? $request->amount : null,
                    'transaction_ref' => isset($request->transaction_ref) && !empty($request->transaction_ref) ? $request->transaction_ref : null,
                    'approval_code' => isset($request->approval_code) && !empty($request->approval_code) ? $request->approval_code : null,
                    'transaction_datetime' => isset($request->transaction_datetime) && !empty($request->transaction_datetime) ? $request->transaction_datetime : null,
                    'payment_channel' => isset($request->payment_channel) && !empty($request->payment_channel) ? $request->payment_channel : null,
                    'payment_status' => isset($request->payment_status) && !empty($request->payment_status) ? $request->payment_status : null,
                    'channel_response_desc' => isset($request->channel_response_desc) && !empty($request->channel_response_desc) ? $request->channel_response_desc : null,
                    'paid_channel' => isset($request->paid_channel) && !empty($request->paid_channel) ? $request->paid_channel : $request->payment_channel,
                    'paid_agent' => isset($request->paid_agent) && !empty($request->paid_agent) ? $request->paid_agent : '2C2P',
                    'updated_at' => now()
                ]);
            }

            //? send mail 
            self::_email([
                'language' => $request->currency,
                'payment_channel' => $request->payment_channel,
                'display_name' => strip_tags($order->display_name),
                'order_id' => $request->order_id,
                'payment_date' => $request->transaction_datetime,
                'payment_status' => $request->payment_status,
                'shipping_name' => strip_tags($order->shipping_name),
                'shipping_tel' => $order->phone,
                'shipping_email' => $order->email,
                'shipping_address' => $order->address .', ' . $order->province_name_th . ' ' . $order->amphur_name_th . ' ' . $order->amphur_name_th . ' ' . $order->district_name_th . ' ' . $order->zipcode
            ]);
        }
    }
    
    /**
     * _email
     *
     * @param  mixed $params
     * @return void
     */
    private static function _email(array $params = [])
    {
        $language = ($params['language'] == "764") ? 'th' : 'en';
        if (isset($params['shipping_email']) && !empty($params['shipping_email'])) {
            \Mail::to($params['shipping_email'])->send(new Mailer([
                'to' => $params['shipping_email'],
                'from' => 'info@bungkie.com',
                'subject' => 'Bungkie: Receipt',
                'title' => 'Bungkie: Receipt',
                'language' => $language,
                'name' => $params['display_name'],
                'order_id' => $params['order_id'],
                'payment_date' => Carbon::parse($params['payment_date'])->locale($language)->isoFormat("LLL"),
                'payment_type' => _2c2p_channel($params['payment_channel'], ($params['language'] == "764") ? 1 : 2),
                'payment_status' => _2c2p_status($params['payment_status']),
                'payment_code' => $params['payment_status'],
                'shipping_name' => $params['shipping_name'],
                'shipping_tel' => $params['shipping_tel'],
                'shipping_email' => $params['shipping_email'],
                'shipping_address' => $params['shipping_address'],
                'layout' => 'receipt-purchase'
            ]));
            $notify = new LineLibrary();
            // ! error
            if (\Mail::failures()) {
                $notify->send("ส่งเมลไม่ได้\r\nemail: " . $params['shipping_email'] . "\r\n", [
                    'type' => false,
                    'class' => "PaymentLibrary",
                    'method' => "_email"
                ]);
                DB::table('tb_logs')->insert([
                    'class' => strtoupper('PaymentLibrary'),
                    'method' => strtoupper('_emeil'),
                    'name' => 'ส่งเมลไม่ได้',
                    'json_data' => json_encode($params),
                    'created_at' => now(),
                ]);
            } else {
                DB::table('tb_logs')->insert([
                    'class' => strtoupper('PaymentLibrary'),
                    'method' => strtoupper('_emeil'),
                    'name' => 'ส่งเมลแล้ว',
                    'json_data' => json_encode($params),
                    'created_at' => now(),
                ]);
            }
        } else {
            DB::table('tb_logs')->insert([
                'class' => strtoupper('PaymentLibrary'),
                'method' => strtoupper('_emeil'),
                'name' => 'ส่งไม่ได้เพราะไม่มีอีเมล',
                'json_data' => json_encode($params),
                'created_at' => now(),
            ]);
        }
    }
    
    /**
     * _notify
     *
     * @param  mixed $order_id
     * @param  mixed $payment_status
     * @return void
     */
    private static function _notify(string $order_id = null, string $payment_status = null)
    {
        if (empty($order_id) && empty($payment_status)) return false;
        $orders = DB::table('tb_order')
            ->select('tb_order_item.shop_id')
            ->join('tb_order_item', 'tb_order_item.order_id', '=', 'tb_order.id')
            ->where('tb_order.reference_id', $order_id)
            ->get()->toArray();
        
        if (!empty($orders)) {
            foreach($orders as $order) {
                //* notify
                Notification::insert([
                    'type' => 1,
                    'message' => json_encode([
                        'title' => "มีออเดอร์ใหม่",
                        'payment_status' => _2c2p_status($payment_status),
                        'type' => "Order",
                        'order' => $order_id
                    ]),
                    'action' => 0,
                    'owner' => $order->shop_id,
                    'created_at' => now()
                ]); 
            }
        }
    }
}