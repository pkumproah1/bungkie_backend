<?php
namespace App\Libraries\Api;
use Illuminate\Support\Facades\DB;
use App\Mail\Mailer;
use App\Models\Tb_product_wholesale as ProductWholesaleModel;
use App\Models\Tb_product as ProductModel;
class WholesaleLibrary
{        
    /**
     * inserted
     *
     * @param  mixed $request
     * @return void
     */
    public static function inserted(array $request = [])
    {
        // ! validate
        if (empty($request)) return array(
            'status' => false,
            'message' => "Missing data or invalid."
        );

        // ! check product type !== B2C
        $product = ProductModel::find($request['product_id']);
        if ($product->product_type == 2) return array(
            'status' => false,
            'message' => 'Product not supported'
        );

        // * inserted wholesale
        $wholesale_id = DB::table('tb_product_wholesale')->insertGetId([
            'product_id' => $request['product_id'],
            'qty' => $request['qty'],
            'unit_qty' => $request['unit'],
            'fname' => $request['fname'],
            'lname' => $request['lname'],
            'company_name' => $request['company_name'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'address' => $request['address'],
            'province' => $request['province'],
            'city' => $request['city'],
            'district' => $request['district'],
            'zipcode' => $request['zipcode'],
            'line' => isset($request['line']) && !empty($request['line']) ? $request['line'] : null,
            'skype' => isset($request['skype']) && !empty($request['skype']) ? $request['skype'] : null,
            'whatsapp' => isset($request['whatsapp']) && !empty($request['whatsapp']) ? $request['whatsapp'] : null,
            'remark' => isset($request['remark']) && !empty($request['remark']) ? $request['remark'] : null,
        ]);

        // ! not inserted
        if (!$wholesale_id) return array(
            'status' => false,
            'message' => 'Not Inserted'
        );

        // * send email for customer
        DB::table('tb_cronjob_email')->insert([
            'service' => 'wholesale-customer',
            'title' => "Bungkie: Wholesale submitted",
            'data' => 'null',
            'email' => $request['email'],
            'status' => 0,
            'created_at' => now()
        ]);

        // * send email for db team
        DB::table('tb_cronjob_email')->insert([
            'service' => 'wholesale-business-development',
            'title' => "Bungkie: Alert Wholesale submitted",
            'data' => json_encode([
                'customer' => array(
                    'company' => $request['company_name'],
                    'phone' => $request['phone'],
                    'email' => $request['email']
                ),
                'link' => url('/') . '/' . 'wholesale/detail/' .  $wholesale_id
            ]),
            'email' => 'sales@bungkie.com',
            'status' => 0,
            'created_at' => now()
        ]);

        // * success
        return array(
            'status' => true,
            'message' => '',
            'data' => array(
                'email' => $request['email']
            )
        );
    }
}