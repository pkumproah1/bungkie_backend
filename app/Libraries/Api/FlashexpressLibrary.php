<?php
namespace App\Libraries\Api;
class FlashexpressLibrary
{
    private $config;
    private $request;
    private $sign;
    public function __construct(array $config = [])
    {
        (array) $this->config = [];
        $this->config = array(
            'secret_key' => config('constants.flash.secret_key'),
            'mchid' => config('constants.flash.merchant_id'),
            'url' => config('constants.flash.flash_url')
        );
    }    
        
    /**
     * request
     *
     * @param  mixed $url
     * @param  mixed $params
     * @return void
     */
    public function request(string $url = null, array $params = [])
    {
        (array) $data = array();
        $data['mchId'] = $this->config['mchid'];
        $data['nonceStr'] = time();
        $new_data = array_merge($data, $params);
        // ? sort array
        ksort($new_data);
        $query = urldecode(http_build_query($new_data));
        $hash = hash('sha256',  $query . "&key=" . $this->config['secret_key']);
        $new_data['sign'] = strtoupper($hash);
        return self::http_request($url, urldecode(http_build_query($new_data)));
    }
    
    /**
     * http_request
     *
     * @param  mixed $url
     * @param  mixed $query
     * @return void
     */
    private function http_request($url, $query = null)
    {
        if (empty($query)) return array(
            'status' => false,
            'message' => 'message'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->config['url'] . $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json'
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        $data = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $new_data = json_decode($data, TRUE);
        curl_close($ch);
        return $new_data;
    }

    /**
     * generate_pdf
     *
     * @param  mixed $url
     * @return void
     */
    public function generate_pdf(string $url = null, string $path = null)
    {
        (array) $data = array();
        $params = [];
        $data['mchId'] = $this->config['mchid'];
        $data['nonceStr'] = time();
        $new_data = array_merge($data, $params);
        // ? sort array
        ksort($data);
        $query = urldecode(http_build_query($new_data));
        $hash = hash('sha256',  $query . "&key=" . $this->config['secret_key']);
        $new_data['sign'] = strtoupper($hash);
        return self::download_pdf($url, urldecode(http_build_query($new_data)), $path);
    }
    
    /**
     * download_pdf
     *
     * @param  mixed $url
     * @param  mixed $query
     * @param  mixed $file_path
     * @return void
     */
    public function download_pdf(string $url = null, $query = null, $file_path = 'pdf')
    {
        $curl = curl_init ();
		$header[] = "Content-type: application/x-www-form-urlencoded";
		$header[] = "Accept: application/json";
		$header[] = "Accept-Language: th";
		curl_setopt ($curl, CURLOPT_URL, $this->config['url'] . $url );
		curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, false );
		curl_setopt ($curl, CURLOPT_HEADER, 1 );
		curl_setopt ($curl, CURLOPT_HTTPHEADER, $header );
		curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ($curl, CURLOPT_POST, true ); // post
		curl_setopt ($curl, CURLOPT_POSTFIELDS, $query ); // post data
		curl_setopt ($curl, CURLOPT_TIMEOUT, 10 );
		curl_setopt ($curl, CURLINFO_HEADER_OUT, true);
		$responseText = curl_exec ($curl);
		if (curl_errno ($curl)) {
            return array(
                'status' => false,
                'message' => curl_error($curl)
            );
		}
		curl_close ($curl);
		list($headers, $body) = explode("\r\n\r\n", $responseText, 2);
		$header_arr = array();
		$header_tmp = explode("\n", $headers);
		foreach($header_tmp as $header_value) {
			$pos = strpos($header_value, ":");
			$k = trim(substr($header_value, 0, $pos));
			$v = trim(substr($header_value, $pos+1));
			if(!empty($k)) {
                $header_arr[$k] = $v;
            }
		}
		$file_name = $header_arr['Content-Disposition'];
		$file_type = $header_arr['Content-Type'];
		$file_save_name = substr($file_name, strrpos($file_name, "=")+1);
		$file_content = $body;
		$filename  = 'public/download/flash/print/' . $file_save_name;
		if(is_writable('public/download/flash/print')) {
			if(!$handle  =  fopen($filename, 'w')) {
                return array(
                    'status' => false,
                    'message' => "cannot open"
                );
			}
     		if(fwrite($handle,  $file_content) ===  FALSE) {
                return array(
                    'status' => false,
                    'message' => "cannot write file"
                );
			}
            
			fclose($handle);
			return array(
                'status' => true,
                'url' => url('/') .'/'. $filename,
                'name' => $file_save_name
            );
		} else {
            return array(
                'status' => false,
                'message' => "file not writable"
            );
		}
        
        return array(
            'status' => false,
            'message' => 'error'
        );
    }

}