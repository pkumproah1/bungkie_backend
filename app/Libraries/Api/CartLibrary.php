<?php
namespace App\Libraries\Api;
use Illuminate\Support\Facades\DB;
use App\Libraries\Api\FlashexpressLibrary;
use App\Models\Tb_product as ProductModel;
use App\Models\Tb_inventory as InventoryModel;
use App\Models\Tb_user_address as UserAddressModel;
use App\Models\Tb_order as OrderModel;
use App\Models\Tb_order_item as OrderItemModel;

class CartLibrary
{
    /**
     * product_is_in_stock
     *
     * @param  mixed $product_id
     * @param  mixed $qty
     * @return void
     */
    static public function product_is_in_stock($product_id = null, $qty = null)
    {
        if (empty($product_id) && empty($qty)) 
            return false;

        $product = DB::table('tb_product')
            ->select('in_stock')
            ->where('id', $product_id)
            ->where('status', 1)
            ->where('approval', 1)
            ->whereNull('deleted_at')
            ->first();
        
        if (empty($product)) 
            return false;

        if ($qty > $product->in_stock) 
            return false;

        return true;
    }
    
    /**
     * calculate
     *
     * @param  mixed $request
     * @return void
     */
    static public function calculate($request, $language = 1, $currency = 1)
    {
        if (empty($request)) return array(
            'status' => false
        );

        // * query product and price
        $products = DB::table('tb_product')
            ->select('tb_product.id as product_id', 'path', 'tb_product_language.name', 'in_stock','is_new',
                'tb_product.status','tb_product.approval', 'operation', 'front_price', 'sale_price', 'owner', 'shop_name')
            ->join('tb_product_price', 'tb_product_price.product_id', '=', 'tb_product.id')
            ->join('tb_product_language', 'tb_product_language.product_id', '=', 'tb_product_price.product_id')
            ->leftJoin('tb_product_images', 'tb_product_images.product_id', '=', 'tb_product_price.product_id')
            ->leftJoin('tb_user_sellers', 'tb_user_sellers.user_id', '=', 'tb_product.owner')
            ->where('tb_product_price.type_price', $currency)
            ->where('tb_product_language.language_id', $language)
            ->where('tb_product_images.rank', 1)
            ->whereIn('tb_product_price.product_id', array_column($request, 'product_id'))
            ->groupBy('tb_product.id')
            ->get();
    
        // ! product !== null
        if ($products) {        
            // ? define variable
            $items = array();
            $prefix_currency = ($currency === 1) ? '฿' : '$';
            $discount = 0;
            $before_total_discount = 0;
            $total_discount = 0;
            // * loop product and filter new value
            foreach($products as $key => $product) {
                // ? checkkkkk!
                $quantity = preg_replace('/\D/', '', implode(" ", array_column(array_filter($request, function($prod) use($product) {
                    return ($prod->product_id == $product->product_id);
                }), "quantity")));

                $item_id = preg_replace('/\D/', '', implode(" ", array_column(array_filter($request, function($prod) use($product) {
                    return ($prod->product_id == $product->product_id);
                }), "item_id")));


                // ! quantity !== null
                if ($quantity) {
                    $path = ($product->is_new === 1)
                        ? url('/') . '/public/statics/images/products/' . $product->product_id . '/m/'
                        : url('/') . '/public/statics/images/';
                    $img = empty($product->path) ? 'default.jpg' : $product->path;

                    if (($quantity <= $product->in_stock) && ($product->status === 1) && ($product->approval === 1)) {
                        $items['InStock'][$key]['productId'] = $product->product_id;
                        $items['InStock'][$key]['inStock'] = $product->in_stock;
                        $items['InStock'][$key]['image'] = $path . $img;
                        $items['InStock'][$key]['name'] = $product->name;
                        $items['InStock'][$key]['inventoryItemId'] = $item_id;
                        $items['InStock'][$key]['url'] = mb_convert_encoding(strtolower(clean($product->name)), 'UTF-8', 'UTF-8');
                        $items['InStock'][$key]['shopId'] = isset($product->owner) && !empty($product->owner) ? $product->owner : 0;
                        $items['InStock'][$key]['shopName'] = isset($product->shop_name) && !empty($product->shop_name) ? $product->shop_name : 'Bungkie';
                        $items['InStock'][$key]['quantity'] = $quantity;
                    } else {
                        $items['OutStock'][$key]['productId'] = $product->product_id;
                        $items['OutStock'][$key]['inStock'] = $product->in_stock;
                        $items['OutStock'][$key]['image'] = $path . $img;
                        $items['OutStock'][$key]['name'] = $product->name;
                        $items['OutStock'][$key]['inventoryItemId'] = $item_id;
                        $items['OutStock'][$key]['url'] = mb_convert_encoding(strtolower(clean($product->name)), 'UTF-8', 'UTF-8');
                        $items['OutStock'][$key]['shopId'] = isset($product->owner) && !empty($product->owner) ? $product->owner : 0;
                        $items['OutStock'][$key]['shopName'] = isset($product->shop_name) && !empty($product->shop_name) ? $product->shop_name : 'Bungkie';
                        $items['OutStock'][$key]['quantity'] = $quantity;
                    }

                    // ! calculate price 
                    $flag_in_out_stock = (($quantity <= $product->in_stock) && ($product->status === 1) && ($product->approval === 1))
                        ? "InStock" : "OutStock";
                    // ? operation !== null
                    if (!empty($product->operation)) {
                        // ? has discount type === baht
                        if (strtolower($product->operation) === "baht") {
                            $total = (($product->front_price - $product->sale_price) * $quantity);
                            $before_discount = ($product->front_price * $quantity);
                            $before_total_discount = $before_total_discount + $before_discount;
                            $discount = $discount + $total;
                            $total_discount = ($total_discount + ($product->sale_price * $quantity));

                            $items[$flag_in_out_stock][$key]['price'] = array(
                                'price' => number_format(($product->front_price - $product->sale_price), 2),
                                'after' => number_format($product->front_price ,2),
                                'total' => number_format($total, 2),
                                'before' => number_format($before_discount, 2),
                                'discount' => number_format(($product->sale_price * $quantity), 2),
                                'discount_percent' => @number_format(($product->sale_price / $product->front_price) * 100, 2),
                                'quantity' => $quantity
                            );
                        // ? has discount type === percent
                        } else if ($product->operation === "percent") {
                            $total = (((100 - $product->sale_price) / 100 * $product->front_price) * $quantity);
                            $discount = $discount + $total;
                            $before_discount = ($product->front_price * $quantity);
                            $before_total_discount = $before_total_discount + $before_discount;
                            $total_discount = ($total_discount + ($product->sale_price * $quantity));
                            $items[$flag_in_out_stock][$key]['price'] = array(
                                'price' => number_format(((100 - $product->sale_price) / 100 * $product->front_price), 2),
                                'total' => number_format($total, 2),
                                'after' => number_format($product->front_price ,2),
                                'before' => number_format($before_discount, 2),
                                'discount' => number_format(($product->sale_price * $quantity), 2),
                                'discount_percent' => @number_format(($product->sale_price / $product->front_price) * 100, 2),
                                'quantity' => $quantity
                            );
                        }
                    // ? dont has discount
                    } else {
                        $total = ($product->front_price * $quantity);
                        $discount = $discount + $total;
                        $items[$flag_in_out_stock][$key]['price'] = array(
                            'price' => number_format($product->front_price, 2),
                            'total' => number_format($total, 2),
                            'discount' => number_format(0, 2),
                            'discount_percent' => number_format(0, 2),
                            'quantity' => $quantity
                        );
                    }

                    // ! 
                    if ($flag_in_out_stock === "OutStock") {
                        $discount = ($discount - $total);
                        $total_discount = ($total_discount - floatval(preg_replace('/[^\d.]/', '', $items[$flag_in_out_stock][$key]['price']['discount'])));
                        $before_total_discount = ($before_total_discount - floatval(preg_replace('/[^\d.]/', '', $items[$flag_in_out_stock][$key]['price']['before'])));
                    }
                }
            }
            
            if ((isset($items['InStock']) && !empty($items['InStock'])) && (isset($items['OutStock']) && !empty($items['OutStock']))) {
                $count = (count($items['InStock']) + count($items['OutStock']));
            } elseif ((isset($items['InStock']) && !empty($items['InStock']))) {
                $count = count($items['InStock']);
            } elseif ((isset($items['OutStock']) && !empty($items['OutStock']))) {
                $count = count($items['OutStock']);
            }

            
            return array(
                'status' => true,
                'data' => array(
                    'mini' => array(
                        'InStock' => isset($items['InStock']) && !empty($items['InStock']) ? $items['InStock'] : null,
                        'OutStock' => isset($items['OutStock']) && !empty($items['OutStock']) ? $items['OutStock'] : null
                    ),
                    'full' => array(
                        'InStock' => isset($items['InStock']) && !empty($items['InStock']) ? array_values(group_shop($items['InStock'])) : null,
                        'OutStock' => isset($items['OutStock']) && !empty($items['OutStock']) ? array_values(group_shop($items['OutStock'])) : null
                    ),
                    'currency' => $prefix_currency,
                    'count' => $count,
                    'totalPrice' =>  number_format($discount, 2),
                    'before_total_discount' => number_format($before_total_discount, 2),
                    'totalDiscount' => number_format($total_discount, 2)
                )
            );   
        }

        // * response no cart
        return array(
            'status' => false
        );
    }
    
    /**
     * calculate_product_and_address
     *
     * @param  mixed $params
     * @param  mixed $userId
     * @return void
     */
    public static function calculate_product_and_address(
        array $params = [],
        int $userId = 0,
        string $orderId = null
    ) {
        
        if (empty($params) && empty($userId)) return array(
            'status' => false,
            'message' => "Missing data or invalid."
        );

        $currency = isset($params['currency']) && !empty($params['currency']) ? $params['currency'] : 1;
        $language = isset($params['language']) && !empty($params['language']) ? $params['language'] : 1;
        $products = isset($params['products']) && !empty($params['products']) ? $params['products'] : [];

        // * find product in inventory
        $inventory = InventoryModel::find_products($userId, $products, 1, $currency);
        if(FALSE === $inventory['status']) return $inventory;

        // * คำนวนราคาสินค้าที่เลือก
        $product_summary = self::_calc_product($inventory['data']);
        $shipping_summary = null;
        // ! ถ้ามี address id ให้คำนวนราคาส่งด้วย
        if (isset($params['addressId']) && !empty($params['addressId'])) {
            $address = UserAddressModel::find_address_by_user_and_id($userId, $params['addressId']);
            if ($address['status'] !== true) return $address;
            $shipping_summary = self::_calc_shipping($inventory['data'], $address['data'], 'flash', $currency, $orderId);
            $product_summary['shipping'] = number_format($shipping_summary['shipping_total'], 2);
            $product_summary['total'] = number_format(floatval(preg_replace('/[^\d.]/', '', $product_summary['total'])) + $shipping_summary['shipping_total'], 2);
        }

        return array(
            'status' => true,
            'data' => $product_summary
        );
    }
    
    /**
     * _calc_shipping
     *
     * @param  mixed $inventory
     * @return void
     */
    protected static function _calc_shipping(
        array $inventory = [],
        object $address = null,
        string $provider = "flash",
        int $currency = 1,
        int $order_id = null
    ) {
        // ! not empty
        if (!empty($inventory)) {
            // * group items
            $items = array_reduce($inventory, function($accumulator, $item) {
                $key = $item->owner;
                if (!array_key_exists($key, $accumulator)) {
                    $accumulator[$key] = array();
                }
                $accumulator[$key]['weight'][] = ($item->s_weight * $item->qty);
                $accumulator[$key]['address']['warehouse'] = $item->warehouse_address;
                $accumulator[$key]['address']['province'] = $item->province_name_th;
                $accumulator[$key]['address']['city'] = $item->amphur_name_th;
                $accumulator[$key]['address']['district'] = $item->district_name_th;
                $accumulator[$key]['address']['zipcode'] = $item->warehouse_zipcode;
                $accumulator[$key]['shipping'] = $item->free_shipping;
                $accumulator[$key]['product_id'] = $item->product_id;
                return $accumulator;
            }, []);

            foreach($inventory as $inv) {
                OrderItemModel::where('order_id', $order_id)
                    ->where('product_id', $inv->product_id)
                    ->update([
                        'customer_address_id' => $address->address_id,
                        'shipping_price' => null,
                        'shipping_provider' => $provider === "flash" ? 1 : 0,
                        'shipping_is_paid' => ($inv->free_shipping == 0) ? 1 : 0
                    ]);
            }

            // ! not empty
            if ($items) {
                $response = array();
                $shipping = 0;
                foreach($items as $shopId => $item) {
                    // ! provider == flash express
                    if ($provider === "flash") {
                        // ? flash express lib. -> cals shipping
                        $flashLibrary = new FlashexpressLibrary();
                        $result = $flashLibrary->request("open/v1/orders/estimate_rate", [
                            'srcProvinceName' => $item['address']['province'],
                            'srcCityName' => str_replace("เขต", "", $item['address']['city']),
                            'srcDistrictName' => $item['address']['district'],
                            'srcPostalCode' => strval($item['address']['zipcode']),
                            'dstProvinceName' => $address->province_name_th,
                            'dstCityName' => str_replace("เขต", "", $address->amphur_name_th),
                            'dstDistrictName' => $address->district_name_th,
                            'dstPostalCode' => strval($address->zipcode),
                            'weight' => (array_sum($item['weight']) >= 1000) ? array_sum($item['weight']) : 1000 
                        ]);

                        // * update order items
                        OrderItemModel::where('order_id', $order_id)
                            ->where('product_id', $item['product_id'])
                            ->update([
                                'customer_address_id' => $address->address_id,
                                'shipping_price' => ($result['data']['estimatePrice'] / 100),
                                'shipping_provider' => 1,
                                'shipping_is_paid' => ($item['shipping'] == 0) ? 1 : 0
                            ]);
                        
                        // * sum shipping
                        if ($item['shipping'] == '0') {
                            if ($result) {
                                $shipping = $shipping + ($result['data']['estimatePrice'] / 100);
                            }
                        }
                    }
                }
                
                $shipping = $shipping;
                if ($currency == 2) {
                    $shipping = ($shipping * 0.033276);
                }
                $response['shipping_total'] = $shipping;
                return $response;
            }
        }
    }

    /**
     * _calc_product
     *
     * @param  mixed $inventory
     * @return void
     */
    protected static function _calc_product(array $inventory = [])
    {
        $subtotal = 0;
        $discount = 0;
        if (!empty($inventory)) {
            foreach ($inventory as $key => $item) {
                $product = ProductModel::select('in_stock','approval','status')->where(['id' => $item->product_id])->first();
                if (($item->qty <= $product->in_stock) && ($product->status === 1) && ($product->approval === 1)) {
                    if ($item->sale_price > 0) {
                        $calc['price'][] = self::__calc($item->front_price, $item->sale_price, $item->qty, $item->operation);
                        $calc = self::__calc($item->front_price, $item->sale_price, $item->qty, $item->operation);
                        $subtotal += $calc['after_total'];
                        $discount += ($item->sale_price * $item->qty);
                    } else {
                        $subtotal += ($item->front_price * $item->qty);
                    }
                }
            }
        }

        return array(
            'subtotal' => number_format(($subtotal + $discount), 2),
            'discount' => number_format($discount, 2),
            'total' => number_format(($subtotal), 2)
        );
    }
    
    /**
     * __calc
     *
     * @param  mixed $price
     * @param  mixed $discount
     * @param  mixed $qty
     * @param  mixed $operation
     * @return void
     */
    protected static function __calc(int $price = 0, int $discount = 0, int $qty = 0, string $operation = null) 
    {
        $response = array();
        if ($operation === "percent") {
            $response = array(
                'before' => $price,
                'after_unit' => ((100 - $discount) / 100 * $price),
                'after_total' => (((100 - $discount) / 100 * $price) * $qty),
                'qty' => $qty,
            );
        } else if ($operation === "baht") {
             $response = array(
                'before' => $price,
                'after_unit' => ($price - $discount),
                'after_total' => (($price - $discount) * $qty),
                'qty' => $qty,
            );
        }

        return $response;
    }
}