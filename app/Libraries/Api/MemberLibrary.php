<?php
namespace App\Libraries\Api;
use Illuminate\Support\Facades\DB;
use App\Mail\Mailer;
use App\Models\Tb_product_wholesale as ProductWholesaleModel;
use App\Models\Tb_product as ProductModel;
class MemberLibrary
{        
    /**
     * inserted
     *
     * @param  mixed $request
     * @return void
     */
    public static function newsletter_inserted(array $request = [])
    {
        // ! validate
        if (empty($request)) return array(
            'status' => false,
            'message' => "Missing data or invalid."
        );

        $newsletter = DB::table('tb_register_newsletter')->where('email', $request['email'])->first();
        if (!empty($newsletter)) return array(
            'status' => false,
            'message' => 'Duplicate email for newsletter.'
        );
        // * inserted wholesale
        $newsletter_id = DB::table('tb_register_newsletter')->insertGetId([
            'email' => $request['email'],
            'status' => 1,
            'created_at' => now()
        ]);
        // ! not inserted
        if (!$newsletter_id) return array(
            'status' => false,
            'message' => 'Not Inserted'
        );

        // * send email for customer
        DB::table('tb_cronjob_email')->insert([
            'service' => 'register-newsletter',
            'title' => "Bungkie: Register newsletter",
            'data' => 'null',
            'email' => $request['email'],
            'status' => 0,
            'created_at' => now()
        ]);

        // * success
        return array(
            'status' => true,
            'data' => array(
                'email' => $request['email']
            )
        );
    }
}