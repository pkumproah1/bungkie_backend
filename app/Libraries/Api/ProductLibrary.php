<?php
namespace App\Libraries\Api;
use Illuminate\Support\Facades\DB;
use App\Libraries\Api\FlashexpressLibrary;
use App\Models\Tb_inventory;
use App\Models\Tb_user_address;
use App\Models\Tb_product as ProductModel;
use App\Models\Tb_order_shipping as OrderShipping;
use App\Models\Tb_order_shipping_tracking as OrderShippingTracking;
use Carbon\Carbon;
class ProductLibrary
{    
    /**
     * homepage
     *
     * @param  mixed $request
     * @param  mixed $language
     * @param  mixed $currency
     * @return void
     */
    public static function homepage(object $request = null, int $language = 1, int $currency = 1)
    {
        $categorys = array();
        if (isset($request->typeId) && !empty($request->typeId)) {
            $lists = explode(",", $request->typeId);
            if ($lists) {
                foreach($lists as $key => $id) {
                    $list = self::_paginate([
                        'type' => 'category',
                        'type_id' => $id,
                        'limit' => $request->limit,
                        'offset' => $request->offset
                    ], $language, $currency);
                    
                    if (!empty($list['data'])) {
                        $categorys['category'][$key] = array(
                            'category' => $list['category'],
                            'data' => $list['data']
                        );
                    }
                }
            }
        }

        $categorys['newArrival'] = self::_paginate([
            'type' => 'new-arrival',
            'limit' => $request->limit,
            'offset' => $request->offset
        ], $language, $currency)['data'];

        $categorys['recommend'] = self::_paginate([
            'type' => 'recommend',
            'limit' => $request->limit,
            'offset' => $request->offset
        ], $language, $currency)['data'];

        $categorys['hotDeals'] = self::_paginate([
            'type' => 'hot-deals',
            'limit' => $request->limit,
            'offset' => $request->offset
        ], $language, $currency)['data'];
        
        return $categorys;
    }
    
       
    /**
     * _paginate
     *
     * @param  mixed $request
     * @param  mixed $language
     * @param  mixed $currency
     * @return void
     */
    public static function _paginate($request, int $language = 1, int $currency = 1)
    {
        // ? get all product by codition request
        $products = ProductModel::homepage_paginate($request, "result", $language, $currency);
        $response = array();
        // * has products
        if ($products) {
            //* ดึง Rating ทั้งหมดมา Filter ตาม Product
            $ratings = ProductModel::get_rating();
            //? Fixed field for filter
            $fields = array('product_id', 'in_stock', 'name', 'base_price', 'path', 'score', 'product_type');
            //* แปลงข้อมูลให้พร้อมใช้งานในรูปแบบของ API
            foreach ($products as $key => $prod) {
                $row = array();
                //* Filter field
                foreach ($fields as $field) {
                    if ($field === "path") {
                        // ? ดึงรูปทั้งหมดมาและเพิ่ม prefix baseurl 
                        $img = ($prod->is_new == 1)
                            ? url('/') . '/public/statics/images/products/'. $prod->product_id . '/m/' . $prod->path
                            : url('/') . '/public/statics/images/' . $prod->path;
                        $row['images'] = $img;
                    } elseif ($field === "product_type") {
                        $type = array(1 => 'BOTH', 2 => 'B2C', 3 => 'B2B');
                        $row['product_type'] = isset($type[$prod->product_type]) ? $type[$prod->product_type] : null;
                    
                    } elseif($field === "name") {
                        // ? จัดการชื่อสินค้าและสร้าง URL
                        $row['name'] = isset($prod->product_name) ? $prod->product_name : 'NO PRODUCT';
                        $row['url'] = mb_convert_encoding(strtolower(clean($prod->product_name)), 'UTF-8', 'UTF-8');
                    } elseif($field === "score") {  
                        //* คำนวณ Rating
                        //? Find ratings by product_id
                        $rating = array_values(array_filter($ratings, function($rat) use($prod) {
                            return ($rat->product_id == $prod->product_id);
                        }));
                        // ? check rating
                        if ($rating) {
                            //* has rating
                            $score;
                            $number;
                            //* loop ratings
                            foreach ($rating as $r) {
                                $score = $r->score;
                                $number = $r->number;
                            }
                            $star = ($score / $number);
                            //* response rating
                            $row['rating'] = array(
                                'star' => number_format($star),
                                'score' => $score,
                                'count' => $number
                            );
                        } else {
                            // * dont has rating response "NULL"
                            $row['rating'] = null; 
                        }
                    } elseif($field === "base_price") {
                        //* นำนวณเงิน คำนวณโปรโมชั่น
                        $price = array();
                        $prefix = ($currency == 1) ? 'THB' : 'USD';
                        if ($prod->operation == "baht") {
                            // ? Discount by "baht"
                            $price['price'] = number_format(($prod->front_price - $prod->sale_price),2);
                            $price['base'] = number_format($prod->front_price, 2);
                            $price['discount'] = number_format($prod->sale_price, 2);
                            $price['display_percent'] = @number_format(($prod->sale_price / $prod->front_price) * 100);
                        } else if ($prod->operation == "percent") {
                            // ? Discount by "percent"
                            $price['price'] = number_format(((100 - $prod->sale_price) / 100 * $prod->front_price),2);
                            $price['base'] = number_format($prod->front_price,2);
                            $price['discount'] = number_format($prod->sale_price,2). '%';
                            $price['display_percent'] = @number_format(($prod->sale_price / $prod->front_price) * 100);
                        } else {
                            // ? No Discount
                            $price['price'] = number_format($prod->front_price,2);
                            $price['base'] = number_format($prod->front_price,2);
                            $price['discount'] = null;
                            $price['display_percent'] = null;
                        }
                        //? Response New Price
                        $row['currency'] = $price;
                    } else {
                        // ? Dont Filter
                        $row[$field] = isset($prod->{$field}) ? $prod->{$field} : 'null';
                    }
                }
                //* new data in result
                $response[] = $row;
            }
        }
        //* Response Data "TRUE"
        return array(
            'data' => $response,
            'category' => array(
                'id' => isset($products['0']->category_id) ? $products[0]->category_id : '',
                'url' => isset($products[0]->category_name) 
                    ? mb_convert_encoding(strtolower(clean($products[0]->category_name)), 'UTF-8', 'UTF-8') 
                    : '',
                'name' => isset($products['0']->category_name) ? $products[0]->category_name : ''
            )
        );
    }
    
    /**
     * tracking
     *
     * @param  mixed $tracking_number
     * @return void
     */
    public static function tracking(string $tracking_number = null)
    {
        if (empty($tracking_number)) return array('status' => false, 'message' => 'Missing Data or invalid.');
        $order = DB::table('tb_order_shipping')
            ->select('pno', 'tb_order.status as order_status', 'tb_order.id as order_id', 'tb_order_shipping_tracking.shipping_id')
            ->where('pno', $tracking_number)
            ->orWhere('tb_order_shipping.reference_id', $tracking_number)
            ->join('tb_order_shipping_tracking', 'tb_order_shipping_tracking.shipping_id', '=', 'tb_order_shipping.id')
            ->join('tb_order', 'tb_order.reference_id', '=', 'tb_order_shipping.reference_id')
            ->groupBy('pno')->first();
        if (!empty($order)) {

            if ($order->order_status != "3") {
                $flashLibrary = new FlashexpressLibrary();
                $result = $flashLibrary->request("open/v1/orders/". $order->pno ."/routes", []);
                if ($result['code'] == "1") {
                    if (!empty($result['data']['routes'])) {
                        if ($result['data']['state'] == "5" && $order->order_status != "3") {
                            // * อัพเดทเป็นจัดส่งแล้ว
                            DB::table('tb_order')->where('id', $order->order_id)
                                ->update(['status' => 3]);
                        } else {
                            // * อัพเดทเป็นกำลังจัดส่ง
                            DB::table('tb_order')->where('id', $order->order_id)
                                ->update(['status' => 2]);
                        }
                        $olds = OrderShippingTracking::where('shipping_id', $order->shipping_id)
                            ->get()->toArray();
                        if (!empty($result['data']['routes'])) {
                            // ? check old
                            $sub_item = array_column($result['data']['routes'], 'message');
                            foreach($olds as $old) {
                                if (in_array($old['message'], $sub_item)) {
                                    $key = array_search($old['message'], $sub_item);
                                    unset($result['data']['routes'][$key]);
                                }
                            }
                        }

                        if (!empty($result['data']['routes'])) {
                            foreach($result['data']['routes'] as $route) {
                                DB::table('tb_order_shipping_tracking')->insert([
                                    'shipping_id' => $order->shipping_id,
                                    'status' => $route['state'],
                                    'message' => $route['message'],
                                    'created_at' => date('Y-m-d H:i:s', $route['routedAt']),
                                    'remark' => json_encode($route)
                                ]);
                            }
                        }
                    }
                } else {
                    return array('status' => false, 'message' => 'Missing Data or invalid.');
                }
            }

            $olds = OrderShippingTracking::where('shipping_id', $order->shipping_id)
                ->orderBy('created_at', 'desc')
                ->get()->toArray();
            if (!empty($olds)) {
                $result = array();
                foreach($olds as $old) {
                    $result[] = array(
                        'created_at' => Carbon::parse($old['created_at'])->locale('th')->isoFormat("LLL"),
                        'message' => $old['message']
                    );
                }
                return array('status' => true, 'data' => $result);
            } else {
                return array('status' => false, 'message' => 'Missing Data or invalid.');
            }
        }
        return array('status' => false, 'message' => 'Missing Data or invalid.');
    }
}