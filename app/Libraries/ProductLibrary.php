<?php
namespace App\Libraries;
use App\Models\Tb_product;
use App\User;
use App\Models\Tb_product_language;
use App\Models\Tb_product_option;
use App\Models\Tb_product_price;
use Illuminate\Support\Facades\DB;
class ProductLibrary
{
        
    /**
     * generate_sku
     *
     * @param  mixed $category_id
     * @param  mixed $prefix
     * @return void
     */
    public function generate_sku($category_id = null, $prefix = "SKU")
    {
        $sku = $prefix . date('Ymd') . rand(00000,99999);
        $check = Tb_product::where('sku', $sku)->first();
        if ($check !== null) {
            self::generate_sku();
        }
        return $sku;   
    }
    
    /**
     * hasRole
     *
     * @param  mixed $role
     * @return void
     */
    static public function hasRole($role = 'seller')
    {
        $userRoles = Auth()->user()->getRoleNames();
        $status = false;
        foreach($userRoles as $userRole){
            if($userRole == $role){
                $status = true;
                break;
            }
        }
        return $status;
    }

        
    /**
     * find_product
     *
     * @param  mixed $id
     * @return void
     */
    public function find_product($id = null)
    {
        if (empty($id)) return;
        $response = null;
        // Product
        $product = Tb_product::where('tb_product.id', $id)
            ->select(
                'id', 'brand', 'sku', 'tags', 's_weight', 's_width', 'owner',
                's_length', 's_height', 'in_stock', 'status', 'recommend',
                'product_type', 'approval'
            );  
        
        if(self::hasRole('seller')) {
            $product->where('owner', Auth()->user()->id);
        }
        
        $product = $product->first();
        // ! verify product
        if (empty($product)) return;

        // * find category by product id
        $response['categorys'] = null;
        $product_category = DB::table('tb_product_category_relation')
            ->where('product_id', $id)->get()->toArray();

        if (!empty($product_category)) {
            $cat = array_reduce($product_category, function($callback, $cat) {
                $callback[] = $cat->category_id;
                return $callback;
            }, []);

            $response['categorys'] = $cat;
        }

        // * find image bt product id
        $response['images'] = null;
        $product_images = DB::table('tb_product_images')
            ->select('product_id', 'path', 'rank', 'is_new')
            ->orderBy('rank', 'asc')
            ->where('product_id', $id)->get()->toArray();
        
        if ($product_images) {
            $images = array();
            foreach($product_images as $img) {
                if ($img->is_new == 1) {
                    $path = url('/') . '/public/statics/images/products/' . $img->product_id . '/m/';
                } else {
                    $path = url('/') . '/public/statics/images/';
                }
                $images[] = array(
                    'name' => $img->path,
                    'url' => $path . $img->path,
                    'rank' => $img->rank
                );
            }
            $response['images'] = $images;
        }

        // product language
        $product_language = Tb_product_language::where('tb_product_language.product_id', $id)
            ->select('name', 'short_description', 'description', 'shipment_description', 'special_condition', 'language_id')
            ->get();
        // verify product language
        if (empty($product_language)) return;

        $product_price = Tb_product_price::where('tb_product_price.product_id', $id)
            ->select('type_price', 'base_price', 'front_price', 'sale_price', 'operation')
            ->get();
        if (empty($product_price)) return;

        // product option [color, size]
        $product_option = new Tb_product_option();
        // Response Data
        $response['product'] = $product->toArray();
        $response['option'] = $product_option->get_options($id);
        // loop new variable for language detail
        foreach($product_language as $l) {
            $response['product_language'][$l['language_id']] = array(
                'name' => $l['name'],
                'short_description' => $l['short_description'],
                'description' => $l['description'],
                'shipment_description' => $l['shipment_description'],
                'special_condition' => $l['special_condition']
            );
        }
        // loop new variable for price
        foreach($product_price as $p) {
            $response['product_price'][$p['type_price']] = array(
                'base_price' => $p['base_price'],
                'front_price' => $p['front_price'],
                'sale_price' => $p['sale_price'],
                'operation' => $p['operation']
            );
        }

        return $response;
    }

}