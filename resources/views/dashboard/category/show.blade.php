@extends('dashboard.base')

@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Category Name: {{ $template->cat_name_en }} {{ $template->cat_name_th }}</h4>
                    </div>
                    <div class="card-body">
                        <!-- <h4>Category Name</h4>
                        <p>{{ $template->cat_name }}</p> -->
                        <h4>Description EN</h4>
                        <p>{{ $template->cat_desc_en }}</p>
                        <h4>Description TH</h4>
                        <p>{{ $template->cat_desc_th }}</p>
                        <h4>Status</h4>
                        <p>{{ $status[$template->cat_status] }}</p>
                        <h4>Main Category</h4>
                        <p>@if($template->main_cat == '1') Yes @else No @endif</p>
                        <!-- <h4>Category Level</h4>
                        <p>{{ $template->cat_level}}</p> -->

                        <a href="{{ url('/category/edit/' . $template->id) }}" class="btn btn-secondary">Edit</a>&nbsp;&nbsp;
                        <a href="{{ route('category.list') }}" class="btn btn-primary">Return</a> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('javascript')

@endsection
