@extends('dashboard.base')

@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Category</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('category.update', $template->id) }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" value="{{$template->id}}" name="id">
                            <div class="form-group row">
                                <label>Category Name EN</label>
                                <input class="form-control" type="text" placeholder="Category Name EN" name="cat_name_en" value="{{$template->cat_name_en}}" required autofocus/>
                            </div>
                            <div class="form-group row">
                                <label>Category Name TH</label>
                                <input class="form-control" type="text" placeholder="Category Name TH" name="cat_name_th" value="{{$template->cat_name_th}}" required />
                            </div>
                            <div class="form-group row">
                                <label>Description EN</label>
                                <textarea class="form-control" name="cat_desc_en" rows="10" placeholder="Content" />{{$template->cat_desc_en}}</textarea>
                            </div>
                            <div class="form-group row">
                                <label>Description TH</label>
                                <textarea class="form-control" name="cat_desc_th" rows="10" placeholder="Content" />{{$template->cat_desc_th}}</textarea>
                            </div>
                            <div class="form-group row">
                                <label>Root Category</label>
                                <select class="form-control" name="sub_cat_id">
									<option value="">-- select root category --</option>
									@foreach($category as $cat)
									<option @if($template->sub_cat_id == $cat->id) selected @endif value="{{$cat->id}}">{{$cat->cat_name_en}} | {{$cat->cat_name_th}}</option>
									@endforeach
								</select>
                            </div>
                            <div class="form-group">
                                <label>Main Category</label><BR>
                                <label><input class="form-control" type="radio" name="main_cat" value="1" @if($template->main_cat == '1') checked @endif /> Active </label>&nbsp;&nbsp;&nbsp;
                                <label><input class="form-control" type="radio" name="main_cat" value="0" @if($template->main_cat == '0') checked @endif/> Inactive </label>
                            </div>
                            <div class="form-group">
                                <label>Status</label><BR>
                                <label><input class="form-control" type="radio" name="cat_status" value="1" @if($template->cat_status == '1') checked @endif /> Active </label>&nbsp;&nbsp;&nbsp;
                                <label><input class="form-control" type="radio" name="cat_status" value="2" @if($template->cat_status == '2') checked @endif /> Inactive </label>
                            </div>
                            <div class="form-group">
                                <label>Legion</label><BR>
                                <label><input class="form-control" type="radio" name="legion" @if($template->legion == 'in') checked @endif value="in"/> India </label>&nbsp;&nbsp;&nbsp;
                                <label><input class="form-control" type="radio" name="legion" @if($template->legion == 'th') checked @endif value="th"/> Thailand </label>&nbsp;&nbsp;&nbsp;
                                <label><input class="form-control" type="radio" name="legion" @if($template->legion == 'inth') checked @endif value="inth" checked/> Both </label>
                            </div>
                            <div class="form-group">
                                <label>Category Image</label>
                                <input class="form-control" type="file"  name="cat_main_image" accept="image/*"/>
                            </div>
                            <button class="btn btn-success" type="submit">Edit</button>
                            <a href="{{ route('category.list') }}" class="btn btn-primary">Return</a> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('javascript')

@endsection
