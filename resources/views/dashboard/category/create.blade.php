@extends('dashboard.base')

@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="flex items-center">
                            <i class="cil-plus mr-1"></i> Add Category
                        </h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" class="form-action">
                            @csrf
                            <div class="form-group">
                                <div class="">
                                    <label>Old Category</label>
                                </div>
                                <div class="flex justify-between h-60">
                                    <div class="w-1/3 bg-gray-400 p-3">
                                        dd
                                    </div>
                                    <div class="w-1/3 bg-red-400">2</div>
                                    <div class="w-1/3 bg-blue-400">3</div>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('javascript')

@endsection
