@extends('dashboard.base')

@section('content')
<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header">
            <i class="fa fa-align-justify"></i>Category List
          </div>
          <div class="card-body">
            <div class="toolbar flex justify-between">
              <div class="ml-auto py-3">
                <button type="button" class="btn btn-labeled btn-info" data-type="save">
                  <span class="btn-label"><i class="cil-plus"></i></span>
                  New Category
                </button>
              </div>
            </div>
            <table class="table table-responsive-sm table-bordered table-striped" id="main_tb">
              <thead>
                <tr>
                  <th width=""></th>                 
                  <th width="120">Status</th>                  
                  <th width="120">Action</th>                  
                </tr>
              </thead>
              <tbody>
                @foreach([1,2,3,4,5] as $k => $v)
                  <tr>
                    <td>
                        <div class="flex">
                          <img src='https://placehold.it/60x60' class="rounded-full"/>
                          <div class="flex flex-col mx-3 justify-center">
                            <a href="#" class="flex items-category">
                              <b>Level 3</b>
                            </a>
                            <span class="text-gray-500">
                              <a href="#" class="text-gray-400">Level 1</a> / <a href="#" class="text-gray-400">Level 2</a></span>
                          </div>
                        </div>
                    </td>
                    <td>
                      <span class="text-success">
                        <a href="#" class="text-primary" data-enabled="{{ $k }}">Enabled</a>
                      </span>
                    </td>
                    <td>
                      <a href="#" class="text-info block">View/Edit</a>
                      <a href="#" class="text-danger">Remove</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('javascript')
<script language="javascript">
$(document).ready(function () {
  $('#main_tb').dataTable();
});
</script>

@endsection
