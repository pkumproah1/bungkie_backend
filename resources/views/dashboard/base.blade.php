<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Bungkie Admin">
    <meta name="author" content="Prapan Kumrpaoh">
    <meta name="baseUrl" content="<?=url('/');?>">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Bungkie Admin </title>
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('public/assets/favicon/favicon.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('public/assets/favicon/favicon.png')}}">
    <link rel="manifest" href="{{asset('resources/assets/favicon/manifest.json')}}">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('public/assets/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <!-- Icons-->
    <link href="{{ asset('public/css/free.min.css') }}" rel="stylesheet"> <!-- icons -->
    <!-- Main styles for this application-->
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <link href="{{ asset('public/css/coreui-chartjs.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/loading.css') }}" rel="stylesheet">
    @yield('css')
  </head>
  <body class="c-app">
    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
      @include('dashboard.shared.nav-builder')
      @include('dashboard.shared.header')
      <div class="c-body">
        <main class="c-main">
          @yield('content')
        </main>
        @include('dashboard.shared.footer')
      </div>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="{{ asset('public/js/coreui.bundle.min.js') }}"></script>
    <script src="{{ asset('public/js/coreui-utils.js') }}"></script>
    <script  type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script  type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
    <script  type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>
    @yield('javascript')
  </body>
</html>
