@extends('dashboard.base')
@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row b-wrapper">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4 id="text-title">Seller Registering Account</h4>
                        <!-- <h4>Verification Data</h4> -->
                    </div>
                    <!-- Form -->
                    <div class="card-body" id="main" style="display: block;">
                        <form method="POST" class="form-action">
                            @csrf
                            <!-- Tab Header -->
                            <div class="add-item">
                                <!-- Account section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Account" role="button" aria-expanded="true" aria-controls="collapseAccount">
                                        <h5>Account</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Account">
                                        <div class="form-group row">
                                            <label for="username">Username <span class="text-danger">*</span></label>
                                            <input class="form-control user" id="username" type="text" placeholder="Username" name="username" maxlength="50" minlength="6" value="{{ $seller->username ?? '' }}" required/>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password">Password <span class="text-danger">*</span></label>
                                            <input class="form-control password" id="password" type="password" placeholder="Password" name="password" maxlength="30" minlength="6" value="{{ $seller->password ?? '' }}" required/>
                                        </div>

                                        <div class="form-group row">
                                            <label for="cpassword">Confirm Password <span class="text-danger">*</span></label>
                                            <input class="form-control c-password" id="cpassword" type="password" placeholder="Confirm Password" name="conf-password" maxlength="30" minlength="6" value="{{ $seller->password ?? '' }}" required/>
                                        </div>
                                    </div>
                                </div>
                                <!-- end Account -->
                                <!-- Basic section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Basic" role="button" aria-expanded="true" aria-controls="collapseBasic">
                                        <h5>Basic Information</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Basic">
                                        <!-- Tax number -->
                                        <div class="form-group row">
                                            <label for="tax">Tax Number</label>
                                            <input class="form-control numeric" type="text" maxlength="13" id="tax" placeholder="Tax Number" name="tax_number" value="{{ $seller->tax_number ?? '' }}" >
                                        </div>

                                        <!-- Company name -->
                                        <div class="form-group row">
                                            <label for="companyname">Company Name <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" id="companyname" placeholder="Company Name" name="company_name" value="{{ $seller->company_name ?? '' }}" required/>
                                        </div>
                                        <!-- Company name -->
                                        <div class="form-group row">
                                            <label for="shopname">Shop Name <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" id="shopname" maxlength="180" placeholder="Shop Name" name="shop_name" value="{{ $seller->shop_name ?? '' }}" required/>
                                        </div>
                                        <!-- brand name -->
                                        <div class="form-group row">
                                            <label for="brand">Brand Name <span class="text-danger">*</span></label>
                                            <input class="form-control" id="brand" type="text" placeholder="Brand Name" name="brand_name" value="{{ $seller->brand_name ?? '' }}" required/>
                                        </div>
                                        <!-- Person In Charge Name -->
                                        <div class="form-group row">
                                            <label for="personCharge">Person In Charge Name <span class="text-danger">*</span></label>
                                            <input class="form-control" id="personCharge" type="text" placeholder="Person In Charge Name" name="person_name" value="{{ $seller->person_name ?? '' }}" required/>
                                        </div>
                                        <!-- Telephone Number -->
                                        <div class="form-group row">
                                            <label for="tel">Telephone Number <span class="text-danger">*</span></label>
                                            <input class="form-control numeric" id="tel" maxlength="10" type="text" placeholder="Telephone Number" name="basic_tel" value="{{ $seller->basic_tel ?? '' }}" required/>
                                        </div>
                                        <!-- Email -->
                                        <div class="form-group row">
                                            <label for="email">Email <span class="text-danger">*</span></label>
                                            <input class="form-control" id="email" type="email" placeholder="Email" name="email" value="{{ $seller->email ?? '' }}" required/>
                                        </div>
                                    </div>
                                </div>
                                <!-- end Basic -->
                                <!-- Address warehouse section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Address" role="button" aria-expanded="true" aria-controls="collapseAddress">
                                        <h5>Warehouse Address</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Address">
                                        <!-- PIC Warehouse Name -->
                                        <div class="form-group row">
                                            <label for="picName">PIC Name <span class="text-danger">*</span></label>
                                            <input class="form-control" id="picName" type="text" placeholder="PIC Name" name="warehouse_name" value="{{ $seller->warehouse_name ?? '' }}" required/>
                                        </div>
                                        <!-- PIC Warehouse Telephone Number -->
                                        <div class="form-group row">
                                            <label for="picTel">PIC Telephone Number <span class="text-danger">*</span></label>
                                            <input class="form-control numeric" id="picTel" maxlength="10" type="text" placeholder="PIC Telephone Number" name="warehouse_tel" value="{{ $seller->warehouse_tel ?? '' }}" required/>
                                        </div>
                                        <!-- Warehouse Address -->
                                        <div class="form-group row">
                                            <label for="warehouse_address">
                                                Address <span class="text-danger">*</span>
                                            </label>
                                            <textarea class="form-control" name="warehouse_address" id="warehouse_address" rows="3" required>{{ $seller->shipping_address ?? '' }}</textarea>
                                        </div>
                                        {{-- Address --}}
                                        <div class="form-group address">
                                            <div class="row">
                                                {{-- Province --}}
                                                <div class="col-md-3">
                                                    <label for="warehouse_province">Province <span class="text-danger">*</span></label>
                                                    <select id="warehouse-province" class="form-control chosen" name="warehouse_province" required>
                                                        <option value="">Select</option>
                                                        @foreach($province as $prov)
                                                            <option value="{{ $prov->province_code }}">{{ $prov->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                {{-- City --}}
                                                <div class="col-md-3">
                                                    <label for="warehouse_city">City <span class="text-danger">*</span></label>
                                                    <select id="warehouse-city" class="form-control chosen" name="warehouse_city" required></select>
                                                </div>
                                                {{-- District --}}
                                                <div class="col-md-3">
                                                    <label for="warehouse_district">District <span class="text-danger">*</span></label>
                                                    <select id="warehouse-district" class="form-control chosen" name="warehouse_district" required></select>
                                                </div>
                                                {{-- zip code --}}
                                                <div class="col-md-3">
                                                    <label for="warehouse_zipcode">Zip code <span class="text-danger">*</span></label>
                                                    <input class="form-control" id="warehouse-zipcode" type="text" name="warehouse_zipcode" required>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- end Address -->

                                <!-- Address Billing section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#AddressBilling" role="button" aria-expanded="true" aria-controls="collapseAddress">
                                        <h5>Billing Address</h5>
                                    </div>
                                    <div class="b-body collapse show" id="AddressBilling">
                                        <!-- Billing Address -->
                                        <div class="form-group row">
                                            <label for="billing_address">Billing Address <span class="text-danger">*</span></label>
                                            <textarea class="form-control" name="billing_address" id="billing_address" rows="3" required>{{ $seller->billing_address ?? '' }}</textarea>
                                        </div>
                                        {{-- Address --}}
                                        <div class="form-group address">
                                            <div class="row">
                                                {{-- Province --}}
                                                <div class="col-md-3">
                                                    <label for="billing_province">Province <span class="text-danger">*</span></label>
                                                    <select id="billing-province" class="form-control chosen" name="billing_province" required>
                                                        <option value="">Select</option>
                                                        @foreach($province as $prov)
                                                            <option value="{{ $prov->province_code }}">{{ $prov->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                {{-- City --}}
                                                <div class="col-md-3">
                                                    <label for="billing_city">City <span class="text-danger">*</span></label>
                                                    <select id="billing-city" class="form-control chosen" name="billing_city" required></select>
                                                </div>
                                                {{-- District --}}
                                                <div class="col-md-3">
                                                    <label for="billing_district">District <span class="text-danger">*</span></label>
                                                    <select id="billing-district" class="form-control chosen" name="billing_district" required></select>
                                                </div>
                                                {{-- zip code --}}
                                                <div class="col-md-3">
                                                    <label for="billing_zipcode">Zip code <span class="text-danger">*</span></label>
                                                    <input class="form-control" id="billing-zipcode" type="text" name="billing_zipcode" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end Address -->

                                <!-- Bank Account section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#BankAccount" role="button" aria-expanded="true" aria-controls="collapseBankAccount">
                                        <h5>Bank Account</h5>
                                    </div>
                                    <div class="b-body collapse show" id="BankAccount">
                                        <!-- Bank -->
                                        <div class="form-group row">
                                            <label>Bank <span class="text-danger">*</span></label>
                                            <select class="form-control chosen-select" name="bank" required data-select="{{ $seller->bank ?? '' }}">
                                                <option value="" selected>-- select bank --</option>
                                                @if (isset($bank_lists))
                                                    @foreach($bank_lists as $b)
                                                        <option value="{{ $b->id }}" >{{ $b->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                        <!-- Account Name -->
                                        <div class="form-group row">
                                            <label for="accountNumber">Account Number <span class="text-danger">*</span></label>
                                            <input class="form-control numeric" id="accountNumber" maxlength="15" type="text" placeholder="Account Number" name="bank_number" maxlength="20" value="{{ $seller->bank_number ?? '' }}" required/>
                                        </div>
                                        <!-- Account Name -->
                                        <div class="form-group row">
                                            <label for="accountName">Account Name <span class="text-danger">*</span></label>
                                            <input class="form-control" id="accountName" type="text" placeholder="Account Name" name="bank_accout" value="{{ $seller->bank_accout ?? '' }}" required/>
                                        </div>

                                        {{-- Payment Cycle --}}
                                        <div class="form-group row">
                                            <label>Payment Cycle <span class="text-danger">*</span></label>
                                            <select class="form-control chosen-select" name="payment_cycle" required>
                                                <option value="1" selected>Weekly</option>
                                                <option value="2" selected>Twice a month</option>
                                                <option value="3" selected>Monthly</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- end bank -->
                                <!-- shipping -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Other" role="button" aria-expanded="true" aria-controls="collapseOther">
                                        <h5>Shipping</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Other">
                                        <div class="form-group">
                                            <div class="border-bottom mb-1">
                                                <label>Free Shipping</label>
                                            </div>
                                            <div class="d-flex">
                                                <div class="form-check form-check-lg">
                                                    <input class="form-check-input form-check-input-lg" type="radio" name="shipping" id="shipping-yes" value="Y">
                                                    <label class="form-check-label" for="shipping-yes">
                                                        Yes
                                                    </label>
                                                </div>
                                                <div class="form-check ml-3">
                                                    <input class="form-check-input" type="radio" name="shipping" id="shipping-no" value="N" checked>
                                                    <label class="form-check-label" for="shipping-no">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end shipping -->
                                <!-- other -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Other" role="button" aria-expanded="true" aria-controls="collapseOther">
                                        <h5>Other</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Other">
                                        <div class="form-group">
                                            <div class="border-bottom mb-1">
                                                <label>Approved</label>
                                            </div>
                                            <div class="d-flex">
                                                <div class="form-check form-check-lg">
                                                    <input class="form-check-input form-check-input-lg" type="radio" name="approve" id="approve-yes" value="Y" checked>
                                                    <label class="form-check-label" for="approve-yes">
                                                        Yes
                                                    </label>
                                                </div>
                                                <div class="form-check ml-3">
                                                    <input class="form-check-input" type="radio" name="approve" id="approve-no" value="N">
                                                    <label class="form-check-label" for="approve-no">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Status -->
                                        <div class="form-group">
                                            <div class="border-bottom mb-1">
                                                <label>Status</label>
                                            </div>
                                            <div class="d-flex">
                                                <div class="form-check form-check-lg">
                                                    <input class="form-check-input form-check-input-lg" type="radio" name="status" id="status-yes" value="Y" checked>
                                                    <label class="form-check-label" for="status-yes">
                                                        Yes
                                                    </label>
                                                </div>
                                                <div class="form-check ml-3">
                                                    <input class="form-check-input" type="radio" name="status" id="status-no" value="N">
                                                    <label class="form-check-label" for="status-no">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end other -->
                                <div class="form-group d-flex justify-content-end">
                                    <a class="btn btn-labeled btn-sm btn-default border" href="{{ route('seller.list') }}">
                                        <span class="btn-label"><i class="cil-media-skip-backward"></i></span>
                                        Return
                                    </a>&nbsp;&nbsp;

                                    <button type="submit" class="btn btn-labeled btn-sm btn-success" data-type="preview">
                                        <span class="btn-label"><i class="cil-check-alt"></i></span>
                                        Next
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End form -->

                    <!-- Preview -->
                    <div class="card-body" id="verification" style="display: none;">
                        <div class="add-item">
                            <!-- Account -->
                            <div class="verify">
                                <div class="header">
                                    <h5>1. Account</h5>
                                </div>
                                <div class="body">
                                    <label>Username: <span class="text-success" id="fill-username"></span></label>
                                    <label>Password: <span class="text-success" id="fill-password"></span></label>
                                </div>
                            </div>
                            <!-- Basic Information -->
                            <div class="verify">
                                <div class="header">
                                    <h5>2. Basic Information</h5>
                                </div>
                                <div class="body">
                                    <label>Tax Number: <span class="text-success" id="fill-tax_number"></span></label>
                                    <label>Company Name: <span class="text-success" id="fill-company_name"></span></label>
                                    <label>Shop Name: <span class="text-success" id="fill-shop_name"></span></label>
                                    <label>Brand Name: <span class="text-success" id="fill-brand_name"></span></label>
                                    <label>Person In Charge Name: <span class="text-success" id="fill-person_name"></span></label>
                                    <label>Telephone Number: <span class="text-success" id="fill-basic_tel"></span></label>
                                    <label>Email: <span class="text-success" id="fill-email"></span></label>
                                </div>
                            </div>
                            <!-- Address -->
                            <div class="verify">
                                <div class="header">
                                    <h5>3. Warehouse Address</h5>
                                </div>
                                <div class="body">
                                    <label>PIC Warehouse Name: <span class="text-success" id="fill-warehouse_name"></span></label>
                                    <label>PIC Warehouse Telephone Number: <span class="text-success" id="fill-warehouse_tel"></span></label>
                                    <label>Address: <span class="text-success" id="fill-warehouse_address"></span></label>
                                    <label>Province: <span class="text-success" id="fill-warehouse_province"></span></label>
                                    <label>City: <span class="text-success" id="fill-warehouse_city"></span></label>
                                    <label>District: <span class="text-success" id="fill-warehouse_district"></span></label>
                                    <label>Zipcode: <span class="text-success" id="fill-warehouse_zipcode"></span></label>
                                </div>
                            </div>
                            <!-- Address -->
                            <div class="verify">
                                <div class="header">
                                    <h5>4. Billing Address</h5>
                                </div>
                                <div class="body">
                                    <label>Address: <span class="text-success" id="fill-billing_address"></span></label>
                                    <label>Province: <span class="text-success" id="fill-billing_province"></span></label>
                                    <label>City: <span class="text-success" id="fill-billing_city"></span></label>
                                    <label>District: <span class="text-success" id="fill-billing_district"></span></label>
                                    <label>Zipcode: <span class="text-success" id="fill-billing_zipcode"></span></label>
                                </div>
                            </div>
                            <!-- Address -->
                            <div class="verify">
                                <div class="header">
                                    <h5>5. Bank Account</h5>
                                </div>
                                <div class="body">
                                    <label>Bank: <span class="text-success" id="fill-bank"></span></label>
                                    <label>Account Number: <span class="text-success" id="fill-bank_number"></span></label>
                                    <label>Account Name: <span class="text-success" id="fill-bank_accout"></span></label>
                                    <label>Payment Cycle: <span class="text-success" id="fill-payment_cycle"></span></label>
                                </div>
                            </div>
                            <div class="verify">
                                <div class="header">
                                    <h5>6. Free Shipping</h5>
                                </div>
                                <div class="body">
                                    <label>Free Shipping: <span class="text-success" id="fill-shipping"></span></label>
                                </div>
                            </div>
                            <!-- Other -->
                            <div class="verify">
                                <div class="header">
                                    <h5>7. Other</h5>
                                </div>
                                <div class="body">
                                    <label>Approve: <span class="text-success" id="fill-approve"></span></label>
                                    <label>Status: <span class="text-success" id="fill-status"></span></label>
                                </div>
                            </div>
                            <!-- button -->
                            <div class="form-group d-flex justify-content-end">
                                <button type="button" class="btn btn-labeled btn-sm btn-default border" data-type="back">
                                    <span class="btn-label"><i class="cil-media-skip-backward"></i></span>
                                    Back
                                </button>&nbsp;&nbsp;
                                <!-- save -->
                                <button type="button" class="btn btn-labeled btn-sm btn-success" data-type="save">
                                    <span class="btn-label"><i class="cil-check-alt"></i></span>
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- end preview -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('css')
    <!-- vendors -->
    <link rel="stylesheet" href="{{ asset('public/vendors/froala_editor_3.2.5/css/froala_editor.pkgd.css') }}">
  
    <!-- Code Mirror CSS file. -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
    <link href="{{ asset('public/assets/pages/product/css/create.css') }}" rel="stylesheet">
    <!-- Include the plugin CSS file. -->
    <link rel="stylesheet" href="{{ asset('public/vendors/froala_editor_3.2.5/css/plugins/code_view.css') }}">
    <link rel="stylesheet" href="{{ asset('public/vendors/froala_editor_3.2.5/css/third_party/image_tui.min.css') }}">
    <link href="{{ asset('public/vendors/chosen/chosen.css') }}" rel="stylesheet">
    <!-- custom -->
    <link href="{{ asset('public/assets/pages/seller/css/create.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tui-image-editor@3.2.2/dist/tui-image-editor.css">
    <link rel="stylesheet" href="https://uicdn.toast.com/tui-color-picker/latest/tui-color-picker.css">

    
@endsection

@section('javascript')
<!-- vendors -->
<script type="text/javascript" src="{{ asset('public/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('public/vendors/chosen/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="{{ asset('public/vendors/jquery.loading.min.js') }}"></script>
<script src="{{ asset('public/vendors/chosen/chosen.jquery.min.js') }}"></script>
<!-- custom -->
<script src="{{ asset('public/assets/location/location_th.js?version=100') }}"></script>
<script src="{{ asset('public/assets/pages/seller/js/create.js?v100') }}"></script>
@endsection



