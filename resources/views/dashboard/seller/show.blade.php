@extends('dashboard.base')

@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4>User Email: {{ $user->email }}</h4>
                    </div>
                    <div class="card-body">
                        <h4>Firstname</h4>
                        <p>{{ $user->fname }}</p>
                        <h4>Lastname</h4>
                        <p>{{ $user->lname }}</p>
                        <h4>Display name</h4>
                        <p>{{ $user->display_name }}</p>
                        <h4>Status</h4>
                        <p>{{ $status[$user->status] }}</p>

                        <a href="{{ url('/user/edit/' . $user->id) }}" class="btn btn-secondary">Edit</a>&nbsp;&nbsp;
                        <a href="{{ route('user.list') }}" class="btn btn-primary">Return</a> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('javascript')

@endsection
