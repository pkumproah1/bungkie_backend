@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>Product List</div>
                    <div class="card-body">
                        @if(Session::has('message'))
                            <div class="row">
                                <div class="col-12">
                                    <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                                </div>
                            </div>
                         @endif
                        <div class="row"> 
                          <a href="{{ route('customer.create') }}" class="btn btn-primary m-2">Add Product</a>
                        </div>
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Display name</th>
                            <th>E-mail</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($users as $user)
                            <tr>
                            <td>{{ $user->fname }}</td>
                              <td>{{ $user->lname }}</td>
                              <td>{{ $user->display_name }}</td>
                              <td>{{ $user->email }}</td>
                              <td>{{ $status[$user->status] }}</td>
                              <!-- <td>
                                <a href="{{ url('/user/show/' . $user->id) }}" class="btn btn-block btn-primary">View</a>
                              </td> -->
                              <td>
                                <a href="{{ url('/user/edit/' . $user->id ) }}" class="btn btn-block btn-primary">Edit</a>
                              </td>
                              <td>
                                @if( $you->id !== $user->id )
                                <form action="{{ route('user.delete', $user->id ) }}" method="POST">
                                  <input type='hidden' name='id' value="{{$user->id}}">
                                    @csrf
                                    <input type='hidden' name='act' value="delete"> <button class="btn btn-danger">Delete</button>
                                </form>
                                @endif
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {{ $users->links() }}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection
