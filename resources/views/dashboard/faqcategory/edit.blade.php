@extends('dashboard.base')

@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit FAQ Category</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('faqcategory.update') }}">
                            @csrf
                            @method('POST')
                            <input type="hidden" name="id" value="{{ $faq->id }}" />
                            <div class="form-group row">
                                <label>Name</label>
                                <input class="form-control" type="text" placeholder="Topic" name="cat_name" required autofocus value="{{ $faq->cat_name }}"/>
                            </div>
                            <div class="form-group row">
                                <label>Description</label>
                                <input class="form-control" type="text" placeholder="Description" name="cat_desc" required value="{{ $faq->cat_desc }}"/>
                            </div>
                            <button class="btn btn-success" type="submit">Submit</button>
                            <a href="{{ route('faqcategory.index') }}" class="btn btn-primary">Return</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('javascript')

@endsection
