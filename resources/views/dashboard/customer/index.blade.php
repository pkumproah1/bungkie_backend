@extends('dashboard.base')

@section('content')
<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
            <div class="card-header">
              <i class="fa fa-align-justify"></i> Customer List</div>
            <div class="card-body">
                @if(Session::has('message'))
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        </div>
                    </div>
                  @endif
                <!-- <div class="row"> 
                  <a href="{{ route('customer.create') }}" class="btn btn-primary m-2">Add Product</a>
                </div>
                <br> -->
                <table class="table table-responsive-sm table-striped" id="table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Group</th>
                    <th>Point</th>
                    <th>Wallet</th>
                    <th>Birthday</th>
                    <th>Status</th>
                    <th>Created</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)
                    <tr>
                      <td>{{ $user->display_name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->menuroles }}</td>
                      <td>{{ $user->point }}</td>
                      <td>{{ $user->wallet }}</td>
                      <td>{{ $user->birthday }}</td>
                      <td>
                        @if($user->status === 'Y')
                          <span class="badge badge-success">Yes</span>
                        @else
                          <span class="badge badge-danger">no</span>
                        @endif
                      </td>
                      <td>{{ $user->created_at }}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection


@section('javascript')
  <script language="javascript">
  $(document).ready(function () {
    $('#table').dataTable();
    //$('.dataTables_length').addClass('bs-select');
  });
  </script>
@endsection
