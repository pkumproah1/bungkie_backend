@extends('dashboard.base')
@section('content')
<div class="container-fluid">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header">
            <i class="fa fa-align-justify"></i> <h5>Purchase</h5>
          </div>
          @if($show)
          <div class="card-toolbar">
            <div class="row">
              <!-- search  -->
              <div class="col-md-4">
                <div class="search-toolbar">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Customer Name</button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item active" href="#">Customer Name</a>
                        <a class="dropdown-item" href="#">Session ID</a>
                      </div>
                    </div>
                    <input type="text" class="form-control" aria-label="Text input with dropdown button">
                  </div>
                </div>
              </div>
              <!-- end search -->
              <div class="col-md-3 offset-md-5">
                <div class="date-toolbar">
                  <label for="date">Date</label> 
                  <input type="text" id="date" class="form-control daterange">
                </div>
              </div>

            </div>

            <div class="card-footer-toolbar">
              <div class="button">
                <button class="btn btn-info">Search</button>
                <button class="btn btn-default border">Reset</button>
              </div>
            </div>
          </div>
          @endif








            <div class="card-body">
                @if(Session::has('message'))
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                        </div>
                    </div>
                  @endif
                <table class="table table-responsive-sm table-striped" id="table">
                <thead>
                  <tr>
                    <th>User</th>
                    <th>Session ID</th>
                    <th>Sale Price</th>
                    <th>Currency</th>
                    <th>Quantity</th>
                    <th>Created</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($purchases as $purchase)
                    <tr>
                      <td>{{ $purchase->display_name }}</td>
                      <td>{{ $purchase->session_id }}</td>
                      <td>{{ $purchase->price }}</td>
                      <td>{{ $purchase->currency }}</td>
                      <td>{{ $purchase->quantity }}</td>
                      <td>{{ $purchase->created_at }}</td>
                      <td>
                        <a href="{{ url('/purchase/view/' . $purchase->session_id) }}">Details</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css') 
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('javascript')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script src="{{ asset('public/assets/pages/purchase/js/index.js') }}"></script>

@endsection
