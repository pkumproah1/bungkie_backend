@extends('dashboard.base')

@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Purchase</h5>
                    </div>
                    <div class="card-body" id="purchase">
                        <section id="basic">
                            <div class="basic">
                                <span>Session ID: {{ $purchases[0]->session_id }}</span>
                                <span>Date: {{ date("Y-m-d", strtotime($purchases[0]->created_at)) }}</span>
                            </div>
                            <div class="address" style="display: none;">
                                <h6>Address:</h6>
                                <address>
                                    asdasdwerqoiwpewqxxsxssxsdfsd
                                    fdsfdsfsdfdsfsdfsdfsdfsdfsdfsdfsdfsfs d
                                </address>
                            </div>
                        </section>
                        @foreach($purchases as $key => $purchase) 
                        <section id="product-detail">
                            <div class="number">{{ $key+1 }}</div>
                            <div class="product-detail">
                                <div class="thumbnail">
                                    @php
                                        $img = "https://via.placeholder.com/400x400?text=No+Image"
                                    @endphp
                                    <img src="{{ asset('storage/app/'.$purchase->path) }}" alt="">
                                </div>
                                <div class="product">
                                    <h5>{{ $purchase->name }}</h5>
                                    <div class="product-description">
                                        <span>Quantity: {{ $purchase->quantity ?? '' }}</span>
                                        <span>Total Price: {{ $purchase->sale_price }}</span>
                                        <span>{{ $purchase->currency }}</span>
                                        <span class="right">Last tracking: <span class="text-success lastStatus">Paid</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="trackings">
                                <div class="header">
                                    <h6><i class="cil-history"></i> Tracking logs</h6>
                                    <div class="button">
                                        @php 
                                            $json = array(
                                                'purchase_id' => $purchase->id,
                                                'product_name' => $purchase->name
                                            );        
                                        @endphp
                                        <button type="button" class="btn btn-labeled btn-sm btn-default border modal-active" 
                                            data-backdrop="static" data-keyboard="false"
                                            data-toggle="modal" data-target="#updatedStatus"
                                            data-json="{{ json_encode($json) }}">
                                            <span class="btn-label"><i class="cil-plus"></i></span>
                                            Update Status
                                        </button>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered ontable" data-id="{{ $purchase->id }}">
                                        <thead>
                                            <th>#</th>
                                            <th>Status</th>
                                            <th>Status Description</th>
                                            <th>Tracking Number</th>
                                            <th>Date</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="updatedStatus" tabindex="-1" role="dialog" aria-labelledby="updateStatusModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="updateStatusModal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="formUpdateStatus" action="POST">
                        @csrf
                        <label>Status</label>
                        <select name="status" id="modalStatus" class="form-control"></select>
                        <label for="remark">Notice </label>
                        <input type="hidden" name="purchase_id">
                        <input type="hidden" name="purchase_number">
                        <textarea name="notice" class="form-control"></textarea>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary updateStatusSubmit">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('css')
<link href="{{ asset('public/vendors/chosen/chosen.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/assets/pages/purchase/css/details.css') }}">
@endsection
@section('javascript')
<script src="{{ asset('public/vendors/chosen/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js"></script>
<script src="{{ asset('public/vendors/jquery.loading.min.js') }}"></script>
<script src="{{ asset('public/assets/pages/purchase/js/details.js') }}"></script>
@endsection
