
      
<div class="c-wrapper">
  <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    <button class="c-header-toggler c-class-toggler d-lg-none mr-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
      <span class="c-header-toggler-icon"></span>
    </button>
    <a class="c-header-brand d-sm-none" href="#">
      <h1>Backoffice Bungkie</h1>
    </a>
    <button class="c-header-toggler c-class-toggler ml-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
      <span class="c-header-toggler-icon"></span>
    </button>
    <?php
        use App\MenuBuilder\FreelyPositionedMenus;
        if(isset($appMenus['top menu'])){
            FreelyPositionedMenus::render( $appMenus['top menu'] , 'c-header-', 'd-md-down-none');
        }
    ?>
    @guest  
          <!-- jsdflksjdlfkjslkfjsdf -->
    @else
    <ul class="c-header-nav ml-auto mr-4">
      <li class="c-header-nav-item dropdown d-md-down-none mx-2">
        <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <i class="cil-bell"></i>
          <span class="badge badge-pill badge-danger">1</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu pt-0">
          <div class="dropdown-header bg-light">
            <strong>คุณมี 4 ออเดอร์ใหม่</strong>
          </div>
          <a class="dropdown-item" href="#">
            <div class="message">
              <div>
                <small class="text-muted">มีออเดอร์ใหม่</small>
                <small class="text-success float-right mt-1">จ่ายเงินแล้ว</small>
              </div>
              <div class="text-truncate font-weight-bold mt-1">
                <span class="text-danger">Order</span> 123456789
              </div>
            </div>
          </a>
          <a class="dropdown-item text-center border-top" href="{{ url('/order') }}"><strong>ดูออเดอร์ทั้งหมด</strong></a>
        </div>
      </li>

      <li class="c-header-nav-item dropdown">
        <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <div class="c-avatar">
            <img class="c-avatar-img" src="{{ env('APP_URL', '') }}/public/assets/img/avatars/6.jpg" alt="user@email.com">
          </div>
        </a>
        
        <div class="dropdown-menu dropdown-menu-right pt-0">              
          <div class="dropdown-header bg-light py-2">
            <strong>Settings</strong>
          </div>
          <a class="dropdown-item" href="{{ url('seller/profile') }}">
            <svg class="c-icon mr-2">
              <use xlink:href="{{ env('APP_URL', '') }}/public/icons/sprites/free.svg#cil-user"></use>
            </svg>
            Profile
          </a>
          <a class="dropdown-item" href="{{ url('seller/profile#profile-setting') }}">
            <svg class="c-icon mr-2">
              <use xlink:href="{{ env('APP_URL', '') }}/public/icons/sprites/free.svg#cil-settings"></use>
            </svg>
            Settings
          </a>

          <a class="dropdown-item" href="{{ url('/sign-out') }} ">
            <svg class="c-icon mr-2">
              <use xlink:href="{{ env('APP_URL', '') }}/public/icons/sprites/free.svg#cil-account-logout"></use>
            </svg>
            Logout
          </a>
        </div>
      </li>
    </ul>
    @endguest

    <div class="c-subheader px-3">
      <ol class="breadcrumb border-0 m-0">
        <li class="breadcrumb-item"><a href="">Home</a></li>
        <?php $segments = ''; ?>
        @for($i = 1; $i <= count(Request::segments()); $i++)
            <?php $segments .= '/'. Request::segment($i); ?>
            @if($i < count(Request::segments()))
                <li class="breadcrumb-item">{{ Request::segment($i) }}</li>
            @else
                <li class="breadcrumb-item active">{{ Request::segment($i) }}</li>
            @endif
        @endfor
      </ol>
    </div>
</header>