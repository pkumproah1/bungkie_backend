@extends('dashboard.base')

@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit FAQ</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('faq.update') }}">
                            @csrf
                            @method('POST')
                            <input type="hidden" name="id" value="{{ $faq->id }}" />
                            <div class="form-group row">
                                <label>Category</label>
                                <select class="form-control" name="faq_cat" required>
                                    <option value="" disabled selected>Please select FAQ category</option>
                                    @foreach($faqCategories as $faqCat)
                                    <option value="{{$faqCat->id}}" {{ ( $faqCat->id == $faq->cat_id) ? 'selected' : '' }}>{{ $faqCat->cat_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row">
                                <label>Topic</label>
                                <input class="form-control" type="text" placeholder="Topic" name="topic" required autofocus value="{{ $faq->topic }}"/>
                            </div>
                            <div class="form-group row">
                                <label>Description</label>
                                <textarea class="form-control" name="desc" rows="20" placeholder="Description" required>{{ $faq->desc }}</textarea>
                            </div>
                            <div class="form-group row">
                                <label>Tag</label>
                                <input class="form-control" type="text" placeholder="Tag" name="tag" required value="{{ $faq->tag }}"/>
                            </div>
                            <button class="btn btn-success" type="submit">Submit</button>
                            <a href="{{ route('faq.index') }}" class="btn btn-primary">Return</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('javascript')

@endsection
