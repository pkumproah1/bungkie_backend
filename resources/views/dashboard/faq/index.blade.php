@extends('dashboard.base')

@section('content')

        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>Faq List</div>
                    <div class="card-body">
                        @if(Session::has('message'))
                            <div class="row">
                                <div class="col-12">
                                    <div class="alert alert-success" role="alert">{{ Session::get('message') }}</div>
                                </div>
                            </div>
                         @endif
                        <div class="row">
                          <a href="{{ route('faq.create') }}" class="btn btn-primary m-2">Add FAQ</a>
                        </div>
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>Topic</th>
                            <th>Description</th>
                            {{-- <th></th>
                            <th></th>
                            <th></th> --}}
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($faqs as $faq)
                            <tr>
                              <td><strong>{{ $faq->topic }}</strong></td>
                              <td><strong>{{ $faq->desc }}</strong></td>
                              {{-- <td>
                                <a href="{{ route('prepareSend', ['id' => $mail->id] ) }}" class="btn btn-warning">Send</a>
                              </td> --}}
                              <td>
                                <a href="{{ url('/faq/' . $faq->id) }}" class="btn btn-primary">View</a>
                              </td>
                              <td>
                                <a href="{{ url('/faq/' . 'edit/' . $faq->id) }}" class="btn btn-primary">Edit</a>
                              </td>
                              <td>
                                <form action="{{ route('faq.destroy', $faq->id ) }}" method="DELETE">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger">Delete</button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {{-- {{ $faq->links() }} --}}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection
