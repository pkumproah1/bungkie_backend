@extends('dashboard.base')

@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Faq: {{ $faq->topic }}</h4>
                    </div>
                    <div class="card-body">
                        <h4>Category</h4>
                        <p>{{ $faqCat->cat_name }}</p>
                        <h4>Topic</h4>
                        <p>{{ $faq->topic }}</p>
                        <h4>Description</h4>
                        <p>{{ $faq->desc }}</p>
                        <h4>Tag</h4>
                        <p>{{ $faq->tag }}</p>
                        <a href="{{ route('faq.index') }}" class="btn btn-primary">Return</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('javascript')

@endsection
