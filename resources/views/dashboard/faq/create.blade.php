@extends('dashboard.base')

@section('content')

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Add FAQ</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('faq.store') }}">
                            @csrf
                            <div class="form-group row">
                                <label>Category</label>
                                <select class="form-control" name="faq_cat" required>
                                    <option value="" disabled selected>Please select FAQ category</option>
                                    @foreach($faqCategories as $faqCat)
                                    <option value="{{$faqCat->id}}">{{ $faqCat->cat_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row">
                                <label>Topic</label>
                                <input class="form-control" type="text" placeholder="Topic" name="topic" maxlength="100" required autofocus/>
                            </div>
                            <div class="form-group row">
                                <label>Tag</label>
                                <input class="form-control" type="text" placeholder="Tag" name="tag" required/>
                            </div>
                            <div class="form-group row">
                                <label>Description</label>
                                <textarea class="form-control" type="text" name="desc" rows="20" placeholder="Description" required>
                                </textarea>
                            </div>
                            <button class="btn btn-success" type="submit">Add</button>
                            <a href="{{ route('faq.index') }}" class="btn btn-primary">Return</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section('javascript')

@endsection
