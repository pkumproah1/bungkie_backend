@extends('dashboard.base')
@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row b-wrapper">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Seller Registering Account</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" class="form-action">
                            @csrf
                            <!-- Tab Header -->
                            <div class="add-item">
                                <!-- Account section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Account" role="button" aria-expanded="true" aria-controls="collapseAccount">
                                        <h5>Account</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Account">
                                        <div class="form-group row">
                                            <label>Username <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="Username" name="username" value="" required/>
                                        </div>

                                        <div class="form-group row">
                                            <label>Password <span class="text-danger">*</span></label>
                                            <input class="form-control" type="password" placeholder="Password" name="password" value="" required/>
                                        </div>

                                        <div class="form-group row">
                                            <label>Confirm Password <span class="text-danger">*</span></label>
                                            <input class="form-control" type="password" placeholder="Confirm Password" name="conf-password" value="" required/>
                                        </div>
                                    </div>
                                </div>
                                <!-- end Account -->

                                <!-- Basic section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Basic" role="button" aria-expanded="true" aria-controls="collapseBasic">
                                        <h5>Basic Information</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Basic">
                                        <!-- Company name -->
                                        <div class="form-group row">
                                            <label>Company Name <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="Company Name" name="company_name" value="" required/>
                                        </div>
                                        <!-- brand name -->
                                        <div class="form-group row">
                                            <label>Brand Name <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="Brand Name" name="brand_name" value="" required/>
                                        </div>
                                        <!-- Person In Charge Name -->
                                        <div class="form-group row">
                                            <label>Person In Charge Name <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="Person In Charge Name" name="person_name" value="" required/>
                                        </div>
                                        <!-- Telephone Number -->
                                        <div class="form-group row">
                                            <label>Telephone Number <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="Telephone Number" name="basic_tel" value="" required/>
                                        </div>
                                        <!-- Email -->
                                        <div class="form-group row">
                                            <label>Email <span class="text-danger">*</span></label>
                                            <input class="form-control" type="email" placeholder="Email" name="email" value="" required/>
                                        </div>
                                    </div>
                                </div>
                                <!-- end Basic -->

                                <!-- Address section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Address" role="button" aria-expanded="true" aria-controls="collapseAddress">
                                        <h5>Address</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Address">
                                        
                                        <!-- Shipping Address -->
                                        <div class="form-group row">
                                            <label for="shipping_address">Shipping Address <span class="text-danger">*</span></label>
                                            <textarea class="form-control" name="shipping_address" id="shipping_address" rows="3" required></textarea>
                                        </div>
                                        <!-- PIC Warehouse Name -->
                                        <div class="form-group row">
                                            <label>PIC Warehouse Name <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="PIC Warehouse Name" name="warehouse_name" value="" required/>
                                        </div>
                                        <!-- PIC Warehouse Telephone Number -->
                                        <div class="form-group row">
                                            <label>PIC Warehouse Telephone Number <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="PIC Warehouse Telephone Number" name="warehouse_tel" value="" required/>
                                        </div>
                                        <!-- Billing Address -->
                                        <div class="form-group row">
                                            <label for="billing_address">Billing Address <span class="text-danger">*</span></label>
                                            <textarea class="form-control" name="billing_address" id="billing_address" rows="3" required></textarea>
                                        </div>

                                    </div>
                                </div>
                                <!-- end Address -->

                                <!-- Bank Account section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#BankAccount" role="button" aria-expanded="true" aria-controls="collapseBankAccount">
                                        <h5>Bank Account</h5>
                                    </div>
                                    <div class="b-body collapse show" id="BankAccount">
                                        <!-- Account Name -->
                                        <div class="form-group row">
                                            <label>Account Name <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="Account Name" name="bank_accout" value="" required/>
                                        </div>
                                        <!-- Account Name -->
                                        <div class="form-group row">
                                            <label>Account Number <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="Account Number" name="bank_number" value="" required/>
                                        </div>
                                        <!-- Bank -->
                                        <div class="form-group row">
                                            <label>Bank <span class="text-danger">*</span></label>
                                            <select class="form-control" name="bank">
                                                <option value="x">x</option>
                                                <option value="x">x2</option>
                                                <option value="x">x3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <!-- end Bank Account -->
                                <div class="form-group">
                                    <label>Hilight</label><BR>
                                    <label><input class="form-control" type="radio" name="hilight_flag" value="Y"/> Yes </label>&nbsp;&nbsp;&nbsp;
                                    <label><input class="form-control" type="radio" name="hilight_flag" value="N" checked/> No </label>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-success" type="submit">Add</button>&nbsp;&nbsp;
                                    <a href="{{ route('product.list') }}" class="btn btn-primary">Return</a> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('css')
    <!-- vendors -->
    <link rel="stylesheet" href="{{ asset('public/vendors/froala_editor_3.2.5/css/froala_editor.pkgd.css') }}">
  
    <!-- Code Mirror CSS file. -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">

    <!-- Include the plugin CSS file. -->
    <link rel="stylesheet" href="{{ asset('public/vendors/froala_editor_3.2.5/css/plugins/code_view.css') }}">
    <link rel="stylesheet" href="{{ asset('public/vendors/froala_editor_3.2.5/css/third_party/image_tui.min.css') }}">
    <link href="{{ asset('public/vendors/chosen/chosen.css') }}" rel="stylesheet">
    <!-- custom -->
    <link href="{{ asset('public/assets/pages/product/css/create.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tui-image-editor@3.2.2/dist/tui-image-editor.css">
    <link rel="stylesheet" href="https://uicdn.toast.com/tui-color-picker/latest/tui-color-picker.css">

    
@endsection

@section('javascript')
<!-- vendors -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

<script type="text/javascript" src="{{ asset('public/vendors/froala_editor_3.2.5/js/froala_editor.pkgd.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.6.7/fabric.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/tui-code-snippet@1.4.0/dist/tui-code-snippet.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/tui-image-editor@3.2.2/dist/tui-image-editor.min.js"></script>

<!-- Include TUI plugin. -->
<script type="text/javascript" src="{{ asset('public/vendors/froala_editor_3.2.5/js/third_party/image_tui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('public/vendors/chosen/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<!-- custom -->
<script src="{{ asset('public/assets/pages/product/js/create.js') }}"></script>
@endsection



