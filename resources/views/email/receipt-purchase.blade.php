<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
  style="width:100%;font-family:Arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta name="x-apple-disable-message-reformatting">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="telephone=no" name="format-detection">
  <title>New email template 2021-02-11</title>
  <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]-->
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
  <!--[if gte mso 9]>
<xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG></o:AllowPNG>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
</xml>
<![endif]-->
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    .es-button {
      mso-style-priority: 100 !important;
      text-decoration: none !important;
    }

    a[x-apple-data-detectors] {
      color: inherit !important;
      text-decoration: none !important;
      font-size: inherit !important;
      font-family: inherit !important;
      font-weight: inherit !important;
      line-height: inherit !important;
    }

    .es-desk-hidden {
      display: none;
      float: left;
      overflow: hidden;
      width: 0;
      max-height: 0;
      line-height: 0;
      mso-hide: all;
    }

    @media only screen and (max-width:600px) {

      p,
      ul li,
      ol li,
      a {
        font-size: 16px !important;
        line-height: 150% !important
      }

      h1 {
        font-size: 30px !important;
        text-align: center;
        line-height: 120% !important
      }

      h2 {
        font-size: 26px !important;
        text-align: center;
        line-height: 120% !important
      }

      h3 {
        font-size: 20px !important;
        text-align: center;
        line-height: 120% !important
      }

      h1 a {
        font-size: 30px !important
      }

      h2 a {
        font-size: 26px !important
      }

      h3 a {
        font-size: 20px !important
      }

      .es-header-body p,
      .es-header-body ul li,
      .es-header-body ol li,
      .es-header-body a {
        font-size: 16px !important
      }

      .es-footer-body p,
      .es-footer-body ul li,
      .es-footer-body ol li,
      .es-footer-body a {
        font-size: 16px !important
      }

      .es-infoblock p,
      .es-infoblock ul li,
      .es-infoblock ol li,
      .es-infoblock a {
        font-size: 12px !important
      }

      *[class="gmail-fix"] {
        display: none !important
      }

      .es-m-txt-c,
      .es-m-txt-c h1,
      .es-m-txt-c h2,
      .es-m-txt-c h3 {
        text-align: center !important
      }

      .es-m-txt-r,
      .es-m-txt-r h1,
      .es-m-txt-r h2,
      .es-m-txt-r h3 {
        text-align: right !important
      }

      .es-m-txt-l,
      .es-m-txt-l h1,
      .es-m-txt-l h2,
      .es-m-txt-l h3 {
        text-align: left !important
      }

      .es-m-txt-r img,
      .es-m-txt-c img,
      .es-m-txt-l img {
        display: inline !important
      }

      .es-button-border {
        display: block !important
      }

      .es-btn-fw {
        border-width: 10px 0px !important;
        text-align: center !important
      }

      .es-adaptive table,
      .es-btn-fw,
      .es-btn-fw-brdr,
      .es-left,
      .es-right {
        width: 100% !important
      }

      .es-content table,
      .es-header table,
      .es-footer table,
      .es-content,
      .es-footer,
      .es-header {
        width: 100% !important;
        max-width: 600px !important
      }

      .es-adapt-td {
        display: block !important;
        width: 100% !important
      }

      .adapt-img {
        width: 100% !important;
        height: auto !important
      }

      .es-m-p0 {
        padding: 0px !important
      }

      .es-m-p0r {
        padding-right: 0px !important
      }

      .es-m-p0l {
        padding-left: 0px !important
      }

      .es-m-p0t {
        padding-top: 0px !important
      }

      .es-m-p0b {
        padding-bottom: 0 !important
      }

      .es-m-p20b {
        padding-bottom: 20px !important
      }

      .es-mobile-hidden,
      .es-hidden {
        display: none !important
      }

      tr.es-desk-hidden,
      td.es-desk-hidden,
      table.es-desk-hidden {
        width: auto !important;
        overflow: visible !important;
        float: none !important;
        max-height: inherit !important;
        line-height: inherit !important
      }

      tr.es-desk-hidden {
        display: table-row !important
      }

      table.es-desk-hidden {
        display: table !important
      }

      td.es-desk-menu-hidden {
        display: table-cell !important
      }

      .es-menu td {
        width: 1% !important
      }

      table.es-table-not-adapt,
      .esd-block-html table {
        width: auto !important
      }

      table.es-social {
        display: inline-block !important
      }

      table.es-social td {
        display: inline-block !important
      }

      a.es-button,
      button.es-button {
        font-size: 20px !important;
        display: block !important;
        border-left-width: 0px !important;
        border-right-width: 0px !important
      }
    }

    .wrapper {
      width: 100%;
      font-family: Arial, sans-serif;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      padding: 0;
      Margin: 0
    }
  </style>
</head>

@php 
  $language = $details['language'] ?? '';
@endphp

<body class="wrapper">
  <div class="es-wrapper-color" style="background-color:#555555">
    <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#555555"></v:fill>
			</v:background>
		<![endif]-->
    <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0"
      style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
      <tr style="border-collapse:collapse">
        <td valign="top" style="padding:0;Margin:0">

          <table class="es-content" cellspacing="0" cellpadding="0" align="center"
            style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%">
            <tr style="border-collapse:collapse">
              <td align="center" style="padding:0;Margin:0">
                <table class="es-content-body" cellspacing="0" cellpadding="0" align="center"
                  style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#F8F8F8;width:600px">
                  <tr style="border-collapse:collapse">
                    <td
                      style="Margin:0;padding-left:10px;padding-right:10px;padding-top:20px;padding-bottom:20px;background-color:#e53644;background-image:url({{ url('/public/assets/img/bg.png')}});background-repeat:no-repeat;background-position:left top"
                      bgcolor="#191919" align="left"
                      background="{{ url('/public/assets/img/bg.png')}}">
                      <table width="100%" cellspacing="0" cellpadding="0"
                        style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                        <tr style="border-collapse:collapse">
                          <td valign="top" align="center" style="padding:0;Margin:0;width:580px">
                            <table width="100%" cellspacing="0" cellpadding="0" role="presentation"
                              style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                              <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;font-size:0px"><a target="_blank"
                                    href="https://bungkie.com"
                                    style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:14px;text-decoration:none;color:#3CA7F1"><img
                                      class="adapt-img"
                                      src="{{ url('/public/assets/img/bungkie.png') }}"
                                      alt width="265"
                                      style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr style="border-collapse:collapse">
                    <td style="padding:20px;Margin:0;background-color:#FFFFFF" bgcolor="#ffffff" align="left">
                      <table width="100%" cellspacing="0" cellpadding="0"
                        style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                        <tr style="border-collapse:collapse">
                          <td valign="top" align="center" style="padding:0;Margin:0;width:560px">
                            <table width="100%" cellspacing="0" cellpadding="0" role="presentation"
                              style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                              <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;padding-top:15px;padding-bottom:15px">
                                  <div>
                                    <h2 style="Margin:0;line-height:36px;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:30px;font-style:normal;font-weight:normal;color:#242424;text-align:center">
                                      <strong>@php echo ($language == "th") ? "ยืนยันคำสั่งซื้อของคุณสำเร็จแล้ว" : "Your order is confirmed." @endphp</strong>
                                    </h2>
                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:tahoma, verdana, segoe, sans-serif;line-height:21px;color:#333333">
                                      @if($language == "th") 
                                      ขอบคุณที่ใช้บริการ Bungkie !
                                      @else
                                      Thank You For Using Bungkie!
                                      @endif
                                    </p>
                                  </div>
                                </td>
                              </tr>
                              @if (isset($details['payment_code']) && $details['payment_code'] == "000")
                                <tr style="border-collapse:collapse">
                                  <td align="left" style="padding:0;Margin:0;padding-top:15px;padding-bottom:15px">
                                    <div>
                                      <h2
                                        style="Margin:0;line-height:14px;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size:12px;font-style:normal;font-weight:normal;color:#242424;text-align:left">
                                        <strong>@php echo ($language == "th") ? "สวัสดีคุณ " : "HI " @endphp {{ $details['name'] ?? '' }},</strong>
                                      </h2>
                                      <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:18px;color:#333333">
                                        &nbsp; &nbsp; &nbsp; &nbsp;@php echo ($language == "th") ? "หมายเลขคำสั่งซื้อของคุณ" : "Your Order" @endphp
                                        <strong>#{{ $details['order_id'] ?? '' }} </strong>
                                        @php echo ($language == "th") ? "เมื่อ" : "placed on" @endphp {{ $details['payment_date'] ?? '' }}
                                        @php echo ($language =="th") ? "ผ่านช่องทาง" : "Via" @endphp 
                                        <b>{{ $details['payment_type'] ?? '' }}</b> @php echo ($language == "th") ? "สำเร็จแล้ว!" : "has been Confirmed." @endphp
                                      </p>
                                      <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:18px;color:#333333">
                                        
                                        @if($language == "th")
                                        กรุณาตรวจสอบความถูกต้องของรายละเอียดการจัดส่ง เนื่องจากที่อยู่และเบอร์โทรศัพท์ไม่สามารถเปลี่ยนแปลงได้หลังคำสั่งซื้อได้รับการยืนยัน
                                        @else
                                        Kindly verify your delivery address and contact information below, as delivery details cannot be changed after an order is placed.
                                        @endif
                                      </p>
                                    </div>
                                  </td>
                                </tr>
                                <tr style="border-collapse:collapse">
                                  <td align="center" style="Margin:0;padding-left:10px;padding-right:10px;padding-top:15px;padding-bottom:15px">
                                    <span class="es-button-border" style="border-style:dashed;border-color:#2CB543;background:#FF2D3B;border-width:0px;display:inline-block;border-radius:19px;width:auto">
                                      <a href="{{ config('constants.front_url') }}/order-received?reference={{ $details['order_id'] ?? '' }}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'lucida sans unicode', 'lucida grande', sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#FF2D3B;border-width:10px 35px;display:inline-block;background:#FF2D3B;border-radius:19px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center">
                                        @php 
                                          echo ($language == "th") ? "ดูรายละเอียดเพิ่มเติม" : "See more details"
                                        @endphp
                                      </a>
                                    </span>
                                  </td>
                                </tr>
                              @else
                              <tr style="border-collapse:collapse">
                                  <td align="center" style="Margin:0;padding-left:10px;padding-right:10px;padding-top:15px;padding-bottom:15px">
                                    <h3 style="color: #ffc107!important;">
                                      @if (isset($details['payment_code']))
                                      {{ _2c2p_status($details['payment_code']) ?? '' }}
                                      @endif
                                    </h3>
                                  </td>
                                </tr>
                              @endif
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  @if (isset($details['payment_code']) && $details['payment_code'] == "000")
                    <tr style="border-collapse:collapse">
                      <td
                        style="Margin:0;padding-top:5px;padding-bottom:5px;padding-left:10px;padding-right:10px;background-color:#FF2D3B"
                        bgcolor="#FF2D3B" align="left">
                        <table width="100%" cellspacing="0" cellpadding="0"
                          style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                          <tr style="border-collapse:collapse">
                            <td valign="top" align="center" style="padding:0;Margin:0;width:580px">
                              <table width="100%" cellspacing="0" cellpadding="0" role="presentation"
                                style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                <tr style="border-collapse:collapse">
                                  <td align="center" style="padding:0;Margin:0">
                                    <h2 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:24px;font-style:normal;font-weight:normal;color:#FFFFFF">
                                      @php 
                                        echo ($language == "th") ? "รายละเอียดคำสั่งซื้อ และ รายละเอียดการจัดส่ง" : "Order Details & Delivery Details"
                                      @endphp
                                    </h2>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr style="border-collapse:collapse">
                      <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px">
                        <!--[if mso]><table style="width:560px" cellpadding="0" cellspacing="0"><tr><td style="width:270px" valign="top"><![endif]-->
                        <table cellpadding="0" cellspacing="0" class="es-left" align="left"
                          style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:left">
                          <tr style="border-collapse:collapse">
                            <td class="es-m-p20b" align="left" style="padding:0;Margin:0;width:270px">
                              <table cellpadding="0" cellspacing="0" width="100%" role="presentation"
                                style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                <tr style="border-collapse:collapse">
                                  <td align="left" style="padding:0;Margin:0">
                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:Arial, sans-serif;line-height:21px;color:#333333">
                                      <strong>@php echo ($language == "th") ? "รายละเอียดคำสั่งซื้อ" : "Order Details" @endphp</strong>
                                    </p>
                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:Arial, sans-serif;line-height:18px;color:#333333">
                                      <strong>@php echo ($language == "th") ? "หมายเลขคำสั่งซื้อ" : "Order No." @endphp</strong> : #{{ $details['order_id'] ?? '' }}<br>
                                      <strong>@php echo ($language == "th") ? "วิธีการจัดส่ง" : "Delivery Type" @endphp </strong> : STANDARD<br>
                                      <strong>@php echo ($language == "th") ? "ช่องทางการชำระเงิน" : "Payment Method" @endphp  </strong>: {{ $details['payment_type'] ?? '' }}<br>
                                      <strong>@php echo ($language == "th") ? "วันและเวลาสั่งซื้อ" : "Order date" @endphp </strong>: {{ $details['payment_date'] ?? '' }}
                                    </p>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <!--[if mso]></td><td style="width:20px"></td><td style="width:270px" valign="top"><![endif]-->
                        <table cellpadding="0" cellspacing="0" class="es-right" align="right" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;float:right">
                          <tr style="border-collapse:collapse">
                            <td align="left" style="padding:0;Margin:0;width:270px">
                              <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                <tr style="border-collapse:collapse">
                                  <td align="left" style="padding:0;Margin:0">
                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:Arial, sans-serif;line-height:21px;color:#333333">
                                      <strong>@php echo ($language == "th") ? "รายละเอียดการจัดส่ง" : "Delivery Details" @endphp</strong><br>
                                      <span style="font-size:12px">
                                        <strong>@php echo ($language == "th") ? "ชื่อลูกค้า" : "Customer Name" @endphp&nbsp;</strong>: {{ $details['shipping_name'] ?? '' }}<br>
                                        <strong>@php echo ($language == "th") ? "เบอร์โทรลูกค้า" : "Customer Phone" @endphp&nbsp;</strong>:{{ $details['shipping_tel'] ?? '' }}<br>
                                        <strong>@php echo ($language == "th") ? "อีเมลลูกค้า" : "Customer Email" @endphp&nbsp;</strong>: {{ $details['shipping_email'] ?? '' }}<br>
                                        <strong>@php echo ($language == "th") ? "ที่อยู่จัดส่ง" : "Address" @endphp&nbsp;</strong>: {{ $details['shipping_address'] ?? '' }}
                                      </span>
                                    </p>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <!--[if mso]></td></tr></table><![endif]-->
                      </td>
                    </tr>
                    <tr style="border-collapse:collapse">
                      <td align="left" style="padding:0;Margin:0;padding-bottom:5px;padding-left:20px;padding-right:20px">
                        <table cellpadding="0" cellspacing="0" width="100%"
                          style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                          <tr style="border-collapse:collapse">
                            <td align="center" valign="top" style="padding:0;Margin:0;width:560px">
                              <table cellpadding="0" cellspacing="0" width="100%" role="presentation"
                                style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                <tr style="border-collapse:collapse">
                                  <td align="left" style="padding:0;Margin:0">
                                    <p
                                      style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:Arial, sans-serif;line-height:21px;color:#333333">
                                      <br>
                                    </p>
                                    <h3 style="Margin:0;line-height:24px;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:20px;font-style:normal;font-weight:bold;color:#333333;text-align:center">
                                      @if($language == "th")
                                      เราพร้อมให้ความช่วยเหลือ
                                      @else
                                      We're here to help
                                      @endif
                                    </h3>
                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:Arial, sans-serif;line-height:21px;color:#333333;text-align:center">
                                      <strong>
                                        <span style="font-size:11px">
                                          @if($language == "th")
                                          ถ้าหากคุณมีปัญหาหรือมีคำถามใด ๆ คุณสามารถโทรติดต่อทีมสนับสนุนของ Bungkie ได้ตลอดเวลา&nbsp;<br>โทรติดต่อ 
                                          @else
                                          If you have any troubles or questions, you can always call Bungkie Support team&nbsp; <br>Call 
                                          @endif
                                          <a target="_blank" href="tel:021147936" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:11px;text-decoration:none;color:#3CA7F1">
                                            Bungkie Customer Service
                                          </a>
                                        </span>
                                      </strong>&nbsp;
                                      <span style="font-size:11px">(09.00am-6.00pm)</span>
                                    </p>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  @endif
                </table>
              </td>
            </tr>
          </table>
          <table cellpadding="0" cellspacing="0" class="es-footer" align="center"
            style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
            <tr style="border-collapse:collapse">
              <td align="center" style="padding:0;Margin:0">
                <table class="es-footer-body" cellspacing="0" cellpadding="0" align="center"
                  style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;width:600px">
                  <tr style="border-collapse:collapse">
                    <td align="left"
                      background="{{ url('public/assets/img/bg.png') }}"
                      style="padding:20px;Margin:0;background-image:url({{ url('public/assets/img/bg.png') }});background-repeat:no-repeat;background-position:left top">
                      <table width="100%" cellspacing="0" cellpadding="0"
                        style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                        <tr style="border-collapse:collapse">
                          <td valign="top" align="center" style="padding:0;Margin:0;width:560px">
                            <table width="100%" cellspacing="0" cellpadding="0" role="presentation"
                              style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                              <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;font-size:0px"><a target="_blank"
                                    href="https://bungkie.com/"
                                    style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#AAAAAA"><img
                                      class="adapt-img"
                                      src="{{ url('/public/assets/img/bungkie2.png') }}"
                                      alt
                                      style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"
                                      width="100"></a></td>
                              </tr>
                              <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;font-size:0px">
                                  <table class="es-table-not-adapt es-social" cellspacing="0" cellpadding="0"
                                    role="presentation"
                                    style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                    <tr style="border-collapse:collapse">
                                      <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px"><a
                                          href="https://www.facebook.com/bungkieofficial/"
                                          style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#AAAAAA"><img
                                            title="Facebook"
                                            src="{{ url('public/assets/img/facebook-logo-white.png') }}"
                                            alt="Fb" width="32" height="32"
                                            style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a>
                                      </td>
                                      <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px"><a
                                          href="https://www.youtube.com/channel/UCCv8pvQDWx-rkBUYh9FzJqg?view_as=subscriber"
                                          style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#AAAAAA"><img
                                            title="Youtube"
                                            src="{{ url('public/assets/img/youtube-logo-white.png') }}"
                                            alt="Yt" width="32" height="32"
                                            style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a>
                                      </td>
                                      <td valign="top" align="center" style="padding:0;Margin:0;padding-right:15px"><a
                                          href="https://www.instagram.com/bungkieofficial/" target="_blank"
                                          style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#AAAAAA"><img
                                            title="Instagram"
                                            src="{{ url('/public/assets/img/instagram-logo-white.png') }}"
                                            alt="Ig" width="32" height="32"
                                            style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a>
                                      </td>
                                      <td valign="top" align="center" style="padding:0;Margin:0"><a
                                          href="https://www.linkedin.com/company/bungkie/" target="_blank"
                                          style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:Arial, sans-serif;font-size:13px;text-decoration:none;color:#AAAAAA"><img
                                            title="Linkedin"
                                            src="{{ url('public/assets/img/linkedin-logo-white.png') }}"
                                            alt="In" width="32" height="32"
                                            style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic"></a>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;padding-top:10px">
                                  <p
                                    style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:Arial, sans-serif;line-height:20px;color:#FFFFFF">
                                    <strong>Central Siam Public (Thailand) Co., Ltd.</strong>
                                  </p>
                                  <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:13px;font-family:Arial, sans-serif;line-height:20px;color:#FFFFFF">
                                    @if($language == "th")
                                      Italthai Tower, ชั้น 31, 2034 ถนนเพชรบุรีตัดใหม่ แขวงบางกะปิ เขตห้วยขวาง กรุงเทพฯ 10310
                                    @else
                                      Italthai Tower, Floor 31, 2034 Petchaburi Rd.,Bangkapi, Huai Khwang, Bangkok 10310
                                    @endif
                                    <br>Tel: 021147936<br>

                                  </p>
                                </td>
                              </tr>
                              <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px">
                                  <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:Arial, sans-serif;line-height:18px;color:#FFFFFF">
                                    @if($language == "th")
                                    <em>อีเมลฉบับนี้ส่งจากระบบอัตโนมัติ กรุณาอย่าตอบกลับ</em>
                                    @else
                                    <em>This is an automatically generated email, please do not reply</em>
                                    @endif
                                  </p>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</body>
</html>