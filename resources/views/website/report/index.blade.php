@extends('layouts.base')
@section('content')
<section id="report">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <div class="title">
                <i class="cil-storage"></i> รายงาน
              </div>
            </div>
            <div class="card-body">
              <div class="tab-content" id="product-tabContent">
                <div class="card-toolbar mb-2">
                  <div class="search-toolbar">
                    <div class="input-group">
                      <input type="text" class="form-control" name="search-text" placeholder="ค้นหาโดยหมายเลขคำสั่งซื้อ">
                    </div>
                    <div class="bottom"> 
                      <div class="dropdown payment-status">
                        <span>Payment Status:</span>
                        <a class="dropdown-toggle" data-payment="000" href="#" id="filter-by" data-toggle="dropdown">
                          Payment Successful
                        </a>
                        <div class="dropdown-menu filter-payment" aria-labelledby="filter-by">
                          <a class="dropdown-item" data-value="000" href="#">Payment Successful</a>
                          <a class="dropdown-item" data-value="001" href="#">Payment Pending</a>
                          <a class="dropdown-item" data-value="002" href="#">Payment Rejected</a>
                          <a class="dropdown-item" data-value="003" href="#">Payment was canceled by user</a>
                          <a class="dropdown-item" data-value="999" href="#">Payment Failed</a>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="right">
                    <div class="date-toolbar">
                      <label for="date">ช่วงเวลา</label> 
                      <div class="daterange-show">
                        <i class="cil-calendar"></i>
                        <input class="form-control date" type="text" name="start" autocomplete="off">
                        <span></span>
                        <input class="form-control date" type="text" name="end" autocomplete="off">
                      </div>
                    </div>
                    <div class="button">
                      <button class="btn btn-info" data-button="submit" type="button">
                        <i class="cil-search"></i>
                      </button>
                      <button class="ml-1 btn btn-default border" data-button="reset" type="button">
                        <i class="cil-reload"></i>
                      </button>
                      <button class="btn btn-success" data-button="export" type="button">
                        <svg style="width:20px; heidht: 20px;" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4" />
                        </svg>
                      </button>
                    </div>
                  </div>
                </div>

                <div class="table-responsive">
                  <table class="table table-striped table-bordered" id="table">
                    <thead>
                      <tr>
                        <th>Date Created</th>
                        <th>Date Received</th>
                        <th>Order Number</th>
                        <th>Transaction Number</th>
                        <th>Quantity</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Payment Status</th>
                        <th>Amount</th>
                        <th>Shipping Price</th>
                        <th>Shipping Is Paid</th>
                        <th>Balance</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('css')
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/pages/report/css/index.css') }}">
@endsection

@section('javascript')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
  <script type="text/javascript" src="{{ asset('public/assets/pages/report/js/report.js') }}"></script>

@endsection
