@extends('layouts.base')

@section('content')
<section id="orderDetail">
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="card">
                <div class="card-header">
                    <div class="title">
                        <i class="cil-storage"></i> จัดการสินค้า
                    </div>
                    <div class="back">
                        <a href="{{ route('order.index') }}" class="btn btn-labeled btn-sm btn-default border">
                            <span class="btn-label"><i class="cil-media-skip-backward"></i></span>
                            กลับ
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div class="reference-detail">
                        <div class="reference">
                            <h4>{{ Request::segment(3) }}</h4>
                        </div>
                        @foreach($orders as $order)
                        <div class="group-shop" data-shopid="1">
                            <div class="nav-bar">
                                <h5>{{ $order['shop_name'] }}</h5>
                                @if($order['pno'] === null)
                                    <div class="action">
                                        <button class="btn btn-labeled btn-sm btn-danger" type="button" data-type="cancel" data-params="{{ json_encode(['reference' => Request::segment(3), 'shopId' => $order['shop_id']]) }}">
                                            <span class="btn-label"><i class="cil-x"></i></span>
                                            ยกเลิกออเดอร์
                                        </button>
                                        <button class="btn btn-labeled btn-sm btn-success" type="button" data-type="order" data-params="{{ json_encode(['reference' => Request::segment(3), 'shopId' => $order['shop_id']]) }}">
                                            <span class="btn-label"><i class="cil-gift"></i></span>
                                            สร้างออเดอร์
                                        </button>
                                    </div>
                                @else
                                    @if(isset($order['download']))
                                    <div class="action">
                                        @foreach($order['download'] as $dl) 
                                        <a href="{{ $dl['file'] }}" target="_blank" class="btn btn-labeled btn-sm btn-info">
                                            <span class="btn-label"><i class="cil-file"></i></span>
                                            {{ ($dl['size'] === "m") ? "ดาวน์โหลด(ขนาดเล็ก)" : "ดาวน์โหลด(ขนาดใหญ่) " }}
                                        </a>
                                        @endforeach
                                    </div>
                                    @endif
                                @endif
                            </div>

                            <div class="table">
                                <div class="header">
                                    <div class="detail">รายละเอียดสินค้า</div>
                                </div>
                                @foreach($order['items'] as $item)
                                <div class="body">
                                    <div class="detail">
                                        <div class="thumbnail">
                                            <img src='{{ $item['image'] }}' alt=''/>
                                        </div>
                                        <div class="product-detail">
                                            <h3>{{ $item['name'] }}</h3>
                                            <span>× {{ $item['qty'] }}</span>
                                            <div class="footer">
                                                <span class="address">{{ $item['address'] }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/pages/order/css/detail.css') }}">
@endsection

@section('javascript')
    <script src="{{ asset('public/vendors/jquery.loading.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/assets/pages/order/js/detail.js') }}"></script>
@endsection