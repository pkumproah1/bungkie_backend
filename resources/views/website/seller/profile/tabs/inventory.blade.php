<div class="card">
    <div class="card-header">
        <h3><i class="cil-comment-square"></i> Inventory</h3>
    </div>
    <div class="card-body">
        <div class="d-flex justify-content-start mb-3">
            <div class="ml-auto">
                <a href="{{ route('product.list') }}">View All Product</a>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="table">
                <thead>
                    <th>Detail</th>
                    <th width="120">In Stock</th>
                    <th width="40">Action</th>
                </thead>
                <tbody>
                    @foreach([1,2,3] as $k)
                    <tr>
                        <td>
                            <div class="item d-flex justify-content-start">
                                <div class="thumbnail">
                                    <img src='https://placehold.it/120x120?text=Thumbnail' alt=''/>
                                </div>
                                <div class="detail">
                                    <div class="product">
                                        <span>Product name</span>
                                    </div>
                                    <div class="category">
                                        <ul class="nav">
                                            <li>Fasion</li>
                                            <li>Shirt</li>
                                            <li>Men</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>20</td>
                        <td>
                            <a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#manageModal">Manage</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="manageModal" tabindex="-1" role="dialog" aria-labelledby="manageModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="manageModalLabel">Product Name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="input-group align-items-center">
                    <label for="stock" class="mr-3">In Stock</label>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="addon">+</span>
                    </div>
                    <input type="text" class="form-control" id="stock" name="stock" aria-label="Text input with dropdown button">
                    <input type="hidden" name="action" value="+">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item modal-action" data-value="+" href="#" >+</a>
                            <a class="dropdown-item modal-action" data-value="-" href="#">-</a>
                        </div>
                    </div>
                    <div class="ml-3">
                        <button type="button" class="btn btn-labeled btn-info">
                            <span class="btn-label"><i class="cil-plus"></i></span>
                            Updated
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>