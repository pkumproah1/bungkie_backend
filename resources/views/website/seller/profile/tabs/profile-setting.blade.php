<div class="card profile-settings">
    <div class="card-header">
        <h3><i class="cil-settings"></i> Profile setting</h3>
    </div>
    <div class="card-body">
        @php 
            $tabs = array(
                ['id' => "profile", 'name' => "Profile Setting"],
                ['id' => "bank", 'name' => "Bank Setting"],
                ['id' => "shop", 'name' => "Shop Setting"],
                ['id' => "password", 'name' => "Password"]
            );
        @endphp
        <div class="row">
            <div class="col-xl-2">
                <ul class="nav flex-column" id="setting-tab" role="tablist">
                    @foreach($tabs as $key => $tab)
                    <li class="nav-item">
                        <a class="nav-link {{ ($key == 2) ? 'active' : '' }}{{ $tab['attr'] ?? '' }}" id="{{ $tab['id'] }}-tab" data-toggle="tab" href="#{{ $tab['id'] }}" role="tab" aria-controls="{{ $tab['id'] }}" aria-selected="{{ ($tab['id'] == $tabs[0]) ? 'true' : 'false' }}">
                            {{ $tab['name'] }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-xl-8">
                <div class="tab-content" id="setting-Content">
                    @foreach($tabs as $key => $tab)
                    <div class="tab-pane fade {{ ($key == 2) ? 'show active' : '' }}" id="{{ $tab['id'] }}" role="tabpanel" aria-labelledby="{{ $tab['id'] }}-tab">
                        <div class="row">
                            <div class="col-md-8">
                                @include('website.seller.profile.setting.' . $tab['id'])
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>