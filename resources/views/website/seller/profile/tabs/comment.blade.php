<div class="card">
    <div class="card-header">
        <h3><i class="cil-comment-square"></i> Comment</h3>
    </div>
    <div class="card-body">
        <ul class="nav" id="commentTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="unread-tab" data-toggle="tab" href="#unread" role="tab" aria-controls="unread" aria-selected="true">Unread</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="read-tab" data-toggle="tab" href="#read" role="tab" aria-controls="read" aria-selected="false">Read</a>
            </li>
        </ul>
        <div class="tab-content" id="commentTab">
            <div class="tab-pane fade show active" id="unread" role="tabpanel" aria-labelledby="unread-tab">
                @includeChild('comment.unread')
            </div>
            <div class="tab-pane fade" id="read" role="tabpanel" aria-labelledby="read-tab">
                @includeChild('comment.unread')
            </div>
        </div>
    </div>
</div>
