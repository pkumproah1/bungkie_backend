
<div class="table-responsive">
    <table class="table table-bordered table-striped" id="table">
        <thead>
            <th>Detail</th>
            <th width="120">In Stock</th>
            <th width="40">Action</th>
        </thead>
        <tbody>
            @foreach([1,2,3] as $k)
            <tr>
                <td>
                    <div class="item d-flex justify-content-start">
                        <div class="thumbnail">
                            <img src='https://placehold.it/120x120?text=Thumbnail' alt=''/>
                        </div>
                        <div class="detail">
                            <div class="product">
                                <span>Product name</span>
                            </div>
                            <div class="category">
                                <ul class="nav">
                                    <li>Fasion</li>
                                    <li>Shirt</li>
                                    <li>Men</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </td>
                <td>20</td>
                <td>
                    <a href="#" data-toggle="modal" data-target="#manageModal">Manage</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>