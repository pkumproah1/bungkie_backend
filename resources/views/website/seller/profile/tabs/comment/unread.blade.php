
<div class="table-responsive">
    <table class="table table-bordered table-striped" id="table">
        <thead>
            <th>Detail</th>
            <th width="150">Created</th>
            <th width="40">Action</th>
        </thead>
        <tbody>
            @foreach([1,2,3] as $k)
            <tr>
                <td>
                    <div class="item d-flex justify-content-start">
                        <div class="thumbnail">
                            <img src='https://placehold.it/120x120?text=Thumbnail' alt=''/>
                        </div>
                        <div class="detail">
                            <div class="product">
                                <span>Product name</span>
                            </div>
                            <div class="comment">
                                <p><span class="text-primary">customer name:</span> Lorem ipsum dolor, sit amet consectetur adipisicing elit. Eligendi hic deleniti quidem ad laboriosam molestiae, eveniet aliquam facilis voluptatem eos at qui tenetur repellendus saepe quas inventore. Hic, aspernatur sit!</p>
                            </div>
                        </div>
                    </div>
                </td>
                <td>19 minute ago.</td>
                <td>
                    <a href="#" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#commentModal">Reply</a>
                    <a href="#" class="text-danger">Delete</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>


<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="commentModalLabel">Reply</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="topic border-bottom mb-3">
                    <p><span class="text-primary">customer name: </span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem facilis doloremque accusantium. Quo voluptas quas, at, error molestias itaque mollitia enim magni inventore nemo accusamus tenetur eum soluta atque deserunt?</p>
                </div>
                <div class="form-group">
                    <label for="reply"><strong>Reply: </strong>Product name</label>
                    <textarea name="reply" class="form-control" id="reply" rows="5"></textarea>
                </div>

                <div class="form-group d-flex justify-content-end">
                    <button type="button" class="btn btn-labeled btn-info">
                        <span class="btn-label"><i class="cil-plus"></i></span>
                        Send
                    </button>
                </div>

                
            </div>
        </div>
    </div>
</div>