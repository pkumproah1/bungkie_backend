@extends('layouts.base')
@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-xl-12">
                <!-- seller profile -->
                <section class="seller-profile">
                    <!-- Cover -->
                    <div class="cover">
                        <div class="background-profile" style="background-image: url(https://image.freepik.com/free-vector/merry-christmas-happy-new-year-banner_1017-22551.jpg);">
                            <a href="#">
                                <i class="cil-camera"></i>
                                <input type="file" class="file_upload" />
                            </a>
                        </div>
                    </div>
                    <!-- end cover -->
                    <!-- profile section -->
                    <div class="shop-profile">
                        <!-- avatar -->
                        <div class="avatar">
                            @php
                                $avatar = (Auth()->user()->avatar) ? asset(Auth()->user()->avatar) : 'https://placehold.it/300x300';
                            @endphp
                            <div class="avatar-image" style="background-image: url({{ $avatar }});">
                                <div class="upload">
                                    <i class="cil-camera"></i><span>Edit avatar</span>
                                    <input type="file" class="file_upload" />
                                </div>
                            </div>
                        </div>
                        <!-- end avatar -->
                        <!-- group menu -->
                        <div class="group-menu">
                            <!-- shop name -->
                            <div class="top">
                                <h3>Name Shop</h3>
                                <a href="#">
                                    <i class="cil-camera"></i> Edit Cover
                                    <input type="file" class="file_upload" />
                                </a>
                            </div>
                            <!-- end shop name -->
                            <!-- tab menu -->
                            <div class="bottom-tab">
                                <ul class="nav" id="myMenuProfile" role="tablist">
                                    <li>
                                        <a class="active" id="overview-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">
                                            <i class="cil-comment-square"></i>
                                            Overview
                                        </a>
                                    </li>
                                    <li>
                                        <a id="inventory-tab" data-toggle="tab" href="#inventory" role="tab" aria-controls="inventory" aria-selected="false">
                                            <i class="cil-comment-square"></i>
                                            Inventory
                                            <span class="badge badge-pill badge-warning">4</span>
                                        </a>
                                    </li>
                                    <li>
                                        
                                        <a id="comment-tab" data-toggle="tab" href="#comment" role="tab" aria-controls="comment" aria-selected="false">
                                            <i class="cil-comment-square"></i>
                                            Comment
                                            <span class="badge badge-pill badge-warning">10+</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a id="profile-setting-tab" data-toggle="tab" href="#profile-setting" role="tab" aria-controls="profile-setting" aria-selected="false">
                                            <i class="cil-settings"></i>
                                            Profile Setting
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- end tab menu -->
                        </div>
                        <!-- end group menu -->
                    </div>
                    <!-- end profile section -->
                </section>
                <!-- end seller profile -->
            </div>
        </div>

        <!-- section tab content -->
        <section id="tab-content-seller">
            <div class="row">
                <div class="col-xl-12">
                    <!-- tab content -->
                    <div class="tab-content" id="myMenuProfile-Content">
                        @php
                            $tabs = ['overview', 'inventory', 'comment', 'profile-setting'];
                        @endphp
                        @foreach($tabs as $tab)
                        <!-- tab -->
                        <div class="tab-pane fade {{ $tab == $tabs[3] ? 'show active' : '' }}" id="{{ $tab }}" role="tabpanel" aria-labelledby="nav-{{ $tab }}-tab">
                            <!-- load view by tab -->
                            @includeChild('tabs.' . $tab)
                        </div>
                        <!-- end tab -->
                        @endforeach
                    </div>
                    <!-- end tab content -->
                </div>
            </div>
        </section>
        <!-- end section tab content -->
    </div>
</div>
@endsection

@section('css') 
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.11/cropper.css">
    <link href="{{ asset('public/vendors/chosen/chosen.css') }}" rel="stylesheet">
    <link href="{{ asset('public/vendors/jquery-file-upload/css/jquery.fileupload.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="{{ asset('public/assets/pages/profile/css/main.css') }}" rel="stylesheet">
@endsection
@section('javascript')



<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.11/cropper.min.js"></script>
<script type="text/javascript" src="{{ asset('public/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('public/vendors/chosen/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="{{ asset('public/vendors/jquery.loading.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="{{ asset('public/vendors/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('public/vendors/jquery-file-upload/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('public/vendors/jquery-file-upload/js/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('public/vendors/nested-sortable/jquery-ui.min.js') }}"></script>
<script src="{{ asset('public/vendors/nested-sortable/nestedSortable.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<!-- Custom Script -->
<script src="{{ asset('public/assets/pages/profile/js/init.js') }}"></script>
<script src="{{ asset('public/assets/pages/profile/js/shop.js') }}"></script>
<script src="{{ asset('public/assets/pages/profile/js/inventory.js') }}"></script>
<script src="{{ asset('public/assets/pages/profile/js/profile.js') }}"></script>
<script src="{{ asset('public/assets/pages/profile/js/bank.js') }}"></script>
<script src="{{ asset('public/assets/pages/profile/js/password.js') }}"></script>




@endsection
