<form class="form-action password-form">
    <h6 class="border-bottom pb-2 mb-3">Change Password</h6>
    @csrf
    <div class="form-group">
        <label for="inputOldPassword">Old Password<span class="text-danger"> *</span></label>
        <input type="password" class="form-control" name="old_password" id="inputOldPassword"required>
    </div>

    <div class="form-group">
        <label for="inputPassword">Password <span class="text-danger"> *</span></label>
        <input type="password" class="form-control password" name="password" id="inputPassword" required>
        <small id="passwordHelp" class="form-text text-muted">Minimum 6 characters</small>
    </div>

    <div class="form-group">
        <label for="inputCPassword">Confirm Password <span class="text-danger"> *</span></label>
        <input type="password" class="form-control" name="cpassword" id="inputCPassword" required>
        <small id="passwordHelp" class="form-text text-muted">Minimum 6 characters</small>
    </div>

    <div class="form-group d-flex justify-content-end border-top pt-2">
        <button type="submit" class="btn btn-labeled btn-info">
            <span class="btn-label"><i class="cil-plus"></i></span>
            Change
        </button>
    </div>
</form>