<form class="form-action shop-form">
    <h6 class="border-bottom pb-2 mb-3">Shop Settings</h6>
    @csrf
    <div class="form-group">
        <div class="avatar">
            <label  class="mb-1">Logo <span class="text-danger"> *</span></label>
            <div class="img">
                @php
                    $avatar = (Auth()->user()->avatar) ? asset('public/statics/images/shop/' . clean($me['seller']->shop_name) . '/avatar/' . Auth()->user()->avatar) : 'https://placehold.it/300x300';
                @endphp
                <img class="img-fluid" id="showAvatar" src="{{ $avatar }}">
            </div>
            <div class="button-upload fileinput-button">
                <button type="button" class="btn btn-sm btn-labeled btn-primary">
                    <span class="btn-label" style="left: -13px;"><i class="cil-image-plus"></i></span>
                    change logo
                </button>
                <input type="file" name="file" id="avatar" accept="image/*">
            </div>
        </div>
    </div>

    <div class="form-group url">
        <label for="inputUrl" class="mb-1">URL <span class="text-danger"> *</span></label>
        <input class="form-control url-only {{ $me['seller']->url ? 'is-valid' : '' }}" id="inputUrl" type="text" name="url" value="{{ $me['seller']->url ?? '' }}">
        <small id="urlHelp" class="form-text text-muted">
            Your Bungkie URL: https://bungkie.com/shopping/<span id="appendUrl">{{ $me['seller']->url ?? '' }}</span>
        </small>
    </div>

    <div class="form-group">
        <label for="inputDesc" class="mb-1">Shop Description<span class="text-danger"> *</span></label>
        <textarea name="shop_description" id="inputDesc" class="form-control" rows="4" required>{{ $me['seller']->shop_description ?? '' }}</textarea>
        <small id="emailHelp" class="form-text text-muted">Shop Description for SEO</small>
    </div>

    <div class="form-group">
        <label for="inputKeyword" class="mb-1"><help>Keyword for SEO by Shop</help></label>
        <input class="form-control tags" id="inputKeyword" type="text" name="keyword" value="{{ $me['seller']->shop_keyword ?? '' }}">
        <small id="urlHelp" class="form-text text-muted">
            Keyword Example: <span class="text-info">Bungkie Shop, bungkie, Iphone, Ipad, Macbook</span>
            <span class="text-warning d-block">(Maximun 5 keyword)</span>
        </small>
    </div>

    <h6 class="border-bottom pb-2 mb-3">Sliders</h6>
    <div class="images">
        <div class="header d-flex align-items-center border-bottom mb-2">
            <div class="details">
                <span class="d-block">Current Image: <span class="text-info">5</span></span>
                <span class="help-block"><span class="text-danger">Image Size (1280 x 300) Only</span> (Maximun 5 Image)</span>
            </div>
            <div class="ml-auto">
                <div class="button-upload fileinput-button">
                    <button type="button" class="btn btn-sm btn-labeled btn-primary">
                        <span class="btn-label" style="left: -13px;"><i class="cil-image-plus"></i></span>
                        Image Upload
                    </button>
                    <input type="file" name="file" id="slider" multiple accept="image/*">
                </div>
            </div>
        </div>
        <div class="body border-top">
            @php 
                $slider = '';
                if (isset($me['sliders'])) {
                    $slider = json_encode($me['sliders']);
                }

            @endphp
            <ol class="nav flex-column sortable" id="appendSlider" data-json="{{ $slider }}">
                @foreach([1,2,3,4,5] as $l)
                
                @endforeach
            </ol>
        </div>
    </div>

    <div class="form-group d-flex justify-content-end border-top pt-2">
        <button type="submit" class="btn btn-labeled btn-info">
            <span class="btn-label"><i class="cil-plus"></i></span>
            Save Setting
        </button>
    </div>
</form>