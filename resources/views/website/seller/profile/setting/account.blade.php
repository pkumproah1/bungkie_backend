<form class="form-action account-form">
    <h6 class="border-bottom pb-2 mb-3">Basic Information</h6>
    <div class="form-group">
        <label for="inputCompany">Company Name<span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="company_name" id="inputCompany" value="Company Name" required>
    </div>

    <div class="form-group">
        <label for="inputBrand">Brand Name <span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="brand_name" id="inputBrand" value="Brand Name" required>
    </div>

    <div class="form-group">
        <label for="inputPerson">Person In Charge Name <span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="person" id="inputPerson" value="Person In Charge Name" required>
    </div>

    <div class="form-group">
        <label for="inputBasicTel">Telephone Number<span class="text-danger"> *</span></label>
        <input type="text" class="form-control" maxlength="10" name="telephone" id="inputBasicTel" value="Telephone Number" required>
    </div>

    <div class="form-group">
        <label for="inputEmail">Email <span class="text-danger"> *</span></label>
        <input type="email" class="form-control" name="email" id="inputEmail" value="Email" required>
    </div>

    <h6 class="border-bottom pb-2 my-3">Address</h6>
    <div class="form-group">
        <label for="inputShippingAddress">Shipping Address <span class="text-danger"> *</span></label>
        <textarea name="shipping_address" id="inputShippingAddress" class="form-control" rows="4">Address</textarea>
    </div>

    <div class="form-group">
        <label for="inputWarehouseName">PIC Warehouse Name <span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="warehouse_name" id="inputWarehouseName" value="" required>
    </div>

    <div class="form-group">
        <label for="inputWarehouseTel">PIC Warehouse Telephone Number <span class="text-danger"> *</span></label>
        <input type="text" class="form-control numberic" maxlength="10" name="warehouse_tel" id="inputWarehouseTel" value="" required>
    </div>

    <div class="form-group">
        <label for="inputBillingAddress">Billing Address <span class="text-danger"> *</span></label>
        <textarea name="billing_address" id="inputBillingAddress" class="form-control" rows="4">Address</textarea>
    </div>

    <div class="form-group d-flex justify-content-end border-top pt-2">
        <button type="button" class="btn btn-labeled btn-info">
            <span class="btn-label"><i class="cil-plus"></i></span>
            Updated
        </button>
    </div>
</form>