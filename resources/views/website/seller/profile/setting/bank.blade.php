<form class="form-action bank-form">
    <h6 class="border-bottom pb-2 mb-3">Bank Setting</h6>
    @csrf
    <div class="form-group">
        <label>Bank <span class="text-danger">*</span></label>
        <select class="form-control chosen-select" name="bank" id="banklist" required data-select="1">
            @if (isset($bank_lists))
                @foreach($bank_lists as $b)
                    <option value="{{ $b->id }}" >{{ $b->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
    

    <div class="form-group">
        <label for="inputAccountNumber">Account Number<span class="text-danger"> *</span></label>
        <input type="text" class="form-control numeric" name="account_number" id="inputAccountNumber" value="{{ $me['seller']->account_number ?? '' }}" required>
    </div>

    <div class="form-group">
        <label for="inputAccountName">Account Name <span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="account_name" id="inputAccountName" value="{{ $me['seller']->account_name ?? '' }}" required>
    </div>

    <div class="form-group d-flex justify-content-end border-top pt-2">
        <button type="submit" class="btn btn-labeled btn-info">
            <span class="btn-label"><i class="cil-plus"></i></span>
            Updated
        </button>
    </div>
</form>