<form class="form-action profile-form">
    <h6 class="border-bottom pb-2 mb-3">Basic Information</h6>
    @csrf
    <div class="form-group">
        <label for="inputCompany">Company Name<span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="company_name" id="inputCompany" value="{{ $me['seller']->company_name ?? '' }}" required>
    </div>

    <div class="form-group">
        <label for="inputTax">Tax ID<span class="text-danger"> *</span></label>
        <input type="text" class="form-control numberic" maxlength="13" name="tax_id" id="inputTax" value="{{ $me['seller']->tax_id ?? '' }}">
    </div>

    <div class="form-group">
        <label for="inputShopName">Shop Name <span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="shop_name" id="inputShopName" value="{{ $me['seller']->shop_name ?? '' }}" required>
    </div>

    <div class="form-group">
        <label for="inputBrand">Brand Name <span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="brand_name" id="inputBrand" value="{{ $me['seller']->brand_name ?? '' }}" required>
    </div>

    <div class="form-group">
        <label for="inputPerson">Person In Charge Name <span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="person" id="inputPerson" value="{{ $me['seller']->person_name ?? '' }}" required>
    </div>

    <div class="form-group">
        <label for="inputBasicTel">Telephone Number<span class="text-danger"> *</span></label>
        <input type="text" class="form-control" maxlength="10" name="basic_telephone" id="inputBasicTel" value="{{ $me['seller']->basic_telephone ?? '' }}" required>
    </div>

    <div class="form-group">
        <label for="inputEmail">Email <span class="text-danger"> *</span></label>
        <input type="email" class="form-control" name="email" id="inputEmail" value="{{ $me['seller']->email ?? '' }}" required>
    </div>

    <h6 class="border-bottom pb-2 my-3">Address</h6>
    <div class="form-group">
        <label for="inputShippingAddress">Shipping Address <span class="text-danger"> *</span></label>
        <textarea name="shipping_address" id="inputShippingAddress" class="form-control" rows="4">{{ $me['seller']->shipping_address ?? '' }}</textarea>
    </div>

    <div class="form-group">
        <label for="inputWarehouseName">PIC Warehouse Name <span class="text-danger"> *</span></label>
        <input type="text" class="form-control" name="warehouse_name" id="inputWarehouseName" value="{{ $me['seller']->warehouse_name ?? '' }}" required>
    </div>

    <div class="form-group">
        <label for="inputWarehouseTel">PIC Warehouse Telephone Number <span class="text-danger"> *</span></label>
        <input type="text" class="form-control numberic" maxlength="10" name="warehouse_tel" id="inputWarehouseTel" value="{{ $me['seller']->warehouse_telephone ?? '' }}" required>
    </div>

    <div class="form-group">
        <label for="inputBillingAddress">Billing Address <span class="text-danger"> *</span></label>
        <textarea name="billing_address" id="inputBillingAddress" class="form-control" rows="4">{{ $me['seller']->billing_address ?? '' }}</textarea>
    </div>

    <div class="form-group d-flex justify-content-end border-top pt-2">
        <button type="submit" class="btn btn-labeled btn-info">
            <span class="btn-label"><i class="cil-plus"></i></span>
            Updated
        </button>
    </div>
</form>