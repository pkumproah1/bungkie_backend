<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="Bungkie Admin">
    <meta name="author" content="Prapan Kumrpaoh">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Bungkie Admin</title>
    <link rel="apple-touch-icon" sizes="57x57" href="public/assets/favicon/favicon.png">
    <link rel="apple-touch-icon" sizes="60x60" href="public/assets/favicon/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="public/assets/favicon/favicon.png">
    <link rel="apple-touch-icon" sizes="76x76" href="public/assets/favicon/favicon.png">
    <link rel="apple-touch-icon" sizes="114x114" href="public/assets/favicon/favicon.png">
    <link rel="apple-touch-icon" sizes="120x120" href="public/assets/favicon/favicon.png">
    <link rel="apple-touch-icon" sizes="144x144" href="public/assets/favicon/favicon.png">
    <link rel="apple-touch-icon" sizes="152x152" href="public/assets/favicon/favicon.png">
    <link rel="apple-touch-icon" sizes="180x180" href="public/assets/favicon/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="public/assets/favicon/favicon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="public/assets/favicon/favicon.png">
    <link rel="icon" type="image/png" sizes="96x96" href="public/assets/favicon/favicon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="public/assets/favicon/favicon.png">
    <link rel="manifest" href="resources/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="public/assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Icons-->
    <link href="{{ asset('public/css/free.min.css') }}" rel="stylesheet"> <!-- icons -->
    <link href="{{ asset('public/css/flag-icon.min.css') }}" rel="stylesheet"> <!-- icons -->
    <!-- Main styles for this application-->
    <link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/coreui-chartjs.css') }}" rel="stylesheet">

  </head>
  <body class="c-app flex-row align-items-center">

    @yield('content') 
    <script src="{{ asset('public/js/coreui.bundle.min.js') }}"></script>
    @yield('javascript')

  </body>
</html>
