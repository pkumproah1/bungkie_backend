@extends('layouts.base')
@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row b-wrapper">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        @if ($product)
                            <h4>Edit Product</h4>
                        @else
                            <h4>Add Product</h4>
                        @endif
                    </div>
                    <div class="card-body">
                        <form method="POST" class="form-action">
                            @csrf
                            <!-- Tab Header -->
                            <div class="add-item">
                                <!-- Category section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Category" role="button" aria-expanded="true" aria-controls="collapseCategory">
                                        <h5>Category</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Category">
                                        <!-- Select Category -->
                                        <div class="form-group row category">
                                            <div class="col-lg-4">
                                                <div class="category-content">
                                                    <label>Main Category</label>
                                                    <div class="category-list">
                                                        <ul id="main" data-categorylist="{{ isset($product['categorys']) ? json_encode($product['categorys']) : json_encode([1]) }}"></ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div class="category-content">
                                                    <label>Level 2</label>
                                                    <div class="category-list">
                                                        <ul id="lv2"></ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div class="category-content">
                                                    <label>Level 3</label>
                                                    <div class="category-list">
                                                        <ul id="lv3"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end category -->

                                <!-- General section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#General" role="button" aria-expanded="true" aria-controls="collapseGeneral">
                                        <h5>General</h5>
                                    </div>
                                    <div class="b-body collapse show" id="General">
                                        <!-- Input Brand -->
                                        <div class="form-group row">
                                            <label>เลือกร้านค้า <span class="text-danger">*</span></label>
                                            <select class="form-control chosen" name="seller" required>
                                                <option value="">เลือกร้านค้า</option>
                                                @php
                                                    $seller = (isset($product['product']['owner']) && !empty($product['product']['owner'])) 
                                                        ? $product['product']['owner'] : '';
                                                @endphp
                                                @if(isset($sellers) && !empty($sellers))
                                                    @foreach($sellers as $key => $v)
                                                        <option value="{{ $v->user_id }}" {{ $seller == $v->user_id ? 'selected' : '' }}>{{ $v->shop_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>

                                        <!-- Input Brand -->
                                        <div class="form-group row">
                                            <label>Brand <span class="text-danger">*</span></label>
                                            <input class="form-control" type="text" placeholder="Brand" name="brand" value="{{ $product['product']['brand'] ?? '' }}" required/>
                                        </div>

                                        <!-- tags -->
                                        <div class="form-group row">
                                            <label>Tags</label>
                                            <input class="form-control tags"  type="text" placeholder="tags" name="tags" value="{{ $product['product']['tags'] ?? '' }}"/>
                                        </div>
                                    </div>
                                </div>
                                <!-- end general -->
                                
                                <!-- Product detail section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Product" role="button" aria-expanded="false" aria-controls="collapseProduct">
                                        <h5>Product</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Product">
                                        <!-- Nav Tabs -->
                                        <ul class="nav nav-tabs" id="body" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="thai-tab" data-toggle="tab" href="#thai" role="tab" aria-controls="thai" aria-selected="true">
                                                    ไทย
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english" aria-selected="false">
                                                    English
                                                </a>
                                            </li>
                                        </ul>
                                        <!-- end nav tabs -->
                                        <!-- Tab Content -->
                                        <div class="tab-content language-pane" id="bodyContent">
                                            @foreach([array('id' => 1,'name' => 'thai'),array('id' => 2,'name' => 'english')] as $lang)
                                            <?php $active = ($lang['name'] === 'thai') ? 'active' : ''; ?>
                                            <div class="tab-pane fade show {{ $active }}" id="{{ $lang['name'] }}" role="tabpanel" aria-labelledby="{{ $lang['name'] }}-tab">
                                                <!-- product -->
                                                <div class="form-group row">
                                                    <label>Product Name <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="text" placeholder="Product Name" name="prod_name[{{ $lang['id'] }}]" value="{{ $product['product_language'][$lang['id']]['name'] ?? '' }}" required />
                                                </div>
                                                <!-- short -->
                                                <div class="form-group row">
                                                    <label>Short Description <span class="text-danger">*</span></label>
                                                    <textarea class="form-control short-editor" placeholder="Short Description" name="short_desc[{{ $lang['id'] }}]" required>{{ $product['product_language'][$lang['id']]['short_description'] ?? '' }}</textarea>
                                                </div>
                                                <!-- description -->
                                                <div class="form-group row">
                                                    <label class="full-fr-element">Full Description <span class="text-danger">*</span></label>
                                                    <textarea class="form-control full-editor" name="full_desc[{{ $lang['id'] }}]" rows="10" placeholder="Content" required>{{ $product['product_language'][$lang['id']]['description'] ?? '' }}</textarea>
                                                </div>
                                                <!-- condition -->
                                                <div class="form-group row">
                                                    <label>Special Condition</label>
                                                    <textarea class="form-control short-editor" name="special_condition[{{ $lang['id'] }}]" rows="10" placeholder="Special Condition">{{ $product['product_language'][$lang['id']]['special_condition'] ?? '' }}</textarea>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <!-- end tab content -->
                                    </div>
                                </div>
                                <!-- end product detail section -->
                                
                                {{-- กำหนดราคา --}}
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Price" role="button" aria-expanded="false" aria-controls="collapsePrice">
                                        <h5>กำหนดราคาขาย</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Price">
                                        <!-- Nav Tabs -->
                                        <ul class="nav nav-tabs" id="priceTab" role="tablist">
                                            @foreach([array('id' => 1, 'name' => 'THB'), array('id' => 2, 'name' => 'USD')] as $key => $tab)
                                            <li class="nav-item">
                                                <a class="nav-link {{ $tab['id'] === 1 ? 'active' : '' }}" id="{{ $tab['name'] }}-tab" data-toggle="tab" href="#{{ $tab['name'] }}" role="tab" aria-controls="{{ $tab['name'] }}" aria-selected="{{ $tab['id'] === 1 ? 'true' : 'false' }}">
                                                    {{ $tab['name'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                        <!-- end nav tabs -->
                                        <!-- Tab Content -->
                                        <div class="tab-content language-pane" id="priceTabContent">
                                            @foreach([array('id' => 1, 'name' => 'THB'), array('id' => 2, 'name' => 'USD')] as $key => $tab)
                                            <div class="tab-pane fade show {{ $tab['id'] === 1 ? 'active' : '' }}" id="{{ $tab['name'] }}" role="tabpanel" aria-labelledby="{{ $tab['name'] }}-tab">
                                                <!-- base price thb -->
                                                <div class="form-group row" style="margin-left: -15px; margin-right: -15px;">
                                                    <div class="col-md-4">
                                                        <label>ราคาต้นทุน <span class="text-danger">*</span></label>
                                                        <input class="form-control price"  type="text" placeholder="Base Price" maxlength="11" name="base_price[{{ $tab['id'] }}]" value="{{ $product['product_price'][$tab['id']]['base_price'] ?? '' }}" required/>
                                                    </div>
                                                    
                                                    <div class="col-md-4">
                                                        <label>ราคาขาย <span class="text-danger">*</span></label>
                                                        <input class="form-control price"  type="text" placeholder="Front Price" maxlength="11" name="front_price[{{ $tab['id'] }}]" value="{{ $product['product_price'][$tab['id']]['front_price'] ?? '' }}" required/>
                                                    </div>
                                                    <!-- sale price thb -->
                                                    <div class="col-md-4">
                                                        <label>ส่วนลด</label>
                                                        <div class="input-group">
                                                            <input class="form-control price"  type="text" placeholder="Discount" maxlength="11" name="sale_price[{{ $tab['id'] }}]" value="{{ $product['product_price'][$tab['id']]['sale_price'] ?? '' }}">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    @if (isset($product['product_price'][$tab['id']]['operation']) && !empty($product['product_price'][$tab['id']]['operation']))
                                                                        <span class="discount">{{ $product['product_price'][$tab['id']]['operation'] }}</span>
                                                                    @else
                                                                        <span class="discount">{{ ($tab['id'] == "1") ? "Bath" : "Dollar" }}</span>
                                                                    @endif
                                                                    
                                                                </button>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item" data-lang="{{ $tab['id'] }}" data-discount="BAHT" href="#">{{ ($tab['id'] === 1) ? 'Baht' : 'Dollar' }}</a>
                                                                    <a class="dropdown-item" data-lang="{{ $tab['id'] }}" data-discount="PERCENT" href="#">Percent</a>
                                                                </div>
                                                                <input type="hidden" name="discount_type[{{ $tab['id'] }}]" value="{{ $product['product_price'][$tab['id']]['operation'] ?? '' }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <!-- Shipping section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Shipping" role="button" aria-expanded="true" aria-controls="collapseShipping">
                                        <h5>Shipping</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Shipping">
                                        <div class="form-group row">
                                            <label>น้ำหนัก (กรัม) <span class="text-danger">*</span></label>
                                            <input class="form-control price" type="text" maxlength="6" placeholder="น้ำหนัก (กรัม)" name="s_weight" value="{{ $product['product']['s_weight'] ?? '' }}" required>
                                            <small id="emailHelp" class="form-text text-muted">ตัวอย่าง: 300</small>
                                        </div>
                                        <div class="form-group row">
                                            <label>ความกว้าง (เซนติเมตร)</label>
                                            <input class="form-control price" type="text" maxlength="6" placeholder="ความกว้าง (เซนติเมตร)" name="s_width" value="{{ $product['product']['s_width'] ?? '' }}">
                                        </div>
                                        <div class="form-group row">
                                            <label>ความยาว (เซนติเมตร)</label>
                                            <input class="form-control price" type="text" maxlength="6" placeholder="ความยาว (เซนติเมตร)" name="s_length" value="{{ $product['product']['s_length'] ?? '' }}">
                                        </div>
                                        <div class="form-group row">
                                            <label>ความสูง (เซนติเมตร)</label>
                                            <input class="form-control price" type="text" maxlength="6" placeholder="ความสูง (เซนติเมตร)" name="s_height" value="{{ $product['product']['s_height'] ?? '' }}">
                                        </div>
                                        <div class="form-group row">
                                            <label>In Stock <span class="text-danger">*</span></label>
                                            <input class="form-control numeric" type="text" maxlength="11" placeholder="In Stock" name="in_stock" value="{{ $product['product']['in_stock'] ?? '' }}" required>
                                        </div>
                                    </div>
                                </div>
                                <!-- end shipping section -->

                                <!-- Attributes section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" data-target="#Attributes" href="#Attributes" role="button" aria-expanded="false" aria-controls="collapseAttributes">
                                        <h5>Attributes</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Attributes">
                                        <div class="attribute">
                                            <div class="nav-tools">
                                                <div class="form-group row">
                                                    <label class="col-md-12">new option name <span class="text-warning">(Maximum 2)</span></label>
                                                    <div class="col-md-11">
                                                        <input class="form-control" type="text" placeholder="ex. color" name="optionName"/>
                                                    </div>
                                                    <div class="col-md-1"><button type="button" id="add-option" class="btn btn-primary btn-block">Add</button></div>
                                                </div>
                                            </div>
                                            <div id="options">
                                                @if (isset($product['option']['data']))
                                                    @foreach($product['option']['data'] as $key => $option) 
                                                        <div class="items" data-id="{{ $key+1 }}" id="item-{{ $key+1 }}">
                                                            <div class="header">
                                                                <span class="name">{{ $option['name'] }}</span>
                                                                <div class="tools">
                                                                    <ul>
                                                                        <li><a href="#" class="remove-option">Remove</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            @php
                                                                $op = implode(',', $option['option']);
                                                            @endphp
                                                            <div class="body">
                                                                <div class="tags-option">
                                                                    <label for="options">Options</label>
                                                                    <input id="option_{{ $key+1 }}" type="text" class="form-control options" value="{{ $op }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                            <input type="hidden" name="attributes" value="{{ $product['option']['json'] ?? '' }}">
                                        </div>
                                    </div>
                                </div>
                                <!-- end attributes section -->
                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Other" role="button" aria-expanded="true" aria-controls="collapseOther">
                                        <h5>ตั้งค่า</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Other">
                                        
                                        {{-- ใช้งาน --}}
                                        <div class="form-group">
                                            <div class="border-bottom mb-1">
                                                <label>ใช้งาน</label>
                                            </div>
                                            <div class="d-flex">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="status" id="status-yes" value="1" {{ isset($product['product']['status']) && ($product['product']['status'] === 1) ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="status-yes">
                                                        Yes
                                                    </label>
                                                </div>
                                                <div class="form-check ml-3">
                                                    <input class="form-check-input" type="radio" name="status" id="status-no" value="0" {{ isset($product['product']['status']) && ($product['product']['status'] === 0) ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="status-no">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- B2B --}}
                                        <div class="form-group">
                                            <div class="border-bottom mb-1">
                                                <label>ประเภทการขาย</label>
                                            </div>
                                            <div class="d-flex">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="product_type" id="product_type-b2c" value="2" {{ isset($product['product']['status']) && ($product['product']['product_type']) === 2 ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="product_type-b2c">
                                                        B2C
                                                    </label>
                                                </div>
                                                <div class="form-check ml-3">
                                                    <input class="form-check-input" type="radio" name="product_type" id="product_type-b2b" value="3" {{ isset($product['product']['status']) && ($product['product']['product_type']) === 3 ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="product_type-b2b">
                                                        B2B
                                                    </label>
                                                </div>
                                                <div class="form-check ml-3">
                                                    <input class="form-check-input" type="radio" name="product_type" id="product_type-both" value="1" {{ isset($product['product']['status']) && ($product['product']['product_type']) === 1 ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="product_type-both">
                                                        Both
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Admin Section --}}
                                        <h6 class="pt-3 text-danger">Admin Section</h6>
                                        {{-- Hilight --}}
                                        <div class="form-group">
                                            <div class="border-bottom mb-1">
                                                <label>Recommend</label>
                                            </div>
                                            <div class="d-flex">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="recommend" id="recommend-yes" value="1" {{ isset($product['product']['recommend']) && ($product['product']['recommend'] === 1) ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="recommend-yes">
                                                        Yes
                                                    </label>
                                                </div>
                                                <div class="form-check ml-3">
                                                    <input class="form-check-input" type="radio" name="recommend" id="recommend-no" value="0" {{ isset($product['product']['recommend']) && ($product['product']['recommend'] === 0) ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="recommend-no">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Approve --}}
                                        <div class="form-group">
                                            <div class="border-bottom mb-1">
                                                <label>Approve</label>
                                            </div>
                                            <div class="d-flex">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="approval" id="approval-yes" value="1" {{ isset($product['product']['approval']) && ($product['product']['approval'] === 1) ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="approval-yes">
                                                        Yes
                                                    </label>
                                                </div>
                                                <div class="form-check ml-3">
                                                    <input class="form-check-input" type="radio" name="approval" id="approval-no" value="0" {{ isset($product['product']['approval']) && ($product['product']['approval'] === 0) ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="approval-no">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="b-card">
                                    <div class="b-header" data-toggle="collapse" href="#Images" role="button" aria-expanded="true" aria-controls="collapseImages">
                                        <h5>รูปสินค้า</h5>
                                    </div>
                                    <div class="b-body collapse show" id="Images">
                                        <div class="product-image">
                                            {{-- Button upload --}}
                                            <div class="button-upload fileinput-button">
                                                <button type="button" class="btn btn-labeled btn-info">
                                                    <span class="btn-label" style="left: -13px;"><i class="cil-image-plus"></i></span>
                                                    อัพรูปสินค้า
                                                </button>
                                                <small class="text-warning">แนะนำให้ใช้รูปที่มีขนาด 1024 x 1024</small>
                                                <input type="file" name="file" class="product_upload" id="product_upload" accept="image/*">
                                                <input type="hidden" name="images">
                                            </div>
                                            
                                            {{-- list item --}}
                                            <div class="list">
                                                <ul class="sortable">
                                                    @if(isset($product['images']) && !empty($product['images']))
                                                        @foreach($product['images'] as $key => $img)
                                                            <li data-id="{{ $img['rank'] }}" data-image="{{ $img['name'] }}">
                                                                <div class="image">
                                                                    <img src="{{ $img['url'] }}" id="image">
                                                                </div>
                                                                <div class="highlight {{ $key === 0 ? 'active' : '' }}">รูปหลัก</div>
                                                                <div class="rank">
                                                                    <a href="#" data-delete="{{ $img['rank'] }}">
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- Button --}}
                                <div class="form-group d-flex justify-content-end">
                                    <a href="{{ route('product.list') }}" class="btn btn-labeled btn-sm btn-default border">
                                        <span class="btn-label"><i class="cil-media-skip-backward"></i></span>
                                        กลับ
                                    </a>

                                    <button class="ml-3 btn btn-labeled btn-sm btn-success" type="submit">
                                        <span class="btn-label"><i class="cil-check-alt"></i></span>
                                        บันทึก
                                    </button>&nbsp;&nbsp;
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('css')
    <!-- vendors -->
    <link rel="stylesheet" href="{{ asset('public/vendors/froala_editor_3.2.5/css/froala_editor.pkgd.css') }}">
    <!-- Code Mirror CSS file. -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
    <link href="{{ asset('public/vendors/jquery-file-upload/css/jquery.fileupload.css') }}" rel="stylesheet">
    <!-- Include the plugin CSS file. -->
    <link rel="stylesheet" href="{{ asset('public/vendors/froala_editor_3.2.5/css/plugins/code_view.css') }}">
    <link rel="stylesheet" href="{{ asset('public/vendors/froala_editor_3.2.5/css/third_party/image_tui.min.css') }}">
    <link href="{{ asset('public/vendors/chosen/chosen.css') }}" rel="stylesheet">
    <!-- custom -->
    <link href="{{ asset('public/assets/pages/product/css/create.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/tui-image-editor@3.2.2/dist/tui-image-editor.css">
    <link rel="stylesheet" href="https://uicdn.toast.com/tui-color-picker/latest/tui-color-picker.css">
    
@endsection

@section('javascript')
<!-- vendors -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.js"></script>
<script src="{{ asset('public/vendors/nested-sortable/jquery-ui.min.js') }}"></script>
<script src="{{ asset('public/vendors/nested-sortable/nestedSortable.js') }}"></script>

<script src="{{ asset('public/vendors/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('public/vendors/jquery-file-upload/js/jquery.fileupload.js') }}"></script>
<script src="{{ asset('public/vendors/jquery-file-upload/js/jquery.iframe-transport.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

<script type="text/javascript" src="{{ asset('public/vendors/froala_editor_3.2.5/js/froala_editor.pkgd.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.6.7/fabric.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/tui-code-snippet@1.4.0/dist/tui-code-snippet.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/tui-image-editor@3.2.2/dist/tui-image-editor.min.js"></script>
<script  type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.bundle.min.js"></script>
<!-- Include TUI plugin. -->
<script type="text/javascript" src="{{ asset('public/vendors/froala_editor_3.2.5/js/third_party/image_tui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('public/vendors/chosen/chosen.jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="{{ asset('public/vendors/jquery.loading.min.js') }}"></script>
<!-- custom -->
<script src="{{ asset('public/assets/pages/product/js/plugin.js') }}"></script>
<script src="{{ asset('public/assets/pages/product/js/create.js') }}"></script>
@endsection



