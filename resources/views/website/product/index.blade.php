@extends('layouts.base')
@section('content')
<section id="productList">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <div class="title">
                <i class="cil-storage"></i> จัดการสินค้า
              </div>
              <a href="{{ route('product.create') }}" class="btn btn-labeled btn-info">
                <span class="btn-label"><i class="fa fa-plus" aria-hidden="true"></i></span>
                เพิ่มสินค้าใหม่
              </a>
            </div>
            <div class="card-body">
              <div class="table-tabs">
                <ul class="nav" id="product-tab" role="tablist">
                  <li role="presentation">
                    <a class="active" id="product-approve-tab" data-toggle="pill" href="#product-approve" role="tab" aria-controls="product-approve" aria-selected="true">
                      อนุมัติ
                    </a>
                  </li>
                  <li role="presentation">
                    <a id="product-wait-approve-tab" data-toggle="pill" href="#product-wait-approve" role="tab" aria-controls="product-wait-approve" aria-selected="false">
                      รออนุมัติ
                    </a>
                  </li>
                </ul>
              </div>
                {{-- tab content --}}
              <div class="tab-content" id="product-tabContent">
                {{-- approve --}}
                <div class="tab-pane fade show active" id="product-approve" role="tabpanel" aria-labelledby="product-approve-tab">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="tabled-approve">
                      <thead>
                        <tr>
                          <th width="1200px">สินค้า</th>
                          <th width="100px">ราคา</th>
                          <th width="100px">สต๊อก</th>
                          <th width="100px">สถานะ</th>
                          <th width="100px">วันที่สร้าง</th>
                          <th width="100px">ดำเนินการ</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
                {{-- end approve --}}

                {{-- wait approve --}}
                <div class="tab-pane fade" id="product-wait-approve" role="tabpanel" aria-labelledby="product-wait-approve-tab">
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered" id="tabled-wait-approve">
                      <thead>
                        <tr>
                          <th width="1200px">สินค้า</th>
                          <th width="100px">ราคา</th>
                          <th width="100px">สต๊อก</th>
                          <th width="100px">สถานะ</th>
                          <th width="100px">วันที่สร้าง</th>
                          <th width="100px">ดำเนินการ</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
                {{-- end wait approve --}}
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <link rel="stylesheet" href="{{ asset('public/assets/pages/product/css/index.css') }}">
@endsection

@section('javascript')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
  <script src="{{ asset('public/assets/pages/product/js/index.js?version=1.0.0') }}"></script>

  <script> 

    $(function() {
      $('.xxx').hover(function(e) {
        console.log(e);

        // $('.dropdown-menu').addClass('show')

      })
    });
  </script>
@endsection
