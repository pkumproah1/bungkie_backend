@extends('layouts.base')
@section('content')
<section id="orderList">
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <div class="title">
                <i class="cil-storage"></i> จัดการคำสั่งซื้อ
              </div>
            </div>
            <div class="card-body">
              <div class="table-tabs">
                <ul class="nav" id="product-tab" role="tablist">
                  @foreach(config('constants.order.tabs') as $key => $tab)
                    <li role="presentation">
                      <a class="{{ $key == config('constants.order.tab.active') ? 'active' : '' }}" id="{{ $tab['segment'] }}-tab" data-toggle="pill" data-type="{{ $tab['type'] }}" href="#{{ $tab['segment'] }}" role="tab" aria-controls="{{ $tab['segment'] }}" aria-selected="{{ $key == config('constants.order.tab.active') ? 'true' : 'false' }}">
                        {{ $tab['name'] }}
                      </a>
                    </li>
                  @endforeach
                </ul>
              </div>

              <div class="tab-content" id="product-tabContent">
                <div class="card-toolbar mb-2">
                  <div class="search-toolbar">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary dropdown-toggle" data-value="1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">หมายเลขคำสั่งซื้อ</button>
                        <div class="dropdown-menu" id="order-filter">
                          <a class="dropdown-item active" data-type="1" href="#">หมายเลขคำสั่งซื้อ</a>
                          <a class="dropdown-item" data-type="2" href="#">ชื่อบัญชีลูกค้า</a>
                          <a class="dropdown-item" data-type="3" href="#">เบอร์โทรศัพทร์ลูกค้า</a>
                        </div>
                      </div>
                      <input type="text" class="form-control" name="search-text">
                    </div>
                  </div>

                  <div class="right">
                    <div class="date-toolbar">
                      <label for="date">ช่วงเวลา</label> 
                      <div class="daterange-show">
                        <i class="cil-calendar"></i>
                        <input class="form-control date" type="text" name="start" autocomplete="off">
                        <span></span>
                        <input class="form-control date" type="text" name="end" autocomplete="off">
                      </div>
                    </div>
                    <div class="button">
                      <button class="btn btn-info" data-button="submit" type="button">
                        <i class="cil-search"></i>
                      </button>
                      <button class="ml-1 btn btn-default border" data-button="reset" type="button">
                        <i class="cil-reload"></i>
                      </button>
                    </div>
                  </div>
                </div>

                <div class="table-responsive">
                  <table class="table table-striped table-bordered" id="table">
                    <thead>
                      <tr>
                        <th>หมายเลขคำสั่งซื้อ</th>
                        <th>ราคา</th>
                        <th>ชื่อบัญชีลูกค้า</th>
                        <th>ชื่อร้านค้า</th>
                        <th>สถานะ</th>
                        <th>เวลา</th>
                        <th>ดำเนินการ</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('css')
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/pages/order/css/index.css?version=101') }}">
@endsection

@section('javascript')
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
  <script type="text/javascript" src="{{ asset('public/assets/pages/order/js/index.js?version=101') }}"></script>

@endsection
