@extends('layouts.base')

@section('content')
<section id="orderDetail">
    <div class="container-fluid">
        <div class="animated fadeIn">
            {{-- ข้อมูลออเดอร์ --}}
            <div class="card">
                <div class="card-header">
                    <div class="title">
                        <i class="cil-storage"></i> ข้อมูลออเดอร์
                    </div>
                    <div class="back">
                        <a href="{{ route('order.index') }}" class="btn btn-labeled btn-sm btn-default border">
                            <span class="btn-label"><i class="cil-media-skip-backward"></i></span>
                            ย้อนกลับ
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="reference-detail">
                        <div class="order">
                            <div class="left">
                                <h3>ORDER DETAILS</h3>
                                <span class="order-id">หมายเลขออเดอร์: {{ $orders['items']['order']['order'] }}</span>
                                <span class="order-date">วันที่ทำรายการ: {{ $orders['items']['order']['date'] }}</span>
                                <span class="order-last-status">
                                    สถานะล่าสุด: <strong>{!! _order_status($orders['items']['order']['status']) !!}</strong>
                                    @if (isset($orders['cancels'][0]['status']) && $orders['cancels'][0]['status'] == 1)
                                        , (<span class="text-success">ดำเนินการเรียบร้อยแล้ว</span>)
                                    @endif
                                </span>
                            </div>
                            <div class="right text-right">
                                <h3>ORDER ACTIONS</h3>
                                <div class="action">
                                    @if(isset($orders['items']['shipping']['download']) && !empty($orders['items']['shipping']['download']))
                                        @if ($orders['items']['order']['status'] != '3')
                                            <button class="btn btn-labeled btn-sm btn-danger" type="button" data-type="cancel" data-params="{{ json_encode(['reference' => $orders['items']['order']['order'], 'shopId' => Request::segment(4) ]) }}">
                                                <span class="btn-label"><i class="cil-x"></i></span>
                                                ยกเลิกออเดอร์
                                            </button>
                                        @endif
                                        @foreach($orders['items']['shipping']['download'] as $dl) 
                                            <a href="{{ $dl['file'] }}" target="_blank" class="btn btn-labeled btn-sm btn-info">
                                                <span class="btn-label"><i class="cil-file"></i></span>
                                                {{ ($dl['size'] === "m") ? "ดาวน์โหลด(ขนาดเล็ก)" : "ดาวน์โหลด(ขนาดใหญ่) " }}
                                            </a>
                                        @endforeach
                                    @else
                                        @if($orders['items']['order']['status'] == '5')
                                            <div class="cancel-detail">
                                                <div class="text-right">
                                                    <span class="d-block">ยกเลิกโดย: <strong class="text-info">{{ $orders['cancels'][0]['created_by'] ?? '' }}, <span class="text-warning">({{ $orders['cancels'][0]['type'] ?? '' }})</span></strong></span>
                                                    <span>วันที่ยกเลิก: <span class="text-primary">{{ $orders['cancels'][0]['created_at'] ?? '' }}</span></span>
                                                    <br />
                                                    ( <a class="text-info show-log" href="#">ดูรายละเอียดการยกเลิกทั้งหมด</a> )
                                                </div>
                                            </div>
                                        @elseif($orders['items']['order']['status'] == '1')
                                            <button class="btn btn-labeled btn-sm btn-danger" type="button" data-type="cancel" data-params="{{ json_encode(['reference' => $orders['items']['order']['order'], 'shopId' => Request::segment(4) ]) }}">
                                                <span class="btn-label"><i class="cil-x"></i></span>
                                                ยกเลิกออเดอร์
                                            </button>
                                            <button class="btn btn-labeled btn-sm btn-success" type="button" data-type="order" data-params="{{ json_encode(['reference' => $orders['items']['order']['order'], 'shopId' => Request::segment(4) ]) }}">
                                                <span class="btn-label"><i class="cil-gift"></i></span>
                                                สร้างออเดอร์
                                            </button>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>

                        @if (isset($orders['cancels']) && !empty($orders['cancels']))
                        <div class="logs mt-4" id="showLogs">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <th>ประเภทการยกเลิก</th>
                                        <th>ยกเลิกโดย</th>
                                        <th>วันที่ยกเลิก</th>
                                    </thead>
                                    <tbody>
                                        @foreach($orders['cancels'] as $log)
                                            <tr>
                                                <td>{{ $log['type'] ?? '' }}</td>
                                                <td>{{ $log['created_by'] ?? '' }}</td>
                                                <td>{{ $log['created_at'] ?? '' }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            {{-- ข้อมูลการการชำระเงิน --}}
            @if ($orders['items']['order']['status'] == '5')
            <div class="card">
                <div class="card-header">
                    <div class="title">
                        <i class="cil-storage"></i> ข้อมูลการการชำระเงิน
                    </div>
                    <div class="back">
                        @if (isset($orders['cancels'][0]['status']) && $orders['cancels'][0]['status'] == 0)
                        <button class="btn btn-labeled btn-sm btn-info" type="button" data-type="refund" data-params="{{ json_encode(['reference' => $orders['items']['order']['order'], 'shopId' => Request::segment(4), 'status' => 2, 'cancel_id' => $orders['cancels'][0]['cancel_id'] ]) }}">
                            <span class="btn-label"><i class="cil-reload"></i></span>
                            ดำเนินการคืนเงิน
                        </button>
                        @elseif(isset($orders['cancels'][0]['status']) && $orders['cancels'][0]['status'] == 2)
                        <button class="btn btn-labeled btn-sm btn-success" type="button" data-type="refund" data-params="{{ json_encode(['reference' => $orders['items']['order']['order'], 'shopId' => Request::segment(4), 'status' => 1, 'cancel_id' => $orders['cancels'][0]['cancel_id'] ]) }}">
                            <span class="btn-label"><i class="cil-check"></i></span>
                            คืนเงินเรียบร้อยแล้ว
                        </button>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="reference-detail">
                        @if (isset($orders['cancels'][0]['purchase']) && !empty($orders['cancels'][0]['purchase']))
                        <div class="purchase-detail">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <th>ยอดเงินรวม</th>
                                        <th>การอ้างอิงการทำธุรกรรม</th>
                                        <th>รหัสการอนุมัติ</th>
                                        <th>สถานะการชำระเงิน</th>
                                        <th>ช่องทางการชำระเงิน</th>
                                        <th>ตัวแทนชำระเงิน</th>
                                        <th>สถานะการคืนเงิน</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $orders['cancels'][0]['purchase']->total ?? '' }}</td>
                                            <td>{{ $orders['cancels'][0]['purchase']->transaction_ref ?? '' }}</td>
                                            <td>{{ $orders['cancels'][0]['purchase']->approval_code ?? '' }}</td>
                                            <td>{{ $orders['cancels'][0]['purchase']->payment_status ?? '' }}</td>
                                            <td>{{ $orders['cancels'][0]['purchase']->payment_channel ?? '' }}</td>
                                            <td>{{ $orders['cancels'][0]['purchase']->paid_channel ?? '' }}</td>
                                            <td>
                                                @if ($orders['cancels'][0]['status'] == 0)
                                                    <span class="text-danger">รอดำเนินการคืนเงิน</span>
                                                @elseif($orders['cancels'][0]['status'] == 2)
                                                    <span class="text-warning">อยู่ระหว่างการดำเนินการคืนเงิน</span>
                                                @elseif($orders['cancels'][0]['status'] == 1)
                                                    <span class="text-success">ดำเนินการเสร็จสิ้น</span>
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            @endif
            {{-- รายละเอียดสินค้าและลูกค้า --}}
            <div class="card">
                <div class="card-header">
                    <div class="title">
                        <i class="cil-storage"></i>รายละเอียดสินค้าและลูกค้า
                    </div>
                </div>
                <div class="card-body">
                    <div class="reference-detail">
                        <div class="order-detail">
                            <div class="shop">
                                <h5>ข้อมูลร้านค้า</h5>
                                <span class="order-id">บริษัท: {{ $orders['items']['shop']['company'] ?? '' }} ({{ $orders['items']['shop']['shop_name'] ?? '' }}), Tax {{ $orders['items']['shop']['tax'] ?? '' }}</span>
                                <span class="order-id">คุณ: {{ $orders['items']['shop']['name'] ?? '' }}, <a href="tel:{{ $orders['items']['shop']['phone'] ?? ''  }}">{{ $orders['items']['shop']['phone'] ?? '' }}</a></span>
                                <span class="order-id">อีเมล: 
                                    <a href="mailto:{{ $orders['items']['shop']['email'] ?? '' }}">
                                    {{ $orders['items']['shop']['email'] ?? '' }}
                                    </a>
                                </span>
                            </div>
                            <div class="customer">
                                <h5>ข้อมูลลูกค้า</h5>
                                <span class="name">{{ $orders['items']['customer']['name'] ?? '' }}, <a href="tel:{{ $orders['items']['customer']['phone'] ?? '' }}">{{ $orders['items']['customer']['phone'] ?? '' }}</a></span>
                                <span class="address">{{ $orders['items']['customer']['address'] ?? '' }}</span>
                                <span class="address">{{ $orders['items']['customer']['address2'] ?? '' }}</span>
                                <span class="email">อีเมล: 
                                    <a href="mailto:{{ $orders['items']['customer']['email'] ?? '' }}" target="_blank">
                                        {{ $orders['items']['customer']['email'] ?? '' }}
                                    </a>
                                </span>
                            </div>
                        </div>
                        
                        <div class="table-responsive {{ $orders['items']['order']['status'] == '5' ? 'canceled-text' : '' }}">
                            @if ($orders['items']['order']['status'] == '5') 
                            <div class="canceled">
                                <h1>{!! _order_status($orders['items']['order']['status']) !!}</h1>
                            </div>
                            @endif
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th width="90">รูปสินค้า</th>
                                        <th>สินค้า</th>
                                        <th width="80">จำนวน</th>
                                        <th width="120">ราคารวม</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders['items']['products'] as $item)
                                    <tr>
                                        <td>
                                            <div class="image">
                                                <a href="{{ url('product/create/' . $item['id']) }}">
                                                    <img src='{{ $item['image'] }}' alt=''/>
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="product-detail">
                                                <a href="{{ url('product/create/' . $item['id']) }}" class="text-info">
                                                    <span class="name">{{ $item['name'] }}</span>
                                                </a>
                                                @if (!empty($item['attr'])) 
                                                <div class="detail">
                                                    <ul>
                                                    @foreach($item['attr'] as $attr)
                                                        <li>
                                                            <span class="key">{{ $attr['key']}}: </span>
                                                            <span class="value">{{ $attr['value']}}</span>
                                                        </li>
                                                    @endforeach
                                                    </ul>
                                                </div>
                                                @endif
                                            </div>
                                        </td>
                                        <td>{{ $item['qty'] }}</td>
                                        <td>{{ number_format($item['price'], 2) }}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="2" align="right">ผลรวมย่อย</td>
                                        <td colspan="1">{{ $orders['sumary']['item_count'] ?? '' }}</td>
                                        <td colspan="1">{{ number_format($orders['sumary']['subtotal'], 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">ค่าจัดส่ง</td>
                                        <td colspan="1">-</td>
                                        <td colspan="1">{{ number_format($orders['sumary']['shipping'], 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right">รวม</td>
                                        <td colspan="1">-</td>
                                        <td colspan="1">{{ number_format($orders['sumary']['total'], 2) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-refund" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-refund">
                    <div class="form-group">
                        <label for="additional">หมายเหตุ</label>
                        <textarea class="form-control" name="additional" id="additional" rows="2"></textarea>
                    </div>
                    <div class="form-group float-right">
                        <button class="btn btn-labeled btn-sm btn-success" type="submit">
                            <span class="btn-label"><i class="cil-check"></i></span>
                            บันทึก
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</section>



@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/assets/pages/order/css/detail.css?version=102') }}">
@endsection

@section('javascript')
    <script src="{{ asset('public/vendors/jquery.loading.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="{{ asset('public/assets/pages/order/js/detail.js?version=102') }}"></script>
@endsection