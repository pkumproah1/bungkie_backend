<footer class="c-footer">
  <div><a href="https://bungkie.com">Bungkie</a> &copy; {{ date('Y')}}</div>
  <div class="ml-auto">Powered by&nbsp;<a href="https://bungkie.com">Bungkie</a></div>
</footer>