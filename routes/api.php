<?php
// * Cronjob 
Route::get('/service/mail', 'CronjobController@emails');
Route::get('/location/province', 'Api\LocationController@get_province');

// * Callback
Route::prefix('callback')->group(function () {
    Route::post('/2c2p', 'Api\PurchaseController@callback2c2p');
    Route::get('/cancel_order', 'Api\PurchaseController@cancel_order');
    // Flash Express
    Route::prefix('/flashexpress')->group(function () {
        Route::post('/courier', 'Api\CallbackController@flash_courier');
        Route::post('/status', 'Api\CallbackController@flash_status');
    });
});

// * Group API by sanctum
Route::group(['middleware' => ['auth:sanctum']], function () {
    // * Product
    Route::prefix('product')->group(function () {
        Route::get('/listitem', 'Api\ProductController@paginate');
        Route::get('/findby', 'Api\ProductController@findby');
        Route::get('/listhome', 'Api\ProductController@listhome');
        Route::get('/more_list', 'Api\ProductController@more_list');
        Route::get('/tracking', 'Api\ProductController@tracking');
    });

    // * Category
    Route::prefix('category')->group(function () {
        Route::get('/', 'Api\CategoryController@index');
    });

    // * contact us
    Route::prefix('contact_us')->group(function () {
        Route::post('/', 'Api\ContactController@savedata');
    });

    // * Shop
    Route::prefix('shop')->group(function () {
        Route::get('/', 'Api\ShopController@index');
    });

    // * member
    Route::prefix('member')->group(function () {
        Route::post('/signup', 'Api\MemberController@signup');
        Route::post('/signin', 'Api\MemberController@signin');
        Route::post('/recovery', 'Api\MemberController@recovery');
        Route::post('/recovery_password', 'Api\MemberController@recovery_password');
        Route::post('/update_password', 'Api\MemberController@update_password');
        Route::post('/update_profile', 'Api\MemberController@update_profile');
        Route::get('/', 'Api\MemberController@profile');
        
        Route::post('/register_newsletter', 'Api\MemberController@register_newsletter');

        //! address
        Route::get('/address', 'Api\MemberController@address');
        Route::post('/address', 'Api\MemberController@save_address');
        Route::put('/address', 'Api\MemberController@edit_address');
        Route::delete('/address', 'Api\MemberController@remove_address');
    });

    // * cart
    Route::prefix('cart')->group(function () {
        Route::put('/', 'Api\CartController@add');
        Route::delete('/remove', 'Api\CartController@remove');
        Route::get('/', 'Api\CartController@index');
        Route::post('/calc_product', 'Api\CartController@calculate_product');
    });

    // * Purchase
    Route::prefix('purchase')->group(function () {
        Route::post('/', 'Api\PurchaseController@index');
        Route::get('/reference', 'Api\PurchaseController@find_order');
		Route::get('/userid', 'Api\PurchaseController@find_order_by_user_id');
    });
    
    // * Wholesale
    Route::prefix('wholesale')->group(function () {
        Route::post('/', 'Api\WholesaleController@save_wholesale');
    });

});

