<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//* #[Route("/cronjob"]
Route::prefix('cronjob')->group(function() {
    //! Service
    Route::get('/recovery', 'CronjobController@recovery');
    Route::get('/flash_notify', 'CronjobController@flash_notify');
});

Route::get('/sign-out', function() {
    Auth::logout();
    return redirect('/login');
});

Route::group(['middleware' => ['get.menu']], function () {
    Auth::routes();
    Route::get('/', 'DashboardController@index');

    //! Role [ ADMIN ]
    Route::group(['middleware' => ['role:admin']], function() {

        // #[Route("/seller"]
        Route::prefix('seller')->group(function() {
            Route::get('/', 'SellerController@index')->name('seller.list');
            Route::get('/create', 'SellerController@create')->name('seller.create');
            Route::post('/inserted', 'SellerController@inserted');
            Route::post('/check_username', 'SellerController@check_username');
        });

        // #[Route("/category"]
        Route::prefix('category')->group(function(){
            Route::get('/', 'CategoryController@index');
            Route::get('/create', 'CategoryController@create');
        });

        // ! for template
        Route::prefix('menu/element')->group(function () { 
            Route::get('/',             'MenuElementController@index')->name('menu.index');
            Route::get('/move-up',      'MenuElementController@moveUp')->name('menu.up');
            Route::get('/move-down',    'MenuElementController@moveDown')->name('menu.down');
            Route::get('/create',       'MenuElementController@create')->name('menu.create');
            Route::post('/store',       'MenuElementController@store')->name('menu.store');
            Route::get('/get-parents',  'MenuElementController@getParents');
            Route::get('/edit',         'MenuElementController@edit')->name('menu.edit');
            Route::post('/update',      'MenuElementController@update')->name('menu.update');
            Route::get('/show',         'MenuElementController@show')->name('menu.show');
            Route::get('/delete',       'MenuElementController@delete')->name('menu.delete');
        });

        Route::prefix('menu/menu')->group(function () { 
            Route::get('/',         'MenuController@index')->name('menu.menu.index');
            Route::get('/create',   'MenuController@create')->name('menu.menu.create');
            Route::post('/store',   'MenuController@store')->name('menu.menu.store');
            Route::get('/edit',     'MenuController@edit')->name('menu.menu.edit');
            Route::post('/update',  'MenuController@update')->name('menu.menu.update');
            Route::get('/delete',   'MenuController@delete')->name('menu.menu.delete');
        });
    });
    
    //! Role [ ADMIN, SELLER ]
    Route::group(['middleware' => ['role:admin|seller']], function () {
        
        //* #[Route("/upload"]
        Route::prefix('upload')->group(function () {
            //! Service
            Route::post('/froala_upload', 'FroalaController@upload');
            Route::post('/profile/seller', 'TempController@upload_shop');
            Route::post('/temp', 'TempController@upload');
        });

        //* #[Route("/profile"]
        Route::prefix('profile')->group(function () {
            //! ROUTE
            Route::get('/', 'ProfileController@index')->name('profile.index');

            //! Service
            Route::post('/has_url', 'ProfileController@has_url');
            Route::post('/update_profile', 'ProfileController@update_profile');
        });

        //* #[Route("/order"]
        Route::prefix('order')->group(function () {
            //! ROUTE
            Route::get('/', 'OrderController@index')->name('order.index');
            Route::get('/view/{reference_id}/{shop_id}', 'OrderController@detail');
            Route::get('/tracking/{pno}', 'OrderController@shipping_tracking');
            Route::get('/cancel_notify/{pno}', 'OrderController@shipping_cencel');
            Route::get('/notifications', 'OrderController@shipping_notifications');

            //! Service
            Route::post('/paginate', 'OrderController@paginate');
            Route::post('/create_order', 'OrderController@shipping_order');
            Route::post('/cancel_order', 'OrderController@cancel_order');
            Route::post('/cancel_log_purchase', 'OrderController@cancel_log_purchase');
        });

        //* #[Route("/product"]
        Route::prefix('product')->group(function () {
            //! ROUTE
            Route::get('/list',      'ProductController@index')->name('product.list');
            Route::get('/',      'ProductController@index');
            Route::get('/create',   'ProductController@create')->name('product.create');
            Route::get('/create/{id}',   'ProductController@create')->name('product.edit');

            Route::get('/xx',   'ProductController@saveimage');

            //! Service
            Route::post('/inserted', 'ProductController@inserted')->name('product.inserted');
            Route::post('/deleted', 'ProductController@removed');
            Route::post('/paginate', 'ProductController@paginate');
            Route::post('/upload', 'TempController@upload_product');
        });

        //* ROUTE #[Route("/category"]
        Route::prefix('category')->group(function () {
            Route::get('/',      'CategoryController@index')->name('Category Home');
        });

        //* ROUTE #[Route("/report"]
        Route::prefix('report')->group(function () {
            //! ROUTE
            Route::get('/purchase', 'ReportController@purchase');

            //! Service
            Route::post('/purchase_export', 'ReportController@purchase_export');
            Route::post('/purchase_paginate', 'ReportController@purchase_paginate');
        });
    });
});